classdef ScatteredInterpolant < handle & matlab.mixin.Copyable
	% This class is needed to deal with arrays of scatteredInterpolants.
	% scatteredInterpolants can be used to interpolate between arbitrary
	% sample points, see >> doc scatteredInterpolant
	
	properties (SetAccess = immutable)
		interpolant (1, 1) scatteredInterpolant;
	end % properties (SetAccess = private)
	
	methods
		function obj = ScatteredInterpolant(x, y, value, varargin)
			% x, y, and value must be double arrays of same length
			% containing all sample data.
			if nargin > 0 % this if-clause is needed to support
				% Characteristic-Array. Whenever a new obj(it) is
				% initialized, the constructor is called without
				% input-arguments. This would lead to errors, that are
				% avoided by this condition.
				NameValue = numeric.ScatteredInterpolant.constructorInputParserHelper(varargin{:});
				x = x(:);
				y = y(:);
				value = value(:);
				nanDeselect = ~isnan(sum([x, y, value], 2));
				obj.interpolant = scatteredInterpolant(...
					x(nanDeselect), y(nanDeselect), value(nanDeselect), ...
					NameValue.Method, NameValue.ExtrapolationMethod);
			else
				obj.interpolant = scatteredInterpolant(...
					[0; 0; 1; 1], [0; 1; 0; 1], [0; 0; 0; 0], varargin{:});
			end
		end % ScatteredInterpolant() constructor
		
		function value = evaluate(obj, varargin)
			% evaluate returns the interpolated values on the ndgrid specified by varargin
			value = zeros([size(varargin{1}), numel(obj)]);
			for it = 1 : numel(obj)
				if ~isempty(obj(it).interpolant.Values)
					% Empty interpolants return 0, others the interpolated value.
					value(:, :, it) = obj(it).interpolant(varargin{:});
				end
			end
 			value = reshape(value, [size(varargin{1}), size(obj)]);
			
% 			The following code does similar as the for-loop, but sadly it
% 			is three times slower and faulty. valueCell =
% 			arrayfun(@helperFunction, obj, 'UniformOutput', false);
%  			value = reshape(permute(cell2mat(valueCell), [2, 3, 1]),...
% 				[cellfun(@numel, myGrid), size(obj)]);
% 			function result = helperFunction(obj)
% 				result = reshape(obj.interpolant(myGrid), [1,
% 				numel(myGrid{1}), numel(myGrid{2})]);
% 			end
		end % evaluate()
		
	end % methods
		
	methods (Access = protected, Hidden = true, Static = true)

		function NameValue = constructorInputParserHelper(NameValue)
			arguments
				NameValue.Method = "linear";
				NameValue.ExtrapolationMethod = "nearest";
			end
		end % constructorInputParser()

	end % methods (Access = protected, Hidden = true, Static = true)
	
end % classdef
