function C = intersect(A, B, optArgs)
% INTERSECT find the intersection
% C = intersect(A, B, optArgs) returns the intersetion of A and B, i.e., all elements that are in A
% and are in B. In contrast, to the interset function from matlab, an absolute tolerance is used to
% compare the elements in A and B. So that also elements that are "numerically" equal are returned
% in C. With the name-value-pair argument "AbsTol" this absolute tolerance can be specified. The
% default value is 1e-12.
%
% see also intersect
arguments
	A (:,:) double;
	B (:,:) double;
	optArgs.AbsTol (1,1) double {mustBeNonnegative} = 1e-12;	
end

nA = numel(A);
M  = zeros(1, nA);
C = [];
% find A(i) in B
for i = 1:numel(A)
   for j = 1:numel(B)
		if isEqualTol(A(i), B(j), optArgs.AbsTol)
			
			% überprüfen ob A(i) bereits in C enthalten ist:
			isAlreadyElementOfC = false;
			for k = 1:length(C)
				if isEqualTol(C(k), A(i), optArgs.AbsTol)
					isAlreadyElementOfC = true;
					break;
				end
			end
			if ~isAlreadyElementOfC
				C(end+1) = A(i);
			end
		end
   end
end
   
end

function e = isEqualTol(a, b, tol)
	e = abs(real(a) - real(b)) <= tol && ...
			abs( imag(a) - imag(b) ) <= tol;
end