function z=trapz_fast(x,y)
% trapz_fast fast trpezoidal integration
% trapz_fast integrates the rows of a matrix.
%
% Description:
% z = trapz_fast(y) performes a quick trapezoidal integration over y, wrt. 
% the uniform distributed argument (0,1)
%
% z = trapz_fast(x,y) integrates y over x.
% x must be a vector and y must be a matrix with size(y,2)=length(x). 
%
% Inputs:
%     x       Discretized space vector
%     y       Discretized integrand function
% Outputs:
%     z       Integration result
%
% Example:
% -------------------------------------------------------------------------
% t = 0:0.1:pi;
% y = sin(t);
% numeric.trapz_fast(t,y)
% >> ans = 1.9975
% -------------------------------------------------------------------------
%
%  see also trapz, trapz_fast_nDim, cumtrapz_fast, cumtrapz_fast_nDim

%  created by Simon Kerschbaum 12/2014
%  modified on 28.09.218 by SK:
%    * increase performance

if size(x,1)~= 1
	if size(x,2)== 1
		x=x.';
	else
		error('x must be a row vector!')
	end
end

if nargin < 2
	y = x;
	x = linspace(0,1,size(x,2));
end
if size(x,2)~= size(y,2)
	error('size of x and y not consistent! (nD case)')
end

if length(x)==1
	z = zeros(size(y));
else
	z = (diff(x) * (y(:,1:size(y,2)-1) + y(:,2:size(y,2))).'/2).';
end