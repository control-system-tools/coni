function M = massMatrix(spatialDomain)
%massMatrix computes the mass-matrix M which can be used to approximate an integral
%		X(z) = M x = int x(z) dz
% This function is a simplified version of numeric.femMatrices.
% See also numeric.femMatrices.

arguments
	spatialDomain (1,1) quantity.Domain;
end

% set the system properties to local variables, for better readability and (maybe) faster access
z = spatialDomain.grid;
N = spatialDomain.n;

% compute the mass matrix
M0 = 1 / 6 * [2, 1; 1, 2];
zDiff = diff(z);
M = zeros(N, N);
for k = 1:N-1
	idxk=[k, k+1];
	M(idxk, idxk) = M(idxk, idxk) + M0 * zDiff(k);
end
end

