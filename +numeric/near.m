function [i, m, s] = near(nominalValue, actualValue, tolerance, relative, optArgs)
%NEAR checks 'equality' for double numerical values.
%   i = near(nominalValue, actualValue, tolerance) checks if
%        m = max( abs(nominalValue - actualValue) ) < tolerance.
%   The argument tolerance is optional and is 10*eps by default.
% [i, m, s] = near(nominalValue, actualValue, tolerance, relative) checks
% if 
%   abs(nominalValue - actualValue) < tolerance.
% with tolerance, the absolute tolerance can be set. By the input arg
% relative, it can be chosen if the relative deviation
%	m = max( | (nominalValue - actualValue) / max(|nominalValue|)
% For this, relative has to be set to true.
% The output parameters are i: logical value if the tolerance holds, m: is
% the maximal deviation and s is a string which is printed if no output is
% requested.
arguments
	nominalValue
	actualValue = zeros(size(nominalValue));
	tolerance = 10*eps;
	relative = 1;
	optArgs.relativeTo;
end

if islogical(relative)
	if isfield( optArgs, "relativeTo")
		relative = optArgs.relativeTo;
	else
		relative = max(abs(nominalValue(:)));
	end
end

absV = abs(nominalValue ./ relative - actualValue ./ relative);
i = all(absV(:) < tolerance);
m = max( absV(:) );

s = sprintf('Is near %i, maximal difference: %d', [i, m]);

if nargout == 0
	i = s;
end

end