function z = cumtrapz_fast_nDim(x,y,dim,equal,ignoreNaN) %#ok<INUSD>
% cumtrapz_fast fast cumulative trapezoidal integral
% 
% Description:
% z = cumtrapz_fast_nDim(y) integrates y over the argument (0,1), where the
%   values of y are assumed to be uniformly distributed over the argument.
%
% z = cumtrapz_fast_nDim(x,y) integrates the first dimension of y over the argument x, if x
%   is a vector. If x is an array, it integrates over the first dimension of x.
%
% z = cumtrapz_fast_nDim(x,y,dim) integrates the dim-th dimension of y over x, if x is a 
%   vector. If x is an array, it integrates over the dim-th dimension of x. The dim-th 
%   dimension of x must have the same length as the dim-th dimension of y.
%   The result z is an array whose dimensions are the concatenated dimensions of:
%   [dimensions of x without the integrated dimension, dimensions of y].
%
% z = cumtrapz_fast_nDim(x,y,dim,'equal') integrates the dim-th dimension of y over the dim-th
%   dimension of x. x and y must have the same size, such that each dimension element of y is
%   integrated wrt. the respective dimension element of x
%
% z = cumtrapz_fast_nDim(x,y,dim,'equal','ignoreNaN') 
%   ignores NaN entries in x and y.
%
% INPUT PARAMETRS:
%   ARRAY     x        : Integration variable
%   ARRAY     y        : Integrand
%   INTEGER   dim(1)   : Dimension of the integrand that shall be integrated. If x is an
%                        array, the dim-th dimension of x is the integration variable.
%             ~        : if a 4th argument is given, x and y must have the same size, such that
%                        each dimension element of y is integrated wrt. the respective 
%                        dimension element of x
%             ~        : if a 5th argument is given, NaNs in x are ignored.
%
% OUTPUT PARAMETERS:
%   ARRAY     z        : Result of the integration
%
% Examples: 
% -------------------------------------------------------------------------
% Integrate multiple functions over multiple arguments in one operation:
% x = [1 2 3;
%      2 3 4];
% is a matrix which contains two rows of integration variables.
% y = [4 5 6;
%      7 8 9];
% contains the corresponding function values. Now
% z = cumtrapz_fast_nDim(x,y,2,'equal')
% will result in z(1,:) = cumtrapz(x(1,:),y(1,:))
%                z(2,:) = cumtrapz(x(2,:),y(2,:))
%
% For further examples, see also doc.multidimensional_operations
% -------------------------------------------------------------------------
%
% see also doc.multidimensional_operations, cumtrapz, cumtrapz_fast, trapz_fast, trapz_fast_nDim

% created by Simon Kerschbaum on 12.09.2016

import misc.*
	if nargin<3
		dim =1;
	end
	if ~isvector(x)
		sizX = size(x);
		if sizX(dim) ~= size(y,dim)
			error('If x is an array, size(x,dim) must equal size(y,dim)!')
		end
	end

	if nargin<2
		y = x;
		x = linspace(0,length(x),1); % integration from 0 to 1
	end
	if isvector(x) && size(x,2)~= 1 && nargin<4 
		x = x(:); % column vector
	end

	sizY = size(y);% here it is better to store the size because it is used so often!
	m = sizY(dim); % discretization points
	if nargin>3
		if sizY ~= size(x)
			error('When passing the "equal"-argument, size(x) must equal size(y)!')
		end
	end
	if dim ~= 1
		yPerm = permute(y,[dim 1:dim-1 dim+1:ndims(y)]); % permute desired dimension to first
		if nargin>3
			xPerm = permute(x,[dim 1:dim-1 dim+1:ndims(x)]); % permute desired dimension to first
		else
			xPerm = x;
		end
		sizYPerm = sizY([dim 1:dim-1 dim+1:ndims(y)]);
	else
		yPerm=y;
		xPerm=x;
		sizYPerm = sizY;
	end
	if nargin>3 && ~ismatrix(x)
		xResh = reshape(xPerm,m,[]); % store all further dimensions in 1
	else
		xResh = xPerm;
	end
	if ~ismatrix(y)
		yResh = reshape(yPerm,m,[]); % store all further dimensions in one
		sizYResh = size(yResh);
	else
		yResh = yPerm;
		sizYResh = sizYPerm;
	end
	
	if isvector(x) && length(x) ~= m
		error(['Number of entries in x not equal to number of entries in dimension ' num2str(dim) ' of y!']);
	end

	if (~isvector(x) && size(x,dim)==1) || (isvector(x) && length(x) == 1) % special case: integrate over point.
		zResh = zeros(sizYResh);
	else
		if ~isvector(x)|| nargin>=4 % y is reshaped such the dim is at first position always!
			if nargin<4 % all combinations of other dimensions are evaluated
				error(['This case is currently implemented wrong.' ...
					'EqualDims is not passed, but needs to be passed before elemWise.'...
					'Probably you forgot to pass ''equal'',''ignoreNaN''.'])
				zResh = cat(dim,zeros([sizX(1:dim-1) 1 sizX(dim+1:ndims(x)) sizYResh(2:end)]), (cumsum(multArray(diff(x,1,dim)/2,(yResh(1:m-1,:) + yResh(2:m,:)),dim,1,'elemWise'),dim))); 
			% by performing multArray, the dim-th dimension of x is automatically shifted to be
			% the last dimension. Then it is collapsed by the multiplication.
			else % further dimensions of x belong to further dimensions of y
				% in that case, size(x)=size(y)! That means interesting dimension is 1!
				if nargin > 4 && any(isnan(xResh(:)))
					dx = diff(xResh);
					dx(isnan(dx)) = 0;
					yResh(isnan(xResh)) = NaN;
					yResh(isnan(xResh)) = 0;
					zResh = [zeros(1,size(yResh,2)); (cumsum(dx/2 .* (yResh(1:m-1,:) + yResh(2:m,:)),1))]; 
				else
					zResh = [zeros(1,size(yResh,2)); (cumsum(diff(xResh)/2 .* (yResh(1:m-1,:) + yResh(2:m,:)),1))]; 
				end
			end
		else
			zResh = [zeros(1,size(yResh,2)); (cumsum(diff(x)/2 .* (yResh(1:m-1,:) + yResh(2:m,:)),1))]; 
		end		
	end
	if ~ismatrix(y)
		if ~isvector(x) && nargin<4
			zPerm = reshape(zResh,[sizX([1:dim-1 dim+1:ndims(x)]) sizYPerm]); % restore all collapsed dimensions
		else
			zPerm = reshape(zResh,sizYPerm); % restore all collapsed dimensions
		end
	else
		zPerm = zResh;
	end
	nxNew = ndims(x)-1; % doesnt work for vector because ndim(x)=2 in that case!
	% shift the integrated dimension back to correct position dim
	if dim~=1
		if nargin<4 % all combinations
			if ~isvector(x)
				z = permute(zPerm,[1:nxNew,nxNew+2:nxNew+dim,nxNew+1,nxNew+dim+1:nxNew+ndims(y)]); % permute integrated dimension back so it is at dimension dim+ndims(x)
			else
				z = permute(zPerm,[2:dim,1,dim+1:ndims(y)]); % permute integrated dimension back so it is at dimension dim+ndims(x)
			end
		else % only dimensions of y
			z = permute(zPerm,[2:dim 1 dim+1:ndims(y)]);
		end
	else
		z  = zPerm; %nothing to do, integrated dimension hasn't collapsed!
	end
end % function







