function [A,B,C,zdisc,Grad_op,MM,BM,CM] = pde_fem(znum,a,b,c,q1,b1,q2,b2,q3,C_op,g,bfun,len)

%
% PDE_FEM   computes a finite dimensional approximation model
%              \hat{x}'(t) = A\hat{x}(t) + Bu(t)
%               \hat{y}(t) = C\hat{x}(t)
%           of the diffusion-convection-reaction system
%              x_t(z,t) = ax_zz(z,t) + bx_z(z,t) + cx(z,t) + q3(z) u_3(t) +
%								g(z) u4(t)
%              x_z(0,t) + q_1x(0,t) = b_1u_1(t)
%              x_z(1,t) + q_2x(1,t) = b_2u_2(t)
%                  y(t) = C_op x
%           of length "length" using FEM. 
%
%   [A,B,C,zdisc,Grad_op,MM] = pde_fem(znum,a,b,c,q1,b1,q2,b2,q3,C_op,g,bfun)
%   [A,B,C,zdisc,Grad_op,MM] = pde_fem(...,length)
%
%   INPUT PARAMETRS:
%	  % TODO: gleiche Typen f�r a,b und c!
%     DOUBLE ARRAY    znum    : number of equidistant discretization points
%     DOUBLE ARRAY    a       : coefficient wrt x_zz
%     DOUBLE ARRAY    b       : coefficient wrt x_z
%     FUNCTION        c(z)    : coefficient wrt x, function of z
%     DOUBLE ARRAY    q1      : boundary conditions at z = 0
%                               > q1 \neq 0 for Robin BC
%                               > q1 = 10^5 for Dirichlet BC  
%                               > q1 = 0 for Neumann BC
%     COLUMN VECTOR   b1      : boundary input
%                               > b1 \neq 0 for inhom. BC
%                               > b1 = 10^5 for Dirichlet BC
%                               > b1 = 0 for hom. BC      
%                                 u1 may be a vector
%     DOUBLE ARRAY    q2      : boundary conditions at z = 0
%                               > q2 \neq 0 for Robin BC
%                               > q2 = 10^5 for Dirichlet BC  
%                               > q2 = 0 for Neumann BC
%     COLUMN VECTOR   b2      : boundary input
%                               > b2 \neq 0 for inhom. BC
%                               > b2 = 10^5 for Dirichlet BC
%                               > b2 = 0 for hom. BC
%                                 u2 may be a vector
%     DOUBLE ARRAY    q3      : distributed input function as anonynous
%                               function of variable z
%     OUTPUT_OPERATOR C_op    : output operator of class OUTPUT_OPERATOR
%     DOUBLE ARRAY    g       : distributed input function as anonynous
%                               function of variable z
%     STRING          bfun    : discretization method
%                               > 'lin' for linear basis functions
%                               > 'quad' for quadratic basis functions
%     DOUBLE (OPT.)   len     : length of the spatial area, default: 1
%
%
% 
%   OUTPUT PARAMETERS:
%
%     DOUBLE ARRAY    A       : system matrix
%     DOUBLE ARRAY    B       : input matrix with respect to the inputs
%                               u1,u2,u3,u4 in this order!
%     DOUBLE ARRAY    C       : output matrix
%     DOUBLE ARRAY    zdisc   : discretization points
%     DOUBLE ARRAY    Grad_op : gradient_operator
%     DOUBLE ARRAY    MM      : Mass-Matrix of the FEM-approximation
%     DOUBLE ARRAY    BM      : Boundary Matrix of FEM-approximation
%     DOUBLE ARRAY    CM      : Convection-Matrix of the FEM-approximation

%
% ben�tigte Unterprogramme
%   -  matfem.m
%   -  Klasse output_operator
%
% history:
% created on 01.10.2013 by Joachim Deutscher
%
% modified on 22.04.2014 by Simon Kerschbaum:
%		* Code-Artefakte aufger�umt
%		* Verteilten Eingriff u3 mit Eingangsfunktion q3(z) hinzugef�gt.
%
% modified on 13.05.2014 by Simon Kerschbaum:
%		* Berechnung der Randbedingungen am linken Rand korrigiert und
%		  kommentiert. Beschreibung siehe FEM.pdf
%
% modified on 07.07.2014 by Simon Kerschbaum:
%       * Umstellung des Ausganges auf Ausgangsoperator mit verteilter
%         Messung
%
% modified on 11.08.2014 by Simon Kerschbaum:
%       * Umstellung des Reaktionsanteils auf ortsverteilten parameter
%
% modified on 13.08.2014 by Simon Kerschbaum:
%       * Ausgangsoperator an Punktmessung cm*x(zm) angepasst
%
% modified on 28.08.2014 by Simon Kerschbaum:
%       * Punktmessung cm*x(zm) auf lineeare Interpolation statt
%         Indexauswahl umgestellt. Vor allem bei kleiner Ortsaufl�sung
%         (Boebachter enorm wichtig!)
%
% modified on 16.09.2014 by Simon Kerschbaum:
%       * weiteren Verteilten Eingang f. St�rung hinzugef�gt
%
% modified on 10.09.2015 by SK:
%       * falls b1 oder b2=0 ist, wird B1 bzw. B2=0 statt [] gesetzt, damit
%         Eingangsvektor immer gleiche Laenge hat!
%
% modified on 02.02.2016 by SK:
%       * variable L�nge des Ortsbereichs eingef�gt (f. hyperb. Systeme)
%
% modified on 12.05.2016 by Ferdinand Fischer:
%       * Erweiterung auf Mehrgr��en Ausgangsoperator
%       * Indomain Punktmessung der ersten Ortsableitung
%
% modified on 17.10.2016 by Simon Kerschbaum:
%       * Korrektur des Eingangsvektors bei Diffusionskoeffizientem
%         ungleich 1 (wichtig)
%
% modified on 03.04.2017 by SK:
%       * Umstellung des Diffusionskoeffizienten auf Ortsabh�ngigkeit
%
% modified on 20.03.2018 by SK:
%       * Eingangskoeffizienten ungleich 1 m�glich
%       * Eingangsvektoren m�glich
%       * BM als Ausgangsparameter
%
% modified on 26.09.2018 by SK:
%       * zeiabh�ngige Diffusion und Konvektion (zweite Dimension der Parameter) m�glich!
%
% modified on 27.11.2018 by SK:
%       * zeitabh�ngige Randbedingungen m�glich (Dirichlet-Koeffizient)

% �berpr�fgen, ob �bergebene Parameter passen:
if ~isa(q3, 'function_handle')
	error('The distributed input q3 must be a function handle of variable z!')
end
if ~isa(C_op, 'dps.output_operator')
	error('C_op must be of class dps.output_operator!')
end

if ~isa(c,'function_handle')
	c=@(z)c+0*z; %Umwandlung in function_handle, falls konstante uebergeben wurde
end
import misc.*

constant_diffusion = 0;
numTa = 1;
if isvector(a) % constante oder ortsabh�ngige Diffusion
	if length(a)==1
		constant_diffusion = 1;
		a = a*ones(znum,1); % zu ortsabh�ngigen Vektor machen
	else
		a=a(:); % Spaltenvektor
	end
else %orts- und zeitabh�ngige Diffusion
	% nix
	numTa = size(a,2);
end

numTb = 1;
if isvector(b) % constante oder ortsabh�ngige Konvektion
	if length(b)==1
		b = b*ones(znum,1); % zu ortsabh�ngigen Vektor machen
	else
		b =b(:); % Spaltenvektor
	end
else % orts- und zeitabh�ngige Konvektion
	numTb = size(b,2);
end

if numTa ~= numTb
	error('a and b must have the same temporal discretization!')
end
numTq2 = numel(q2); % urspr�ngliche Anzahl in q2
numTq1 = numel(q1); % urspr�ngliche Anzahl in q1
if numel(q1)>1
	if numTa > 1 && numel(q1)~= numTa
		error('q1 must have the same temporal discretization as a!')
	end
	if numel(q2)==1
		q2 = repmat(q2,numTq1,1); % beide zeitabhaengig oder konstant!
	end
end
if numel(q2)>1
	if numTa > 1 && numel(q2)~= numTa
		error('q2 must have the same temporal discretization as a!')
	end
	if numel(q1) == 1
		q1 = repmat(q1,numTq2,1); % beide zeitabhaengig oder konstant!
	end
end
numTq = numel(q1);
numT = max([numTa;numTq]);
	

% Erzeugung des �quidistanten Gitters
if nargin == 12
	Lz = 1;                       % L�nge des Ortsbereichs
else
	Lz = len;
end
ze       = linspace(0,Lz,znum);     % Diskretisierungpunkte

% Auswertung der Randbedingungen
% Der Typ der Randbedingungen darf sich mit der Zeit nicht �ndern, f�r pde_fem ist aber nur
% dieser Typ entscheidend. Deswegen kann dieser f�r t=0 untersucht werden.
if q1(1) == 10^5
    bc_z0 = 'dir';
elseif any(q1 ~= 0) 
    bc_z0 = 'rob';
	% Im Fall gemischter Randbedingungen muss das Vorzeichen von q_1
	% umgekehrt werden! Siehe FEM.pdf!
	% sonst wird Form 
	%   -x_z(0,t) + q_1x(0,t) = b_1u_1(t) betrachtet!
	q1=-q1;
else 
    bc_z0 = 'neu'; 
end

if q2(1) == 10^5
    bc_z1 = 'dir';
elseif any(q2 ~= 0) 
    bc_z1 = 'rob';
else 
    bc_z1 = 'neu'; 
end

% Bestimmung der FEM-Matrizen
if bfun == 'lin'
% 	if constant_diffusion
		[MM,DM,CM,BM] = matfem(ze,bc_z0,bc_z1,'MM','DM','CM','BM');
% boundary matrix erh�lt am Rand 1000 (Dirichlet), 0 (Neumann) oder 1 (Robin)! Mehr passiert
% nicht! Wird hinterher sowieso noch manuell gemacht, weil noch mit a(z,t) multipliziert werden
% muss!
% 	else
% 		[MM,DM,CM,BM] = matfem_piecewise(ze,a,bc_z0,bc_z1,'MM','DM','CM','BM');
% 	end
    zdisc = ze;
else
	error('nonlinear spatial grid currently not implemented!')
% 	if constant_diffusion
		[MM,DM,CM,BM,zdisc] = matfem(ze,bc_z0,bc_z1,'MM','DM','CM','BM','quad');
% 	else
% 		[MM,DM,CM,BM,zdisc] = matfem_piecewise(ze,lambda,bc_z0,bc_z1,'MM','DM','CM','BM','quad');
% 	end
end    

% Bestimmung des Zustandsmodells
% BM braucht nur eine Zeitabhaengigkeit, wenn eines der q zeitabhaengig ist. Ansonsten gibt
% sich die dritte Dimension durch die sp�tere Addition.
% preallocate BM:
BM = repmat(BM,1,1,numT);
for tIdx = 1:numT
	if numTa == 1
		tIdxa = 1;
	else
		tIdxa = tIdx;
	end
	if numTq == 1
		tIdxq = 1;
	else
		tIdxq = tIdx;
	end
	if  bc_z0 == 'rob' | bc_z0 == 'dir'
		BM(1,1,tIdx) = q1(tIdxq)*(a(1,tIdxa)); % HIER GEHT DIFFUSIONSKOEFFIZIENT BEI z=0 ein.
	end    
	if  bc_z1 == 'rob' | bc_z1 == 'dir'
		BM(end,end,tIdx) = q2(tIdxq)*(a(end,tIdxa));
	end    
end
%Fazit: -MM\BM bildet ab, wie sich ein Zustand, an dem eine Randbedingung vorliegt, selbst
%beeinflusst. Kannn also problemlos auch im inneren so gemacht werden.

c_par_dis = c(zdisc);
% Hier spielt dritte Dimension keine Rolle, wird einfach mitgeschleift
if isvector(a)
	lambda_disc = repmat(a,1,znum);
else
	lambda_disc = permute(a,[1 3 2]); % create new dimension
	lambda_disc = repmat(lambda_disc,1,znum,1);
end
if isvector(b)
	b_disc = repmat(b,1,znum);
else
	b_disc = permute(b,[1 3 2]); % create new dimension
	b_disc = repmat(b_disc,1,znum,1);
end
% Achtung: anhand Beschreibung von Matmol nicht ganz eindeutig, wie der
% ortsabhaengige Parameter c ueber F implementiert werden muss. Wohl aber
% so korrekt:
% lambda_disc(1,:) = 1;
% lambda_disc(end,:) = 1; % im ersten und letzten Eintrag wird die Diffusion bereits in der Boundary-Matrix ber�cksichtigt!
% if constant_diffusion
% Achtung: 3-point-centered-Schema, das von matfem verwendet wird ist f�r
% Konvektion nicht so gut geeignet! Deshalb Vorzeichen des
% Konvektionskoeffizienten untersuchen und mit FD Konvektionsmatrix selbst
% erzeugen! Nur f�r uniform spacing aktuell!
dz=diff(zdisc);%(zL-z0)/(n-1);
dz=dz(1);
r1fdz=1/dz;
if 0%~any(b_disc > 0)
	 D=diag(-1*ones(znum-1,1),-1)+diag(+1*ones(znum,1),0);         
	 D(1,1:2) = [-1 1];
	 D=r1fdz*D;
	 convection = b_disc.*D;
elseif 0% ~any(b_disc < 0)
	D=diag(-1*ones(znum,1),0)+diag(+1*ones(znum-1,1),+1);      
    D(znum,(znum-1):znum) = [-1 1];
	D=r1fdz*D;
	convection = b_disc.*D;
else
	convection = multArray(MM^(-1),b_disc).*CM;
end
A = -multArray(MM^(-1),lambda_disc.*DM + BM) + diag(c_par_dis) + convection;
% else
% 	A = -MM\(DM - b*CM  + BM) + diag(c_par_dis);
% end

p1 = length(b1);
B1 = zeros(znum,p1,numT); 
% for all left inputs
for j =1:p1
	for tIdx=1:numT
		if numTa == 1
			tIdxa = 1;
		else
			tIdxa = tIdx;
		end
		if b1(j) == 10^5 % Dirichlet
			B1(1,j,tIdx) = BM(1,1,tIdx);
		elseif b1(j) == 0 % kein Eingriff
			B1(1,j,tIdx) = 0; % ACHTUNG: HIER GEAENDERT!
		else % b1 irgendwass
			% In diesem Fall liegen Neumannsche oder gemischte Randbedingungen vor.
			% Um auf die eingangs beschriebene Form zu kommen, muss das Vorzeichen
			% von b umgekehrt werden (Einheitsvektor aus Rand heraus)!
			B1(1,j,tIdx) = -1*a(1,tIdxa)*b1(j); %stimmt!
		end
	end
end

p2 = length(b2);
B2 = zeros(znum,p2,numT); 
% for all right inputs
for j =1:p2
	for tIdx=1:numT
		if numTa == 1
			tIdxa = 1;
		else
			tIdxa = tIdx;
		end
		if b2(j) == 10^5
			B2(end,j,tIdx) = BM(end,end,tIdx);
		elseif b2 == 0
			B2(end,j,tIdx) = 0; % ACHTUNG: HIER GEAENDERT!
		else % b2 irgendwas
			B2(end,j,tIdx) = 1*a(end,tIdxa)*b2(j);
		end
	end
end

B = misc.multArray(MM^(-1),[B1 B2]); % Randeingriffe behandelt.
% Jetzt wird noch der verteilte Eingriff addiert.
B3 = q3(zdisc);
B4 = g(zdisc);
% Falls kein verteilter Eingriff wurde vielleicht @(z) 0 �bergeben, sodass
% kein Vektor entsteht! abfangen:
if B3 == 0
	B3 = 0*zdisc;
end
if B4 == 0
	B4 = 0*zdisc;
end
B3 = repmat(B3(:),1,1,numT); %(Sicherstellen, dass Spaltenvektor)
B4 = repmat(B4(:),1,1,numT); %(Sicherstellen, dass Spaltenvektor)

B = [B B3 B4];

%% Berechnung des Ausgangsoperators:
Grad_op = MM\CM;
C = zeros(C_op.m, znum);
for k=1:C_op.m
    c0 = C_op.c_0(k);
	cd0 = C_op.c_d0(k);
	c1 = C_op.c_1(k);
	cd1 = C_op.c_d1(k);
	cc  = C_op.c_c;
	cm = C_op.c_m(k);
	zm = C_op.z_m(k);
    cdm = C_op.c_dm(k);
	% Integral ueber verteilten Ausgang als Produkt schreiben, wie bei
	% Zustandsregler:
	c_dis = cc(zdisc);
    c_dis=c_dis(k,:);
	
	c_dis = c_dis*MM;
	
    sel_matrix = getSelectionMatrix(znum, zdisc, zm);
    
    C(k,:) = cd0*Grad_op(1,:)+c0*[1 zeros(1,znum-1)]...
                +cd1*Grad_op(end,:)+c1*[zeros(1,znum-1) 1]...
                +c_dis...
                +cm*sel_matrix ...
                +cdm*sel_matrix*Grad_op;
end


function sel_matrix = getSelectionMatrix(znum, zdisc, z)
	% Fuer Punktmessung wird linear zwischen den beiden naechstgelegenen
	% Punkten interpoliert!
 
	idx_up = find(zdisc>=z,1);
	idx_down = length(zdisc) - find(fliplr(zdisc)<=z,1)+1;
	
	z_up = zdisc(idx_up);
	z_down = zdisc(idx_down);
	
	diff_z_up = z_up-z;
	diff_z_down = z - z_down;
	diff_ges = z_up-z_down;
	sel_matrix = zeros(1,znum);

    if diff_ges == 0 % es wurde genau ein Abtastpunkt erwischt!
		sel_matrix(1,idx_up) = 1;
	else
		sel_matrix(1,idx_down)=diff_z_up/diff_ges;
		sel_matrix(1,idx_up)=diff_z_down/diff_ges;
    end
