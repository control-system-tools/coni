classdef Function < quantity.Discrete
	% quantity.Function allows to perform calculations with matrix-valued functions which were
	% initialized as function_handle. This class is a child of quantity.Discrete. In
	% combination with quantities of the type quantity.Discrete and quantity.Symbolic one can use
	% discrete data, symbolic variables and function_handles in the same computations with the same
	% syntax. See quantity.example for examples.
	%
	% See also quantity.Function.Function (constructor), quantity.example, quantity.Domain,
	%	quantity.Discrete, quantity.Symbolic.
	
	properties (SetAccess = protected)
		% Value of the function: function_handle or double
		valueContinuous (1, 1);
	end
	
	properties
		% flag to chose if the function should be evaluated on single points or using vectors.
		% If false, the grids of the domain will be passed as matrices (respectively vectors) to the
		% function. If true, the grids will be passed point wise to the function in a for loop.
		% The use of the grids as matrices may be faster, but sometimes it is hard (or not possible)
		% to write a function that can be called with vectors or matrices. The default value is 
		% pointWiseEvaluation = false. The default value cannot be changed for quantity.Symbolic
		% ojects.
		pointWiseEvaluation (1,1) logical = false;
	end
	
	methods
		%--------------------
		% --- Constructor ---
		%--------------------
		function obj = Function(valueOriginal, myDomain, optArgs)
			arguments
				valueOriginal = []
				myDomain (1,:) quantity.Domain = quantity.Domain.empty();
				optArgs.name (1,1) string = "";
				optArgs.pointWiseEvaluation (1,1) logical = false;
				optArgs.valueDiscrete;
			end
			parentVarargin = {};	% by default the parent-class is called with no input.
			
			% only do something if there are some paremters. otherwise an
			% empty object will be returned. This is important to allow the
			% initialization as object array.
			if nargin > 0
				sizeValue = size(valueOriginal);
				fun = cell(sizeValue);
				
				% modification of some input parameters to allow the
				% initialization of the object array
				for k = 1:numel(valueOriginal)
					if iscell(valueOriginal)
						fun{k} = valueOriginal{k};
					elseif isnumeric(valueOriginal)
						fun{k} = valueOriginal(k);
					elseif isa(valueOriginal, 'function_handle')
						fun{k} = valueOriginal;
					elseif isa(valueOriginal, 'sym')
						fun{k} = matlabFunction(valueOriginal);
					else
						error('Type of valueContinuous not supported')
					end
				end
				
				% verify that all arguments of each 'fun' are consitent with the domain names
				for k = 1:numel(fun)
					names = misc.functionArguments(fun{k});
					
					if numel(myDomain) == 0 && isempty(names)
						% no test of the assertion is needed. Indeed, the obj quantity.Domain.empty
						% has no name entry. Hence, it returns empty, which thorws an error in the
						% strjoin function of the below assertion. Unfortunately, this function is
						% also validated if the assertion holds. Hence, the special case for empty
						% myDomain is checked with this if - else construct.
					elseif isempty(myDomain)
						assert( numel(myDomain) == numel(names) & ...
							numel(myDomain) == numel(myDomain.find(names)), ...
							'quantity:Function:domain', ...
							"The argument names of the function must be compatible with the names " + ...
							"of the domains. You defined a function with the arguments " + ...
							mat2str(names) + " but an empty domain.");
					else
						assert( numel(myDomain) == numel(names) & ...
							numel(myDomain) == numel(myDomain.find(names)), ...
							'quantity:Function:domain', ...
							"The argument names of the function must be compatible with the names " + ...
							"of the domains. You defined a function with the arguments " + ...
							mat2str(names) + " but a domain with the names " + ...
							mat2str(names) + ".");
					end
				end
				
				if isfield( optArgs, "valueDiscrete" )
					parentVarargin = [optArgs.valueDiscrete, {myDomain}, {"name"}, {optArgs.name}];
				else
					parentVarargin = [{cell(sizeValue)}, {myDomain}, {"name"}, {optArgs.name}];
				end
			end
			
			obj@quantity.Discrete(parentVarargin{:});
			if nargin > 0
				obj(numel(fun)).valueContinuous = fun{end};
				for it = 1 : (numel(fun) - 1)
					obj(it).valueContinuous = fun{it};
				end
				obj = reshape(obj, sizeValue);
				[obj.pointWiseEvaluation] = deal( optArgs.pointWiseEvaluation );
				
			end
		end % Function constructor

		function f = function_handle(obj)
			% function_handle returns the function_handle of the obj.
			if numel(obj) == 1
				f = @obj.valueContinuous;
			else
				F = cell(size(obj));
				for k = 1:numel(obj)
					F{k} = @(varargin) obj(k).valueContinuous(varargin{:});
				end

				f = @(varargin) reshape((cellfun( @(c) c(varargin{:}), F)), size(obj));
			end
		end % function_handle
		
		function Q = quantity.Discrete(obj, NameValue)
		% quantity.Discrete casts obj into an quantity.Discrete object
			arguments
				obj;
				NameValue.domain quantity.Domain = obj(1).domain;
				NameValue.name (1, 1) string = obj(1).name;
			end
			Q = quantity.Discrete(obj.on(NameValue.domain), NameValue.domain, ...
				"name", NameValue.name);
		end % quantity.Discrete()
		
		function value = obj2value(obj, myDomain, recalculate)
			% obj2value returns the numerical value of obj on the grid specified by myDomain.
			%
			%	value = obj2value(obj) returns the valueDiscrete in the form 
			%		size(value) = [obj(1).domain.n, objSize]
			%
			%	obj2value(obj, myDomain) returns the valueDiscrete in the form 
			%		size(value) = [myDomain.n, objSize]
			%
			%	obj2value(obj, myDomain, recalculate) by default, if the domain fits to the domain
			%		stored in obj.domain, then no new evaluation of the function handle is needed.
			%		However, with recalculate = true, a new evaluation of the function_handle is
			%		enforced.
			
			arguments
				obj;
				myDomain = obj(1).domain;
				recalculate (1, 1) logical = false;
			end % arguments
			
			if ~recalculate && isequal(myDomain, obj(1).domain)
				value = obj2value@quantity.Discrete(obj);
			else
				% otherwise the function has to be evaluated on the new domain
				value = cell(size(obj));
				ndGrd = myDomain.ndgrid;
				sortedNdGrid = cell(numel(myDomain), 1);
				domainNames = [myDomain.name];
				
				for k = 1:numel(obj)
					
					if isempty( myDomain )
						sortedNdGrid = ndGrd;
					else
						% verify that the order of the grid is the same as of the function arguments:
						functionArguments = misc.functionArguments( obj(k).valueContinuous );
						
						logIdx = cell(numel(functionArguments), 1);
						
						for it = 1:numel(functionArguments)
							logIdx{it} = strcmp(domainNames, functionArguments(it));
							sortedNdGrid{it} = ndGrd{ logIdx{it} };
						end						
					end
					
					tmp = obj(k).evaluateFunction( sortedNdGrid{:} );
					value{k} = tmp(:);
				end
				value = reshape( cell2mat(value), [ gridLength(myDomain), size(obj)]);
			end
		end % obj2value()
		
		function value = at(obj, point)
			% AT evaluates the object at a given point 
			% value = at(obj, point) evaluates the quantity at the point specified by
			%	point = [x, y, ... ]
			% where [x, y, ... ] is a row vector of the coordinates of the requested value.
			% If a list of coordinates is requested, i.e., x = col(x1, x2, ... ), y = col( y1, y2,
			% ...) ..., then a list of the corresponding values is returned.
			arguments
				obj
				point (:,:) double;
			end
			
			value = zeros( [size(point,1), size(obj)]);
			for it = 1 : numel(obj)
				for j = 1:size(point,1)
					pointJ = num2cell( point(j,:) );
					value(j, it) = obj(it).valueContinuous( pointJ{:} );
				end
			end

			if size(point,1) == 1
				value = reshape( value, size(obj) );
			else
				value = reshape( value, [size(point,1), size(obj)] );
			end
			
		end % at()
		
	end % methods
	
	methods
		function mObj = uminus(obj)
			% Unary minus.
			%     -A negates the elements of A.
			%  
			%     B = uminus(A) is called for the syntax '-A.
			mObj = obj.copy();
			
			for k = 1:numel(obj)
				mObj(k).valueContinuous = @(varargin) - obj(k).valueContinuous(varargin{:});
				mObj(k).valueDiscrete = - obj(k).valueDiscrete;
			end
			
			mObj.setName("-" + obj(1).name);
		end % uminus
				
		function I = int(obj, domain, lowerBound, upperBound)
			% int integration based on integral-method for function_handles
			%	
			%	result = int(obj) integrates obj all its domains from its lower bound to the upper
			%		bound.
			%	
			%	result = int(obj, domain) integrates obj over the domain from its lower bound to 
			%		the upper bound. To integrate over multiple domains, specify domain as a
			%		string-array.
			%	
			%	result = int(obj, domainName, lowerBound, upperBound) integrates obj over 
			%		the integration domain from the lowerBound to the upperBound.
			%		- domain must be a string(-array) coinciding with the domain of the integrated 
			%			obj. 
			%		- lowerBound and upperBound define the boundaries of the integration domain. 
			%			These can be either doubles for definite integrals or a domain specified by
			%			a string or a quantity.Domain for an indefinite intergral. If lowerBound or
			%			upperBound are variable, then the result will contain a related domain.
			%			In order to perform integration over multiple domains, specify lowerBound
			%			and upperBound as cell-arrays.
			%
			% See also quantity.Discrete.diff
			
			arguments
				obj;
				domain string = [obj(1).domain.name];
				lowerBound = [obj(1).domain.find(domain).lower];
				upperBound = [obj(1).domain.find(domain).upper];
			end
			
			assert(numel(domain) == 1, ...
				"This method is yet only implemented for integration w.r.t. 1 integration variable");
			assert(numel(domain) == numel(lowerBound), ...
				"dimension of lowerBound must be equal to dimension of domain");
			assert(numel(domain) == numel(upperBound), ...
				"dimension of upperBound must be equal to dimension of domain");
			if iscell(lowerBound)
				lowerBound = lowerBound{1};
			end
			if iscell(upperBound)
				upperBound = upperBound{1};
			end
			assert(isnumeric(lowerBound) || isstring(lowerBound), ...
				"integral bounds must be string or numeric");
			assert(isnumeric(upperBound) || isstring(upperBound), ...
				"integral bounds must be string or numeric");

			% get the desired domain. This step is necessary, as the input argument domain can be
			% either a string or a quantity.Domain object.
			domain = obj(1).domain.find(domain);
			
			% calculate last element first for preallocation
			I_val = numeric.cumIntegral( ...
				@(varargin)obj(end).evaluateFunction(varargin{:}), domain.grid);
			result(numel(obj), 1) = quantity.Discrete(I_val, obj(1).domain);
			for k = 1 : (numel(obj)-1)
				I_val = numeric.cumIntegral( ...
					@(varargin)obj(k).evaluateFunction(varargin{:}), domain.grid);
				result(k) = quantity.Discrete(I_val, obj(1).domain);
			end
			% int_lowerBound^upperBound f(.) = F(upperBound) - F(lowerBound)
			I = reshape(...
				result.subs([domain.name], upperBound) - result.subs([domain.name], lowerBound), ...
				size(obj));
			if isa(I, "quantity.Discrete")
				I.setName("int(" + obj(1).name + ")");
			end % if isa(I, "quantity.Discrete")
			
		end % int()
		
		function x = rdivide(A, B)
			%  ./  Right array divide.
			%     A./B divides each element of A by the corresponding element of B. A and 
			%     B must have same size or one might be a scalar.
			
			% ensure that A and B have same size:
			if isequal(size(A), size(B))
				% do nothing
			elseif isscalar(A)
				A = repmat(A, size(B));
			elseif isscalar(B)
				B = repmat(B, size(A));
			else
				error("A and B must have same size or one must be scalar to perform A./B")
			end
			
			xSize = size(B);
			x(xSize(1), xSize(2)) = A(end) / B(end);
			for it = 1 : numel(x)-1
				x(it) = A(it) / B(it);
			end
		end % rdivide()
		
		function x = ldivide(B, A)
			%  .\  Left array divide.
			%     B.\A divides each element of A by the corresponding element of B. A and 
			%     B must have same size or one might be a scalar.
			
			% ensure that A and B have same size:
			if isequal(size(A), size(B))
				% do nothing
			elseif isscalar(A)
				A = repmat(A, size(B));
			elseif isscalar(B)
				B = repmat(B, size(A));
			else
				error("A and B must have same size or one must be scalar to perform B.\A")
			end
			
			xSize = size(B);
			x(xSize(1), xSize(2)) = B(end) \ A(end);
			for it = 1 : numel(x)-1
				x(it) = B(it) \ A(it);
			end
		end % ldivide()

	end % methods (Access = public)
	
	methods (Access = protected)
		
		function v = evaluateFunction(obj, varargin)
			% evaluateFunction evaluates the function_handle obj.valueContinuous on the discrete
			% points defined by varargin.
			if obj.pointWiseEvaluation == true
				assert(all( size(obj) == 1 ))
				coord = cell(1, length(varargin));
				for i = 1:numel(varargin{1})
					for k = 1:length(varargin)
						coord{k} = varargin{k}(i);
					end
					v(i) = obj.valueContinuous(coord{:});
				end
			else
				v = obj.valueContinuous(varargin{:});
			end
		end % evaluateFunction
		
	end % methods (Access = protected)
	
	methods (Static)%, Access = protected)
		function [o] = zero(varargin)
			% ZERO creates an array containing only zeros with the same
			% dimensions as input ndgrids. This is needed for, function
			% handles that return a constant value and should be able to be
			% called on a ndgrid.
			if nargin >= 1
				o = zeros(size(varargin{1}));
			elseif nargin == 0
				o = 0;
			end
		end
	end % methods (Static)
	
end % classdef Function

