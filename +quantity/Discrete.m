classdef  (InferiorClasses = {?quantity.Symbolic, ?signals.Piecewise, ...
		?signals.UniformDistributedSignal, ?signals.NormalDistributedSignal}) ...
		Discrete < handle & matlab.mixin.Copyable & matlab.mixin.CustomDisplay
	% quantity.Discrete allows to perform calculations with double-arrays which represent
	% matrix-valued functions discretized on a grid as if it would be a function. In combination
	% with quantity.Discrete and quantity.Function one can use discrete data, symbolic variables and
	% function_handles in the same computations with the same syntax. For this linear interpolation
	% is used. See quantity.example for examples.
	%
	% See also quantity.Discrete.Discrete (constructor), quantity.example, quantity.Domain, 
	%	quantity.Symbolic, quantity.Function.

	properties (SetAccess = protected)
		% Discrete values of the quantity
		valueDiscrete double;
	end
	
	properties
		% Name of this object
		name (1,1) string;

		% quantity.Domain specifies the name of the domain and the discrete grid
		domain;
	end
	
	methods
		function obj = Discrete(value, domain, optArgs)
			% quantity.Discrete.Discrete constructs a quantity.Discrete object, which allows to
			% perform calculations with double-arrays which represent matrix-valued functions 
			% discretized on a grid as if it would be a function. For this linear interpolation is 
			% used. See quantity.example for examples.
			%
			%	obj = quantity.Discrete(value, domain) creates obj which represents a matrix-valued
			%		function which is initialised based discretized values on the grid specified by
			%		domain to obtain the numeric data value.
			%		- domain must be a quantity.Domain object. To specify multiple domains, use 
			%			domain as an quantity.Domain array.
			%		- value can be specified in two ways:
			%			1) As a double-array of the first dimensions represent the domains in the
			%				order in the domain-array, while the last two dimensions represent the
			%				dimension of the original matrix-valued function, hence
			%					size(value) == [domain.n, size(obj)]
			%			2) As a cell-array of double arrays in which every cell-element represents a
			%				matrix element of the original function, end every cell-element must
			%				contain a double array fitting to the domain, i.e., 
			%					size(value) == size(obj) and size(value{it} == [domain.n].
			%
			%	obj = quantity.Symbolic([...], "name", name) sets the name of the quantity to the 
			%		string name.
			%
			% See also quantity.Discrete (class description), quantity.example,  ...
			%	quantity.Discrete.ones, quantity.Discrete.eye, quantity.Discrete.zeros, ...
			%	quantity.Domain,  quantity.Symbolic, quantity.Function.
			
            arguments
				value = []
				domain (1,:) quantity.Domain = quantity.Domain.empty();
				optArgs.name (1,1) string = "";
			end
			% to allow the initialization as object array, the constructor
			% must be allowed to be called without arguments, i.e. nargin == 0.
			% Then no parameters are set.
			if nargin == 1 && isa(value, "quantity.Discrete")
				% if nargin == 1 it can be a conversion of child-classes or an empty object
				% allows the conversion of a quantity object without extra check if the object is 
				% already from class quantity.Discrete
				obj = value;
				
			elseif nargin > 1
				% convert double valued valueOriginal to cell-valued value original
				if ~iscell(value)
					value = quantity.Discrete.value2cell(value, numel(domain));
				end
				
				if isempty( value )
					obj = quantity.Discrete.empty( size(value) );
					return
				end
				
				% Check if the grid fits to the values. In addition, catch the case if all values
				% are empty. This is required for the initialization of quantity.Function and
				% quantity.Symbolic objects
				assert(isempty(domain) ... % constant case
					|| all( cellfun(@isempty, value ), "all" ) ... % empty case
					|| isequal([domain.n], size(value{1}, 1 : max(1, numel(domain)))) ... % usual case
					|| (isrow(value{1}) && ... % row-vector case (including next line)
						isequal([1, domain.n], size(value{1}, 1 : max(1, numel(domain)+1)))), ...
					"grids do not fit to valueOriginal");
				
				% set valueDiscrete and preallocate by setting last element first.
				if numel(domain) == 1
					obj(numel(value)).valueDiscrete = value{numel(value)}(:);
					for k = 1 : (numel(value)-1)
						% for quantities on a single domain, ensure that
						% the discrete values are stored as column-vector
						% by using the (:) operator.
						obj(k).valueDiscrete = value{k}(:);
					end
				else
					obj(numel(value)).valueDiscrete = value{numel(value)};
					for k = 1 : (numel(value) - 1)
						obj(k).valueDiscrete = value{k};
					end
				end % if numel(myDomain) == 1
				
				%% set further properties
				[obj.domain] = deal(domain);
				obj.setName( optArgs.name );
				%% reshape object from vector to matrix
				obj = reshape(obj, size(value));				
			end
		end % Discrete() constructor
		
		function itIs = isNumber(obj)
			%ISNUMBER checks if the quantity has an empty domain.
			%
			% See also quantity.Discrete.isConstant
			itIs = isempty(obj(1).domain);
		end % isNumber()
		
		function itIs = isConstant(obj)
			%ISCONSTANT checks if the quantity is a constant
			% the quantity is interpreted as constant if for each quantity-array element
			% all discrete values are equal.
			%
			% See also quantity.Discrete.isNumber
			itIs = true;
			% or if all entries have the same value
			for k = 1:numel(obj)
				itIs = itIs && all( diff( obj(k).valueDiscrete ) == 0, 'all');
			end
		end % isConstant()
		
		function valueDiscrete = get.valueDiscrete(obj)
			% check if the value discrete for this object
			% has already been computed.
			empty = isempty(obj.valueDiscrete);
			if any(empty(:))
				obj.valueDiscrete = obj.obj2value(obj.domain, true);
			end
			valueDiscrete = obj.valueDiscrete;
		end % get.valueDiscrete

		function exportData = exportData(obj, varargin)
			% EXPORTDATA initializes a generator for a *.dat file
			% exportData = exportData(obj, varargin) initializes a export.Data object, which is
			% useful to export the data to a *.dat file. With this file a plot of the data can be
			% generated in latex/tikz.
			% With varargin, the properties of the export.Data object can be specified.
			arguments
				obj
			end
			arguments (Repeating)
				varargin
			end
			
			% make the object names:
			headers = cell(1, numel(obj) + 1);
			headers{1} = obj(1).domain(1).name;
			headers(2:end) = num2cell(obj.getIndexedName());
			if obj.nargin == 1
				thisGrid = {obj(1).domain.grid};
				exportData = export.dd(...
					'M', [thisGrid{:}, obj.valueDiscrete], ...
					'header', headers, varargin{:});
			elseif obj.nargin == 2
				if numel(obj) == 1
					exportData = export.ddd( "x", obj.domain(1).grid, "y", obj.domain(2).grid, ...
						"z", obj.on(), varargin{:});
				else
					error('Not yet implemented')
				end
			else
				error('Not yet implemented')
			end
		end % exportData()
		
		function d = double(obj)
			% double convertes the quantity into a double and returns the values of obj.on()
			d = obj.on();
		end % double()
		
		function quantityFun = quantity.Function(obj)
			% quantity.Function converts the quantity.Discrete obj to a quantity.Function object, by
			% using the griddedInterpolant to implement a continous function.
			%	
			%	quantityFun = quantity.Function(obj) creates quantityFun which is a
			%		quantity.Function object that contains the values if obj by using an interpolant
			%		on the discrete values of obj, to obtain a function_handle.			
			quantityFun(size(obj)) = quantity.Function(); % preallocation
			for k = 1:numel(obj)
				F = griddedInterpolant(obj(k).domain.ndgrid{:}, obj(k).on());
				functionArguments = strjoin([obj(1).domain.name], ",");
 				functionHandle = eval("@(" + functionArguments + ") F(" + functionArguments + ")");
				args = obj(k).optArgList;
				quantityFun(k) = quantity.Function(functionHandle, obj(1).domain, args{:});
			end
			
			quantityFun = reshape(quantityFun, size(obj));
		end % quantity.Function()
		
		function o = signals.PolynomialOperator(obj, varargin)
			A = cell(size(obj, 3), 1);
			for k = 1:size(obj, 3)
				A{k} = obj(:,:,k);
			end
			o = signals.PolynomialOperator(A, varargin{:});
		end
		
		function o = quantity.Symbolic(obj)
			% quantity.Symbolic conerts an empty quantity.Discrete object into an empty
			% quantity.Symbolic object. Obviously, this only works for quantities, that do not have
			% a domain.
			if isempty(obj)
				o = quantity.Symbolic.empty(size(obj));
			else
				error('Not yet implemented')
			end
		end % quantity.Symbolic()
		
		function b = BasicVariable(obj, optArgs)
			% BasicVariable cast of a quantity.Discrete object to signals.BasicVariable
			% b = BasicVariable(obj, optArgs) will cast the quantity.Discrete 'obj' to a
			% signals.BasicVariable. With the optional name-value-pair argument "numDiff", the
			% number of pre computed derivatives is defined. The default value is 3.
			arguments
				obj
				optArgs.numDiff (1,1) double {mustBeInteger, mustBePositive}= 3;
			end
			
			assert( obj.nargin == 1, "Only for quantities defined on one domain implemented")
			d = obj(1).domain;
			
			for i = 1:numel(obj)
				derivatives = cell(optArgs.numDiff,1);
				for k = 1:optArgs.numDiff
					derivatives{k} = obj(i).diff( d, k );
				end

				b(i) = signals.BasicVariable( obj(i), derivatives );
			end
			
			b = reshape(b, size(obj));
			
		end % BasicVariable()
		
		function obj = setName(obj, newName)
			% setName Set the name for the quantity.
			% obj = setName(obj, newName, optArgs) will set the name property of the quantity. If it
			% is an object array, all elements will have the same name.
			arguments
				obj
				newName (1,1) string
			end			
			[obj.name] = deal(newName);
		end % setName()
		
	end % methods
	
	methods (Access = public)
		
		function names = getIndexedName(obj, optArgs)
			arguments
				obj
				optArgs.idx
				optArgs.format = ""
			end
			% getIndexedName The name of the object with the indizes of the matrix.
			dims = ndims(obj);
			objSize = size(obj);
			names = repmat("", objSize);
			objName = obj(1).name;
			
			% if it is a column vector, ndims will also return 2. 
			if iscolumn(obj)
				dims = 1;
			end

			if numel(obj) == 1
				names = objName;
			else
				for i = 1:numel(obj)
					idx = cell(dims,1);
					[idx{:}] = ind2sub( objSize, i);
					
					if optArgs.format == "latex"
						names(i) = "$" + objName + "_{" + sprintf("%i", [idx{:}]) + "}$";
					else
						names(i) = objName + "_" + sprintf("%i", [idx{:}]);
					end
					
				end
			end
			
			if isfield( optArgs, "idx")
				names = names( optArgs.idx );
			end
			
		end % getIndexedName
		
		function [z_idx, z_grid, z_value] = findZeros(obj, optArg)
			arguments
				obj,
				optArg.tol = 100*eps;
			end
			assert(obj(1).nargin == 1, ...
				"This function is only implemented for functions dependening on one argument so far");
			
			z_idx = cell(size(obj));
			z_grid = z_idx;
			z_value = z_idx;
			for it = 1:numel(obj)
				
				data = obj(it).valueDiscrete;
				
				zeros = find( abs(data) <= optArg.tol);
				upCrossing = find( data(1:end-1) <= 0 & data(2:end) > 0);
				downCrossing = find( data(1:end-1) >= 0 & data(2:end) < 0);
				
				% note: the accuracy of the up and down crossing may be increased by an
				% interpolation to compute the exact zero crossing, at least of the interpolation.
				% However, this should be implemented if higher accurracy is required.
				
				z_idx{it} = unique( [zeros; upCrossing; downCrossing]);
				z_grid{it} = obj(1).domain.grid(z_idx{it});
				z_value{it} = data(z_idx{it});
			end
		end % findZeros()
		
		function h = hash(obj)
			%HASH create a SHA-1 hash value for this object
			% h = hash(obj) will copmute the SHA-1 hash value based on the
			% data "valueDiscrete", "domain.name", "domain.grid",
			% "obj.name"
			% The hash value can be used to get a "short" identifier for
			% this object.
			
			% create a huge array of all relevant quantity data
			data = {[ obj.valueDiscrete ], obj(1).domain.name, ...
				    obj(1).domain.grid, obj(1).name};
			h = misc.hash(data);
		end % hash()
		
		function thisDomain = compositionDomain(obj, domainName)
			% compositionDomain is a helper function for compose.
			arguments
				obj (1, 1) quantity.Discrete;
				domainName (1, 1) string;
			end
			
			dDiscrete = obj.on();
			
			% vectorization of the n-d-grid: compositionDomain	
			thisDomain = quantity.Domain(domainName, dDiscrete(:));
		end % compositionDomain
		
		function obj = compose(obj, g, optionalArgs)
			% COMPOSE compose two functions
			%
			%	obj_hat = compose(obj, G, varargin) composes the function f defined by OBJ with the
				%	function given by G. In particular,
				%		f_hat(z,t) = f( z, g(z,t) )
				%	if f(t) = obj, g is G and f_hat is OBJ_hat.
			arguments
				obj
				g (1, 1) quantity.Discrete;
				optionalArgs.domain (1, 1) string {mustBe.domainOf(obj, optionalArgs.domain)} ...
					= obj(1).domain(1);
			end
			
			% quick workaround to apply to matrix-valued quantities
			if numel(obj) > 1
				% ensure that the object is of type quantity.Discrete and 
 				obj = quantity.Discrete( obj );
				optArgs = misc.struct2namevaluepair( optionalArgs );
				for k = 1:numel(obj)
% 					if obj(k).iszero
% 						TODO Verify that the domain is correct!
%  						obj_hat(k) = quantity.Discrete.zeros(1, g.domain) ;
% 					else
					obj(k) = compose(obj(k), g, optArgs{:});
% 					end
				end
				return
			end
			
			originalDomain = obj(1).domain;
			[idx, logOfDomain] = originalDomain.index(optionalArgs.domain);
			
			%if there are domains, that are defined both for obj and g, for instance z in the
			%case f(z, g(z)), they require special attention. First of all, it has to be
			%ensured, that the z-domain of g, has the same grid as the z-domain of f, which is
			%implemented here.
			% find the domains that are defined for both obj and g:
			if all(logOfDomain == 1)
				intersectDomain = [];
			else
				intersectDomain = intersect([originalDomain( ~logOfDomain ).name], [g(1).domain.name]);
				if ~isempty(intersectDomain)
					% if there are domains both defined for obj and g, then check if they are same,
					% and change the domain of g if they are not.
					intersectDomainG = g(1).domain.find(intersectDomain);
					intersectDomainF = obj(1).domain.find(intersectDomain);
					for it = 1 : numel(intersectDomain)
						if ~isequal(intersectDomainG(it), intersectDomainF(it))
							g = g.changeDomain(intersectDomainF(it));
						end
					end
				end % if ~isempty(intersectDomain)
			end % if all(logOfDomain == 1)
			
			% get the composition domain:
			%	For the argument y of a function f(y), which should be
			%	composed by y = g(z,t), a new domain is created by evaluating g(z,t).
			composeOnDomain = g.compositionDomain(optionalArgs.domain);
			sizeComposeDomain = composeOnDomain.n;
			
			% check if the composition domain is in the range of definition of obj.domain.
			if ~composeOnDomain.isSubDomainOf( originalDomain(idx) )
				warning('quantity:Discrete:compose', ....
					"The composition domain is not a subset of obj.domain, i.e. the values of g " ...
					+ "exceed the grid of obj.domain. Missing values will be extrapolated.");
			end
			
			% evaluation on the new grid:
			%	the order of the domains is important. At first, the domains which will not be
			%	replaced are taken. The last domain must be the composed domain. For example: a
			%	function f(x, y, z, t), where y should be composed with g(z, t) will be resorted to
			%	f_(x, z, t, y) and then evaluated with y = g(z,t)
			
			% #remark: optimize the memory consumption of this function.
			%	1) only consider the unique grid points in evaluationDomain 
			%		-> seems to be complicated and unsure if it has a benefit!			%	
			%	2) do the conversion of the evaluationDomain directly to the target domain.
			evaluationDomain = [originalDomain( ~logOfDomain ), composeOnDomain ];
			
			newValues = obj.on( evaluationDomain );
			
			% reshape the new values into a 2-d array so that the first dimension is any domain but
			% the composition domain and the last dimension is the composition domain
			sizeOldDomain = prod( [originalDomain( ~logOfDomain ).n] );
			
			newValues = reshape(newValues, [sizeOldDomain, sizeComposeDomain]);

			% rearrange the computed values, to have the same dimension as the required domain
			% consider the domain 
			%		f( z, g(z,t) ) = f(z, g(zeta,t) )|_{zeta = z}
			tmpDomain = [originalDomain( ~logOfDomain ), g(1).domain ];
			% newValues will be reshaped into the form
			%	f(z, t, zeta)
			newValues = reshape( newValues, [tmpDomain.gridLength, 1] );
			
			if ~isempty(intersectDomain)
				% now the common domains, i.e., zeta = z must be merged: For this, use intersect to
				% find the common domains. The comparison is applied to the domain names. This is
				% required, because intersect only works with objects of the same type. If one of
				% the domains is an quantity.EquidistantDomain, the direct call of intersect on the
				% domains will lead to an error.
				idx = tmpDomain.index( intersectDomain );
				% take the diagonal values of the common domain, i.e., z = zeta		
				% use the diag_nd function because it seems to be faster
				% then the diagNd function, although the values must be
				% sorted.
				newValues = misc.diagNd(newValues, idx);
			end
			
			% *) build a new valueDiscrete on the correct grid.		
			obj = quantity.Discrete( newValues, tmpDomain.join, ...
				"name", obj.name + " ° " + g.name);
			
		end % compose()
		
		function value = on(obj, myDomain, gridNames)
			% ON evaluation of the quantity on a certain domain.
			%
			%	value = on(obj) or value = obj.on(); evaluates the quantity on its standard grid.
			%
			%	value = obj.on( myDomain ) evalutes the quantity on the grid specified by myDomain.
			%		The order of the domains in domain, will be the same as from myDomain.
			%
			%	value = obj.on( grid ) evaluates the quantity specified by grid. Grid must be a 
			%		cell-array with the grids as elements.
			%
			%	value = obj.on( grid, gridName ) evaluates the quantity specified by grid. Grid must
			%		be a cell-arry with the grids as elements. By the gridName parameter the order 
			%		of the grid can be specified.

			if isempty(obj)
				value = zeros(size(obj));
			else
				if nargin == 1
					% case 0: no domain was specified, hence the value is requested
					% on the default grid defined by obj(1).domain.
					value = reshape(cat(numel(obj(1).domain)+1, obj(:).valueDiscrete), ...
						[obj(1).domain.gridLength(), size(obj)]);
					
				elseif nargin == 2 && (iscell(myDomain) || isnumeric(myDomain))
					% case 1: a domain is specified by myDomain as a grid
					newGrid = misc.ensureIsCell(myDomain);

					% Check if the object has a domain with a name:
					% If there is no name, the object has an empty quantity.Domain, i.e., it is a
					% constant. Then, the gridNames should be ''
					if isNumber(obj) % true if domain of obj is empty
						gridNames = repmat({''}, length(newGrid));
					else
						gridNames = {obj(1).domain.name};
					end

					% initialize the new domain
					clear('myDomain');
					myDomain(1:length(newGrid)) = quantity.Domain();					
					for k = 1:length(newGrid)
						myDomain(k) = quantity.Domain(gridNames{k}, newGrid{k});
					end
					value = obj.obj2value(myDomain);
				else
					% Since in the remaining cases the order of the domains is not 
					% neccessarily equal to the order in obj(1).domain, this is 
					% more involved:
					if nargin == 2
						% case 2: a domain is specified by a myDomain = domain-array
						% nothing has to be done to obtain the domain.

					elseif nargin == 3
						% case 3: a domain is specified by a grid and a grid
						% name. Then, the first input parameter is the grid,
						% i.e., myGrid = myDomain and the second is the grid
						% name.
						% Since the order of the domains is not neccessarily equal to the
						% order in obj(1).domain, this is more involved:
						myDomain = misc.ensureIsCell(myDomain);
						gridNames = misc.ensureString(gridNames);

						assert(all(cellfun(@(v)isvector(v), myDomain)), ...
							'The cell entries for a new grid have to be vectors')

						newGrid = myDomain;
						clear('myDomain');
						myDomain(1:length(newGrid)) = quantity.Domain();	
						for k = 1:length(newGrid)
							myDomain(k) = quantity.Domain(gridNames{k}, newGrid{k});
						end
					else
						error('wrong number of input arguments')
					end
					
					% Check if obj can be evaluated on myDomain and determine a permutation in the
					% case of a different order of obj(1).domain and myDomain.
					if isNumber(obj)
						gridPermuteIdx = 1:numel(myDomain);
					else
						assert(numel(myDomain) == numel(obj(1).domain), ...
							'Wrong grid for the evaluation of the object');
						% compute the permutation index, in order to bring the
						% new domain in the same order as the original one.
						gridPermuteIdx = obj(1).domain.getPermutationIdx(myDomain);
						
						assert(any(gridPermuteIdx ~= 0), "grid could not be found.")
					end			
					% get the valueDiscrete data for this object. Apply the
					% permuted myDomain. Then the obj2value will be evaluated
					% in the order of the original domain. The permutation to
					% the new order will be done in the next step.
					originalOrderedDomain(gridPermuteIdx) = myDomain;
					
					value = obj.obj2value(originalOrderedDomain);
					value = permute(value, [gridPermuteIdx, numel(gridPermuteIdx)+(1:ndims(obj))]);
				end
			end % if isempty(obj)
		end % on()
		
		function [B, I] = sort(obj, direction)
			% sort   Sort in ascending or descending order.
			%	B = sort(A) sorts in ascending order.
			%	The sorted output B has the same type and size as A:
			%	- For vectors, sort(A) sorts the elements of A in ascending order.
			%	- No implementation for matrices or N-D-arrays yet.
			%
			% 	B = sort(A,DIRECTION) specify the sort direction. DIRECTION must be:
			%         'ascend'  - (default) Sorts in ascending order.
			%         'descend' - Sorts in descending order.
			% 
			%     [B,I] = sort(A,...) also returns a sort index I which specifies how the
			%     elements of A were rearranged to obtain the sorted output B:
			%     - If A is a vector, then B = A(I).
			%
			% Note that this sort method might cause errors if there is nor strict-ordering
			% possible, see try-catch block in this code.
			arguments
				obj {mustBe.vector};
				direction (1, 1) string = "ascend";
			end % arguments
			
			[~, I] = sort(obj.at( [obj(1).domain.lower] ), direction);
			B = obj(I);
			try 
				assert(B.issorted(direction), "sort was unsuccessful.");
			catch
				[~, I] = sort(obj.at( [obj(1).domain.upper] ), direction);
				B = obj(I);
				assert(B.issorted(direction), "sort was unsuccessful.");
			end
				
		end % sort()
		
		function isIt = issorted(obj, direction, NameValue)
			% issorted   Check if data is sorted.
			%	TF = issorted(A) returns TRUE if the elements of A are sorted in
			%	ascending order, namely, returns TRUE if A and SORT(A) are identical:
			%	- For vectors, issorted(A) returns TRUE if A is a sorted vector.
			% 
			%	TF = issorted(A,DIRECTION) check if
			%	A is sorted according to the specified direction. DIRECTION must be:
			%		'ascend'          - (default) Checks if data is in ascending order.
			%		'descend'         - Checks if data is in descending order.
			%		'monotonic'       - Checks if data is in either ascending or
			%							descending order.
			%		'strictascend'    - Checks if data is in ascending order and does
			%							not contain duplicate or missing elements.
			%		'strictdescend'   - Checks if data is in descending order and does
			%							not contain duplicate or missing elements.
			%		'strictmonotonic' - Checks if data is in either ascending or
			%							descending order and does not contain duplicate
			%							or missing elements.
			arguments
				obj {mustBe.vector};
				direction (1, 1) string = "ascend";
				NameValue.tolerance (1, 1) double = 10*eps;
			end % arguments
			
			diffOfElements = quantity.Discrete.zeros([numel(obj)-1, 1], obj(1).domain);
			for it = 1 : numel(diffOfElements)
				diffOfElements(it) = obj(it+1) - obj(it);
			end
			
			isIt = false;
			switch direction
				case "ascend" % diffOfElements >= 0 
					if MAX(-diffOfElements) <= NameValue.tolerance
						isIt = true;
					end
				case "descend" % diffOfElements <= 0 
					if MAX(diffOfElements) <= NameValue.tolerance
						isIt = true;
					end
				case "monotonic" % diffOfElements >= 0  || diffOfElements <= 0 
					if (MAX(-diffOfElements) <= NameValue.tolerance) ...
							|| (MAX(diffOfElements) <= NameValue.tolerance)
						isIt = true;
					end
					
				case "strictascend" % diffOfElements >= 0 
					if MAX(-diffOfElements) < NameValue.tolerance
						isIt = true;
					end
					
				case "strictdescend" % diffOfElements <= 0 
					if MAX(diffOfElements) < NameValue.tolerance
						isIt = true;
					end
					
				case "strictmonotonic" % diffOfElements >= 0  || diffOfElements <= 0 
					if (MAX(-diffOfElements) < NameValue.tolerance) ...
							|| (MAX(diffOfElements) < NameValue.tolerance)
						isIt = true;
					end
					
				otherwise
					error("direction = " + direction + "is not valid");
			end
		end % issorted()
		
		function newObj = sortDomain(obj, varargin)
			%SORTDOMAIN sorts the domain of the object in a desired order
			% obj = sortDomain(obj) sorts the grid in alphabetical order.
			% obj = sortDomain(obj, 'descend') sorts the grid in descending alphabetical order.
			newObj = obj.copy();
			% only sort the grids if there is something to sort
			if ~isempty(newObj) && newObj(1).nargin > 1
				
				[sortedDomain, I] = newObj(1).domain.sort(varargin{:});
				[newObj.domain] = deal(sortedDomain);
				
				for k = 1:numel(newObj)
					newObj(k).valueDiscrete = permute(obj(k).valueDiscrete, I);
				end
			end
		end % sortDomain()
		
		function c = horzcat(a, varargin)
			%HORZCAT Horizontal concatenation.
			%   [A B] is the horizontal concatenation of objects A and B
			%   from the class quantity.Discrete. A and B must have the
			%   same number of rows and the same grid. [A,B] is the same
			%   thing. Any number of matrices can be concatenated within
			%   one pair of brackets. Horizontal and vertical concatenation
			%   can be combined together as in [1 2;3 4].
			%
			%   [A B; C] is allowed if the number of rows of A equals the
			%   number of rows of B and the number of columns of A plus the
			%   number of columns of B equals the number of columns of C.
			%   The matrices in a concatenation expression can themselves
			%   by formed via a concatenation as in [A B;[C D]].  These
			%   rules generalize in a hopefully obvious way to allow fairly
			%   complicated constructions.
			%
			%   N-D arrays are concatenated along the second dimension. The
			%   first and remaining dimensions must match.
			%
			%   C = HORZCAT(A,B) is called for the syntax '[A  B]' when A
			%   or B is an object.
			%
			%   Y = HORZCAT(X1,X2,X3,...) is called for the syntax '[X1 X2
			%   X3 ...]' when any of X1, X2, X3, etc. is an object.
			%
			%	See also HORZCAT, CAT.
			c = cat(2, a, varargin{:});
		end
		function c = vertcat(a, varargin)
			%VERTCAT Vertical concatenation.
			%   [A;B] is the vertical concatenation of objects A and B from
			%   the class quantity.Discrete. A and B must have the same
			%   number of columns and the same grid. Any number of matrices
			%   can be concatenated within one pair of brackets. Horizontal
			%   and vertical concatenation can be combined together as in
			%   [1 2;3 4].
			%
			%   [A B; C] is allowed if the number of rows of A equals the
			%   number of rows of B and the number of columns of A plus the
			%   number of columns of B equals the number of columns of C.
			%   The matrices in a concatenation expression can themselves
			%   by formed via a concatenation as in [A B;[C D]].  These
			%   rules generalize in a hopefully obvious way to allow fairly
			%   complicated constructions.
			%
			%   N-D arrays are concatenated along the first dimension. The
			%   remaining dimensions must match.
			%
			%   C = VERTCAT(A,B) is called for the syntax '[A; B]' when A
			%   or B is an object.
			%
			%   Y = VERTCAT(X1,X2,X3,...) is called for the syntax '[X1;
			%   X2; X3; ...]' when any of X1, X2, X3, etc. is an object.
			%
			%   See also HORZCAT, CAT.
			c = cat(1, a, varargin{:});
		end
		function c = cat(dim, a, varargin)
			%CAT Concatenate arrays.
			%   CAT(DIM,A,B) concatenates the arrays of objects A and B
			%   from the class quantity.Discrete along the dimension DIM.
			%   CAT(2,A,B) is the same as [A,B]. CAT(1,A,B) is the same as
			%   [A;B].
			%
			%   B = CAT(DIM,A1,A2,A3,A4,...) concatenates the input arrays
			%   A1, A2, etc. along the dimension DIM.
			%
			%   When used with comma separated list syntax, CAT(DIM,C{:})
			%   or CAT(DIM,C.FIELD) is a convenient way to concatenate a
			%   cell or structure array containing numeric matrices into a
			%   single matrix.
			%
			%   Examples:
			%     a = magic(3); b = pascal(3);
			%     c = cat(4,a,b)
			%   produces a 3-by-3-by-1-by-2 result and
			%     s = {a b};
			%     for i=1:length(s),
			%       siz{i} = size(s{i});
			%     end
			%     sizes = cat(1,siz{:})
			%   produces a 2-by-2 array of size vectors.
			
			if nargin == 1
				objCell = {a};
			else
				objCell = [{a}, varargin(:)'];
				
				% this function has the peculiarity that the input argument a does
				% not have to be an quantity.Discrete object. So it has to
				% be checked which of the input arguments is an
				% quantity.Discrete object. This is considered to give
				% the basic values for the initialization of new
				% quantity.Discrete values
				isEmpty = cellfun(@(o) isempty(o), objCell);
								
				if all(isEmpty)
					% use the built-in function directly:
					c = builtin('cat', dim, a, varargin{:});
					return
				else
					% remove all empty objects:
					objCell = objCell(~isEmpty);
				end
			end
			
			% from here on we assume that we have only non empty entries in the objCell.
			nElements = numel( objCell );
			% assert that all elements in objCell are quantity.Discrete objects, otherwise create an
			% quantity.Discrete object for them.
			for k = 1:nElements
				if isa(objCell{k}, "quantity.Discrete")
					o = objCell{k};
				elseif isnumeric( objCell{k} )
					o = quantity.Discrete( objCell{k}, quantity.Domain.empty());
				else
					error( "Concatenation for quantity.Discrete objects with " + ...
						class( objCell{k} ) + " not implemented!");
				end
				% sort the grid names of each quantity
				objCell{k} = o.sortDomain();
			end
						
			% find the finest domain:
			newDomain = objCell{1}(1).domain;
			for it = 2 : nElements
				newDomain = newDomain.join(objCell{it}(1).domain);
			end
			
			% change the domain to the newDomain
			for it = 1 : nElements
				objCell{it} = objCell{it}.changeDomain(newDomain);
			end
			
			% finally do the concatenation:
			argin = [{dim}, objCell(:)'];
			c = builtin('cat', argin{:});
		end % cat()
		
		function Y = blkdiag(A, varargin)
			% blkdiag  Block diagonal concatenation of matrix input arguments.
			%									|A 0 .. 0|
			% Y = blkdiag(A,B,...)  produces	|0 B .. 0|
			%									|0 0 ..  |
			% Yet, A, B, ... must have the same gridName and grid.
			if nargin == 1
				Y = copy(A);
			else
				B = varargin{1};
				if isempty(B)
					Y = A;
				else
					assert(isequal([A(1).domain.name], [B(1).domain.name]), ...
						"only implemented for same gridName");
					% otherwise cat() throws an error
					Y = [A, zeros(size(A, 1), size(B, 2)); ...
						zeros(size(B, 1), size(A, 2)), B];
				end
				if nargin > 2
					Y = blkdiag(Y, varargin{2:end});
				end
			end
		end % blkdiag()
		
		function K = kron(X, Y)
			% kron   Kronecker tensor product.
			%     kron(X,Y) is the Kronecker tensor product of X and Y.
			%     The result is a large matrix formed by taking all possible
			%     products between the elements of X and those of Y. For
			%     example, if X is 2 by 3, then kron(X,Y) is
			%  
			%        [ X(1,1)*Y  X(1,2)*Y  X(1,3)*Y
			%          X(2,1)*Y  X(2,2)*Y  X(2,3)*Y ]
			xSize = size(X);
			ySize = size(Y);
			kSize = xSize .* ySize;

			if isnumeric(X)
				xName = "c";
				yName = Y(1).name;
				kDomain = Y(1).domain;
				yData = Y.on();
				kElementSelctor = cell(1, numel(kDomain));
				for it = 1 : numel(kElementSelctor)
					kElementSelctor{it} = 1:1:kDomain(it).n;
				end
				kValue = zeros([kDomain.n, kSize]);
				for xRow = 1 : xSize(1)
					for xCol = 1 : xSize(2)
						kValue(kElementSelctor{:}, (1:ySize(1)) + ySize(1)*(xRow-1), (1:ySize(2)) + ySize(2)*(xCol-1)) ...
							=  X(xRow, xCol) * yData;
					end % for jt = 1 : xSize(2)
				end % for it = 1 : xSize(1)
				K = quantity.Discrete(kValue, kDomain, ...
					"name", "kron(" + xName + ", " + yName + ")");

			elseif isnumeric(Y)
				yName = "c";
				xName = X(1).name;
				kDomain = X(1).domain;
				xData = X.on();
				xData = reshape(xData, [prod([kDomain.n]), xSize]);
				yData = permute(Y, [3, 1, 2]);
				kValue = zeros([prod([kDomain.n]), kSize]);
				for xRow = 1 : xSize(1)
					for xCol = 1 : xSize(2)
						kValue(:, (1:ySize(1)) + ySize(1)*(xRow-1), (1:ySize(2)) + ySize(2)*(xCol-1)) ...
							= xData(:, xRow, xCol) .* yData;
					end % for jt = 1 : xSize(2)
				end % for it = 1 : xSize(1)
				K = quantity.Discrete(reshape(kValue, [kDomain.n, kSize]), kDomain, ...
					"name", "kron(" + xName + ", " + yName + ")");
			else
				xName = X(1).name;
				yName = Y(1).name;
				% inspired by quantity.Discrete.innerMTimes-method:
				[idx, ~] = computePermutationVectors(X, Y);
				domainX = X(1).domain;
				domainY = Y(1).domain;
				kDomain = [ join(domainX(idx.A.common), domainY(idx.B.common)), ...
					domainX(~idx.A.common), domainY(~idx.B.common) ];
				valueX = X.on(kDomain(sort(kDomain.index([domainX.name]))));
				valueY = Y.on(kDomain(sort(kDomain.index([domainY.name]))));
				xElementSelctor = cell(1, numel(domainX));
				xDomainNew = kDomain.find(domainX);
				for it = 1 : numel(xElementSelctor)
					xElementSelctor{it} = 1:1:xDomainNew(it).n;
				end
				kElementSelctor = cell(1, numel(kDomain));
				for it = 1 : numel(kElementSelctor)
					kElementSelctor{it} = 1:1:kDomain(it).n;
				end
				kValue = zeros([kDomain.n, kSize]);
				for xRow = 1 : xSize(1)
					for xCol = 1 : xSize(2)
						kValue(kElementSelctor{:}, ...
							(1:ySize(1)) + ySize(1)*(xRow-1), (1:ySize(2)) + ySize(2)*(xCol-1)) ...
							= misc.multArray(valueX(xElementSelctor{:}, xRow, xCol), ...
							valueY, numel(xElementSelctor)+1, numel(size(valueY))+1, idx.common);
					end % for jt = 1 : xSize(2)
				end % for it = 1 : xSize(1)

				K = quantity.Discrete(kValue, kDomain, ...
					"name", "kron(" + xName + ", " + yName + ")");
			end
		end % kron()
		
		function solution = solveAlgebraic(obj, rhs)
			% solveAlgebraic solves
			%	obj( x ) == rhs
			% for the variable specified x. Both x and rhs must be scalars.
			% The input parameter findBetween specifies minimum and maximum of the
			% values of obj, between which the solution should be searched.
			arguments
				obj (1, 1);
				rhs (1, 1) double;
			end
			assert(obj(1).nargin == 1, "only implemented for quantites on 1 domain");
			
			solution = interp1(obj.on(), obj.domain.grid, rhs);
		end % solveAlgebraic()
		
		function inverse = invert(obj)
			% inverse solves the function representet by the quantity for
			% its variable, for instance, if obj represents y = f(x), then
			% invert returns an object containing x = f^-1(y).
			% Yet, this is only implemented for obj with one variable
			% (grid).
			
			arguments
				obj (1, 1);
			end
			
			assert(numel(obj.nargin) == 1, "invert is only implemented for quantities on 1 domain");
			inverse = quantity.Discrete(obj(1).domain.grid, ...
                quantity.Domain(obj(1).name, obj.on()), "name", obj.domain.name);
		end % invert()
		
		function solution = solveDVariableEqualQuantity(obj, varargin)
			% solves the first order ODE
			%	dvar / ds = obj(var(s))
			%	var(0) = ic
			% to obtain var(s, ic) depending on both the argument s and the initial
			% condition ic. Herein, obj may only depend on one variable / gridName / ...
			% domain.
			assert(numel(obj(1).domain) == 1, ...
				'this method is only implemented for quanitities with one domain');
			
			myParser = misc.Parser();
			myParser.addParameter('initialValueGrid', obj(1).domain(1).grid);
			myParser.addParameter('variableGrid', obj(1).domain(1).grid);
			myParser.addParameter('newGridName', 's');
			myParser.parse(varargin{:});
			
			variableGrid = myParser.Results.variableGrid(:);
			myGridSize = [numel(variableGrid), ...
				numel(myParser.Results.initialValueGrid)];
			
			% the time (s) vector has to start at 0, to ensure the IC. If
			% variableGrid does not start with 0, it is separated in
			% negative and positive parts and later combined again.
			positiveVariableGrid = [0; variableGrid(variableGrid > 0)];
			negativeVariableGrid = [0; flip(variableGrid(variableGrid < 0))];
			
			% solve ode for every entry in obj and for every initial value
			odeSolution = zeros([myGridSize, numel(obj)]);
			for it = 1 : numel(obj)
				for icIdx = 1 : numel(myParser.Results.initialValueGrid)
					resultGridPositive = [];
					odeSolutionPositive = [];
					resultGridNegative = [];
					odeSolutionNegative = [];
					tempInterpolant = obj(it).getInterpolant();
					if numel(positiveVariableGrid) > 1
						[resultGridPositive, odeSolutionPositive] = ...
							ode45(@(y, z) tempInterpolant.evaluate(z), ...
							positiveVariableGrid, ...
							myParser.Results.initialValueGrid(icIdx));
					end
					if numel(negativeVariableGrid) >1
						[resultGridNegative, odeSolutionNegative] = ...
							ode45(@(y, z) tempInterpolant.evaluate(z), ...
							negativeVariableGrid, ...
							myParser.Results.initialValueGrid(icIdx));
					end
					if any(variableGrid == 0)
						resultGrid = [flip(resultGridNegative(2:end)); 0 ; resultGridPositive(2:end)];
						odeSolution(:, icIdx, it) = [flip(odeSolutionNegative(2:end)); ...
							myParser.Results.initialValueGrid(icIdx); odeSolutionPositive(2:end)];
					else
						resultGrid = [flip(resultGridNegative(2:end)); resultGridPositive(2:end)];
						odeSolution(:, icIdx, it) = [flip(odeSolutionNegative(2:end)); ...
							odeSolutionPositive(2:end)];
					end
					assert(isequal(resultGrid(:), variableGrid(:)));
				end % for icIdx = 1 : numel(myParser.Results.initialValueGrid)
			end % for it = 1 : numel(obj)
			
			% return result as quantity-object
			solution = quantity.Discrete(...
				reshape(odeSolution, [myGridSize, size(obj)]), ...
				[quantity.Domain(myParser.Results.newGridName, variableGrid), ...
				 quantity.Domain('ic', myParser.Results.initialValueGrid)], ...
				'name', "solve(" + obj(1).name + ")");
		end % solveDVariableEqualQuantity()
		
		function solution = subs(obj, oldDomainName, varargin)
			% subs   Symbolic substitution.  Also used to evaluate expressions numerically.
			%
			% subs(obj, oldDomainName, varargin) replaces the old domains of obj specified by the
			% string-array oldDomainName with the elements of varargin. Varargin might contain:
			%
			%	a) quantity.domain-arrays, then the domains specified by oldDomainName are replaced
			%		by the new domains (both name and grid of the domain).
			%
			%	b) numeric grid-arrays, then the grid of the domains specified by oldDomainName are
			%		replaced by the new grid.
			%
			%	c) numeric scalars, then the domain of the domains specified by oldDomainName are
			%		evaluated pointwise at this numeric value. Hence, the resulting quantity of the
			%		substitution will not depend on this domains anymore.
			%
			%	d) string-arrays, then the domain of the domains specified by oldDomainName are
			%		renamed with the new names.
			
			assert(isstring(oldDomainName), "oldDomainName must be a string-array.");
			
			[newValues, oldDomainName] = quantity.Discrete.subsParser(oldDomainName, varargin);
			
			solution = copy(obj);
			if ~isempty(solution) && ~isNumber(obj)
				for it = 1 : numel(oldDomainName)
					if isstring(newValues{it})
						% if newValues{it} is a string, it is converted in a domain, to use the same
						% implementation as for quantity.Domain-objects in newValues.
						newValues{it} = solution(1).domain.find(oldDomainName(it)).rename(newValues{it}, oldDomainName(it));
					end
					
					if isnumeric(newValues{it})
						solution = solution.subsNumeric(oldDomainName(it), newValues{it});
						
					else
						solution = solution.subsDomain(oldDomainName(it), newValues{it});
						
					end % if-else
				end % for it = 1 : numel(oldDomainName)
			end % if - isempty
		end % subs()
		
		function solution = subsNumeric(obj, oldDomainName, value)
			% subsNumeric   Numeric substitution.
			%
			% subsNumeric(obj, oldDomainName, value) replaces the old domains of obj specified by the
			% string-array oldDomainName with the numeric values in value.
			%
			% Example: subs(f(z, zeta, t), ["z", "zeta"], [1, 2]) = f(1, 2, t) = g(t).
			
			arguments
				obj quantity.Discrete;
				oldDomainName (:, 1) string;
				value (:, 1) double;
			end % arguments
			
			assert(numel(oldDomainName) == numel(value), "for every domain to be substituted, ", ...
				"there must be defined one numeric scalar value");
			
			
			numberDomains(1, numel(oldDomainName)) = quantity.Domain(oldDomainName(end), value(end));
			for it = 1 : (numel(value)-1)
				numberDomains(it) = quantity.Domain(oldDomainName(it), value(it));
			end
			
			otherDomains = obj(1).domain.remove(numberDomains);
			solution = reshape(obj.on([numberDomains, otherDomains]), [otherDomains.n, size(obj)]);
			
			if ~isempty(otherDomains)
				solution = quantity.Discrete(solution, otherDomains, "name", obj(1).name);
			end
		end % subsNumeric()
		
		function obj = subsDomain(obj, oldDomainName, newDomain)
			% subsDomain   Symbolic substitution.
			%
			% subsDomain(obj, oldDomainName, newDomain) replaces the old domains of obj specified by 
			% the scalar string oldDomainName with the new domain newDomain. This implements both
			% change of the name of domains as well as change of the grid.
			%
			% Example: 
			%	z = quantity.Domain("z", linspace(0, 1, 11));
			%	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
			%	zLr = quantity.Domain("z", linspace(0, 1, 5));
			%	eta = quantity.Domain("eta", linspace(0, 1, 11));
			%	f = z.Discrete() * zeta.Discrete();
			%	f.subsDomain("zeta", eta)	% = f(z, eta)
			%	f.subsDomain("z", zLr)		% = f(z, zeta)
			%	f.subsDomain("zeta", zLr)	% = f(z) 
			%			-> the finest grid is chosen in the latter case, hence z and not zLr.
			arguments
				obj quantity.Discrete;
				oldDomainName (1, 1) string;
				newDomain (1, 1) quantity.Domain;
			end
			
			if strcmp(oldDomainName, newDomain.name) % no renaming
				obj = obj.changeDomain(newDomain);
			elseif any(strcmp([obj(1).domain.name], newDomain.name)) % merge two domains
				obj = obj.subsDomainMerge(oldDomainName, newDomain);
			else % renaming of one domain neccessary
				obj = obj.subsDomainRename(oldDomainName, newDomain);
			end
		end % subsDomain()
		
		function value = at(obj, point)
			% AT evaluates the object at a given point 
			% value = at(obj, point) evaluates the quantity at the point specified by
			%	point = [x, y, ... ]
			% where [x, y, ... ] is a row vector of the coordinates of the requested value.
			% If a list of coordinates is requested, i.e., x = col(x1, x2, ... ), y = col( y1, y2,
			% ...) ..., then a list of the corresponding values is returned.
			arguments
				obj
				point (:,:) double;
			end
			
			value = zeros([size(point,1), numel(obj)]);
			for i = 1:size(point,1)
				value(i,:) = reshape(obj.on(num2cell(point(i,:))), [1, numel(obj)]);
			end

			if size(point,1) == 1
				value = reshape(value, size(obj));
			else
				value = reshape( value, [size(point,1), size(obj)] );
			end

		end % at()
		
		function value = atIndex(obj, varargin)
			% ATINDEX returns the valueDiscrete at the requested index.
			% value = atIndex(obj, varargin) returns the
			% quantity.Discrete.valueDiscrete at the index defined by
			% varargin.
			%	value = atIndex(obj, 1) returns the first element of
			%	"valueDiscrete"
			%	value = atIndex(obj, ':') returns all elements of
			%	obj.valueDiscrete in vectorized form.
			%	value = atIndex(obj, 1, end) returns the obj.valueDiscrete
			%	at the index (1, end).
			%	If a range of index is requested, the result is returned
			%	with the grids as indizes. If scalar values are requested,
			%	than the grid dimensions are neglected.
			if nargin == 1
				
				if numel(obj(1).domain) == 1
					value = zeros(obj(1).domain.gridLength, 1);
				else
					value = zeros(obj(1).domain.gridLength);
				end
				if isempty(value)
					value = 0;
				end
			else
				if ~iscell(varargin)
					varargin = {varargin};
				end
				value = cellfun(@(v) v(varargin{:}), {obj.valueDiscrete}, ...
					'UniformOutput', false);
				
				valueSize = size(value{1});
				
				if all(cellfun(@numel, varargin) == 1) && all(cellfun(@isnumeric, varargin))
					outputSize = [];
				else
					outputSize = valueSize(1:obj(1).nargin);
				end
				
				value = reshape([value{:}], [outputSize, size(obj)]);
			end
		end % atIndex()
		
		function n = nargin(obj)
			% nargin is the number of domains defined for the object.
			n = numel(obj(1).domain);
			for it = 2 : numel(obj)
				assert(n == numel(obj(it).domain), ...
					"Not every element of this quantity has the same number of domains");
			end
		end % nargin()
		
		function H = plot(obj, optArgs)
			% plot plots a quantity.Discrete.
			%
			% H = plot(obj, optArgs) will plot the quantity.Discrete object 'obj'. The result
			% depends on the dimension and domain of 'obj'. If 'obj' is defined only on a single
			% domain, it is plotted as normal 2d plot. For each element of 'obj' a subplot is
			% created. If 'obj' is defined on two domain s, it is plotted with surf. If the
			% dimension of 'obj' is greater 2, multiple figures are created. With further name-value
			% pair inputs the behavior can be specified:
			%
			%	name2title (logical): The name of the quantity is plotted into the title of the 
			%						  figure.
			%
			%	dock (logical): The figure is docked.
			%
			%	name2axis (logical): The name of the quantity is plotted into the y-axis or z-axis
			%						 label
			%
			%	nameWithIndex (logical): The name of 'obj' is displayed with its index.
			%
			%	grabFocus (logical): If the figure already exists, it does not grab the focus when
			%	                     'obj' is plotted. This seems to be only relevant on linux OS
			%
			%	hold (logical): If 'obj' is plotted into an existing axis, the "hold on" option is
			%	                acitvated, so that multiple plots can be created in the same axis.
			%
			%	figureId:  figureId leads to the following behaviour:
			% 				<not set>   : create a new figure and plot into it
			% 			  integer > 0 : take the figure handle with figureId and plot into it
			% 						0 : take the current figure/axis and plot into it
			%	smash (logical): chose if the quantities should be plotted into subplots or all in one:
			%				smash = false [default] : plot each element of obj into a own subplot
			%				smash = true : plot all elements of obj into the specified figure/axis
			%
			%	style: The same style options as for the matlab standard plot can be used. The
			%	       matlab shortcuts can be used with the name-value pair argument "style": 
			%		   This works so far only for all elements of the quantity-array. I.e., the 
			%          style argument is applied for the plot of each element. However, if they 
			%          should be plotted in different styles a workaround is to plot each quantity 
			%          element individually.
			%
			%	export (logical): If true and matlab2tikz [1] is somewhere in your matlab path, then
			%						the plot is exported to tikz in your current directory.
			%						[1] Nico Schlömer (2021). matlab2tikz/matlab2tikz
			%						(https://github.com/matlab2tikz/matlab2tikz), GitHub. Retrieved
			%						April 30, 2021.
			%
			%	exportOptions: If export == true, then with this cell exportOptions, one can pass
			%					further inputs to matlab2tikz as name-value pairs.
			%
			%	... : All further name-value-arguments of the matlab plot function can be also
			%	passed to specify the style of the figure.
			
			arguments
				obj
				optArgs.name2title (1,1) logical = quantity.Settings.instance().plot.name2title;
				optArgs.dock (1,1) logical = quantity.Settings.instance().plot.dock;
				optArgs.name2axis (1,1) logical = quantity.Settings.instance().plot.name2axis;
				optArgs.nameWithIndex (1,1) logical = quantity.Settings.instance().plot.nameWithIndex;
				optArgs.grabFocus (1,1) logical = quantity.Settings.instance().plot.grabFocus;
				optArgs.hold (1,1) logical = false;
				optArgs.export (1,1) logical = false;
				optArgs.figureId (1,1) double {mustBeInteger};
				optArgs.smash (1,1) logical = false;
				optArgs.exportOptions (1, 1) cell;
				optArgs.style (1,1) string;
				optArgs.LineWidth;
				optArgs.Color;
				optArgs.LineStyle;
				optArgs.Marker;
				optArgs.MarkerIndices; 
				optArgs.MarkerEdgeColor; 
				optArgs.MarkerFaceColor; 
				optArgs.MarkerSize;
				optArgs.DatetimeTickFormat; 
				optArgs.DurationTickFormat;
				optArgs.log (1,1) string;
				optArgs.grid (1,1) logical = false;
			end
	
			additionalPlotOptions = misc.struct2namevaluepair(optArgs, "fieldNames", ...
				["LineWidth", "Color", "LineStyle", "Marker", "MarkerIndices", ...
				"MarkerEdgeColor", "MarkerFaceColor", "MarkerSize", "DatetimeTickFormat", ...
				"DurationTickFormat"]);
			
			if prod(obj(1).domain.gridLength) == 1 && ~isfield( optArgs, "Marker" )
				additionalPlotOptions = [additionalPlotOptions(:)', {"Marker"}, {"x"}];
			end
			
			if isfield(optArgs, "style")
				additionalPlotOptions = [{optArgs.style}, additionalPlotOptions(:)'];
			end
			
			H = [];
			for figureIdx = 1:size(obj, 3)
				if ~isfield( optArgs, "figureId")
					% create a new figure for plotting
					h = figure();
					if optArgs.dock
						set(h, 'WindowStyle', 'docked');
					else
						set(h, 'WindowStyle', 'normal');
					end
				elseif optArgs.figureId <= 0
					% use the current figure/axis 
					h = gcf;
				else
					% open the figure with the figureId
					figureID = optArgs.figureId + figureIdx - 1;
					
					if ishandle(figureID) && optArgs.grabFocus == false
						set(groot, 'CurrentFigure', figureID);
						h = gcf();
					else
						h = figure( figureID );
					end
				end
				H = [H, h];
				
				assert(~isempty(obj), 'Empty quantities can not be plotted');
				assert(obj.nargin() <= 2, 'plot only supports quantities with 2 gridNames');
				
				subplotRowIdx = 1:size(obj, 1);
				subpotColumnIdx = 1:size(obj, 2);
				
				idx = 1 : numel(obj(:,:,figureIdx));
				idx = reshape(idx, size(obj, 2), size(obj, 1))';
				
				for rowIdx = subplotRowIdx
					for columnIdx = subpotColumnIdx
						if ~optArgs.smash					
							subplot(size(obj, 1), size(obj, 2), idx(rowIdx, columnIdx));
						end
						if optArgs.hold || optArgs.smash
							hold on;
						else
							hold off;
						end
						
						if isempty(obj(rowIdx, columnIdx, figureIdx))
							warning('you are trying to plot an empty quantity');
						elseif obj.nargin() == 0
							plot(0, ...
								obj(rowIdx, columnIdx, figureIdx).valueDiscrete, ...
								additionalPlotOptions{:});
						elseif obj.nargin() == 1
							
							if isfield( optArgs, "log" )
								if optArgs.log == "x"
									semilogx(...
										obj(rowIdx, columnIdx, figureIdx).domain(1).grid(:), ...
										obj(rowIdx, columnIdx, figureIdx).valueDiscrete, ...
										additionalPlotOptions{:});
								else
									error("Unknown plot option optArgs.log = " + optArgs.log);
								end
							else
								plot(...
									obj(rowIdx, columnIdx, figureIdx).domain(1).grid(:), ...
									obj(rowIdx, columnIdx, figureIdx).valueDiscrete, ...
									additionalPlotOptions{:});
							end
							xlim(obj(rowIdx, columnIdx, figureIdx).domain(1).grid([1, end]));
						elseif obj.nargin() == 2
							misc.isurf(obj(rowIdx, columnIdx, figureIdx).domain(1).grid(:), ...
								obj(rowIdx, columnIdx, figureIdx).domain(2).grid(:), ...
								obj(rowIdx, columnIdx, figureIdx).valueDiscrete, ...
								additionalPlotOptions{:});
							xlim(obj(rowIdx, columnIdx, figureIdx).domain(1).grid([1, end]));
							ylim(obj(rowIdx, columnIdx, figureIdx).domain(2).grid([1, end]));
							ylabel(labelHelper(2), 'Interpreter','latex');
						else
							error('number inputs not supported');
						end
						xlabel(labelHelper(1), 'Interpreter','latex');
						
						if optArgs.name2title
							title(titleHelper(), 'Interpreter','latex');
						end
						
						if optArgs.name2axis
							if obj.nargin() == 1
								ylabel(titleHelper(), 'Interpreter','latex')
							else
								zlabel(titleHelper(), 'Interpreter','latex')
							end
						end
						a = gca();
						a.TickLabelInterpreter = 'latex';
						a.UserData = obj(rowIdx, columnIdx, figureIdx);
						
						if optArgs.grid
							grid on;
						end
						
						
					end % for columnIdx = subpotColumnIdx
				end % for rowIdx = subplotRowIdx
			end % for figureIdx = 1:size(obj, 3)
			
			if optArgs.export
				if exist("matlab2tikz.m", "file") == 2
					matlab2tikz(optArgs.exportOptions{:});
				else
					warning("To export figures to LaTeX, the matlab2tikz [1] script is needed. " ...
						+ "Please add it to your path. [1] Nico Schlömer (2021). "...
						+ "matlab2tikz/matlab2tikz (https://github.com/matlab2tikz/matlab2tikz), " ...
						+ "GitHub. Retrieved April 30, 2021.");
				end
			end
			
			function myLabel = labelHelper(gridNumber)
				if ~isempty(obj(rowIdx, columnIdx, figureIdx).domain)
					myLabel = "$$" + greek2tex(obj(rowIdx, columnIdx, figureIdx).domain(gridNumber).name) + "$$";
				else
					myLabel = "";
				end
			end % labelHelper()
			function myTitle = titleHelper()
				
				quantityName = greek2tex(obj(rowIdx, columnIdx, figureIdx).name);
				if strlength( quantityName ) > 20
					quantityName = "\star";
				end
				
				if numel(obj) <= 1 || ~optArgs.nameWithIndex || optArgs.smash
					% title without indices for the case:
					%	*) there is only one element
					%	*) names should be explicitly displayed without index
					%	*) all elements are plotted into the same axis
					myTitle = "$${" + quantityName + "}$$";
				elseif ndims(obj) <= 2
					myTitle = "$$[{" + quantityName ...
						+ "]}_{" + num2str(rowIdx) + num2str(columnIdx) + "}$$";
				else
					myTitle = "$${[" + quantityName ...
						+ "]}_{" + num2str(rowIdx) + num2str(columnIdx) + num2str(figureIdx) + "}$$";
				end
			end % titleHelper()
			function myText = greek2tex(myText)
				if ~contains(myText, "\")
					oldStr = ["alpha", "beta", "gamma", "delta", "epsilon", "zeta", "eta", ...
						"theta", "vartheta", "iota", "kappa", "lambda", "mu", "nu", "xi", "pi", ...
						"rho", "sigma", "varsigma", "tau", "phi", "chi",  "psi", "omega", ...
						"Gamma", "Delta", "Theta", "Lambda", "Xi", "Pi", "Sigma", "\Upsilon", ...
						"Phi", "Psi", "Omega"];
					newStr = "\" + oldStr + " ";
					oldStr = [oldStr, " ° "];
					newStr = [newStr, " \circ "];
					myText = replace(myText, oldStr, newStr);
				end
			end % greek2tex()
		end % plot()
		
		function [s] = optArgList(obj, exclude)
			% OPTARGLIST returns optional arguments of the quantity.Discrete
			%	S = optArgList(OBJ, EXCLUDE) returns the optional arguments for the OBJ as
			%	cell-array. With EXCLUDE as string-array, properties can be excluded
			%		
			assert(numel(obj) == 1, "argList must NOT be called for an array object");
			s = struct(obj);
			s = rmfield(s, "domain");
			if nargin == 2
				s = rmfield(s, exclude);
			end
			s = misc.struct2namevaluepair(s);
		end % optArgList
		
		function s = struct(obj, varargin)
			% struct reads all properties of the object and writes it in a struct.
			if (nargin == 1) && isa(obj, "quantity.Discrete")
				tempProperties = fieldnames(obj);
				si = num2cell( size(obj) );
				s(si{:}) = struct();
				for l = 1:numel(obj)
					for k = 1:length(tempProperties)
						s(l).(tempProperties{k}) = obj(1).(tempProperties{k});
					end
				end
			else
				% Without this else-case
				% this method is in conflict with the default matlab built-in
				% calls struct(). When calling struct('myFieldName', myQuantity) this
				% quantity-method is called (and errors) and not the
				% struct-calls-constructor.
				s = builtin('struct', obj, varargin{:});
			end
		end % struct()
		
		function b = flipDomain(obj, myDomainName)
			% flipDomain implements a flip of the domain, for example with a(z), z \in [0, 1]
			% b(z) = a(1-z) = a.flipDomain('z');
			% or for the multidimensional case with c(z, zeta)
			% d(z, zeta) = c(1-z, 1-zeta) = c.flipDomain(["z", "zeta"];
			% if z and zeta are domains on (0,1).
			%
			% See also quantity.Discrete.changeDomain.
			arguments
				obj
				myDomainName string
			end
			bMat = obj.on();
			
			idx = obj(1).domain.index(myDomainName);
			
			if idx == 0
				warning("conI:quantity.Discrete:flipDomain", "The object is not defined on the requested quantity %s", myDomainName );
			else
				for it = 1 : numel(myDomainName)
					bMat = flip(bMat, idx(it));
				end
			end
			
			b = quantity.Discrete(bMat, obj(1).domain, "name", "flip(" + obj(1).name + ")");
		end % flipDomain()
		
		function newObj = changeDomain(obj, domain)
			% changeDomain allows to change the grid of discretization.
			%
			%	newObj = changeDomain(obj, domain) creates newObj, which is a copy of obj, but 
			%		obj.domain that shares the same name as the input parameter domain is exchanged
			%		with the input parameter domain. This allows to change the grid of 
			%		discretization. The input domain may also be a vector.
			%
			% See also quantity.Discrete.subs, quantity.Discrete.flipDomain.
			
			% create a copy of the object, because the change of the domain does not allow to reuse
			% the handle.
			newObj = obj.copy();
			
			if isempty(obj)
				% if the object is empty, we are ready here.
				return;
			end
			
			if isNumber(obj)
				newDomain = domain;
			else
				% find the indices of domains to be replaced:
				indexNew = obj.getDomain.index([domain.name]);
				assert(numel(domain) == numel(indexNew), ...
					"Quantity:Discrete:changeDomain", ...
					"Trying to exchange the domains " + mat2str([domain.name]) + ...
					" but the considered quantity.Discrete is only defined for " ...
					+ mat2str([obj.getDomain.name]) + ".")
				% initialization of the newDomain array as quantity.Domain
				% array. This is required in order to handle also
				% quantity.EquidistantDomains:
				newDomain(1:obj(1).nargin) = quantity.Domain();
				newDomain(:) = obj.getDomain();

				newDomain(indexNew) = domain;
				assert(all(strcmp([newDomain.name], [obj.getDomain.name])), ...
					"rearranging grids failed");
			end
 					
			[newObj.domain] = deal(newDomain);
			dimDomain = numel(newDomain);
			valueDiscreteNew = permute(obj(:).on(newDomain), [dimDomain+1, 1:dimDomain, dimDomain+2]);
			gridLength = [newDomain.n, 1, 1];
			for it = 1 : numel(obj)
				newObj(it).valueDiscrete = reshape(valueDiscreteNew(it, :), gridLength);
			end
		end % changeDomain()
		
	   function newObj = replaceGrid(obj, myNewDomain, optArgs)
			   % REPALCEGRID change the grid of the quantity.
			   %       newObj = REPLACEGRID(obj, MYNEWDOMAIN, "gridName", NEWGRIDNAME)
			   % replace the grid of the obj quantity. The order of grid and
			   % gridName in the obj properties remains unchanged, only the
			   % grid points are exchanged.
			   %
			   % TODO:
			   %       newObj = REPLACEGRID(obj, domain) changes the domain of the
			   %       object specified by the name of DOMAIN into DOMAIN.
			   %
			   %       newObj = REPLACEGRID(obj, domain, 'domainName', NEWNAME) changes the domain of the
			   %       object specified by NEWNAME into DOMAIN. 

			   arguments
					   obj
					   myNewDomain quantity.Domain
					   optArgs.domainName = [myNewDomain.name];
			   end

			   if isempty(obj)
					   newObj = obj.copy();
					   return;
			   end

			   assert( intersect([obj(1).domain.name], optArgs.domainName) == optArgs.domainName );

			   if obj(1).isNumber()
					   error("Not yet implemented")
			   else
					   indexNew = obj(1).domain.index(optArgs.domainName);
					   % initialization of the newDomain array as quantity.Domain
					   % array. This is required in order to handle also
					   % quantity.EquidistantDomains:
					   newDomain(1:obj(1).nargin) = quantity.Domain();
					   newDomain(:) = obj(1).domain;

					   for it = 1 : length(indexNew)
							   newDomain(indexNew(it)) = ...
									   quantity.Domain(myNewDomain(it).name, myNewDomain(it).grid);
					   end
			   end

			   newObj = obj.copy();
			   [newObj.domain] = deal(newDomain);
	   end % replaceGrid
		
		function [F] = stateTransitionMatrix(obj, varargin)
			% STATETRANSITIONMATRIX compute the state-transition-matrix
			%	[F0] = stateTransitionMatrix(obj, varargin) computation of the
			% state-transition matrix for a ordinary differential equation
			%	d / dt x(t) = A(t) x(t)
			% with A as this object. The system matrix can be constant or
			% variable. It is the solution of
			%	d / dt F(t,t0) = A(t) F(t,t0)
			%		   F(t,t) = I.
			% Optional parameters for the computation can be defined as
			% name-value-pairs. The options are:
			%	# 'grid' : the grid for which the state-transition matrix
			%	should be computed.
			%	# 'gridName1' : the name of the first independent variable.
			%	For F(t,t0) this is t. The default is the first gridName of
			%	this object.
			%	# 'gridName2' : the name of the second independent
			%	variable. For F(t,t0) this is t0. The default is the first
			%	gridName + '0'.
			
			assert(size(obj,1) == size(obj,2), 'The quantity must be a quadratic matrix for the computation of state-transition matrices');
			assert(obj(1).nargin <= 1, 'Computation of state-transition matrix is only defined for quantities with one independent variable');
			
			ip = misc.Parser();
			ip.addParameter('grid', []);
			ip.addParameter('gridName1', '');
			ip.addParameter('gridName2', '');
			ip.parse(varargin{:});
			myGrid = ip.Results.grid;
			gridName1 = ip.Results.gridName1;
			gridName2 = ip.Results.gridName2;
			
			if ip.isDefault('grid')
				assert(obj(1).nargin == 1, 'No grid is defined for the computation of the state-transition matrix. Use the name-value pair option "grid" to define the grid.');
				myGrid = obj(1).domain(1).grid;
			end
			
			if ip.isDefault('gridName1')
				assert(obj(1).nargin == 1, 'No gridName is defined. Use name-value-pairs property "gridName1" to define a grid name');
				gridName1 = [obj(1).domain.name];
			end
			
			if ip.isDefault('gridName2')
				gridName2 = [char(gridName1) '0'];
			end
			
			
			assert(numel(myGrid) > 1, 'If the state transition matrix is computed for constant values, a spatial domain has to be defined!')
			
			myDomain = [quantity.Domain(gridName1, myGrid), ...
				quantity.Domain(gridName2, myGrid)];
			
			if isNumber(obj)
				% for a constant system matrix, the matrix exponential
				% function can be used.
				z = sym(gridName1, 'real');
				zeta = sym(gridName2, 'real');
				f0 = expm(obj.atIndex(1)*(z - zeta));
				F = quantity.Symbolic(f0, myDomain);
				
			elseif isa(obj, 'quantity.Symbolic')
				f0 = stateSpace.transitionMatrix.odeSolver_par(obj.function_handle, myGrid);
				F = quantity.Discrete(f0, myDomain);
			else
				f0 = stateSpace.transitionMatrix.odeSolver_par( ...
					obj.on(myGrid), ...
					myGrid );
				F = quantity.Discrete(f0, myDomain);
			end
		end % stateTransitionMatrix
		
		function s = sum(obj, dim)
			% sum Sum of elements
			% s = sum(X) is the sum of all elements of the array X.
			% s = sum(X, DIM) is the sum along the dimensions specified by DIM.
			arguments
				obj;
				dim = 1:ndims(obj);
			end % arguments
			s = quantity.Discrete(sum(obj.on(), obj.nargin + dim), obj(1).domain, ...
				'name', "sum(" + obj(1).name + ")");
		end % sum()
		
		function P = prod(obj, dim)
			% prod Product of elements
			% P = prod(X) is the product of all elements of the vector X.
			% P = prod(X, DIM) is the product along the dimensions specified by DIM.
			arguments
				obj;
				dim = 1:ndims(obj);
			end % arguments
			P = quantity.Discrete(prod(obj.on(), obj(1).nargin + dim), obj(1).domain, ...
				'name', "prod(" + obj(1).name + ")");
		end % prod()
		
		function y = sqrt(x)
			% quadratic root for scalar and diagonal quantities
			y = quantity.Discrete(sqrt(x.on()), x(1).domain, ...
                    'name', "sqrt(" + x(1).name + ")");
		end % sqrt()
		
		
		function y = sqrtm(x)
			% quadratic root for matrices
			if isscalar(x)
				% use sqrt(), because its faster.
				y = sqrt(x);
			elseif (size(x, 1) == size(x, 2)) && ismatrix(x)
				% implementation of quadratic root pointwise in space in a
				% simple for-loop.
				xMat = x.on();
				permuteGridAndIdx = [[-1, 0] + ndims(xMat), 1:x.nargin];
				permuteBack = [(1:x.nargin)+2, [1, 2]];
				xPermuted = permute(xMat, permuteGridAndIdx);
				yUnmuted = 0*xPermuted;
				for k = 1 : prod(x(1).domain.gridLength)
					yUnmuted(:,:,k) = sqrt(xPermuted(:,:,k));
				end
				y = quantity.Discrete(permute(yUnmuted, permuteBack), x(1).domain, ...
					"name", "sqrtm(" + x(1).name + ")");
			else
				error('sqrtm() is only implemented for quadratic matrices');
			end
		end % sqrtm()
		
		function P = power(obj, p)
			% a.^p elementwise power
			P = quantity.Discrete(obj.on().^(p), obj(1).domain, ...
				"name", obj(1).name + ".^{" + num2str(p) + "}");
		end % power()
		
		function P = mpower(a, p)
			% Matrix power a^p is matrix or scalar a to the power p.
			if p == 0
				P = setName(eye(size(a)) + 0*a, "I");
			elseif p < 0
				if numel(a) > 1
					warning("mpower(a, p) implements  a^p. " ...
						+ "For matrices a with negative exponent p, inv(a^(-p)) is returned. " ...
						+ "This represents a division from left, " ...
						+ "maybe division from right is needed in your case!")
				end % this warning is not important in the scalar case.
				P = inv(mpower(a, -p));
			else % p > 0
				assert(p==floor(p), "power p must be integer");
				P = a;
				for k = 1:(p-1)
					P = P * a;
				end
			end
		end % mpower()
		
		function s = num2str(obj)
			s = obj.name;
		end % num2str()
		
		function P = times(a, b)
			%.*  Array multiply (times)
			% a.*b denotes element-by-element multiplication. a and b must have compatible sizes. 
			assert( all( size(a) == size(b), "all" ), "the a and b must have the same size");
			
			if isnumeric(b)
				P = a.copy();
			else
				P = b.copy();
			end
			
			for it = 1 : numel(a)
				P(it) = a(it) * b(it);
			end
			
			P = reshape(P, size(a));
		end % times()
		
		function P = mtimes(a, b)
			%  *   Matrix multiply (mtimes).
			%     X*Y is the matrix product of X and Y.  Any scalar (a 1-by-1 matrix)
			%     may multiply anything.  Otherwise, the number of columns of X must
			%     equal the number of rows of Y.
			arguments
				a {mustBe.matrix(a)};
				b {mustBe.matrix(b)};
			end
			
			% numeric computation of the matrix product
			if isempty(b) || isempty(a)
				sa = size(a);
				sb = size(b);
				
				if all( sa == 1)
					% a is a scalar
					P = quantity.Discrete.empty( sb );
				elseif all( sb == 1)
					% b is a scalar
					P = quantity.Discrete.empty( sa );
				elseif any( sa(1:end-1) == 0 ) || any( sb(2:end) == 0 )
					% a or b have an empty dimension:
					P = quantity.Discrete.empty([sa(1:end-1) sb(2:end)]);
				else
					error("quantity:Discrete:mTimes", ...
						"Error: quantities with dimensions (n, 0) * (0, m) cannot be multiplied");
				end
				
				return
			end
			if isa(b, "double")
				if numel(b) == 1
					% simple multiplication in scalar case
					P = quantity.Discrete(a.on() * b, a(1).domain, "name", a(1).name + num2str(b));
					return
				else
					b = quantity.Discrete(b, quantity.Domain.empty());
				end
			end
			
			if isa(a, "double")
				P = (b.' * a.').';
				% this recursion is safe, because isa(b, 'double') is considered in
				% the if above.
				
				if isnumeric(P)
					return
				else
					P.setName("c " + b(1).name);
					return
				end
			end
			if a.isNumber() && ~b.isNumber()
				% If the first argument a is constant in the sense, that it is defined on an empty
				% domain, then bad things will happen. To avoid this, we calculate
				%	a * b = (b' * a')'
				% instead. Thus we have to exchange both tranposed values transpose the result in
				% the end.
				P = (b' * a')';
				P.setName(a(1).name + " " + b(1).name);
				return
			elseif a.isNumber() && b.isNumber()
				P = a.on() * b.on();
				return 
			end
			if isscalar(b)
				% do the scalar multiplication in a loop
				P(numel(a), 1) = innerMTimes(a(end), b);
				for k = 1 : (numel(a)-1)
					P(k) = innerMTimes(a(k), b);
				end
				P = reshape(P, size(a));
				return
			end
			
			if isscalar(a)
				% do the scalar multiplication in a loop
				P(numel(b), 1) = innerMTimes(a, b(end));
				for k = 1 : (numel(b)-1)
					P(k) = innerMTimes(a, b(k));
				end
				P = reshape(P, size(b));
				return
			end
			
			P = innerMTimes(a, b);
		end % mtimes()
		
		function P = innerMTimes(a, b)
			% innerMTimes is a helper method for mtimes.
			assert(size(a, 2) == size(b, 1), ['For multiplication the ', ...
				'number of columns of the left array', ...
				'must be equal to number of rows of right array'])
			
			% misc.multArray is very efficient, but requires that the
			% multiple dimensions of the input are correctly arranged.
			[idx, permuteGrid] = computePermutationVectors(a, b);
			
			domainA = a(1).domain;
			domainB = b(1).domain;
			
			% The joined domain of quantity A and B is a mixture from both
			% domains. If the domains have the same name, it must be
			% checked if they have the same discretization. This is done by
			% the function "join". It returns the finer grid.
			% If the domain names do not coincide, they are just
			% appended. At first, the domains of qunatity A, then the
			% domains of quantity B.
			joinedDomain = [ join(domainA(idx.A.common), domainB(idx.B.common)), ...
				domainA(~idx.A.common), domainB(~idx.B.common) ];
			if a(1).nargin == 0
				valueA = a.on();
			else
				valueA = a.on(joinedDomain(sort(joinedDomain.index([domainA.name]))));
			end
			if b(1).nargin == 0
				valueB = b.on();
			else
				valueB = b.on(joinedDomain(sort(joinedDomain.index([domainB.name]))));
			end
			
			% do the multidimensional tensor multiplication and permute the
			% values to the right order 
			C = misc.multArray(valueA, valueB, idx.A.value(end), idx.B.value(1), idx.common);
			C = permute(C, permuteGrid);
			P = quantity.Discrete(C, joinedDomain, "name", a(1).name + " " + b(1).name);
		end % innerMTimes()
		
		function y = inv(obj)
			% inv inverts the matrix obj pointwise over the domain.
			assert(ismatrix(obj) && (size(obj, 1) == size(obj, 2)), ...
				"obj to be inverted must be quadratic");
			objDiscreteOriginal = obj.on();
			if isscalar(obj)
				% use ./ for scalar case
				y = quantity.Discrete(1 ./ objDiscreteOriginal, obj(1).domain, ...
					"name", "(" + obj(1).name + ")^{-1}");
			else
				% reshape and permute objDiscrete such that only one for-loop is needed.
				objDiscreteReshaped = permute(reshape(objDiscreteOriginal, ...
					[prod(obj(1).domain.gridLength), size(obj)]), [2, 3, 1]);
				invDiscrete = zeros([prod(obj(1).domain.gridLength), size(obj)]);
				
				for it = 1 : size(invDiscrete, 1)
					invDiscrete(it, :, :) = inv(objDiscreteReshaped(:, :, it));
				end
				
				y = quantity.Discrete(reshape(invDiscrete, size(objDiscreteOriginal)), ...
                    obj(1).domain, "name", "{(" + obj(1).name + ")}^{-1}");
			end
		end % inv()

		function y = pinv(obj)
			% pinv calculates the pseudo invserse of the matrix obj pointwise over the domain.
			objDiscreteOriginal = obj.on();
			% reshape and permute objDiscrete such that only one for-loop is needed.
			objDiscreteReshaped = permute(reshape(objDiscreteOriginal, ...
				[prod(obj(1).domain.gridLength), size(obj)]), [2, 3, 1]);
			invDiscrete = zeros([prod(obj(1).domain.gridLength), size(obj)]);
			
			for it = 1 : size(invDiscrete, 1)
				invDiscrete(it, :, :) = pinv(objDiscreteReshaped(:, :, it));
            end
			
            sizeInverse = size(on(obj.'));
			y = quantity.Discrete(reshape(invDiscrete, sizeInverse), ...
                obj(1).domain, "name", "{(" + obj(1).name + ")}^{-1}");
		end % inv()
		
		function objT = transpose(obj)
			%  .' Transpose.
			%     X.' is the non-conjugate transpose.
			%  
			%     B = transpose(A) is called for the syntax A.' when A is an object.
			objT = builtin('transpose', copy(obj));
			if ~isempty(obj)
				objT.setName("{" + obj(1).name + "}^{T}");
			else
				objT.setName("{.}^{T}");
			end
		end % transpose(obj)
		
		function objCt = ctranspose(obj)
			% '   Complex conjugate transpose.   
			%      X' is the complex conjugate transpose of X. 
			%  
			%      B = ctranspose(A) is called for the syntax A' (complex conjugate
			%      transpose) when A is an object.
			objT = obj.';
			
			if ~isempty(objT)
			
				objCtMat = conj(objT.on());
				objCt = quantity.Discrete(objCtMat, obj(1).domain, ...
					"name", "{" + obj(1).name + "}^{H}");
			else
				objCt = objT;
			end
		end % ctranspose(obj)
		
		function y = exp(obj)
			% exp is the exponential function using obj as the exponent.
			%
			% See also quantity.Discrete.expm
			
			y = quantity.Discrete(exp(obj.on()), obj(1).domain, ...
				'name', "exp(" + obj(1).name + ")");
		end % exp()
		
		function y = expm(x)
			% expm() is the matrix-exponential function using obj as the exponent.
			%
			% See also quantity.Discrete.exp
			
			if isscalar(x)
				% use exp(), because its faster.
				y = exp(x);
			elseif ismatrix(x)
				% implementation of expm pointwise in space in a simple
				% for-loop.
				xMat = x.on();
				permuteGridAndIdx = [[-1, 0] + ndims(xMat), 1:x.nargin];
				permuteBack = [(1:x.nargin)+2, [1, 2]];
				xPermuted = permute(xMat, permuteGridAndIdx);
				yUnmuted = 0*xPermuted;
				for k = 1 : prod([x(1).domain.n])
					yUnmuted(:,:,k) = expm(xPermuted(:,:,k));
				end
				y = quantity.Discrete(permute(yUnmuted, permuteBack), x(1).domain, ...
					"name", "expm(" + x(1).name + ")");
			end
		end % expm()
		
		function y = sin(obj)
			% sin    Sine of argument in radians.
			%	sin(X) is the sine of the elements of X.
			%
			% See also quantity.Discrete.cos
			
			y = quantity.Discrete(sin(obj.on()), obj(1).domain, ...
				"name", "sin(" + obj(1).name + ")");
		end % sin()
		
		function y = cos(obj)
			% cos    Cosine of argument in radians.
			%	cos(X) is the cosine of the elements of X.
			%
			% See also quantity.Discrete.sin
			
			y = quantity.Discrete(cos(obj.on()), obj(1).domain, ...
				"name", "cos(" + obj(1).name + ")");
		end % cos()
		
		function [V, D, W] = eig(obj)
			% eig    Eigenvalues and eigenvectors pointwise over the domain
			%	E = eig(A) produces a column vector E containing the eigenvalues of 
			%	a square matrix A.
			%
			%	[V,D] = eig(A) produces a diagonal matrix D of eigenvalues and 
			%	a full matrix V whose columns are the corresponding eigenvectors  
			%	so that A*V = V*D.
			%
			%	[V,D,W] = eig(A) also produces a full matrix W whose columns are the
			%	corresponding left eigenvectors so that W'*A = D*W'.
			assert(numel(obj(1).domain)==1, "quantity.Discrete/eig is only implemented for one domain");
			assert((ndims(obj) <= 2) && (size(obj, 1) == size(obj, 2)), ...
				"quantity.Discrete/eig is only implemented for quadratic quantities");
			
			objDisc = permute(obj.on(), [2, 3, 1]);
			if nargout == 1
				% E = eig(A)
				VDisc = zeros([obj(1).domain.n, size(obj, 1)]);
				for it = 1 : obj(1).domain.n
					VDisc(it, :) = eig(objDisc(:, :, it));
				end % for it = 1 : obj(1).domain.n
				V = quantity.Discrete(VDisc, obj(1).domain, "name", "V");
			else
				% [V,D] = eig(A) or [V,D,W] = eig(A)
				VDisc = zeros([obj(1).domain.n, size(obj)]);
				DDisc = zeros([obj(1).domain.n, size(obj)]);
				WDisc = zeros([obj(1).domain.n, size(obj)]);
				VDiscOld = zeros(size(obj));
				WDiscOld = zeros(size(obj));
				for zt = 1 : obj(1).domain.n
					[VDiscTemp, DDisc(zt, :, :), WDiscTemp] = eig(objDisc(:, :, zt));
					if zt > 1
						VDiscOld(:, :) = VDisc(zt-1, :, :);
						WDiscOld(:, :) = WDisc(zt-1, :, :);
						for jt = 1 : size(obj, 1)
							% as the builtin eig() scales the eigenvectors, as slight change of the
							% values can cause a change of the sign of the eigenvector. As eig is
							% called pointwise, often a non-smooth eigenvector-function results.
							% Due to this, every eigenvector is compared with the previous one and
							% if there it makes the result more smooth, the sign is changed.
							if norm(VDiscTemp(:, jt) - VDiscOld(:, jt)) ...
									> norm(VDiscTemp(:, jt) + VDiscOld(:, jt))
								VDiscTemp(:, jt) = -VDiscTemp(:, jt);
							end
							if norm(WDiscTemp(:, jt) - WDiscOld(:, jt)) ...
									> norm(WDiscTemp(:, jt) + WDiscOld(:, jt))
								WDiscTemp(:, jt) = -WDiscTemp(:, jt);
							end
						end % for jt = 1 : size(obj, 1)
					end
					VDisc(zt, :, :) = VDiscTemp;
					WDisc(zt, :, :) = WDiscTemp;
				end % for it = 1 : obj(1).domain.n
				D = quantity.Discrete(DDisc, obj(1).domain, "name", "D");
				W = quantity.Discrete(WDisc, obj(1).domain, "name", "W");
				V = quantity.Discrete(VDisc, obj(1).domain, "name", "V");
			end
		end % eig()
		
		function [l, u] = luDecomposition(obj)
			% luDecomposition split matrix OBJ into lower triangular matrix l and upper triangular
			% matrix u, such that l * u = obj.
			n = size(obj, 1);
			assert(numel(obj) == n*n, "obj must be a quadratic matrix");
			u = obj;
			l = obj.eye([n, n], obj(1).domain);

			% simple algorithm from wikipedia:
			for it = 1 : n-1
				for k = it+1 : n
					l(k, it) = u(k, it) / u(it, it);
					for jt = it : n
						u(k, jt) = u(k, jt) - l(k, it) * u(it, jt);
					end % for jt
				end % for k
			end % for it
		end % luDecomposition()
		
		function [V, J, l] = jordanReal(obj)
			% jordanReal Real Jordan form of the matrix OBJ pointwise over the (scalar) domain.
			% [V, J] = jordanReal(OBJ) computes the transformation matrix V and the jordan
			% block matrix J, so that OBJ*V = V*J. The columns of OBJ are the generalised 
			% eigenvectors.
			%
			% [V, J, l] = jordanReal(OBJ) also returns the lengths of the jordan
			% blocks. l is a vector containing the lengths. So length(l) is the
			% number of jordan blocks.
			assert(numel(obj(1).domain)==1, "quantity.Discrete/jordanReal is only implemented for one domain");
			assert((ndims(obj) <= 2) && (size(obj, 1) == size(obj, 2)), ...
				"quantity.Discrete/jordanReal is only implemented for quadratic quantities");
			
			objDisc = permute(obj.on(), [2, 3, 1]);
			
			VDisc = zeros([obj(1).domain.n, size(obj)]);
			JDisc = zeros([obj(1).domain.n, size(obj)]);
			lDisc = zeros([obj(1).domain.n, size(obj, 1)]);
			for it = 1 : obj(1).domain.n
				[VDisc(it, :, :), JDisc(it, :, :), lTemp] = ...
					misc.jordanReal(objDisc(:, :, it));
				lDisc(it, 1:numel(lTemp)) = lTemp;
			end % for it = 1 : obj(1).domain.n
			V = quantity.Discrete(VDisc, obj(1).domain, "name", "V");
			J = quantity.Discrete(JDisc, obj(1).domain, "name", "J");
			l = quantity.Discrete(lDisc, obj(1).domain, "name", "l");
		end % jordanReal()
		
		function y = log(obj)
		%  log    Natural logarithm.
		%     log(X) is the natural logarithm of the elements of X.
		%     Complex results are produced if X is not positive.
			y = quantity.Discrete(log(obj.on()), obj(1).domain, ...
				'name', "log(" + obj(1).name + ")");
		end % log()
		
		function y = log10(obj)
		% log10  Common (base 10) logarithm.
		% log10(X) is the base 10 logarithm of the elements of X.   
		% Complex results are produced if X is not positive.
			y = quantity.Discrete(log10(obj.on()), obj(1).domain, ...
				'name', "log10(" + obj(1).name + ")");
		end % log10()
		
		function xNorm = l2norm(obj, integrationDomain, optArg)
			% l2norm calculates the l2 norm
			%	xNorm = sqrt(int(x.' * weight * x ddomain)). The integral is defined from the upper
			%	and lower bound of the respective domain.
			%
			%	xNorm = l2norm(x) returns sqrt(int x.' * x ddomain), while the integral is
			%	performed over every domain
			%
			%	xNorm = l2norm(x, integrationDomain) returns sqrt(int x.' * x ddomain), while the 
			%	integral is performed over every domain specified by the string-array
			%	integrationDomain. Default value is [obj(1).domain.name]
			%	
			%	l2norm(..., "weight", weight) considers the weight according to
			%	xNorm = sqrt(int x.' * weight * x ddomain). Default value is the identity matrix.
			%
			%	l2norm(...., "elementWise", true) returns the l2norm computed element wise.
			arguments
				obj;
				integrationDomain string = [obj.getDomain.name];
				optArg.weight (:, :) double = eye(size(obj, 1));
				optArg.elementWise (1,1) logical = false;
			end
			
			if optArg.elementWise
				for k = 1:numel(obj)
					xNorm(k) = obj(k).l2norm(integrationDomain);
				end
				xNorm = reshape(xNorm, size(obj));
			else
				xNorm = sqrtm( int(obj.' * optArg.weight * obj, integrationDomain) );
				if isa(xNorm, 'quantity.Discrete')
					xNorm = xNorm.setName("||" + obj(1).name + "||_{L2}");
				end
			end
		end % l2norm()
		
		function xNorm = norm(obj, p)
			% norm implements the vector norm, similar to builtin matlab function norm
			% norm(V,P) returns the p-norm of V defined as SUM(ABS(V).^P)^(1/P).
			%
			% norm(X) is the same as norm(X,2). (euclidian norm)
			
			arguments
				obj;
				p (1, 1) double = 2;
			end
			
			if p == 2
				xNorm = sqrt(sum(abs(obj).^p));
			else
				xNorm = sum(abs(obj).^p).^(1/p);
			end
		end % norm(obj, p)
		
		function xNorm = quadraticNorm(obj, varargin)
			% calculates the quadratic norm, i.e,
			%	xNorm = sqrt(x.' * x).
			% Optionally, a weight can be defined, such that instead
			%	xNorm = sqrt(x.' * weight * x).
			
			myParser = misc.Parser();
			myParser.addParameter('weight', eye(size(obj, 1)));
			myParser.parse(varargin{:});
			xNorm = sqrtm(obj.' * myParser.Results.weight * obj);
			xNorm.setName("||" + obj(1).name + "||_{2}");
		end % quadraticNorm()

		function xNorm = maxNorm(obj, domain)
			% maxNorm computes the maximum norm of the quantity object.
			% That is, it computes the maximum over all dimensions of the
			% object and the specified domains.
			%	xNorm = maxNorm(obj)
			%	computes the maximum over all dimensions
			%
			%	xNorm = maxNorm(obj, "z")	or	xNorm = maxNorm(obj, zDomain)
			%	computes the maximum over the domain named "z" and all
			%	dimensions
			arguments
				obj;
				domain = [];
			end
			xDiscAbs = abs(obj.on());
			xDomain = obj.getDomain();
			if isempty(domain)
				xNorm = quantity.Discrete(max(xDiscAbs,[], numel(xDomain)+1:numel(xDomain)+length(size(obj)) ), xDomain);
			else
				if isa(domain, "quantity.Domain")
					domainName = [domain.name];
				elseif isa(domain, "string")
					domainName = domain;
				else
					error("domain must be of type string of quantity.Domain");
				end
				domainHitVec = zeros(size([xDomain.name]));
				for idx = 1:length(domainName)
					domainHitVec = domainHitVec | strcmp([xDomain.name], domainName(idx));
				end
				domainVec = find(domainHitVec);
				antiDomainVec = find(~domainHitVec);
				domainVec = cat(2, domainVec, numel(obj.getDomain)+1:numel(obj.getDomain)+length(size(obj)) );
				xNorm = quantity.Discrete(permute(max(xDiscAbs,[], domainVec), ...
					[antiDomainVec, domainVec]),...
					xDomain(antiDomainVec));
			end
		end % maxNorm
		
		function x = mldivide(A, B)
			% mldivide x = A\B is the solution x to the equation Ax = B. Matrices A and B must have
			% the same number of rows. This method uses the method inv.
			x = inv(A) * B;
		end % mldivide()
		
		function x = mrdivide(B, A)
			% mRdivide x = A/B is the solution x to the equation x A = B. Matrices A and B must have
			% the same number of columns. This method uses the method inv.
			x = B * inv(A);
		end % mrdivide()
		
		function x = rdivide(A, B)
			%  ./  Right array divide.
			%     A./B divides each element of A by the corresponding element of B. A and 
			%     B must have same size or one might be a scalar.
			
			xSize = max(size(B), size(A));
			if isnumeric(A)
				xDomain = B(1).domain;
				xName = "c ./ " + B(1).name;
				if isscalar(A)
					xData = A ./ B.on();
				elseif isscalar(B)
					xData = zeros([numel(A), B(1).domain.n]);
					for it = 1 : numel(A)
						xData(it, :) =  A(it) ./ B.on();
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				else
					assert(isequal(size(A), size(B)), ...
						"inputs must have same size or one must be scalar");
					xData = zeros([numel(B), B(1).domain.n]);
					for it = 1 : numel(A)
						xData(it, :) =  A(it) ./ B(it).on();
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				end
				x = reshape(quantity.Discrete(xData, xDomain, "name", xName), xSize);
				
			elseif isnumeric(B)
				xDomain = A(1).domain;
				xName = A(1).name + " ./ c";
				if isscalar(B)
					xData = A.on() ./ B;
				elseif isscalar(A)
					xData = zeros([numel(B), A(1).domain.n]);
					for it = 1 : numel(B)
						xData(it, :) =  A.on() ./ B(it);
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				else
					assert(isequal(size(A), size(B)), ...
						"inputs must have same size or one must be scalar");
					xData = zeros([numel(A), A(1).domain.n]);
					for it = 1 : numel(A)
						xData(it, :) =  A(it).on() ./ B(it);
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				end
				x = reshape(quantity.Discrete(xData, xDomain, "name", xName), xSize);
				
			else
				if isscalar(A)
					x = copy(B);
					for it = 1 : numel(x)
						x(it) = A / B(it);
					end
				elseif isscalar(B)
					x = copy(A);
					for it = 1 : numel(x)
						x(it) = A(it) / B;
					end
				else
					assert(isequal(size(A), size(B)), ...
						"inputs must have same size or one must be scalar")
					x = copy(A);
					for it = 1 : numel(A)
						x(it) = A(it) / B(it);
					end
				end
				x.setName(A(1).name + " ./ " + B(1).name);
			end
		end % rdivide()
		
		function x = ldivide(B, A)
			%  .\  Left array divide.
			%     B.\A divides each element of A by the corresponding element of B. A and 
			%     B must have same size or one might be a scalar.
			
			xSize = max(size(B), size(A));
			if isnumeric(A)
				xDomain = B(1).domain;
				xName = B(1).name + " .\ c";
				if isscalar(A)
					xData = B.on() .\ A;
				elseif isscalar(B)
					xData = zeros([numel(A), B(1).domain.n]);
					for it = 1 : numel(A)
						xData(it, :) =  B.on() .\ A(it);
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				else
					assert(isequal(size(A), size(B)), ...
						"inputs must have same size or one must be scalar");
					xData = zeros([numel(B), B(1).domain.n]);
					for it = 1 : numel(A)
						xData(it, :) =  B(it).on() .\ A(it);
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				end
				x = reshape(quantity.Discrete(xData, xDomain, "name", xName), xSize);
				
			elseif isnumeric(B)
				xDomain = A(1).domain;
				xName = "c .\ " + A(1).name;
				if isscalar(B)
					xData = B .\ A.on();
				elseif isscalar(A)
					xData = zeros([numel(B), A(1).domain.n]);
					for it = 1 : numel(B)
						xData(it, :) =  B(it).\ A.on();
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				else
					assert(isequal(size(A), size(B)), ...
						"inputs must have same size or one must be scalar");
					xData = zeros([numel(A), A(1).domain.n]);
					for it = 1 : numel(A)
						xData(it, :) =  B(it).\ A(it).on();
					end
					xData = permute(xData, [1+(1:numel(xDomain)), 1]);
				end
				x = reshape(quantity.Discrete(xData, xDomain, "name", xName), xSize);
			else
				if isscalar(A)
					x = copy(B);
					for it = 1 : numel(x)
						x(it) = A \ B(it);
					end
				elseif isscalar(B)
					x = copy(A);
					for it = 1 : numel(x)
						x(it) = A(it) \ B;
					end
				else
					assert(isequal(size(A), size(B)), ...
						"inputs must have same size or one must be scalar")
					x = copy(A);
					for it = 1 : numel(A)
						x(it) = B(it) \ A(it);
					end
				end
				x.setName(B(1).name + " .\ " + A(1).name);
			end
		end % ldivide()
		
		function empty = isempty(obj)
			% ISEMPTY checks if the quantity object is empty
			%   empty = isempty(obj)
			
			% Check if there is any dimension which is zero
			empty = any(size(obj) == 0);
			
			% If the constructor is called without any arguments, an object
			% is still created ( this is required to allow object-arrays),
			% but the object should be recognized as an empty object. Thus,
			% we can test if the domain has been set with a domain object.
			% This happens only in the part of the constructor which is
			% entered if a valid quantity is initialized.
			empty = empty || ~isa(obj(1).domain, 'quantity.Domain');
			
		end % isempty()
		
		function result = isdiag(obj, tolerance)
			% ISDIAG returns true if all non-diagonal elements of the quantity array
			% are zero, i.e. it is a diagonal array. Otherwise, returns false.
			% 
			%	result = isdiag(obj) returns true if all off-diagonal elements are zero.
			%
			%	result = isdiag(obj, tolerance) returns true if all off-diagonal elements are
			%		absolute less than the double value specified by tolerance.
			%		Default: tolerance = 10*eps
			
			arguments
				obj quantity.Discrete;
				tolerance (1, 1) double = 10*eps;
			end
			selector = ~logical(eye(size(obj)));
			if (numel(selector) == 1) ||(MAX(abs(obj(selector))) <= tolerance)
				result = true;
			else
				result = false;
			end
		end % isdiag()
		
		function result = iseye(obj)
			% iseye returns true if obj is an quadratic identity matrix.
			result = isdiag(obj) && ismatrix(obj) ...	% diagonal matrix
				&& (size(obj, 1) == size(obj, 2)) ...			% quadratic
				&& (MAX(abs(obj - eye(size(obj)))) < 10*eps);	% diag(matrix) == 1
		end % iseye()
		
		function result = isnan(obj, anyNan)
			%  isnan  True for Not-a-Number.
			%     isnan(X) returns an array that contains true's where
			%     the elements of X are NaN's and false's where they are not.
			%
			%     isnan(X, true) returns the same as any(isnan(X), 'all')
			
			arguments
				obj
				anyNan = false;
			end
			
			if anyNan
				result = any(isnan(obj.on()), 'all');
			else
				result = true(size(obj));
				for it = 1 : numel(obj)
					result(it) = any(isnan(obj(it).on()), 'all');
				end
			end	
		end % isnan()
		
		function result = iszero(obj, allZero)
			%  iszero  True for obj == 0.
			%     isnan(X) returns an array that contains true's where
			%     the elements of X == 0 and false's where they are not.
			%
			%     iszero(X, true) returns the same as all(iszero(X), 'all')
			
			arguments
				obj
				allZero = false;
			end
			
			if allZero
				result = all(obj.on() == 0, 'all');
			else
				result = reshape(all(obj.on() == 0, 1:1:obj(1).nargin), size(obj));
			end	
		end % iszero()
		
		function myGrid = gridOf(obj, myDomainName)
			% gridOf returns the grid corresponding to the domain named myDomainName
			if ~iscell(myDomainName)
				gridIdx = obj(1).domain.index(myDomainName);
				if gridIdx > 0
					myGrid = obj(1).domain(gridIdx).grid;
				else
					myGrid = [];
				end
			else
				myGrid = cell(size(myDomainName));
				for it = 1 : numel(myGrid)
					gridIdx = obj(1).domain.index(myDomainName{it});
					if gridIdx > 0
						myGrid{it} = obj(1).domain(gridIdx).grid;
					else
						myGrid{it} = [];
					end
				end
			end
		end % gridOf()
		
		function result = diff(obj, domain, k)
			% diff computation of the derivative
			%
			%	result = diff(obj) calculates the first order derivative w.r.t. all domains of obj.
			%
			%	result = diff(obj, domain) calculates the first order derivative w.r.t. the domains
			%		specified by domain. The input domain can be an array of strings or of
			%		quantity.Domain.
			%
			%	result = diff(obj, domain, k) calculates the k-th derivative w.r.t. the 
			%		specified domain.
			%
			% See also quantity.Discrete.int.
			arguments
				obj;
				domain (1,1) string = [obj(1).domain.name];
				k uint64 = 1;
			end
			
			if obj.isNumber()
				result = quantity.Discrete(zeros(size(obj)), obj(1).domain, ...
					"name", "(d_{.}" + obj(1).name + ")");
				return
			end
			
			if k == 0
				result = obj.copy();
			elseif k > 0
				result = obj.diff_inner(k, domain);
			else
				error("only defined for derivatives of order k = 0, 1, 2, ...");
			end
		end % diff()
		
		
		function Xdiff = diffFd(X, domain, k, scheme)
			% diff computation of the derivative using finite differences method (FD). Yet only
			% implemented for quantities defined on 1 domain.
			%
			%	result = diffFd(obj) calculates the first order derivative w.r.t. the domain of obj.
			%
			%	result = diffFd(obj, domain) calculates the first order derivative w.r.t. the 
			%		domains specified by domain. The input domain can be an array of strings or of
			%		quantity.Domain.
			%
			%	result = diffFd(obj, domain, k) calculates the k-th derivative w.r.t. the 
			%		specified domain. Currently, only k = 1, 2 is supported. Default: k = 1.
			%
			%	result = diffFd(obj, domain, k, scheme) the string scheme allows to define the
			%		finite differences scheme used for obtaining the differentation matrix. There
			%		are "5pointCentral" (default) and "3pointCentral" available.
			%
			% See also quantity.Discrete.diff, numeric.fd.matrix.
			arguments
				X quantity.Discrete;
				domain string = X(1).domain(1).name;
				k double {mustBeInteger} = 1;
				scheme = "5pointCentral";
			end

			assert(numel(X(1).domain) == 1, "yet only implemented for quantites defined on 1 domain");
			assert(numel(domain) == 1, "yet only implemented for 1 domain");
			domain = X(1).domain.find(domain);
			if isempty(domain)
				% quantity is not defined on the domain -> result is 0
				Xdiff = 0 * X;
				return;
			end
			
			diffMat = numeric.fd.matrix(scheme, domain.grid, k);
			Xdomain  = X.on();
			XdiffDomain = zeros(size(Xdomain));
			for it = 1 : numel(X)
				XdiffDomain(:, it) = diffMat * Xdomain(:, it);
			end % for it = 1 : numel(X)

			Xdiff = quantity.Discrete(XdiffDomain, domain, ...
				"name", "d_{" + domain.name + "}" + X(1).name);
		end % diffFd()
		
        function F = int(obj, domainName, lowerBound, upperBound)
			% int trapezoidal integration
			%	
			%	result = int(obj) integrates obj all its domains from its lower bound to the upper
			%		bound.
			%	
			%	result = int(obj, domainName) integrates obj over the domain from its lower bound to 
			%		the upper bound. To integrate over multiple domains, specify domain as a
			%		string-array.
			%	
			%	result = int(obj, domainName, lowerBound, upperBound) integrates obj over 
			%		the integration domain from the lowerBound to the upperBound.
			%		- domain must be a string(-array) coinciding with the domain of the integrated 
			%			obj. 
			%		- lowerBound and upperBound define the boundaries of the integration domain. 
			%			These can be either doubles for definite integrals or a domain specified by
			%			a string or a quantity.Domain for an indefinite intergral. If lowerBound or
			%			upperBound are variable, then the result will contain a related domain.
			%			In order to perform integration over multiple domains, specify lowerBound
			%			and upperBound as cell-arrays.
			%
			% See also quantity.Discrete.diff
			
			% the default values for the input arguments are set after the arguments block, so that
			% a meaningful error handling is possible.
			arguments
				obj;
				domainName string = [];
				lowerBound = [];
				upperBound = [];
			end
		
			% set the default parameters for the input arguments
			if isempty(obj)
				% if the object is empty, return an empty object again;
				F = obj.copy;
				return
			end
					
			if isempty(domainName) 
				% set the default domain if no domain is specified.
				domainName = [obj(1).domain.name];
			end
			
			% verify that the quantity is defined on the required domain
			if isempty( obj(1).domain.find(domainName) )
				error("quantity:Discrete:int", ...
						"Error: the quantity is not defined on the domain " + domainName + " requested for the integration");
			end
			
			% set the default value for the upper and lower bounds
			if isempty(lowerBound)
				lowerBound = {obj(1).domain.find(domainName).lower};
			end
			
			if isempty(upperBound)
				upperBound = {obj(1).domain.find(domainName).upper};
			end
			
			lowerBound = misc.ensureIsCell(lowerBound);
			upperBound = misc.ensureIsCell(upperBound);
			
			assert(numel(domainName) == numel(lowerBound), ...
				"dimension of lowerBound must be equal to dimension of domain");
			assert(numel(domainName) == numel(upperBound), ...
				"dimension of upperBound must be equal to dimension of domain");
			
            integrationDomain = obj(1).domain.find(domainName);
            
			% integrate over all specified domains
			F = obj;
            
            % the function is iteratively integrated over all specified
            % domains.
			for it = 1 : numel(integrationDomain)
                
%                 % get index of domain
                intGridIdx = F.getDomain.index(domainName(it));
				
                % do the integration over the it-th domain
                Fnum = numeric.cumtrapz_fast_nDim(integrationDomain(it).grid, ...
					F.on(), intGridIdx);
                
                F = quantity.Discrete(Fnum, F.getDomain());
                
                % int_lowerBound^upperBound f(.) = F(upperBound) - F(lowerBound)
                F = F.subs(domainName(it), upperBound{it}) ...
                      - F.subs(domainName(it), lowerBound{it});
                  
                % repeat until all integration domains are used.
            end
			
            if isa(F, "quantity.Discrete")
				F.setName("int(" + obj(1).name + ")");
			end
		end % int()
		
		function C = plus(A, B)
			%  +   Plus.
			%     A + B adds matrices A and B
			assert(isequal(size(A), size(B)), 'plus() not supports mismatching sizes')
			
			if isempty(A) || isempty(B)
				C = quantity.Discrete.empty(size(A));
				return
			end
			
			% for support of numeric inputs:
			if ~isa(A, 'quantity.Discrete')
				if isnumeric(A)
					A = quantity.Discrete(A, quantity.Domain.empty(), 'name', "c");
				else
					error('Not yet implemented')
				end
			elseif ~isa(B, 'quantity.Discrete')
				if isnumeric(B)
					B = quantity.Discrete(B, quantity.Domain.empty(), 'name', "c");
				else
					B = quantity.Discrete(B, A(1).domain, 'name', "c");
				end
			end
			
			% combine both domains with finest grid
			joinedDomain = join(A(1).domain, B(1).domain);
			
			[aDiscrete] = A.expandValueDiscrete(joinedDomain);
			[bDiscrete] = B.expandValueDiscrete(joinedDomain);
			
			% create result object
			C = quantity.Discrete(aDiscrete + bDiscrete, joinedDomain, ...
				'name', A(1).name + "+" + B(1).name);
		end % plus
		
		function C = minus(A, B)
			%  -   Minus. 
			%     A - B subtracts matrix B from A.
			C = A + (-B);
			if ~isempty(C)
				if isnumeric(A)
					[C.name] = deal("c-" + B(1).name);

				elseif isnumeric(B)
					[C.name] = deal(A(1).name + "-c");
				else
					[C.name] = deal(A(1).name + "-" + B(1).name);
				end
			end
		end %minus()
		
		function C = uplus(A)
			% unitary plus: C = +A
			C = copy(A);
		end % uplus()
		
		function C = uminus(A)
			% unitary plus: C = -A
			C = (-1) * A;
			if ~isempty(A)
				[C.name] = deal("-" + A(1).name);
			end
		end % uminus()
		
		function [P, supremum] = relativeErrorSupremum(A, B)
			% relativeErrorSupremum compute the relative error of the supremum
			% [P, supremum] = relativeErrorSupremum(A, B) computes the supremum of the absolute
			% difference between A and B and returns the relative error of A - B with respect to
			% this supremum. The relative error is returned as P and the supremum is returned as
			% SUPREMUM.
			assert(numel(A) == numel(B), 'Not implemented')
			
			P = A.copy();
			
			if ~isa(B, 'quantity.Discrete')
				B = quantity.Discrete(B);
			end
			supremum = nan(size(A));
			for k = 1:numel(A)
				supremum(k) = max(max(abs(A(k).valueDiscrete), abs(B(k).valueDiscrete)));
				P(k).valueDiscrete = (A(k).valueDiscrete - B(k).valueDiscrete) ./ supremum(k);
				P(k).name = sprintf('%.2g', supremum(k));
			end
			supremum = reshape(supremum, size(A));
		end % relativeErrorSupremum()
		
		function [i, m, s] = near(obj, B, varargin)
			% NEAR compare with numerical tolerance
			%
			% [i, m, s] = near(obj, B) compares the numerical
			%	values of this object with the numerical values of B. B has
			%	to be a quantity.Discrete object.
			% [i, m, s] = near(obj, B, tolerance) compares the numerical
			% values with respect to the given tolerance. Default is 10eps.
			% [i, m, s] = near(obj, B, tolerance, relative) compares the
			% numerical values with respect to the given tolerance and if
			% relative is true, the values are compared, relativley to the
			% maximal value of obj.on(). If relative is a numerical value.
			% The values are compared reltivley to this.
			%
			% The output i is a logical value that is true if the values
			% are inside the given tolerance. m is the maximal derivation
			% of both numerical values. s is the text that is printed if
			% the function is called without output.
			
			if nargin == 1
				b = 0;
			elseif isnumeric(B)
				b = B;
			else
				b = B.on(obj(1).domain);
			end
			
			[i, m, s] = numeric.near(...
				obj.on(), ...
				b, varargin{:});
			if nargout == 0
				i = s;
			end
		end % near
		
		function maxValue = MAX(obj)
			% MAX returns max(obj.on(), [], "all"), i.e. the pointwise maximum of all obj.
			maxValue = max(obj.on(), [], 'all');
		end % MAX()
		
		function [maxValue, maxArg] = max(obj, optArgs)
			% max returns the maximal value of all elements of "obj" over all
			% variables as a double array.
			arguments
				obj
				optArgs.AbsTol = 1e-12;
			end
			if obj.isempty()
				maxValue = zeros(size(obj));
				maxArg = obj(1).domain;
			elseif obj.isNumber()
				maxValue = max( obj.on(), [], 'all' );
				maxArg = [];
			else
				maxValue = max(obj.on(), [], 1:obj(1).nargin);
				maxValue = reshape(maxValue, size(obj));
				
				if nargout >= 2
					% find the grid points for the maxima values
					idx = cell(size(obj));
					ndGrids = obj(1).domain.ndgrid();
					maxArg = cell(size(obj));

					for i = 1:numel(obj)
						% find all maximas in the given tolerance:
						idx{i} = find( abs( obj(i).on () - maxValue(i) ) < optArgs.AbsTol );
						% find the grid points for the given maxima
						for j = 1:numel(ndGrids)
							maxArg{i}(:,j) = ndGrids{j}(idx{i});
						end						
					end
				end
			end
		end % max()
		
		function [minValue, minArg] = min(obj, optArgs)
			% min returns the minimum value of all elements of A over all
			% variables as a double array.
			arguments
				obj
				optArgs.AbsTol = 1e-12;
			end		
			
			if obj.isempty()
				minValue = zeros(size(obj));
				minArg = obj(1).domain;
			else
				minValue = reshape(min(obj.on(), [], 1:obj(1).nargin), size(obj));
				
				if nargout >= 2
					idx = cell(size(obj));
					ndGrids = obj(1).domain.ndgrid();
					minArg = cell(size(obj));
					
					for i = 1:numel(obj)
						idx{i} = find( abs( obj(i).on() - minValue(i) ) < optArgs.AbsTol );
						for j = 1:numel(ndGrids)
							minArg{i}(:,j) = ndGrids{j}(idx{i});
						end
					end
				end				
			end
		end % min()
		
		function absQuantity = abs(obj)
			% abs returns the absolut value of the quantity as a quantity
			if isempty(obj)
				absQuantity = quantity.Discrete.empty(size(obj));
			else
				absQuantity = quantity.Discrete(abs(obj.on()), obj(1).domain, ...
					"name", "|" + obj(1).name + "|");
			end
		end % abs()
		
		function boundedObj = bound(obj, bounds)
			% bound limits the value of obj between lower and/or upper bounds.
			% In detail, this method returns boundedObj defined as
			%
			%				 { upperBound	for all values obj > upperBound
			%	boundedObj = { obj			for all values lowerBound < obj < upperBound
			%				 { lowerBound	for all values obj > lowerBound
			%
			%	boundedObj = bound(obj) no bounds are defined, hence boundedObj = obj.copy().
			%
			%	boundedObj = bound(obj, boundScalar) the scalar double input is used for the lower
			%		and the upper, i.e. boundupperBound = abs(scalar) and lowerBound = -abs(scalar).
			%
			%	boundedObj = bound(obj, boundVector) the double-vector input is used for the 
			%		lower and the upper bounds. The smaller element is the lower bound, the greater
			%		value is the upper bound. Use inf and -inf, if no upper or lower bound is of
			%		interest.
			%
			% See also abs, min, max, MAX.
			
			arguments
				obj;
				bounds (:, 1) double = [];
			end % arguments
			
			data = obj.on();
			if isscalar(bounds)
				upperBound = abs(bounds);
				lowerBound = -upperBound;
			elseif numel(bounds) == 2
				upperBound = max(bounds);
				lowerBound = min(bounds);
				
			elseif isempty(bounds)
				boundedObj = obj.copy();
				return;
			else
				error("bounds must specify an absolut bound [-bounds, bounds], or it must " ...
					+ "define a upper and a lower bound [bounds1, bounds2].");
			end
			
			data(data>upperBound) = upperBound;
			data(data<lowerBound) = lowerBound;
			
			boundedObj = quantity.Discrete(data, obj(1).domain, "name", obj(1).name);
		end % bound()
		
		function sgnObj = sign(obj)
			% SIGN Signum function.
			% For each element of obj, sign(obj) returns 1 if the element
			%     is greater than zero, 0 if it equals zero and -1 if it is
			%     less than zero.
			%
			%     See also abs.
			sgnObj = quantity.Discrete( sign( obj.on() ), obj(1).domain, "name", ...
				"sgn(" + obj(1).name + ")");
		end % sign
		
		function d = det(obj, setName)
			% det(X) returns the the determinant of the squre matrix X
			arguments
				obj;
				setName = true;
			end
			if isempty(obj)
				d = quantity.Discrete.empty(1);
			elseif numel(obj) == 1
				d = copy(obj);
			elseif ismatrix(obj) && (size(obj, 1) == size(obj, 2))
				if size(obj, 1) == 2
					d = obj(1, 1) * obj(2, 2) - obj(2, 1) * obj(1, 2);
				elseif size(obj, 1) == 3
					d = prod(obj(logical([1, 0, 0; 0, 1, 0; 0, 0, 1]))) ...
						+ prod(obj(logical([0, 1, 0; 0, 0, 1; 1, 0, 0]))) ...
						+ prod(obj(logical([0, 0, 1; 1, 0, 0; 0, 1, 0]))) ...
						- prod(obj(logical([0, 0, 1; 0, 1, 0; 1, 0, 0]))) ...
						- prod(obj(logical([1, 0, 0; 0, 0, 1; 0, 1, 0]))) ...
						- prod(obj(logical([0, 1, 0; 1, 0, 0; 0, 0, 1])));
				else
					% maybe a more sophisticated implementation would be nice.
					d = 0;
					for it = 1 : size(obj, 1)
						selector = true(size(obj));
						selector(:, 1) = false;
						selector(it, :) = false;
						d = d + (-1)^(it-1)*obj(it, 1) * det(reshape(obj(selector), size(selector)-1), false);
					end % for it = 1 : size(obj, 1)
				end
			else
				error("det is only defined for quadratic matrices");
			end
			
			if setName
				d.setName("det(" + obj(1).name + ")");
			end
		end % det()
		
		function y = real(obj)
			% real() returns the real part of the obj.
			y = quantity.Discrete(real(obj.on()), obj(1).domain, ...
				'name', "real(" + obj(1).name + ")");
		end % real()
		
		function y = imag(obj)
			% real() returns the imaginary part of the obj.
			y = quantity.Discrete(imag(obj.on()), obj(1).domain, ...
				'name', "imag(" + obj(1).name + ")");
		end % imag()
		
		function meanValue = mean(obj, dim)
			% mean(dim) returns the mean value over all domain as a double array 
			% of the size of obj.
			%
			% mean(obj, 'all') returns the mean value of all elements of A over all
			% domain as a scalar double.
			arguments
				obj;
				dim = [];
			end
			if strcmp(dim, "all")
				meanValue = mean(obj.on(), 'all');
			else
				meanValue = reshape(mean(obj.on(), 1:obj(1).nargin), size(obj));
			end
		end % mean()
		
		function medianValue = median(obj, dim)
			% median(dim) returns the median value over all domain as a double array 
			% of the size of obj.
			%
			% median(obj, 'all') returns the median value of all elements of A over all
			% domain as a scalar double.
			arguments
				obj;
				dim = [];
			end
			if strcmp(dim, "all")
				medianValue = median(obj.on(), 'all');
			else
				medianValue = reshape(median(obj.on(), 1:obj(1).nargin), size(obj));
			end
		end % mean()
		
		function value = obj2value(obj, myDomain, recalculate)
			% obj2value returns the numerical value of obj on the grid specified by myDomain.
			%
			%	value = obj2value(obj) returns the valueDiscrete in the form 
			%		size(value) = [obj(1).domain.n, objSize]
			%
			%	obj2value(obj, myDomain) returns the valueDiscrete in the form 
			%		size(value) = [myDomain.n, objSize]
			%		after performing a linear interpolation
			%
			%	obj2value(obj, myDomain, recalculate) the recalculate option has no effect. This
			%		input argument is only implemented for compatibility with
			%		quantity.Function.obj2value
			
			arguments
				obj;
				myDomain = obj(1).domain;
				recalculate = true;
			end % arguments
				
			if nargin >= 2 && ~isequal( myDomain, obj(1).domain )
				% if a new domain is specified for the evaluation of
				% the quantity, ...
				if obj.isNumber()
					% ... duplicate the constant value on the desired domain
					value = reshape(cat(1, obj(:).valueDiscrete), [ones([1, numel(myDomain)]), size(obj)]);
					value = repmat(value, [myDomain.n, ones(1, ndims(obj))]);
				else
					%... do an interpolation based on the old data.
					[tempInterpolant, indexGrid] = obj.getInterpolant();
					value = tempInterpolant.evaluate(myDomain.grid, indexGrid{:});
				end
			else
				value = reshape(cat(numel(obj(1).domain)+1, obj(:).valueDiscrete), ...
					[obj(1).domain.gridLength(), size(obj)]);
			end
		end % obj2value()
		
		function [myInterpolant, indexGrid] = getInterpolant(obj)
			% getInterpolant creates a numeric.interpolant object to interpolate values of obj.
			value = reshape(cat(numel(obj(1).domain)+1, obj(:).valueDiscrete), ...
				[obj(1).domain.gridLength(), size(obj)]);
			indexGrid = misc.indexGrid(size(obj));
			myInterpolant = numeric.interpolant([{obj(1).domain.grid}, indexGrid{:}], value);
		end % getInterpolant()
		
		function result = diag2vec(obj)
			% diag2vec creates a vector of quantities by selecting the diagonal elements of the 
			% quantity array obj
			assert(ndims(obj) <= 2, "quantity.diag2vec is only implemented for quantity matrices");
			
			I = logical(eye(size(obj)));
			result = copy([obj(I(:))]);
		end % diag2vec()
		
		function result = vec2diag(obj)
			% This method creates a diagonal matrix of quantities which
			% carries the elements of vec on its diagonal
			assert(isvector(obj), 'quantity.vec2diag is only implemented for quantity vectors');
			
			result = obj(1).zeros([1, 1]*numel(obj), obj(1).domain);
			objCopy = copy(obj);
			for it = 1 : numel(obj)
				result(it, it) = objCopy(it);
			end
		end % vec2diag
		
		function f = fourierTransform(obj, omega)
			%FOURIERTRANSFORM numerical computation of the fourier transform
			% f = fourierTransform(obj, omega) computes the fourier transform of the function
			% defined by obj. For this the transformation
			%	F(w) = int f(t) * exp( -i * t * w ) dt
			% is computed. The bounds for the integration are the upper and lower bound of
			% obj.domain, i.e., the function specified in obj is continued with zeros for t -> infty
			% and t -> - infty.
								
			for i = 1:numel(obj)
			
				F = zeros(omega.n, 1);			
				for k = 1:omega.n
					F(k) = trapz(obj(i).domain.grid, ...
						obj(i).on() .* exp( - 1i * obj(i).domain.grid * omega.grid(k)));
				end
				f(i) = quantity.Discrete( F, omega);
			end
			f= reshape(f, size(obj));
		end	% fourierTransform
        
        function domain = getDomain(obj, domainName)
            % getDomain gets the domain of a quantity.Discrete % 
            % domain = getDomain(obj) returns an array of all domains of obj. % 
            % domain = getDomain(obj, domainName) returns an array of all domains of obj specified by the string-array domainName. % 
            % See also quantity.Discrete.getGrid
            arguments
               obj;
               domainName = [];
            end
            if isempty(obj)
                error("Quantity:Discrete:getDomain", "Trying to get property 'domain' of an empty quantity.");
            elseif ~isempty(domainName)
                domain = obj(1).domain.find(domainName);
            else
                domain = obj(1).domain;
            end
        end
	end% (Access = public)
	
	methods (Static)
		
		function P = ones(valueSize, domain, varargin)
			%ONES initializes an ones-quantity.Discrete object
			%	P = ones(VALUESIZE, DOMAIN) creates a matrix of size
			%	VALUESIZE on the DOMAIN with ones as entries.
			
			if any( valueSize == 0)
				P = quantity.Discrete.empty(valueSize);
			else
				O = ones([domain.n, valueSize(:)']);
				P = quantity.Discrete(O, domain, varargin{:});
			end
		end % ones()
		
		function I = eye(N, domain, varargin)
			%  eye Identity matrix.
			%     eye(N, domain) is the N-by-N identity matrix on the domain domain.
			%  
			%     eye([M,N], domain) is an M-by-N matrix with 1's on the diagonal and zeros 
			%		elsewhere.
			%     eye([M,N], domain, varargin) passes further inputs to the quantity.Discrete
			%     constructor.
			
			if isscalar(N)
				N = [N, N];
			elseif ~ismatrix(N)
				error("eye only supports ndims(N) == 1 or 2");
			end
			
			if any( N == 0)
				I = quantity.Discrete.empty(N);
			else
				IvalueDiscrete = reshape(eye(N), [ones(1, numel(domain)), N]);
				IvalueDiscrete = repmat(IvalueDiscrete, [domain.n, 1]);
				I = quantity.Discrete(IvalueDiscrete, domain, varargin{:});
			end
		end % eye()
		
		function P = zeros(valueSize, domain, varargin)
			%ZEROS initializes an zero quantity.Discrete object
			%	P = zeros(VALUESIZE, DOMAIN) creates a matrix of size
			%	VALUESIZE on the DOMAIN with zero entries.
			
			if any( valueSize == 0)
				P = quantity.Discrete.empty(valueSize);
			else
				O = zeros([domain.n, valueSize(:)']);
				P = quantity.Discrete(O, domain, varargin{:});
			end
		end % zeros()
		
		function valueCell = value2cell(value, numberOfDomains)
			% value2cell splits the double-array value of the size [gridSize, cellSize] into a cell
			% of the size [cellSize] containing double-arrays of the size [gridSize] as elements.
			% This method is used in the constructor of quantity.Discrete.

			valueSize = [size(value), 1, 1];
			gridSelector = cell(numberOfDomains, 1);
			for it = 1 : numberOfDomains
				gridSelector{it} = 1:1:valueSize(it);
			end
			valueCell = cell(valueSize(numberOfDomains+1 : end));
			for it = 1 : numel(valueCell)
				valueCell{it} = value(gridSelector{:}, it);
			end
		end % value2cell()
		
		function [newValuesSorted, oldDomainName] = subsParser(oldDomainName, newValues)
			% subsParser is a helper function to parse the inputs of subs. In this method, the
			% elements of the newValues-cell-array are splitted, such that there is one cell element
			% for every oldDomainName. Specificly, string-arrays and domain-arrays in newValues are
			% split, while numeric-scalars remain unchanged and numeric-arrays are replaced by 
			% domains.
			% Additionally, the case of swapping arguments like of f(z, zeta) is considered, such
			% that f.subs(["z", "zeta"], ["zeta", "z"]) will result in f(zeta, z) and not in f(z,z).
			arguments
				oldDomainName string;
				newValues cell;
			end
			
			newValuesSorted = cell(size(oldDomainName));
			sortedIdx = 1;
			for it = 1 : numel(newValues)
				if isnumeric(newValues{it}) && isvector(newValues{it})
					if isscalar(newValues{it})
						newValuesSorted{sortedIdx} = newValues{it};
					else % isvector(newValues{it}) == true
						newValuesSorted{sortedIdx} = ...
							quantity.Domain(oldDomainName(sortedIdx), newValues{it});
					end
					sortedIdx = sortedIdx + 1;
					
				elseif isstring(newValues{it}) || isa(newValues{it}, "quantity.Domain")
					for jt = 1 : numel(newValues{it})
						newValuesSorted{sortedIdx} = newValues{it}(jt);
						sortedIdx = sortedIdx + 1;
					end
					
				else
					error("newValues for subs in varargin of subs(obj, oldDomainName, varargin)" ...
						+ " must be numeric scalars or vectors or arrys of quantity.Domain or string");
					
				end % if-else
			end % for it = 1 : numel(newValues)
			
			assert(sortedIdx-1 == numel(oldDomainName), "number of elements in oldDomainName and " ...
				+ "in varargin must be equal when calling subs(obj, oldDomainName, varargin)");
			
			
			% Additionally, the case of swapping arguments like of f(z, zeta) is considered, such
			% that f.subs(["z", "zeta"], ["zeta", "z"]) will result in f(zeta, z) and not in f(z,z):
			% If a quantity f(z, zeta) should be substituted like 
			%	subs(f, ["z", "zeta"], "zeta", "z"}) 
			% this would be faulty, as after the first substituion 
			%	subs(f, "z", "zeta"), 
			% the result would be f(zeta, zeta). Hence the 2nd subs(f, zeta, z) will result in 
			% f(z, z) and not in f(zeta, z) as intended.
			% This is solved, by an additonal substitution: 
			%	f.subs(z,zetabackUp).subs(zeta,z).subs(zetabackUp,zeta)
			for it = 1 : numel(newValuesSorted)
				if isstring(newValuesSorted{it}) ...
						&& any(strcmp(newValuesSorted{it}, oldDomainName(it:end)))
					
					newValuesSorted{end+1} = newValuesSorted{it};
					oldDomainName(end+1) = oldDomainName(it) + "backUp";
					newValuesSorted{it} = oldDomainName(end);
					
				elseif isa(newValuesSorted{it}, "quantity.Domain") ...
						&& any(strcmp(newValuesSorted{it}.name, oldDomainName(it:end)))
					
					newValuesSorted{end+1} = newValuesSorted{it}.name;
					oldDomainName(end+1) = oldDomainName(it) + "backUp";
					newValuesSorted{it} = newValuesSorted{it}.rename(oldDomainName(end));
				end
			end % for it = 1 : numel(newValuesSorted)
			
		end % subsParser()
		
	end %% (Static)
	
	methods(Access = protected)
		
		function obj = subsDomainMerge(obj, oldDomainName, newDomain)
			% subsDomainMerge()	Symbolic substitution for which 2 domains need to be merged.
			% Example:
			%	z = quantity.Domain("z", linspace(0, 1, 11));
			%	zLr = quantity.Domain("z", linspace(0, 1, 5));
			%	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
			%	f = z.Discrete() + zeta.Discrete()	% = f(z, zeta)
			%	f.subsDomainMerge("zeta", zLr)		% = f(z) 
			%			-> the finest grid is chosen in the latter case, hence z and not zLr.
			% This is a helper function for subsDomain, hence it is protected.
			
			% find domain to be merged
			[idxOldDomain, idxOldDomainLogical] = index(obj(1).domain, oldDomainName);
			[idxNewDomain, idxNewDomainLogical] = index(obj(1).domain, newDomain.name);
			
			% pick finest grid.
			newDomain = join(join(newDomain, obj(1).domain(idxNewDomain)), ...
				obj(1).domain(idxOldDomain).rename(newDomain.name));
			
			domainForOn = obj(1).domain;
			domainForOn(idxOldDomain) = quantity.Domain(oldDomainName, newDomain.grid);
			domainForOn(idxNewDomain) = newDomain;
			
			% get value Discrete
			newValueMerged = misc.diagNd(obj.on(domainForOn), [idxOldDomain, idxNewDomain]);
			newDomain = [newDomain, domainForOn(~(idxOldDomainLogical | idxNewDomainLogical))];
			
			% create new obj
			obj = quantity.Discrete(newValueMerged, newDomain, "name", obj(1).name);
		end % subsDomainMerge()
		
		function obj = subsDomainRename(obj, oldDomainName, newDomain)
			% subsDomainMerge()	Symbolic substitution for which 2 domains need to be merged.
			% Example:
			%	z = quantity.Domain("z", linspace(0, 1, 11));
			%	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
			%	f = z.Discrete()					% = f(z)
			%	f.subsDomainRename("z", zeta)		% = f(zeta) 
			%			-> the finest grid is chosen in the latter case, hence z and not zLr.
			% This is a helper function for subsDomain, hence it is protected.
			
			oldDomainNameNewGrid = quantity.Domain(oldDomainName, newDomain.grid);
			obj = obj.changeDomain(oldDomainNameNewGrid);
			newDomainComplete = obj(1).domain.rename(newDomain.name, oldDomainName);
			[obj.domain] = deal(newDomainComplete);
		end % subsDomainRename()
		
		function [valDiscrete] = expandValueDiscrete(obj, newDomain)
			% EXPANDVALUEDISCRETE expand the discrete value on the
			% newDomain
			%	[valDiscrete] = ...
			%       expandValueDiscrete(obj, newDomain) expands the
			%       discrete values on a new domain. So that a function
			%			f(z,t) = f1(z) + f2(t)
			%	can be computed.
		
			gridJoinedLength = newDomain.gridLength;
			
			% get the index of obj.domain in the joined grid
			[~, logicalIdx] = newDomain.index([obj(1).domain.name]);
			% evaluate the 
			valDiscrete = obj.on( newDomain(logicalIdx) );
			oldDim = ndims(valDiscrete);
			valDiscrete = permute(valDiscrete, [(1:sum(~logicalIdx)) + oldDim, 1:oldDim] );
			valDiscrete = repmat(valDiscrete, [gridJoinedLength(~logicalIdx), ones(1, ndims(valDiscrete))]);
			%
			valDiscrete = reshape(valDiscrete, ...
				[gridJoinedLength(~logicalIdx), gridJoinedLength(logicalIdx), size(obj)]);
			
			% permute valDiscrete such that grids are in the order specified
			% by gridNameJoined.
			index = 1:numel(logicalIdx);
			gridOrder = [index(~logicalIdx), index(logicalIdx)];
			index(gridOrder) = 1:numel(logicalIdx);
			
			valDiscrete = permute(valDiscrete, [index, numel(logicalIdx)+(1:ndims(obj))]);
		end % expandValueDiscrete()
		
		function result = diff_inner(obj, k, diffGridName)
			% diff_inner is a helper function for quantity.Discrete.diff.
			
			index = obj(1).domain.index(diffGridName);
			if index == 0
				% in this case obj is not defined on diffGridName, and thus constant w.r.t. it.
 				result = quantity.Discrete.zeros(size(obj), obj(1).domain, ...
 					"name", "(d_{" + diffGridName + "}" + obj(1).name + ")");
 				return;
			end

			permutationVector = 1 : (obj(1).nargin+ndims(obj));

			objDiscrete = permute(obj.on(), ...
				[permutationVector(index), ...
				permutationVector(permutationVector ~= index)]);
			[spacing, idx] = getSpacing(obj);

			if iscolumn(objDiscrete)
				derivativeDiscrete = gradient(objDiscrete, ...
					spacing{idx == index}, ...
					spacing{idx ~= index});
			else
				spacing = [spacing(idx == index), spacing(idx ~= index)];
				[~, derivativeDiscrete] = gradient(objDiscrete, ...
					spacing{2}, spacing{idx~=2});
			end

			rePermutationVector = [2:(index), ...
				1, (index+1):ndims(derivativeDiscrete)];
			result = quantity.Discrete(...
				permute(derivativeDiscrete, rePermutationVector), obj(1).domain, ...
				"name", "(d_{" + diffGridName + "}" + obj(1).name + ")");

			if k > 1
				% if a higher order derivative is requested, call the function
				% recursivly until the first-order derivative is reached
				result = result.diff(diffGridName, k-1);
			end
		end % diff_inner()
		
			
		function [mySpace, idx] = getSpacing(obj)
			% getSpacing returns a cell array of the spacing needed for gradient(),
			% in diff_inner including a spacing for the array dimensions of obj.
			mySpace = {obj(1).domain(:).grid};
			if isscalar(obj)
				% do nothing
			elseif iscolumn(obj)
				mySpace = [mySpace, (1 : 1 : numel(obj)).'];
			else % ismatrix(obj)
				mySpace = [mySpace, misc.indexGrid(size(obj)).'];
			end
			idx = (1 : 1 : numel(mySpace)).';
		end % getSpacing(obj)
		
		function [idx, permuteGrid] = computePermutationVectors(a, b)
			% computePermutationVectors computes the required permutation vectors to use in
			% misc.multArray for multiplication of quantities.
			
			% 1) find common entries
			if isempty(b(1).domain) || isempty(a(1).domain)
				common = [];
			else
				common = intersect([a(1).domain.name], [b(1).domain.name]);
			end
			
			commonA = false(1, a.nargin());
			commonB = false(1, b.nargin());
			
			idxA0 = 1:a.nargin();
			idxB0 = 1:b.nargin();
			
			gridA = zeros(1, a.nargin());
			gridB = zeros(1, b.nargin());
			
			for k = 1:numel(common)
				c = common{k};
				
				% 2) find logical indices for the common entries
				cA = strcmp(c, [a(1).domain.name]);
				commonA = commonA | cA;
				
				% 3) exchange the order of the logical indices
				gridA(k) = idxA0(cA);
				
				cB = strcmp(c, [b(1).domain.name]);
				commonB = commonB | cB;
				
				gridB(k) = idxB0(cB);
			end
			
			gridA(numel(common)+1:end) = idxA0(~commonA);
			gridB(numel(common)+1:end) = idxB0(~commonB);
			
			valueA = a.nargin + (1:numel(size(a)));
			valueB = b.nargin + (1:numel(size(b)));
			
			idx.A.permute = [gridA, valueA];
			idx.A.grid = gridA;
			idx.A.value = valueA;
			idx.A.common = commonA;
			
			idx.B.permute = [gridB, valueB];
			idx.B.grid = gridB;
			idx.B.value = valueB;
			idx.B.common = commonB;
			
			idx.common = 1:numel(common);
			
			% permutation for the new grid
			nGridA = a.nargin();				% number of the grid dimensions of quantity a
			nValueA = ndims(a) - 1;				% number of the value dimensions of a minus the multiplied dimension
			nGridB = b.nargin() - numel(common);% number of the grid dimensions of quantity b minus the common dimensions that are part of nGridA
			nValueB = ndims(b) - 1;				% number of the value dimensions minus the multiplied dimensions
			
			idxGrid = 1:(nGridA + nGridB + nValueA + nValueB);
			
			lGridA = [true(1, nGridA), false(1, nValueA), false(1, nGridB), false(1, nValueB)];
			lGridVA = [false(1, nGridA), true(1, nValueA), false(1, nGridB), false(1, nValueB)];
			lGridB = [false(1, nGridA), false(1, nValueA), true(1, nGridB), false(1, nValueB)];
			lGridVB = [false(1, nGridA), false(1, nValueA), false(1, nGridB), true(1, nValueB)];
			
			% Creates the permutation vector to bring the result of
			% misc.multArray into the required form for the
			% quantity.Discrete class.
			permuteGrid = [idxGrid(lGridA), idxGrid(lGridB), idxGrid(lGridVA), idxGrid(lGridVB)];
		end % computePermutationVectors
		
		function f = setValueContinuous(obj, f)
		end
		function f = getValueContinuous(obj, f)
		end
		
		function cpObj = copyElement(obj)
			% copyElement Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
		end % copyElement()
		
		function s = getPropertyGroups(obj)
			% getPropertyGroups is used to display the correct values of quantity objects.
			
			if isempty(obj)
				s = getPropertyGroups@matlab.mixin.CustomDisplay(obj);
				return;
			else
				s = getPropertyGroups@matlab.mixin.CustomDisplay(obj(1));
			end
			
			if numel(obj) ~= 1
				s.PropertyList.valueDiscrete = ...
					[sprintf('%ix', obj(1).domain.gridLength, size(obj)) sprintf('\b')];
			end
		end % getPropertyGroups
		
	end % methods (Access = protected)
	
end % classdef
