classdef (InferiorClasses = {?quantity.Symbolic}) Piecewise < quantity.Discrete
	% quantity.Piecewise allows to assemble quantity.Discrete objects which are defined on
	% consecutive segments of a domain in order to implement functions that are defined piecewise on
	% that domain.
	% This inherits from quantity.Discrete.
	%
	% See also quantity.Piecewise.Piecewise (Constructor), quantity.Discrete, quantity.Function,
	% quantity.Symbolic, quantity.Domain.

	properties
		% Cell of the quantities defined on distinct segments of the domain
		quantities cell;
		% the full domain on which the values are defined piecewise
		domain2join (1, 1) quantity.Domain;
		% defines if upper the values at the upper end of the domain-segments are used or not.
		upperBoundaryIncluded (:, 1) logical;
		% boolean variable, if true an additional point is added at the split-points of the domain
		addPoint (1, 1) logical;
	end
	
	methods
		function obj = Piecewise(quantities, optArgs)
			% obj = Piecewise(quantities, optionalArgs) creates a quantity.Piecewise object.
			%
			% The parameter 'quantities' must be a cell array containg quantity.Discrete objects
			% that are defined on consecutive segments of the same domain.
			%
			% With the optional name-value pair input 'domain2join' the domain that should be
			% assembled from the entries in 'quantities' can be as a quantity.Domain object. Its
			% default value is the first domain of the first cell-element in quantities is taken,
			% i.e., quantities{1}(1).domain(1).
			%
			% The name-value pair input 'name' sets the name property of the resulting
			% quantity.Piecewise object.
			%
			% The name-value input 'upperBoundaryIncluded' is a logical vector which specifies which
			% value at the assembling point should be taken from the left or the right function. If
			% the value at a assembling point should be taken from the left function, the
			% corresponding entry must be set to true, if it should be taken from the right
			% function, it must be set to false. Default value is true.
			%
			% If, e.g., f1(t) is defined on 0 <= t < T1 and f2(t) on T1 <= t <= T2. Then, the
			% piecewise function 
			%			/ f1(t): 0 <= t < T1
			%	f(t) =  |
			%			\ f2(t): T1 <= t <= T2
			% can be created with 
			%	f = quantity.Piecewise({ f1, f2}, "upperBoundaryIncluded", true), 
			% assuming f1 and f2 are quantity.Discrete objects. With the option,
			% "upperBoundaryIncluded" it is ensured that f(T1) = f2(T1) is taken.
			%
			% See also quantity.Piecewise (class description), quantity.Discrete, quantity.Function,
			% quantity.Symbolic, quantity.Domain.
			
			arguments
				quantities (:,1) cell = {};
				% the default value for the domainToJoin is an empty quantity.Domain. This is
				% required in order to allow initialization of quantity.Piecewise-arrays
				optArgs.domain2join (1,1) quantity.Domain = quantity.Domain();
				optArgs.name string = "";
				optArgs.upperBoundaryIncluded (:, 1) logical = repmat( true, numel(quantities)-1, 1);
				optArgs.addPoint (1,1) logical = false;
			end
			
			initArgs = {};
			
			if nargin > 0
				if isempty( optArgs.domain2join )
					% If the optional arg has not been set, use the grid of the functions
					optArgs.domain2join = quantities{1}(1).domain;
				end	
				% ensure all have the same domain
				d = quantities{1}(1).domain;
				assert( all( cellfun(@(q) all( strcmp( [d.name], [q(1).domain.name] ) ), quantities) ) , ...
					'The quantities for the piecewise combination must have the same domain names' );
				assert(length(quantities) >= 2, 'Only one quantity is given for the piecewise combination. At least 2 are required.');
				
				domain2joinIndex = quantities{1}(1).domain.index(optArgs.domain2join);

				joinedGrid = quantities{1}(1).domain.find(optArgs.domain2join.name).grid;
				joinedValues = quantities{1}.on();
				joinedValues = permute(joinedValues, ...
						[domain2joinIndex, setdiff((1:ndims(joinedValues)), domain2joinIndex)]);

				if optArgs.addPoint
					
					delta = 1e-6; % TODO this should be adapted to the step size		
					% ensure the domains fit to each other and then concatenate them
					for k = 2:length(quantities)
						dkm1 = quantities{k-1}(1).domain.find(optArgs.domain2join.name);
						dk = quantities{k}(1).domain.find(optArgs.domain2join.name);

						assert(numeric.near(dkm1.upper, dk.lower), "Domains do not connect to each other");

						% *) join the domains and the discrete values to a common
						tmpGrid = dk.grid;
						
						
						% to avoid identical grid points (the interpolant does not like them) add a
						% slight shift:
						if optArgs.upperBoundaryIncluded(k-1)
							% the value for the assembling point should be taken from the left values
							% -> the first point in the new grid is slightly shifted to the right
							tmpGrid(1,:) = tmpGrid(1,:) + (tmpGrid(2,:)-tmpGrid(1,:)) * delta;
							
							tmpValues = on( quantities{k}.subs(dk.name, quantity.Domain( dk.name, tmpGrid) ) );
						else
							% the value for the assembling point should be taken from the right
							% values:
							% -> the last grid point in the old domain is slightly shifted to the
							% left
							joinedGrid(end,:) = joinedGrid(end,:) - (joinedGrid(end,:)-joinedGrid(end-1,:)) * delta;
							
							% actually the values should be reinterpolated on the new grid
							tmpValues = quantities{k}.on();							
						end
						
						
						
						
						joinedGrid = [joinedGrid; tmpGrid]; %#ok<AGROW>

						tmpValues = permute(tmpValues, ...
								[domain2joinIndex, setdiff(1:ndims(tmpValues), domain2joinIndex)]);
						
						joinedValues = cat(1, joinedValues, reshape(tmpValues, size(tmpValues)));
					
					end
					
				else
					% ensure the domains fit to each other and then concatenate them
					for k = 2:length(quantities)
						dkm1 = quantities{k-1}(1).domain.find(optArgs.domain2join.name);
						dk = quantities{k}(1).domain.find(optArgs.domain2join.name);

						assert(numeric.near(dkm1.upper, dk.lower), "Domains do not connect to each other");

						% *) join the domains and the discrete values to a common
						tmpGrid = dk.grid;
						tmpValues = quantities{k}.on();
						joinedGrid = [joinedGrid; tmpGrid(2:end,:)]; %#ok<AGROW>

						tmpValues = permute(tmpValues, ...
								[domain2joinIndex, setdiff(1:ndims(tmpValues), domain2joinIndex)]);
						if optArgs.upperBoundaryIncluded(k-1)
							% the value for the assembling point should be taken from the left values
							joinedValues = cat(1, joinedValues, ...
								reshape(tmpValues(2:end,:), size(tmpValues) - eye(1, ndims(tmpValues))));
						else
							% the value for the assembling point should be taken from the right values
							joinedValues = cat(1, ...
								reshape(joinedValues(1:end-1,:), size(joinedValues) - eye(1, ndims(joinedValues))), ...
								tmpValues);
						end
					end
				
				end
				
				joinedValues = ipermute(joinedValues, ...
						[domain2joinIndex, setdiff((1:ndims(joinedValues)), domain2joinIndex)]);
				
				joinedDomain = quantities{1}(1).domain.copy.replace(...
						quantity.Domain(optArgs.domain2join.name, joinedGrid));
				initArgs = {joinedValues, joinedDomain, "name", optArgs.name};
			end
			
			obj@quantity.Discrete(initArgs{:});
			
			if nargin > 0
				[obj.quantities] = deal(quantities);
				[obj.domain2join] = deal(optArgs.domain2join);
				[obj.upperBoundaryIncluded] = deal(optArgs.upperBoundaryIncluded);
				[obj.addPoint] = deal(optArgs.addPoint);
			end
		end % Piecewise
		
		function result = diffPiecewise(obj, domain, k)
			% diffPiecewise piecewise computation of the derivative
			%
			%	result = diffPiecewise(obj) calculates the first order derivative w.r.t. all domains
			%		of obj.
			%
			%	result = diffPiecewise(obj, domain) calculates the first order derivative w.r.t. the
			%		domains specified by domain. The input domain can be an array of strings or of
			%		quantity.Domain.
			%
			%	result = diffPiecewise(obj, domain, k) calculates the k-th derivative w.r.t. the 
			%		specified domain.
			%
			% See also quantity.Discrete.diff.
			arguments
				obj;
				domain (1,1) string = [obj(1).domain.name];
				k uint64 = 1;
			end
			
			resultQuantities = obj.quantities;
			for it = 1 : numel(resultQuantities)
				resultQuantities{it} = resultQuantities{it}.diff(domain, k);
			end
			result = quantity.Piecewise(resultQuantities, ...
				"domain2join", obj.domain2join, ...
				"upperBoundaryIncluded", obj.upperBoundaryIncluded, ...
				"addPoint", obj.addPoint);
		end % diffPiecewise()

		function thisQuantity = quantity.Discrete(obj, NameValue)
			% quantity.Discrete casts obj into an quantity.Discrete object
			arguments
				obj;
				NameValue.domain quantity.Domain = obj(1).domain;
				NameValue.name (1, 1) string = obj(1).name;
			end
			thisQuantity = reshape(quantity.Discrete( cat(numel(obj(1).domain)+1, obj.valueDiscrete), ...
				NameValue.domain, "name", NameValue.name), size(obj));
		end % quantity.Discrete()
	end % methods
	
end % classdef