classdef Settings < handle
	%SETTINGS object to save some settings for the quantity class

	properties (Access = public)
		plot = struct(...
			'dock', false, ...
			'name2title', true, ...
			'name2axis', false, ...
			'nameWithIndex', true, ...
			'grabFocus', true);
		progressBar = struct('show', true);
	end
	
	properties (GetAccess = public, SetAccess = private)
		version = "v1.2";
	end
	
	methods (Access = private)
		function obj = Settings()
		end
	end

	methods (Static)
		function obj = instance()
			persistent uniqueObj
			if isempty(uniqueObj)
				obj = quantity.Settings();
				uniqueObj = obj;
			else
				obj = uniqueObj;
			end
		end
	end
	
end

