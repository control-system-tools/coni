classdef Symbolic < quantity.Function
	% quantity.Symbolic allows to perform calculations with matrix-valued functions which were
	% initialized as symbolics. This class is a child of quantity.Discrete and quantity.Function. In
	% combination with quantities of the type quantity.Discrete and quantity.Function one can use
	% discrete data, symbolic variables and function_handles in the same computations with the same
	% syntax. See quantity.example for examples.
	%
	% See also quantity.Symbolic.Symbolic (constructor), quantity.example, quantity.Domain,
	%	quantity.Discrete, quantity.Function.
	
	properties (SetAccess = protected)
		% Symbolic values of the quantity
		valueSymbolic sym;
		% Symbolic variables that occur in valueSymbolic
		variable (:, 1) sym;
	end
	
	properties 
		% If symbolicEvaluation is set true, valueSymbolic is evaluated with symbolic substition 
		% sym/subs. Otherwise, the function_handle of the property valueContinuous is
		% evaluated. By default symbolicEvaluation = false is set, for the sake of faster
		% computations.
		symbolicEvaluation (1,1) logical = false;
	end
	
	methods
		
		function obj = Symbolic(valueContinuous, domain, optArgs)
			% quantity.Symbolic.Symbolic constructs a quantity.Symbolic object, which allows to
			% perform calculations with matrix-valued functions which were initialized as symbolics.
			% See quantity.example for examples.
			%
			%	obj = quantity.Symbolic(value, domain) creates obj which represents a matrix-valued
			%		function which is initialised based on a symbolic array.
			%		- domain must be a quantity.Domain object. To specify multiple domains, use 
			%			domain as an quantity.Domain array.
			%		- value can be specified in two ways:
			%			1) As a double or symbolic array
			%			2) As a cell-array in which every cell-element represents a
			%				matrix element of the original function, end every cell-element contains
			%				a function_handle, symbolic or numeric scalar function.
			%
			%	obj = quantity.Symbolic([...], "name", name) sets the name of the quantity to the 
			%		string name.
			%
			%	obj = quantity.Symbolic([...], "symbolicEvaluation", symbolicEvaluation) set the
			%		logical property symbolicEvaluation. If symbolicEvaluation is set true,
			%		valueSymbolic is evaluated with symbolic substition sym/subs. Otherwise, the
			%		function_handle of the property valueContinuous is evaluated. By default
			%		symbolicEvaluation = false is set, for the sake of faster computations.
			%
			% See also quantity.Symbolic (class description), quantity.example, ...
			%	quantity.Symbolic.ones, quantity.Symbolic.eye, quantity.Symbolic.zeros, ...
			%	quantity.Domain,  quantity.Discrete, quantity.Function.
			arguments
				valueContinuous = []
				domain (1,:) quantity.Domain = quantity.Domain.empty();
				optArgs.name (1,1) string = "";
				optArgs.symbolicEvaluation (1,1) logical = false;
			end % arguments
			
			parentVarargin = {};
			
			if nargin > 0
				
				if isa(valueContinuous, "symfun")
					valueContinuous = simplify( formula(valueContinuous) );
				end
				
				fun = cell(size(valueContinuous));
				symb = cell(size(valueContinuous));
				for k = 1:numel(valueContinuous)
					
					if iscell(valueContinuous)
						fun{k} = valueContinuous{k};
					elseif isa(valueContinuous, "sym") || isnumeric(valueContinuous)
						fun{k} = valueContinuous(k);
					end
					
					if isa(fun{k}, "sym")
						symb{k} = fun{k};
						fun{k} = quantity.Symbolic.setValueContinuous(symb{k}, [domain.name]);
					elseif isnumeric(fun{k})
						symb{k} = sym(fun{k});
						fun{k} = quantity.Symbolic.setValueContinuous(symb{k}, [domain.name]);
					elseif isa(fun{k}, "function_handle")
						symb{k} = sym(fun{k});
					else
						error("valueContinuous has to be symbolic, function_handle or numeric, "...
							+ "but it is a " + class(fun{k}));
					end
				end
				
				parentVarargin = [{fun}, {domain}, {"name"}, {optArgs.name}];
			end
			% call parent class
			obj@quantity.Function(parentVarargin{:});
			
			if nargin > 0
				
				if isempty(obj)
					obj = quantity.Symbolic.empty(size(obj));
				end
				
				% set the remaining properties
				[obj(:).variable] = deal(sym([domain.name]));
				[obj(k).symbolicEvaluation] = deal(optArgs.symbolicEvaluation);
				for k = 1 : numel(valueContinuous)
					obj(k).valueSymbolic = symb{k};
				end
				
				for it = 1 : numel(obj(1).variable)
					assert( any(strcmp(string(obj(1).variable(it)), [domain.name])), ...
						"quantity:Symbolic:Initialisation", ...
						"For every symbolic variable there must be a domain, but there is no " ...
						+ "domain named " + string(obj(1).variable(it)) + ".");
				end % for it = 1 : numel(obj(1).variable)
				
			end % if nargin > 0
			
		end % Symbolic-Constructor
		
		function isEq = eq(A, B)
			%==  Equal.
			%   A == B does element by element comparisons between A and B and returns
			%   an array with elements set to logical 1 (TRUE) where the relation is
			%   true and elements set to logical 0 (FALSE) where it is not. A and B
			%   must have compatible sizes. In the simplest cases, they can be the same
			%   size or one can be a scalar. Two inputs have compatible sizes if, for
			%   every dimension, the dimension sizes of the inputs are either the same
			%   or one of them is 1.
			%	
			%	Two quantity.Symbolic objects are equal if they have the
			%	same symbolic expression, same gridSize and same
			%	variables.
			if isempty(A)
				if isempty(B)
					if isequal(size(A),size(B))
						isEq = true;
						return
					else
						isEq = false;
						return
					end
				else
					isEq = false;
					return;
				end
			else
				if isempty(B)
					isEq = false;
					return
				end
			
				isEq = all(size(A) == size(B));

				if ~isEq
					return;
				end

				isEq = isEq && isequal([A.valueSymbolic], [B.valueSymbolic]);
				isEq = isEq && isequal(A(1).domain, B(1).domain);
				isEq = isEq && isequal(A.variable, B.variable);
			end
		end % eq()
		
		function res = ne(A,B)
			% ~= not equal.
			res = ~(A==B);
		end
		
		function f = function_handle(obj)
			f = quantity.Symbolic.setValueContinuous(obj.sym, [obj(1).domain.name]);
		end % function_handle
		
		function F = quantity.Function(obj, NameValue)
			% quantity.Function casts the quantity.Symbolic obj to the parent class
			% quantity.Function
			%
			%	F = quantity.Function(obj) converts the obj to a quantity.Function F
			%
			%	F = quantity.Function(obj, "name", name) additional changes the name of F to name.
			%
			%	F = quantity.Function(obj, "domain", domain) additional changes the domain of F to
			%		domain.
			arguments
				obj;
				NameValue.domain quantity.Domain = obj(1).domain;
				NameValue.name (1, 1) string = obj(1).name;
			end
			F(numel(obj), 1) = quantity.Function(obj(end).function_handle(), ...
					NameValue.domain, "name", NameValue.name);
			for k = 1 : (numel(obj)-1)
				F(k) = quantity.Function(obj(k).function_handle(), ...
						NameValue.domain, "name", NameValue.name);
			end
			F = reshape(F, size(obj));
		end % quantity.Function()
		
	end % methods
	
	%% mathematical operations
	methods (Access = public)
		function c = cat(dim, a, varargin)
			%CAT Concatenate arrays.
			%   CAT(DIM,A,B) concatenates the arrays of objects A and B
			%   from the class quantity.Discrete along the dimension DIM.
			%   CAT(2,A,B) is the same as [A,B].
			%   CAT(1,A,B) is the same as [A;B].
			%
			%   B = CAT(DIM,A1,A2,A3,A4,...) concatenates the input
			%   arrays A1, A2, etc. along the dimension DIM.
			%
			%   When used with comma separated list syntax, CAT(DIM,C{:}) or 
			%   CAT(DIM,C.FIELD) is a convenient way to concatenate a cell or
			%   structure array containing numeric matrices into a single matrix.
			%
			%   Examples:
			%     a = magic(3); b = pascal(3); 
			%     c = cat(4,a,b)
			%   produces a 3-by-3-by-1-by-2 result and
			%     s = {a b};
			%     for i=1:length(s), 
			%       siz{i} = size(s{i});
			%     end
			%     sizes = cat(1,siz{:})
			%   produces a 2-by-2 array of size vectors.
			%     
			%   See also NUM2CELL.

			%   Copyright 1984-2005 The MathWorks, Inc.
			%   Built-in function.
			if nargin == 1
				objCell = {a};
			else
				% Later, in the parents method cat@quantity.Discrete, it is ensured
				% that all quantites have the same domain.
				objCell = [{a}, varargin(:).'];
			end
			
			% convert double or symbolic inputs into quantity.Symbolic. For this, find a domain
			% frist, then convert them.
			myDomain = quantity.Domain.empty();
			for it = 1 : numel(objCell)
				if isa(objCell{it}, "quantity.Discrete") ...
						&& ~isempty(objCell{it}(1).domain)
					myDomain = objCell{it}(1).domain;
					break;
				end
			end
			
			for it = 1:numel(objCell)
				if ~isempty(objCell{it})% empty inputs are dealt with in cat@quantity.Discrete later
					if (isa(objCell{it}, "double") || isa(objCell{it}, "sym"))
						objCell{it} = quantity.Symbolic(objCell{it}, myDomain);
					end
				end
			end
			c = cat@quantity.Discrete(dim, objCell{:});
		end % cat()
		
		function K = kron(X, Y)
			% kron   Kronecker tensor product.
			%     kron(X,Y) is the Kronecker tensor product of X and Y.
			%     The result is a large matrix formed by taking all possible
			%     products between the elements of X and those of Y. For
			%     example, if X is 2 by 3, then kron(X,Y) is
			%  
			%        [ X(1,1)*Y  X(1,2)*Y  X(1,3)*Y
			%          X(2,1)*Y  X(2,2)*Y  X(2,3)*Y ]
			
			if isnumeric(X)
				xName = "c";
				yName = Y(1).name;
				jointDomain = Y(1).domain;
				xVal = X;
				yVal = Y.sym();
			else
				xName = X(1).name;
				if isa(X, "quantity.Symbolic")
					xVal = X.sym();
				else
					K = kron@quantity.Discrete(X, Y);
					return;
				end
				if isnumeric(Y)
					yName = "c";
					jointDomain = X(1).domain;
					yVal = Y;
				elseif isa(X, "quantity.Symbolic")
					yVal = Y.sym();
					yName = Y(1).name;
					jointDomain = join(X(1).domain, Y(1).domain);
				else
					K = kron@quantity.Discrete(X, Y);
					return;
				end
			end
			
			K = quantity.Symbolic(kron(xVal, yVal), jointDomain, ...
				"name", "kron(" + xName + ", " + yName + ")");
		end % kron()
		
		function solution = subsNumeric(obj, oldDomainName, value)
			% subsNumeric   Numeric substitution.
			%
			% subsNumeric(obj, oldDomainName, value) replaces the old domains of obj specified by the
			% string-array oldDomainName with the numeric values in value.
			%
			% Example: subs(f(z, zeta, t), ["z", "zeta"], [1, 2]) = f(1, 2, t) = g(t).
			
			arguments
				obj quantity.Discrete;
				oldDomainName (:, 1) string;
				value (:, 1) double;
			end % arguments
			
			assert(numel(oldDomainName) == numel(value), "for every domain to be substituted, ", ...
				"there must be defined one numeric scalar value");
						
			remainingDomains = obj(1).domain.remove(oldDomainName);
			solution = subs(obj.sym, misc.str2cell(oldDomainName), value);
			if ~isempty(remainingDomains)
				solution = quantity.Symbolic(solution, remainingDomains, "name", obj(1).name);
			else 
				solution = double(solution);
			end
		end % subsNumeric()
		
		function solution = solveAlgebraic(obj, rhs)
			% solveAlgebraic solves
			%	obj( x ) == rhs
			% for the variable specified x.
			% Both x and rhs must be scalars.
			% The input parameter findBetween specifies minimum and maximum of the
			% values of obj, between which the solution should be searched.
			arguments
				obj (1, 1);
				rhs (1, 1) double;
			end
			assert(obj(1).nargin == 1, "only implemented for quantites on 1 domain");
			warning("off", 'symbolic:solve:FallbackToNumerical')
			symbolicSolution = solve(obj(:).sym == rhs(:), obj(1).variable);
			warning("on", 'symbolic:solve:FallbackToNumerical')
			solution = double(symbolicSolution);
		end % solveAlgebraic()
		
		function solution = solveDVariableEqualQuantity(obj, varargin)
			%% solves the first order ODE
			%	dvar / ds = obj(var(s))
			%	var(s=0) = ic
			% for var(s, ic). Herein, var is the (only) continuous variale
			% obj.variable. The initial condition of the IVP is a variable
			% of the result var(s, ic).
			assert(numel(obj(1).domain) == 1, ...
				"this method is only implemented for quanitities with one domain");
			
			myParser = misc.Parser();
			myParser.addParameter("initialValueGrid", obj(1).domain(1).grid);
			myParser.addParameter("variableGrid", obj(1).domain(1).grid);
			myParser.addParameter("newGridName", "s");
			myParser.parse(varargin{:});
			
			s = sym(myParser.Results.newGridName);
			syms ic varNew(s)
			symbolicSolution = sym(zeros(size(obj)));
			for it = 1:numel(obj)
				symbolicSolution(it) = ...
					dsolve(diff(varNew(s), s) == subs(obj(it).valueSymbolic, ...
					obj(it).variable, varNew(s)), varNew(0)==ic);
			end
			solution = quantity.Symbolic(symbolicSolution, ...
				[quantity.Domain(myParser.Results.newGridName, myParser.Results.variableGrid), ...
					quantity.Domain("ic", myParser.Results.initialValueGrid)], ...
				"name", "solve(" +  obj(1).name + ")");
		end % solveDVariableEqualQuantity()
		
		function sym = sym(obj)
			% sym returns valueSymbolic of a quantity.Symbolic-array.
			sym = reshape([obj.valueSymbolic], size(obj));
		end % sym()
				
		function s = sum(obj, dim)
			% sum Sum of elements
			% s = sum(X) is the sum of all elements of the array X.
			% s = sum(X, DIM) is the sum along the dimensions specified by DIM.
			arguments
				obj;
				dim = 1:ndims(obj);
			end % arguments
			if ndims(obj) <= 2
				s = quantity.Symbolic(sum(obj.sym(), dim), obj(1).domain, ...
					"name", "sum(" + obj(1).name + ")");
			else
				% the nD-array is permuted and reshaped, such that the dimension which is summed is
				% the first dimension, and all the other dimensions are stacked in the 2nd
				% dimension. Then the first dimension is summed up and dimensions a are sorted back
				% in the original order.
				sDims = 1:ndims(obj);
				sSize = size(obj);
				sSym = obj.sym();
				nonDimSelector = all(sDims ~= dim.', 1);
				sSym = permute(sSym, [dim, sDims(nonDimSelector)]);
				sSym = reshape(sSym, [prod(sSize(dim)), prod(sSize(nonDimSelector))]);
				sSum = sum(sSym, 1);
				sSum = reshape(sSum, [ones(1, numel(dim)), sSize(nonDimSelector)]);
				sSum = ipermute(sSum, [dim, sDims(nonDimSelector)]);
				s = quantity.Symbolic(sSum, obj(1).domain, ...
					"name", "sum(" + obj(1).name + ")");
			end
		end % sum()
		
		function P = prod(obj, dim)
			% prod Product of elements
			% P = prod(X) is the product of all elements of the vector X.
			% P = prod(X, DIM) is the product along the dimensions specified by DIM.
			arguments
				obj;
				dim = 1:ndims(obj);
			end % arguments
			
			if ndims(obj) <= 2
				P = quantity.Symbolic(prod(obj.sym(), dim), obj(1).domain, ...
					"name", "prod(" + obj(1).name + ")");
			else
				% the nD-array is permuted and reshaped, such that the dimension which is summed is
				% the first dimension, and all the other dimensions are stacked in the 2nd
				% dimension. Then the first dimension is summed up and dimensions a are sorted back
				% in the original order.
				pDims = 1:ndims(obj);
				pSize = size(obj);
				pSym = obj.sym();
				nonDimSelector = all(pDims ~= dim.', 1);
				pSym = permute(pSym, [dim, pDims(nonDimSelector)]);
				pSym = reshape(pSym, [prod(pSize(dim)), prod(pSize(nonDimSelector))]);
				pProd = prod(pSym, 1);
				pProd = reshape(pProd, [ones(1, numel(dim)), pSize(nonDimSelector)]);
				pProd = ipermute(pProd, [dim, pDims(nonDimSelector)]);
				P = quantity.Symbolic(pProd, obj(1).domain, ...
					"name", "sum(" + obj(1).name + ")");
			end
		end % prod()
		
		function P = power(obj, p)
			% a.^p elementwise power
			P = quantity.Symbolic(power(obj.sym(), p), obj(1).domain, ...
				"name", obj(1).name + ".^{" + num2str(p) + "}");
		end % power()
		
		function P = mpower(obj, p)
			% Matrix power a^p is matrix or scalar a to the power p.
			P = quantity.Symbolic(mpower(obj.sym(), p), obj(1).domain, ...
				"name", obj(1).name + "^{" + num2str(p) + "}");
		end % mpower()
		
		function d = det(obj)
			% det(X) returns the the determinant of the squre matrix X
			if isempty(obj)
				d = quantity.Discrete.empty(1);
			else
				d = quantity.Symbolic(det(obj.sym()), obj(1).domain, ...
					"name", "det(" + obj(1).name + ")");
			end
		end % det()
		
		function y = sqrt(x)
			% quadratic root for scalar and diagonal symbolic quantities
			y = quantity.Symbolic(sqrt(x.sym()), x(1).domain, ...
				"name", "sqrt(" + x(1).name + ")");
		end % sqrt()
		
		function y = sqrtm(x)
			% quadratic root for matrices of symbolic quantities
			y = quantity.Symbolic(sqrtm(x.sym()), x(1).domain, ...
				"name", "sqrtm(" + x(1).name + ")");
		end % sqrtm()
		
		function b = flipDomain(obj, flipDomainName)
			% flipDomain implements a flip of the domain, for example with a(z), z \in [0, 1]
			% b(z) = a(1-z) = a.flipDomain('z');
			% or for the multidimensional case with c(z, zeta)
			% d(z, zeta) = c(1-z, 1-zeta) = c.flipDomain(["z", "zeta"];
			% if z and zeta are domains on (0,1).
			%
			% See also quantity.Discrete.changeDomain.
			arguments
				obj
				flipDomainName string;
			end
		
			idx = obj(1).domain.index( flipDomainName );
			flippedDomainName(numel(idx)) = obj(1).domain(idx(numel(idx))).upper ...
				- sym(flipDomainName(numel(idx)));
			for k = 1 : (numel(idx)-1)
				flippedDomainName(k) = obj(1).domain(idx(k)).upper - sym( flipDomainName(k) );
			end
			
			b = quantity.Symbolic(subs(obj.sym, num2cell( flipDomainName ), ...
				num2cell( flippedDomainName ) ), obj(1).domain, ...
				"name", "flip(" + obj(1).name + ")");
		end % flipDomain()
				
		function mObj = uminus(obj)
			% unitary minus: C = -A
			mObj = quantity.Symbolic(-obj.sym, obj(1).domain, "name", "-" + obj(1).name);
		end % uminus()
		
		function mObj = uplus(obj)
			% unitary plus: C = +A
			mObj = copy(obj);
		end % uplus()
 		
		function C = mtimes(a, b)
			%  *   Matrix multiply (mtimes).
			%     X*Y is the matrix product of X and Y.  Any scalar (a 1-by-1 matrix)
			%     may multiply anything.  Otherwise, the number of columns of X must
			%     equal the number of rows of Y.
			arguments
				a {mustBe.matrix(a)};
				b {mustBe.matrix(b)};
			end
			
			% if one input is ordinary matrix, this is very simple.
			if isempty(b) ||  isempty(a)
				C = quantity.Symbolic( mtimes@quantity.Discrete(a, b) );
				return
			end
			if isnumeric(b)
				C = quantity.Symbolic(a.sym() * b, a(1).domain, ...
					"name", a(1).name + " c");
				return
			end
			if isnumeric(a)
				C = quantity.Symbolic(a * b.sym(), b(1).domain, ...
					"name", "c " + b(1).name);
				return
			end
			if ~(isa(b, "quantity.Symbolic")) && isa(b, "quantity.Function")
				C = (b.' * a.').';
				return;
			end
			
			parameters.name = a(1).name + " " + b(1).name;
			parameters = misc.struct2namevaluepair(parameters);
			
			C = quantity.Symbolic(a.sym() * b.sym(), join(a(1).domain, b(1).domain), ...
				parameters{:});
		end % mtimes()
		
		function C = plus(a, b)
			%  +   Plus.
			%     A + B adds matrices A and B
			assert(isequal(size(a), size(b)), "terms must be of same size");
			if ~isa(a, "quantity.Discrete")
				if isnumeric(a)
					a = quantity.Symbolic(a, quantity.Domain.empty(), "name", "c");
				else
					error("Not yet implemented")
				end					
			else
				if ~isa(b, "quantity.Discrete")
					if isnumeric(b)
						b = quantity.Symbolic(b, quantity.Domain.empty(), "name", "c");
					else
						error("Not yet implemented")
					end					
				end
			end
					
			C = quantity.Symbolic(a.sym() + b.sym(), join(a(1).domain, b(1).domain), ...
				"name", a(1).name + "+" + b(1).name);
		end % plus()
		
		function absQuantity = abs(obj)
			% abs returns the absolut value of the quantity as a quantity
			absQuantity = quantity.Symbolic(abs(obj.sym()), obj(1).domain, ...
				"name", "|" + obj(1).name + "|");	
		end % abs()
		
		function y = real(obj)
			% real() returns the real part of the obj.
			y = quantity.Symbolic(real(obj.sym()), obj(1).domain, ...
				"name", "real(" + obj(1).name + ")");
		end % real()
		
		function y = imag(obj)
			% imag() returns the real part of the obj.
			y = quantity.Symbolic(imag(obj.sym()), obj(1).domain, ...
				"name", "imag(" + obj(1).name + ")");
		end % imag()
		
		function y = inv(obj)
			% inv inverts the matrix obj.
			y = quantity.Symbolic(inv(obj.sym()), obj(1).domain, ...
				"name", "(" + obj(1).name + ")^{-1}");
		end % inv()
		
		function y = exp(obj)
			% exp() is the exponential function using obj as the exponent.
			y = quantity.Symbolic(exp(obj.sym()), obj(1).domain, ...
				"name", "exp(" + obj(1).name + ")");
		end % exp()
		
		function y = expm(obj)
			% exp() is the matrix-exponential function using obj as the exponent.
			y = quantity.Symbolic(expm(obj.sym()), obj(1).domain, ...
				"name", "expm(" + obj(1).name + ")");
		end % expm()
		
		function y = sin(obj)
			% sin    Sine of argument in radians.
			%	sin(X) is the sine of the elements of X.
			%
			% See also quantity.Symbolic.cos
			
			y = quantity.Symbolic(sin(obj.sym()), obj(1).domain, ...
				"name", "sin(" + obj(1).name + ")");
		end % sin()
		
		function y = cos(obj)
			% cos    Cosine of argument in radians.
			%	cos(X) is the cosine of the elements of X.
			%
			% See also quantity.Symbolic.sin
			
			y = quantity.Symbolic(cos(obj.sym()), obj(1).domain, ...
				"name", "cos(" + obj(1).name + ")");
		end % cos()
		
		function y = log(obj)
		%  log    Natural logarithm.
		%     log(X) is the natural logarithm of the elements of X.
		%     Complex results are produced if X is not positive.
			y = quantity.Symbolic(log(obj.sym()), obj(1).domain, ...
				"name", "log(" + obj(1).name + ")");
		end % log()
		
		function y = log10(obj)
		% log10  Common (base 10) logarithm.
		% log10(X) is the base 10 logarithm of the elements of X.   
		% Complex results are produced if X is not positive.
			y = quantity.Symbolic(log10(obj.sym()), obj(1).domain, ...
				"name", "log10(" + obj(1).name + ")");
		end % log10()
		
		function y = ctranspose(obj)
			% ctranspose() or ' is the complex conjugate tranpose
			y = quantity.Symbolic(conj(obj.sym().'), obj(1).domain, ...
				"name", "{" + obj(1).name + "}^{H}");
		end % expm()
			
		function C = int(obj, domain, lowerBound, upperBound)
			% int symbolic integration
			%	
			%	result = int(obj) integrates obj all its domains from its lower bound to the upper
			%		bound.
			%	
			%	result = int(obj, domain) integrates obj over the domain from its lower bound to 
			%		the upper bound. To integrate over multiple domains, specify domain as a
			%		string-array.
			%	
			%	result = int(obj, domainName, lowerBound, upperBound) integrates obj over 
			%		the integration domain from the lowerBound to the upperBound.
			%		- domain must be a string(-array) coinciding with the domain of the integrated 
			%			obj. 
			%		- lowerBound and upperBound define the boundaries of the integration domain. 
			%			These can be either doubles for definite integrals or a domain specified by
			%			a string or a quantity.Domain for an indefinite intergral. If lowerBound or
			%			upperBound are variable, then the result will contain a related domain.
			%			In order to perform integration over multiple domains, specify lowerBound
			%			and upperBound as cell-arrays.
			
			arguments
				obj;
				domain string = [obj(1).domain.name];
				lowerBound = {obj(1).domain.find(domain).lower};
				upperBound = {obj(1).domain.find(domain).upper};
			end
			
			lowerBound = misc.ensureIsCell(lowerBound);
			upperBound = misc.ensureIsCell(upperBound);
			
			assert(numel(domain) == numel(lowerBound), ...
				"dimension of lowerBound must be equal to dimension of domain");
			assert(numel(domain) == numel(upperBound), ...
				"dimension of upperBound must be equal to dimension of domain");
			
			% get the desired domain. This step is necessary, as the input argument domain can be
			% either a string or a quantity.Domain object.
			domain = obj(1).domain.find(domain);
			
			if obj.nargin == numel(domain) ...
					&& isnumeric([lowerBound{:}]) && isnumeric([upperBound{:}])
				% if the integration has fixed upper and lower bounds, and should be applied for all
				% independent variables, use the implementation based on function handles, because
				% it is much faster:
				C = int@quantity.Function(obj, [domain.name], lowerBound, upperBound);
			else
				try
					% try to compute the symbolic integration
					if isnumeric(lowerBound) && isnumeric(upperBound)
						% if both bounds a numeric the integral can be copmuted directly with both
						% bounds. Then, the integral domain must be removed:
						symbolicIntegral = int(obj.sym, domain.name, lowerBound, upperBound);
						remainingVarialbes = symvar(symbolicIntegral);
						if isempty(remainingVarialbes)
							C = double(symbolicIntegral);
						else
							C = quantity.Symbolic(symbolicIntegral, obj(1).domain.remove(domain));
						end
					else
						% sometimes symbolic integration results in indefinite integrals, which can
						% not used afterwards. Hence, this case causes an error in order to reach
						% the catch-block below
						warning("off", "warning:replace:lastwarn:storage");
						warning("warning:replace:lastwarn:storage", "replace lastwarn");
						warning("off", "symbolic:generate:IndefiniteIntegral");
						% integrate w.r.t. all integration variables one by one
						symbolicIntegral = obj.sym;
						for it = 1 : numel(domain)
							symbolicIntegral = int(symbolicIntegral, domain(it).name);
						end % for it = 1 : numel(domain)
						warning("on", "symbolic:generate:IndefiniteIntegral");
						warning("on", "warning:replace:lastwarn:storage");
						[myWarn, myWarnId] = lastwarn();
						assert(~strcmp(myWarnId, "symbolic:generate:IndefiniteIntegral"), ...
							myWarn + " -> Using int-method of quantity.Function or *.Discrete instead.")
						C = quantity.Symbolic(symbolicIntegral, obj(1).domain);
						C = subs(C, domain.name, upperBound) - ...
							subs(C, domain.name, lowerBound);					
					end
				catch
					try
						% try to compute the integration by function
						% handles
						C = int@quantity.Function(obj, [domain.name], lowerBound, upperBound);
						
					catch
						% last fall back to compute the integration by
						% discrete values. This should work always
						C = int(quantity.Discrete(obj), domain, lowerBound, upperBound);
					end
				end % try-catch
			end % if-else
		end % int
		
	end % methods (Access = public)
	
	methods (Access = protected)
		
		function v = evaluateFunction(obj, varargin)
			% Evaluates the symbolic expression of this quantity. If the
			% flag symbolicEvaluation is set true, the expression is evaluated
			% with subs(...) otherwise a function handle is used.
			if obj.symbolicEvaluation
				v = double(subs(sym(obj), obj.variable, varargin{:}));
			else
				v = evaluateFunction@quantity.Function(obj, varargin{:});
			end
		end % evaluateFunction

		function cpObj = copyElement(obj)
			% copyElement Make a shallow copy of all properties
			cpObj = copyElement@quantity.Discrete(obj);
		end % copyElement()
		
		function result = diff_inner(obj, k, diffGridName)
			% diff_inner is a helper function for quantity.Symbolic.diff.
			result = obj.copy();
			result.setName(deal("(d_{" + diffGridName +"}" + result(1).name + ")"));
			[result.valueDiscrete] = deal([]);
			for l = 1:numel(obj)
				result(l).valueSymbolic = diff(obj(l).valueSymbolic, diffGridName, k);
				result(l).valueContinuous = obj.setValueContinuous(result(l).valueSymbolic, ...
					[obj(1).domain.name]);
			end
		end % diff_inner()
		
		function obj = subsDomainMerge(obj, oldDomainName, newDomain)
			% subsDomainMerge()	Symbolic substitution for which 2 domains need to be merged.
			% Example:
			%	z = quantity.Domain("z", linspace(0, 1, 11));
			%	zLr = quantity.Domain("z", linspace(0, 1, 5));
			%	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
			%	f = z.Discrete() + zeta.Discrete()	% = f(z, zeta)
			%	f.subsDomainMerge("zeta", zLr)		% = f(z) 
			%			-> the finest grid is chosen in the latter case, hence z and not zLr.
			% This is a helper function for subsDomain, hence it is protected.
			
			% find domain to be merged
			[idxOldDomain, idxOldDomainLogical] = index(obj(1).domain, oldDomainName);
			[idxNewDomain, idxNewDomainLogical] = index(obj(1).domain, newDomain.name);
			
			% pick finest grid.
			newDomain = join(join(newDomain, obj(1).domain(idxNewDomain)), ...
				obj(1).domain(idxOldDomain).rename(newDomain.name));
			newDomainComplete = [newDomain, obj(1).domain(~(idxOldDomainLogical | idxNewDomainLogical))];
			
			% create new obj
			obj = quantity.Symbolic(subs(obj.sym, oldDomainName, newDomain.name), ...
				newDomainComplete, "name", obj(1).name);
		end % subsDomainMerge()
		
		function obj = subsDomainRename(obj, oldDomainName, newDomain)
			% subsDomainMerge()	Symbolic substitution for which 2 domains need to be merged.
			% Example:
			%	z = quantity.Domain("z", linspace(0, 1, 11));
			%	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
			%	f = z.Discrete()					% = f(z)
			%	f.subsDomainRename("z", zeta)		% = f(zeta) 
			%			-> the finest grid is chosen in the latter case, hence z and not zLr.
			% This is a helper function for subsDomain, hence it is protected.
			
			oldDomainNameNewGrid = quantity.Domain(oldDomainName, newDomain.grid);
			obj = obj.changeDomain(oldDomainNameNewGrid);
			newDomainComplete = obj(1).domain.rename(newDomain.name, oldDomainName);
			obj = quantity.Symbolic(subs(obj.sym, oldDomainName, newDomain.name), ...
				newDomainComplete, "name", obj(1).name);
		end % subsDomainRename()
		
	end % (Access = protected)
	
	methods (Static)
		
		function P = zeros(valueSize, varargin)
			% zeros initializes an zero quantity.Discrete object of the size
			%specified by the input valueSize.
			P = quantity.Symbolic(zeros(valueSize), varargin{:});
		end % zeros()
		
		function P = ones(valueSize, varargin)
			% ones initializes an ones-quantity.Discrete object
			%	P = ones(VALUESIZE, DOMAIN) creates a matrix of size
			%	VALUESIZE on the DOMAIN with ones as entries.
			P = quantity.Symbolic(ones(valueSize), varargin{:});
		end % ones()
		
		function P = eye(N, varargin)
			%  eye Identity matrix.
			%     eye(N, domain) is the N-by-N identity matrix on the domain domain.
			%  
			%     eye([M,N], domain) is an M-by-N matrix with 1's on the diagonal and zeros 
			%		elsewhere.
			%     eye([M,N], domain, varargin) passes further inputs to the quantity.Discrete
			%     constructor.
			P = quantity.Symbolic(eye(N), varargin{:});
		end % eye()

	end % methods (Static)
	
	methods (Static, Access = protected)
		
		function [f, functionArguments] = setValueContinuous(f, symVar)
			% setValueContinuous converts a symbolic f into a matlabFunction.
			%
			%	f = setValueContinuous(f, symVar)
			arguments
				f;
				symVar (:, 1) string;
			end
			
			if isa(f, "sym")			
				if isempty(symvar(f))
					% convert the name of the symbolic variables into a comma separated list and
					% then create a function handle by building the function string and evaluate it
					% in matlab. That the function returns the right dimension is ensured by the
					% "zero" function.
					functionArguments = strjoin( string(symVar), ",");
					f = eval("@(" + functionArguments + ") " + ...
						"double(f) + quantity.Function.zero(" + functionArguments + ")");

				else
					f = matlabFunction(f, "Vars", convertStringsToChars( symVar ) );
				end
			end
		end % setValueContinuous()
		
	end % methods (Static, Access = protected)
end % classdef
