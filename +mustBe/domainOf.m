function domainOf(quan, domain)
% mustBe.domainOf(quan, domain) Validate that the domain is a domain of quan. Otherwise, an error is
% thrown.
%
%	mustBe.domainOf(quan, domain) asserts that quan (type: quantity.Discrete) is defined on the
%		domain (type: string or quantity.Domain).
arguments
	quan quantity.Discrete;
	domain (1, 1) string; % casts quantity.Domain into string
end % arguments

assert(any(strcmp([quan(1).domain.name], domain)), ...
	"mustBe:domainOf:domainNotFound", "The domain is not a domain of the considered quantity.");
end %mustBe.matrix()