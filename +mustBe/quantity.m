function quantity(A)
%mustBe.quantity 
%

warning('DEPRICATED? Is this mustBe.quantity function really required?')

   if ~isempty(A) && ~isa(A, 'quantity.Discrete') 
      error('Value assigned to Data property must be a quantity.Discrete.')
   end
end