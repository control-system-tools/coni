function diagonal(A)
%mustBe.diagonal Validate that A is diagonal Matrix or else throw an error.
%
% history:
%   -   created on 14.09.2018 by Jakob Gabriel

	if ~isdiag(A)
		error('Value assigned to Data property must be diagonal.');
	end
end