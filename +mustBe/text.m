function text(A)
%mustBe.text verifies if A is a string or a char 

	if ~(isstring(A) || ischar(A))
		error('Value assigned to Data property must be a string or a char.');
	end
end
