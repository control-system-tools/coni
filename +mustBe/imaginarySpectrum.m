function imaginarySpectrum(A)
%mustBe.imaginarySpectrum asserts that the eigenvalues of the matrix A are
% solely imaginary.

   assert(all(real(eig(A)) == 0, 'all'), ...
	   "Spectrum of matrix is not imaginary, but has nonzero real part")
end % imaginarySpectrum()