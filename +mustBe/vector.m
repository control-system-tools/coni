function vector(A)
%mustBe.vector Validate that A is vector or else throw an error.

assert(isvector(A), 'Value assigned to Data property must be 2-dimensional and quadratic.');

end % mustBe.vector()