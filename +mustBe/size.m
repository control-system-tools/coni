function size(A, dim)
%mustBe.size Verifies if A has the size dim
% size(A, dim) throws an error if A is not of size dim. This is usefull to validate the size of
% input arguments. 

assert( numel(A) == prod(dim) && all( size(A) == dim ), ...
	"conI:mustBe", ...
	"Value assigned to Data property must be size " + misc.vector2string(dim) + ...
	" . However, it is of size " + misc.vector2string( size(A) ) + ".");

end % mustBe.vector()