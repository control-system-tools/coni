function matrix(A)
%mustBe.matrix(A) Validate that A is a matrix or else throw an error.
	assert(ismatrix(A), "mustBe:matrix:noMatrix", "Value assigned to Data property must be a matrix.")
end %mustBe.matrix()