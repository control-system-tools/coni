function [tests] = femMatricesTest()
tests = functiontests(localfunctions);
end

function mTest(testCase)

spatialDomain = quantity.EquidistantDomain("z", 0, 1, 'stepNumber', 3);
I = quantity.Discrete.ones(1, spatialDomain);

[M, D2, L, P] = numeric.femMatrices(spatialDomain, ...
	'alpha', quantity.Discrete.ones(1, spatialDomain), ...
	'b', I, 'c', I, "silent", true);

[~, D1] = numeric.femMatrices(spatialDomain, ...
	'beta', quantity.Discrete.ones(1, spatialDomain), "silent", true);

[~, D0] = numeric.femMatrices(spatialDomain, ...
	'gamma', quantity.Discrete.ones(1, spatialDomain), "silent", true);



M_ = spatialDomain.stepSize / 6 * ...
	[2, 1, 0; ...
	 1, 4, 1; ...
	 0,	1, 2];
 
D2_ = 1 / spatialDomain.stepSize * ...
	[-1  1  0; ...
	  1 -2  1; ...
	  0  1 -1];

D1_ = 1 / 2 * ...
	[-1  1 0; ...
	 -1  0 1; ...
	  0 -1 1];
  
D0_ = spatialDomain.stepSize / 6 * ...
	[2, 1, 0; ...
	 1, 4, 1; ...
	 0,	1, 2];
  
testCase.verifyEqual( M, M_, 'AbsTol', 10*eps );
testCase.verifyEqual( D2, D2_, 'AbsTol', 10*eps);
testCase.verifyEqual( D1, D1_, 'AbsTol', 10*eps);
testCase.verifyEqual( D0, D0_, 'AbsTol', 5e-5);
testCase.verifyEqual( L, M_ * I.on(), 'AbsTol', 10*eps);
testCase.verifyEqual( P, I.on()' * M_, 'AbsTol', 10*eps);
end

function compareMassMatrix(tc)
spatialDomain = quantity.Domain("z", [0, 0.1, 0.15, 0.2, 0.3, 0.4]);
M = numeric.femMatrices(spatialDomain, "silent", true);
M2 = numeric.massMatrix(spatialDomain);
tc.verifyEqual(M, M2, "AbsTol", 10*eps);
end % compareMassMatrix