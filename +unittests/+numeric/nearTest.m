
function [tests] = nearTest()
tests = functiontests(localfunctions);
end

function testScalarValues(testCase)
verifyTrue(testCase, numeric.near(sqrt(25), 5));
verifyFalse(testCase, numeric.near(sqrt(23), 5));
end

function testVectorValues(testCase)

%%
z = (0:0.4:pi).';
verifyTrue(testCase, numeric.near(sqrt(z.^2), z));

end

function testMatrix(testCase)
%%
z = (0:0.4:pi).';
M = [sin(z).^2 + cos(z).^2, sin(z) ./ cos(z), 1 + tan(z).^2; ...
     sqrt(1 - cos(z).^2), sqrt(1 - cos(z).^2) ./ cos(z), sqrt(z.^2)];
    

Mtest = [ones(size(z)), tan(z), 1 ./ cos(z).^2; ...
           sin(z), tan(z), z];
       
verifyTrue(testCase, numeric.near(M, Mtest));
         


end


function testIntersect(testCase)

a = rand(5,5);
b = rand(7,9);

a(1) = 1 + 1e-10;
a(7) = 2;

b(1) = 1;
b(8) = 2;

c = numeric.intersect( a, b, "AbsTol", 1e-9);
testCase.verifyEqual( c, [1, 2], "AbsTol", 1e-9 );

end
