function [tests ] = testPiecewise()
% TESTQUANTITY Unittests for the quantity objects
%   Add all unittests for the quantity files here
tests = functiontests(localfunctions());
end

function testInit(testCase)

t1 = quantity.Domain('t', 1:3);
t2 = quantity.Domain('t', 3:5);
t3 = quantity.Domain('t', 5:7);

f1 = quantity.Discrete(t1.grid, t1);
f2 = quantity.Discrete(flip(t1.grid), t2);
f3 = quantity.Discrete(t1.grid, t3);

F = quantity.Piecewise({ f1 f2 f3});

testCase.verifyEqual(F.valueDiscrete, [1 2 3 2 1 2 3]')

end

function testAssemblingPoint(testCase)

t1 = quantity.Domain('t', 1:3);
t2 = quantity.Domain('t', 3:5);
t3 = quantity.Domain('t', 5:7);

f1 = quantity.Discrete(1 * ones(t1.n,1), t1);
f2 = quantity.Discrete(2 * ones(t2.n,1), t2);
f3 = quantity.Discrete(3 * ones(t3.n,1), t3);

F = quantity.Piecewise({f1 f2 f3}, 'upperBoundaryIncluded', [true, false]);

testCase.verifyEqual(F.at(t1.upper), 1);
testCase.verifyEqual(F.at(t2.upper), 3);

end


function testC0Functions(testCase)

t1 = quantity.Domain('t', 1:3);
t2 = quantity.Domain('t', 3:5);
t3 = quantity.Domain('t', 5:7);

f1 = quantity.Discrete(1 * ones(t1.n,1), t1);
f2 = quantity.Discrete(2 * ones(t2.n,1), t2);
f3 = quantity.Discrete(3 * ones(t3.n,1), t3);

F = quantity.Piecewise({f1 f2 f3}, "addPoint", true);

testCase.verifyEqual( F.on(), [1 1 1 2 2 2 3 3 3].')

F2 = quantity.Piecewise({f1 f2 f3}, "addPoint", true, "upperBoundaryIncluded", [true, false]);
testCase.verifyEqual( F2.at(3), 1);
testCase.verifyEqual( F2.at(5), 3);
end


function testC0FunctionsIntegration(testCase)

t1 = quantity.Domain('t', 1:3);
t2 = quantity.Domain('t', 3:5);
t3 = quantity.Domain('t', 5:7);

f1 = quantity.Discrete(0 * ones(t1.n,1), t1);
f2 = quantity.Discrete(1 * ones(t2.n,1), t2);
f3 = quantity.Discrete(0 * ones(t3.n,1), t3);

F = quantity.Piecewise({f1 f2 f3}, "addPoint", true);

testCase.verifyEqual( int(F), 2);

end

function testDiffPiecewise(tc)
% 
t0 = 1;
T = 2;
t = quantity.Domain("t", linspace(0, 4, 401));
tSplit = t.split(t0+[0, T/2, T]);
% trajectory for an output
y1s = quantity.Piecewise({...
	quantity.Symbolic.zeros(1, tSplit(1)), ...
	(2*(tSplit(2).Symbolic()-t0)/T)^2, ...
	1 + 4*(tSplit(3).Symbolic()-t0-T/2)/T - (2*(tSplit(3).Symbolic()-t0-T/2)/T)^2, ...
	2*quantity.Symbolic.ones(1, tSplit(4))}, ...
	"addPoint", false);
y2s = quantity.Piecewise({...
	quantity.Symbolic.zeros(1, tSplit(1)), ...
	(tSplit(2).Symbolic()-t0)/T, ...
	0.5 + (tSplit(3).Symbolic()-t0-T/2)/T, ...
	quantity.Symbolic.ones(1, tSplit(4))}, ...
	"addPoint", false);
ys = setName([y1s; y2s], "ys");

% differential parametrization of some control input
% a) evaluate using diffPicewise
u1sA = -0.25*ys(1).diffPiecewise("t", 2) + 0.25*ys(1).diffPiecewise("t", 1) - 2*ys(1);
u2sA = -0.25*ys(1).diffPiecewise("t", 2) + 0.25*ys(1).diffPiecewise("t", 1) + ys(2).diffPiecewise("t", 1) - ys(2);
usA = setName([u1sA; u2sA], "usA");
% b) evaluate using diff
ysDiscrete = quantity.Discrete(ys);
u1sB = -0.25*ysDiscrete(1).diff("t", 2) + 0.25*ysDiscrete(1).diff("t", 1) - 2*ysDiscrete(1);
u2sB = -0.25*ysDiscrete(1).diff("t", 2) + 0.25*ysDiscrete(1).diff("t", 1) + ysDiscrete(2).diff("t", 1) - ysDiscrete(2);
usB = setName([u1sB; u2sB], "usB");

% plot([usA, usB, usA-usB]);

% check if results are similar but not equal
tc.verifyTrue(mean(abs(usA-usB), "all") <= 5e-3);
tc.verifyTrue(mean(abs(usA-usB), "all") >= 10*eps);
tc.verifyTrue(median(abs(usA-usB), "all") == 0);
end % testDiffPiecewise

















