To run multiple unittests in packages use:
	result = run(matlab.unittest.TestSuite.fromPackage('unittests', 'IncludingSubpackages', true))
If you want to run only the tests for a subpackage, you can use the code:
	result = run(matlab.unittest.TestSuite.fromPackage('unittests.<subpackage name>', 'IncludingSubpackages', true))
	
If you want to run only a single test of a subpackage, you can use the helper function
	misc.runSingleTest(testSuite, testName)

Some tests need reference results or save data locally in order to improve speed. 
This is usually done in a setupOnce() function. Therefore, calling unittests for the first time, might take
a lot longer then afterwards.
