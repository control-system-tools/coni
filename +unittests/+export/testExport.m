function [tests] = testExport()

tests = functiontests(localfunctions);

end

function nameValuePairstest(tc)

% verify the normal initialization
p = export.namevalues(["a", "b", "c"], {1, 2, "c"}, ...
	"basepath", "+unittests", "foldername", "+export", "filename", "testNameValuePairs");
p.export;

tc.verifyEqual( readtable( p.path ), ...
	readtable( fullfile( p.basepath, p.foldername, "verifyNameValuePairs.dat")) );

delete(p.path)

% verify the conversion of a structure
p2 = export.namevalues.struct2namevalues(struct(  "a", 1, "b", 2, "c", "c" ), ...
	"basepath", "+unittests", "foldername", "+export", "filename", "testNameValuePairsStructure");
p2.export;

tc.verifyEqual( readtable( p2.path ), ...
	readtable( fullfile( p2.basepath, p2.foldername, "verifyNameValuePairs.dat")) );

delete(p2.path);

end

function ddtest(tc)

t = (0:0.2:10)';            % Darstellung als Spaltenvektor ist wichtig!
y1 = sin(t);
y2 = cos(t);

data = export.dd(...
	'M', [t, y1, y2], ...
	'header', {'t', 'y1', 'y2'}, ...
	'filename', 'plot', ...
	"basepath", "+unittests", "foldername", "+export");

data.export();

tc.verifyEqual( readtable( data.path ), ...
	readtable( fullfile( data.basepath, data.foldername, "verifyPlot.dat")) );

delete(data.path);

end

function dddtest(tc)

t = (0:0.2:10)';
z = (0:20)';

[X, Y] = meshgrid(t , z);
w = sin(X) + cos(Y);
% surf(X,Y,w)

data = export.ddd(...
	z, t, w, ...
	'filename', 'surf', ...
	"basepath", "+unittests", "foldername", "+export", ...
	'N', 60);

data.export();

tc.verifyEqual( readtable( data.path ), ...
	readtable( fullfile( data.basepath, data.foldername, "verifySurf.dat")) );

delete(data.path);

end