function [tests] = testKronColumnwise()
	tests = functiontests(localfunctions);
end

function testKron(tc)
A = 1:1:(2^2); A = reshape(A, sqrt(numel(A)) * [1, 1]);
B = magic(2);
tc.verifyEqual(misc.kronColumnwise(A, B), kron(A, B));
end % testKron()

function testBVector(tc)
A = [1, 2; 3, 4];
A11 = A(1, 1);
A12 = A(1, 2);
A21 = A(2, 1);
A22 = A(2, 2);

B = [1, 2, 3];
B1 = B(1, 1);
B2 = B(1, [2, 3]);
tc.verifyEqual(misc.kronColumnwise(A, B, [1, 1], [1, 2]), ...
	[kron(A11, B1), kron(A12, B2); kron(A21, B1), kron(A22, B2)])
end % testBVector()

function testMatrix(tc)
A = 1:1:(4^2); A = reshape(A, sqrt(numel(A)) * [1, 1]);
A11 = A(1:2, 1:2);
A12 = A(1:2, 3:4);
A21 = A(3:4, 1:2);
A22 = A(3:4, 3:4);

B = [1, 2, 3; 4, 5, 6]+2;
B1 = B(:, 1);
B2 = B(:, [2, 3]);
tc.verifyEqual(misc.kronColumnwise(A, B, [2, 2], [1, 2]), ...
	[kron(A11, B1), kron(A12, B2); kron(A21, B1), kron(A22, B2)])
end % testMatrix()