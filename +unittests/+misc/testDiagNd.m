%% test misc.diagNd.m
function [tests] = testDiagNd()
	% result = runtests('unittests.misc.testDiagNd')
	tests = functiontests(localfunctions);
end

function testDiag2d(tc)
%% Matrix
testData = rand(123, 123);
tc.verifyEqual(diag(testData), misc.diagNd(testData));
end

function testDiagNdNonSquare(tc)
testData = ones([5, 4]);
tc.verifyEqual(misc.diagNd(testData), diag(testData));
testData = ones([3, 4]);
tc.verifyEqual(misc.diagNd(testData), diag(testData));
end % testDiagNdNonSquare

function testDiagNdComplicated(tc)
test1.data = rand(2, 3, 2, 4, 2, 5);
test2.data = rand(3, 2, 2, 4, 2, 5);
test3.data = rand(2, 3, 2, 4, 5, 2);

test1.result = misc.diagNd(test1.data, [1, 3, 5]);
test2.result = misc.diagNd(test2.data, [2, 3, 5]);
test3.result = misc.diagNd(test3.data, [1, 3, 6]);

test1.forResult = zeros(2, 3, 4, 5);
test2.forResult = zeros(2, 3, 4, 5);
test3.forResult = zeros(2, 3, 4, 5);
for it = 1 : 2
	test1.forResult(it,:,:,:) = squeeze(test1.data(it, :, it, :, it, :));
	test2.forResult(it,:,:,:) = squeeze(test2.data(:, it, it, :, it, :));
	test3.forResult(it,:,:,:) = squeeze(test3.data(it, :, it, :, :, it));
end
tc.verifyEqual(test1.forResult, test1.result);
tc.verifyEqual(test2.forResult, test2.result);
tc.verifyEqual(test3.forResult, test3.result);
end


function testDiagNdLessComplicated(tc)
test1.data = rand(2, 3, 2);
test2.data = rand(3, 2, 2);
test3.data = rand(2, 2, 3);

test1.result = misc.diagNd(test1.data, [1, 3]);
test2.result = misc.diagNd(test2.data, [2, 3]);
test3.result = misc.diagNd(test3.data, [1, 2]);

test1.forResult = zeros(2, 3);
test2.forResult = zeros(2, 3);
test3.forResult = zeros(2, 3);
for it = 1 : 2
	test1.forResult(it,:) = squeeze(test1.data(it, :, it));
	test2.forResult(it,:) = squeeze(test2.data(:, it, it));
	test3.forResult(it,:) = squeeze(test3.data(it, it, :));
end
tc.verifyEqual(test1.forResult, test1.result, 'test1');
tc.verifyEqual(test2.forResult, test2.result, 'test2');
tc.verifyEqual(test3.forResult, test3.result, 'test3');
end
