function [tests] = testSynchronisationWithSpectrumConstraints()
	% result = runtests('unittests.misc.testSynchronisationWithSpectrumConstraints')
	tests = functiontests(localfunctions);
end

function testExample(tc)
mySubSystems = {[0, 1; -1, 0], [0, 1; -1, 0], [0, 1; 0, 0]};
A = blkdiag(mySubSystems{:});
B = blkdiag([0; 1], [0; 1], [0; 1]);
adjacency = [0, 0, 0; 2, 0, 1; 0, 3, 0];
laplacian = diag(sum(adjacency, 2)) - adjacency;

margin = 1;
discRadius = 10;
sectorAngle = pi/4;
[gain, X, Y, dynamics] = misc.synchronisationWithSpectrumConstraints(A, B, laplacian, ...
	"margin", margin, "discRadius", discRadius, "sectorAngle", sectorAngle);

spectrum = eig(dynamics);
spectrumStable = setdiff(...
	round(spectrum, 14), ...
	round(eig(A), 14));

tc.verifyTrue(all(real(spectrumStable) < - margin));
tc.verifyTrue(all(abs(spectrumStable) < discRadius));
tc.verifyTrue(all(angle(-spectrumStable) < sectorAngle));
end % testExample