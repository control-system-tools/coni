function [tests] = testOutputRegulation()
tests = functiontests(localfunctions());
end

function testSiso(tc)
%% Parameter anlegen
% Strecke 
%	  x_t = A x + b u + g d
%		y = C x
A = [-2, 1; 0, -3];
b = [0; 1];
g = [-1; 0];
c = [1, 0];
strecke = ss(A, [b, g], [c; eye(size(A))], [], ...
	"InputName", ["u", "d"], "OutputName", ["y", "x(1)", "x(2)"]);
% Eingang des state spaces: [u; d] = col(Stellsignal, Störung)
% Ausgang des state spaces: [y; x] = col(Regelgröße, Zustände)

% Signal Model
% Störmodell: Sinus mit Kreisfrequenz w = 1 -> sin(t)
w = 1;
Sd = [0, 1; -w^2, 0];
% Führungsmodell: Rampe -> t
Sr = [0, 1; 0, 0];
% Zusammengefügtes Signalmodel
%	 v_t = S v
% [d; r] = p v = [pd; pr] v
S = blkdiag(Sd, Sr);
p = blkdiag([1, 0], [1, 0]);
pd = p(1, :);
pr = p(2, :);

[Pi, Gamma, K] = stateSpace.outputRegulation(A, b, c, g, S, pd, pr, 3*eig(A));

% verifiziere Lösung
residuum1 = Pi * S - A * Pi - g * pd - b * Gamma;
residuum2 = c * Pi - pr;
tc.verifyEqual(max(abs([residuum1; residuum2]), [], "all"), 0, "AbsTol", 1e-12)
tc.verifyEqual(eig(A-b*K), 3*eig(A), "AbsTol", 1e-6);

%% Simulation
regler = ss([], [], [], [-K, K*Pi + Gamma], ...
	"InputName", ["x(1)", "x(2)", "v(1)", "v(2)", "v(3)", "v(4)"], "OutputName", "u");
t = linspace(0, 5, 151);
% Führungssignal = Sägezahn
rampenSteigung = 1;
r = mod(t+0.5, 10).';
r_t = rampenSteigung + 0*r;

% Störung & Zeitableitung: Bestimmung durch Simulation
dSim = initial(ss(Sd, [], eye([2, 2]), []), [1; 1], t);
d = dSim(:, 1);
d_t = dSim(:, 2);

% Signalmodell Zustand
v = [d, d_t, r, r_t];

% Simulation der Strecke
closedLoopSystem = connect(strecke, regler, ["d", "v"], "y");
% Störverhalten
v = [d, d_t, 0*r, 0*r_t];
y.stoerverhalten = lsim(closedLoopSystem, [d, v], t);
% Führungsverhalten
v = [0*d, 0*d_t, r, r_t];
y.fuehrungsverhalten = lsim(closedLoopSystem, [0*d, v], t);

tc.verifyEqual(y.stoerverhalten(t>2), zeros(size(y.stoerverhalten(t>2))), "AbsTol", 1e-5)
tc.verifyEqual(y.fuehrungsverhalten(t>2)-r(t>2), zeros(size(y.fuehrungsverhalten(t>2))), "AbsTol", 1e-5)

% figure(); subplot(2, 1, 1);
% plot(t, y.stoerverhalten); subplot(2, 1, 2);
% plot(t, y.fuehrungsverhalten); hold on;
% plot(t, r, "LineStyle", "--");
end % testSiso()