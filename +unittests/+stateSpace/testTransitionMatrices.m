function [tests] = testTransitionMatrices()
%TESTTRANSITIONMATRICES Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testFundamentalMatrix(testCase)

N = 1e1;
domain(1) = quantity.EquidistantDomain("z", 0, 1, "stepNumber", N);
domain(2) = quantity.EquidistantDomain("xi", 0, 1, "stepNumber", N);

syms z xi
A = [0 1; 1 0];

%% compute comparative solution
P1 = quantity.Symbolic( expm(A * (z -xi)), domain);


%%
AA = quantity.Symbolic( A, domain(1) );
P2 = stateSpace.transitionMatrix.fundamentalMatrices(AA, domain);
testCase.verifyEqual( P1.on(), P2.on(), "AbsTol", 1e-10);

%%
P3 = stateSpace.transitionMatrix.odeSolver_par( AA.on(domain(1).grid), domain(1).grid );
P3 = quantity.Discrete(P3, domain);

triangleDomainSelector = P3*0;
% with the odeSolver method the state transition matrix is only computed on the triangle domain
% 0 < t < 1, 0 < tau < t, the other values are NaNs. By copmuting P1 + triangleDomainSelector the
% corresponding values in P1 are also set to NaN.
testCase.verifyEqual( on(P1 + triangleDomainSelector), P3.on(), "AbsTol", 1e-10);

end

function testPeanoBaker(testCase)

syms z xi
A = [0 1; 1 0];

%% compute comparative solution
P1 = expm(A * z);
P1 = matlabFunction(P1(:));

%% compute transition matrix with the peanobaker series
P2 = stateSpace.transitionMatrix.peanoBakerSeries0(A, z, xi, 0, 9);
P2 = matlabFunction(P2(:));

%% evaluate both solutions
z = linspace(0, 1, 1e4);

p1 = P1(z);
p2 = P2(z);

verifyTrue(testCase, max(max(p1 - p2)) < 3e-7);

end