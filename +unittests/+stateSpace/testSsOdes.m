function [tests] = testSsOdes()
% unittests for the class stateSpace.Odes.
tests = functiontests(localfunctions());
end

function testInitSignalModel(tc)
signalModel = stateSpace.Odes.initSignalModel(...
	"reference", struct("shape", ["constant"; "sinusoidal"], "frequency", [0; 2]), ...
	"disturbance", struct("shape", "ramp"));
tc.verifyEqual(signalModel.odes.A, blkdiag(0, [0, 2; -2, 0], [0, 1; 0, 0]));
tc.verifyTrue(isempty(setdiff( ...
	string(signalModel.OutputName), ...
	cat(1, "reference(" + [1; 2] +")", "referenceState("+[1; 2; 3]+")", ...
		"disturbance(1)", "disturbanceState(" + [1; 2] +")"))))

signalModel = stateSpace.Odes.initSignalModel(...
	"reference", struct("shape", "constant"));
tc.verifyEqual(signalModel.odes.A, 0);
tc.verifyEqual(signalModel.odes.C, [1; 1]);
tc.verifyEqual(string(signalModel.OutputName), ["reference(1)"; "referenceState"]);
end % testInitSignalModel

function testConstructor(tc)
ss1 = ss(1, 1, 1, 3, 'InputName', 'u1', 'OutputName', 'y1');
ss2 = ss(2, 2, 2, 4, 'InputName', 'u2', 'OutputName', 'y2');
testOdes = stateSpace.Odes(ss1, 'ss1', ss2, 'ss2');
testOde1 = stateSpace.Odes(ss1, 'ss1');
testOde2 = stateSpace.Odes(ss2, 'ss2');
testOdes12 = testOde1 + testOde2;
tc.verifyTrue(isequal(testOdes12, testOdes))
tc.verifyTrue(isequal(testOdes12.odes, testOdes.odes))
testOdes12backupRemove1 = copy(testOdes12);
testOdes12backupRemove1 = testOdes12backupRemove1.remove('ss1');
tc.verifyTrue(isequal(testOdes12backupRemove1, testOde2))
tc.verifyTrue(isequal(testOdes12backupRemove1.odes, testOde2.odes))
end % testConstructor()

function testExchange(tc)
ss1 = ss(1, 1, 1, 3, 'InputName', 'u1', 'OutputName', 'y1');
ss2 = ss(2, 2, 2, 4, 'InputName', 'u2', 'OutputName', 'y2');
Ode = stateSpace.Odes(ss1, 'ss1');
Ode.exchange('ss1', ss2);
time = linspace(0, 1, 21);
[Yss2, ~, Xss2] = step(ss2, time);
[YOde, ~, XOde] = step(Ode.odes, time);
tc.verifyTrue(isequal(XOde, Xss2));
tc.verifyTrue(isequal(Yss2, YOde(:, 2)));
end % testExchange(tc)

function testInputOutputName(tc)
myOdes = stateSpace.Odes(ss(zeros(2), 0.8*magic(2), eye(2), [], ...
	'InputName', 'error.measurement1', 'OutputName', 'observer.integrator'), 'integrator');
tc.verifyEqual(myOdes.InputName, myOdes.ss{1}.InputName)
tc.verifyEqual(myOdes.OutputName, ...
	[myOdes.ss{1}.OutputName; 'observer.integrator(1)'; 'observer.integrator(2)'])
end % testInputOutputName(tc)