function [tests] = testFlatSystem()
tests = functiontests(localfunctions());
end

function testFlatOutput(tc)

a = 1;
k = 1;
A = [-a a 0;
	  a -2*a a; 
	  0 a -k*a];

b = 1;
B = [b 0; 0 0; 0 b];
[C, r, de] = stateSpace.flatOutput(A, B);

tc.verifyEqual( C, [0 1 0; 0 0 1]);
tc.verifyEqual( r, [2; 1] );

t = quantity.Domain("t", linspace(0, 3));
f1 = stateSpace.setPointChange(0, 1, 0, 3, 3);
f1 = quantity.Symbolic( f1, t);
yf(1,1) = signals.BasicVariable( f1, {f1.diff(t, 1), f1.diff(t,2)} );


f2 = stateSpace.setPointChange(0, 2, 0, 3, 3);
f2 = quantity.Symbolic( f2, t);
yf(2,1) = signals.BasicVariable( f2, {f2.diff(t, 1), f2.diff(t, 2)} );

x = de.X.applyTo(yf);
u = de.U.applyTo(yf);
y = [yf.fun]';

sys = ss(A, B, C, []);
[sim.y, ~, sim.x] = lsim(sys, u.on(), t.grid);
tc.verifyEqual( sim.y, y.on(), "AbsTol", 1e-3);
tc.verifyEqual( sim.x, x.on(), "AbsTol", 2e-4 );

end

function testFlatSystemBfullRank(tc)
A = [zeros(2, 2), eye(2, 2); -eye(2, 2), 2*eye(2, 2)];
B = [zeros(2, 2); rot90(diag([2, 3]))];
C = eye(2, 4);

sys = stateSpace.FlatSystem(A, B, C);

% plan trajectory
t = quantity.Domain("t", linspace(0, 5, 1001));
yA = [0; 0];
yB = [2; 3];
tA = 1;
tB = 4;
n = max(sys.degree); % -> no jump in input
algorithm = "sum";
flatOutputSymbolic = stateSpace.setPointChange(yA, yB, tA, tB, n, "algorithm", algorithm);
% put flat output together
stepSize = diff(t.grid(1:2));
t0A = quantity.Domain("t", t.lower : stepSize : tA);
tAB = quantity.Domain("t", tA : stepSize : tB);
tBend = quantity.Domain("t", tB : stepSize : t.upper);
trajectory.flatOutput = quantity.Piecewise(...
	{yA * quantity.Discrete.ones([1, 1], t0A), ...
	quantity.Symbolic(flatOutputSymbolic, tAB), ...
	yB * quantity.Discrete.ones([1, 1], tBend)});

% parametrize state and input
trajectory.input = sys.parametrizeInput(trajectory.flatOutput);
trajectory.state = sys.parametrizeState(trajectory.flatOutput);

% simulate
simData = stateSpace.simulate(sys.stateSpace, trajectory.input.on(), t.grid, zeros(sys.n, 1), ...
	"structuredOutput", true);
tc.verifyEqual(MAX(abs(simData.state - trajectory.state)), 0, "AbsTol", 1e-3);
tc.verifyEqual(MAX(abs(simData.flatOutput - trajectory.flatOutput)), 0, "AbsTol", 1e-3);

% if false
% 	plot(trajectory.input);
%  	plot([simData.state, trajectory.state, simData.state - trajectory.state]);
%  	plot([simData.flatOutput, trajectory.flatOutput, simData.flatOutput - trajectory.flatOutput]);
% end
end % testFlatSystemBfullRank()

function testFlatSystemBredundant(tc)
A = [zeros(2, 2), eye(2, 2); -eye(2, 2), 2*eye(2, 2)];
B = [zeros(2, 2); rot90(diag([2, 3]))];
B = [B, B * diag([exp(1), pi])];
C = eye(2, 4);

sys = stateSpace.FlatSystem(A, B, C);

% plan trajectory
t = quantity.Domain("t", linspace(0, 5, 1001));
yA = [0; 0];
yB = [2; 3];
tA = 1;
tB = 4;
n = max(sys.degree); % -> no jump in input
algorithm = "sum";
flatOutputSymbolic = stateSpace.setPointChange(yA, yB, tA, tB, n, "algorithm", algorithm);
% put flat output together
stepSize = diff(t.grid(1:2));
t0A = quantity.Domain("t", t.lower : stepSize : tA);
tAB = quantity.Domain("t", tA : stepSize : tB);
tBend = quantity.Domain("t", tB : stepSize : t.upper);
trajectory.flatOutput = quantity.Piecewise(...
	{yA * quantity.Discrete.ones([1, 1], t0A), ...
	quantity.Symbolic(flatOutputSymbolic, tAB), ...
	yB * quantity.Discrete.ones([1, 1], tBend)});

% parametrize state and input
trajectory.input = sys.parametrizeInput(trajectory.flatOutput);
trajectory.inputMinimal = sys.parametrizeInput(trajectory.flatOutput, "mode", "minimum");
tc.verifyEqual(MAX(abs(trajectory.inputMinimal - sys.Bminimizer * trajectory.input)), 0, "AbsTol", 1e-12);
trajectory.state = sys.parametrizeState(trajectory.flatOutput);

% simulate
simData = stateSpace.simulate(sys.stateSpace, trajectory.input.on(), t.grid, zeros(sys.n, 1), ...
	"structuredOutput", true);
tc.verifyEqual(MAX(abs(simData.state - trajectory.state)), 0, "AbsTol", 1e-3);
tc.verifyEqual(MAX(abs(simData.flatOutput - trajectory.flatOutput)), 0, "AbsTol", 1e-3);

% if false
% 	plot(trajectory.input);
% 	plot([simData.state, trajectory.state, simData.state - trajectory.state]);
% 	plot([simData.flatOutput, trajectory.flatOutput, simData.flatOutput - trajectory.flatOutput]);
% end
end % testFlatSystemBredundant()

function testFlatSystemBredundant2(tc)
A = [zeros(2, 2), eye(2, 2); [-0.0785, 0.0482, -0.5927, 0.2409; -1.348, -0.0015, -6.7405, -0.2073]];
B = [zeros(2, 4); [-0.6439, 0.6439, -0.4373, -0.4373; 3.9478, 3.9478, -0.0713, 0.0713]];
C = eye(2, 4);

sys = stateSpace.FlatSystem(A, B, C);

% plan trajectory
t = quantity.Domain("t", linspace(0, 5, 1001));
yA = [0; 0];
yB = [2; 3];
tA = 1;
tB = 4;
n = max(sys.degree); % -> no jump in input
algorithm = "sum";
flatOutputSymbolic = stateSpace.setPointChange(yA, yB, tA, tB, n, "algorithm", algorithm);
% put flat output together
stepSize = diff(t.grid(1:2));
t0A = quantity.Domain("t", t.lower : stepSize : tA);
tAB = quantity.Domain("t", tA : stepSize : tB);
tBend = quantity.Domain("t", tB : stepSize : t.upper);
trajectory.flatOutput = quantity.Piecewise(...
	{yA * quantity.Discrete.ones([1, 1], t0A), ...
	quantity.Symbolic(flatOutputSymbolic, tAB), ...
	yB * quantity.Discrete.ones([1, 1], tBend)});

% parametrize state and input: version 1
trajectory.input = sys.parametrizeInput(trajectory.flatOutput);
trajectory.inputMinimal = sys.parametrizeInput(trajectory.flatOutput, "mode", "minimum");
tc.verifyEqual(MAX(abs(trajectory.inputMinimal - sys.Bminimizer * trajectory.input)), 0, "AbsTol", 1e-12);
trajectory.state = sys.parametrizeState(trajectory.flatOutput);

% simulate
simData = stateSpace.simulate(sys.stateSpace, trajectory.input.on(), t.grid, zeros(sys.n, 1), ...
	"structuredOutput", true);
tc.verifyEqual(MAX(abs(simData.state - trajectory.state)), 0, "AbsTol", 1e-3);
tc.verifyEqual(MAX(abs(simData.flatOutput - trajectory.flatOutput)), 0, "AbsTol", 1e-3);

% if false
% 	plot(trajectory.input);
% 	plot([simData.state, trajectory.state, simData.state - trajectory.state]);
% 	plot([simData.flatOutput, trajectory.flatOutput, simData.flatOutput - trajectory.flatOutput]);
% end

% parametrize state and input: version 2
distributeInput = [eye(2); diag([-1, 1])];
trajectory.input = sys.parametrizeInput(trajectory.flatOutput, "distributeInput", distributeInput);
trajectory.inputMinimal = sys.parametrizeInput(trajectory.flatOutput, "mode", "minimum");
tc.verifyEqual(MAX(abs(trajectory.inputMinimal - sys.Bminimizer * trajectory.input)), 0, "AbsTol", 1e-12);

% simulate
simData = stateSpace.simulate(sys.stateSpace, trajectory.input.on(), t.grid, zeros(sys.n, 1), ...
	"structuredOutput", true);
tc.verifyEqual(MAX(abs(simData.state - trajectory.state)), 0, "AbsTol", 1e-3);
tc.verifyEqual(MAX(abs(simData.flatOutput - trajectory.flatOutput)), 0, "AbsTol", 1e-3);

% if false
% 	plot(trajectory.input);
% 	plot([simData.state, trajectory.state, simData.state - trajectory.state]);
% 	plot([simData.flatOutput, trajectory.flatOutput, simData.flatOutput - trajectory.flatOutput]);
% end
end % testFlatSystemBredundant()