function [tests] = testMustBe()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testMustBeSize(tc)

A = ones(1,2,3);
tc.verifyError( @() mustBe.size(A, [1 2] ), "conI:mustBe" )
tc.verifyWarningFree( @() mustBe.size(A, [1 2 3] ) )

end