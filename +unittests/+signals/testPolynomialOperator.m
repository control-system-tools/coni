function [tests] = testPolynomialOperator()
%TESTGEVREY Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function setupOnce(testCase)
%%
z = sym('z', 'real');
s = sym('s');
Z = quantity.Domain("z", linspace(0, 1, 11));

% init with symbolic values
A = cat(3, [ 0, 0, 0, 0; ...
		     1, 0, 0, 0; ...
			 0, 1, 0, 0; ...
			 0, 0, 1, 0], ...
            [0, 0, 0, -3 + z; ...
			 0, 0, 0,   0; ...
			 0, 0, 0,   0; ...
			 0, 0, 0,   0], ...
			[0, 0, 0, -4; ...
			 0, 0, 0, 0; ...
			 0, 0, 0, 0; ...
			 0, 0, 0, 0]);

B = cat(3, [1, 0, 0, 0]', zeros(4, 1), zeros(4, 1));
		 
for k = 1:3
	a{k} = quantity.Symbolic(A(:,:,k), Z);
end
		 
A = signals.PolynomialOperator(a, 's', s);

testCase.TestData.A = A;
testCase.TestData.a = a;

end

function testFindCommonRoots(testCase)

syms s 

p1 = (s-1) * (s - 3) * (s - .9)^3;
p2 = (s-1) * (s - 3) * (s - 8)^4;
p3 = 0;
p4 = (s-1) * (s-3);

A = signals.PolynomialOperator([p1, p2; p3, p4]);
r = A.findCommonZeros();

testCase.verifyEqual(r, [3 1], "AbsTol", 1e-12);
end


function testZeroForm(testCase)

% test the scalar case:
syms s
rng(0); % use pseudo-random entries
p = rand();

for k = 1:10
	rng(k);
	p = p + s^k * rand;
end

A = signals.PolynomialOperator(p);

testCase.verifyEqual( double( coeffs(A.zerosForm) ), double( coeffs( p ) ), "RelTol", 1e-12);

% test the matrix case
n = 2;
rng(0); % use pseudo-random entries
P = sym( rand(2,4) );
for i = 1:size(P,1)
	for j = 1:size(P,2)
		rng(0);
		for k = 1:ceil( rand(1) * 10)
			rng(0);
			P(i,j) = P(i,j) + s^k * rand();
		end	
	end
end
	
A = signals.PolynomialOperator(P);
AP = A.zerosForm();
for i = 1:prod( size(A, [2, 3]))
	testCase.verifyEqual( double( coeffs( AP(i) ) ), double( coeffs(P(i))), "RelTol", 1e-12);
end
	
% test the case with zero at zero:
p=0;
for k = 1:3
	p = p + s^k * k;
end
A = signals.PolynomialOperator( p );

testCase.verifyEqual( double( coeffs(A.zerosForm) ), double( coeffs( p ) ), "RelTol", 1e-12);
end

function testCat(testCase)

[testCase.TestData.A.truncate] = deal( true );

AA = [ testCase.TestData.A, testCase.TestData.A];
testCase.verifyEqual( size(AA), [3 4 8]);
testCase.verifyTrue( AA(1).truncate) 
end

function testPolynomial2Coefficients(tc)

syms s;
rng(1);
A0 = rand(2);
rng(2);
A1 = rand(2);
rng(3);
A2 = rand(2);
rng(4);
A3 = rand(2);

A = signals.PolynomialOperator( A0 + A1*s + A2*s^2 + A3*s^3 );
coefficients = signals.PolynomialOperator.polynomial2coefficients(A.sym, s);

tc.verifyEqual(coefficients(:,:,1), A0, "AbsTol", 10*eps);
tc.verifyEqual(coefficients(:,:,2), A1, "AbsTol", 10*eps);
tc.verifyEqual(coefficients(:,:,3), A2, "AbsTol", 10*eps);
tc.verifyEqual(coefficients(:,:,4), A3, "AbsTol", 10*eps);

coefficients2 = double(A);
tc.verifyEqual( coefficients, coefficients2);

% 
ac = double( signals.PolynomialOperator(s^5, "s", s) );
tc.verifyEqual(ac(:), misc.unitVector(6,6));

end

function testName(testCase)

K = magic(5);
op.K = signals.PolynomialOperator(K, "name", "K");
testCase.verifyEqual( op.K.name, "K")

end

function testInit(testCase)

K = magic(5);
op.K = signals.PolynomialOperator(K);

testCase.verifyEqual( size(op.K), [1 5 5]);

end

function testEmpty(testCase)

	e = signals.PolynomialOperator.empty();
	testCase.verifyTrue( isempty( e ) );

end

function testApplyTo(testCase)
	
	% test the application of the differential operator by the planning and simulation of a
	% reference trajectory
	A = [0 1 0; 0 0 1; - (1:3)];
	B = [0 0 1]';
	C = [1 0 0];
	
	sys = ss(A, B, C, []);
	detsIA = signals.PolynomialOperator(num2cell(flip(charpoly(A))));
	adjB = signals.PolynomialOperator( adjoint(sym('s')*eye(size(A)) - A) * B );
	
	t = sym('t', 'real');
	sol.y = quantity.Symbolic( t.^3 .* (1 - t).^3, ...
		quantity.Domain("t", linspace(0, 1, 1e2)));
	
	sol.u = detsIA.applyTo(sol.y);
	sol.x = adjB.applyTo(sol.y);
	
	[sim.y, sim.t, sim.x] = stateSpace.lsim(sys, sol.u, sol.y.domain);
	
	testCase.verifyEqual( sol.x.on(), sim.x.on(), "AbsTol", 1e-3);
	
	% test the application of the differential operator by the computation of a partial sum of a
	% series
	ak = arrayfun( @(k) 0.5^(k) / factorial(2*k), 0:14);

	A = signals.PolynomialOperator( num2cell( ak ) );
	phi = signals.GevreyFunction("numDiff", 14);
	
	[y, tol, nStop] = A.applyTo(phi);
	[truncated.y, truncated.tol, truncated.nStop] = A.applyTo(phi, "relativeIncrementTolerance", 1e-2);
	
	testCase.verifyLessThan( max( abs( y - truncated.y ) ) / max( abs( y ) ), 1e-2);
	testCase.verifyEqual( y.on(), truncated.y.on(), "AbsTol", 1e-3);
	testCase.verifyWarning( @() A.applyTo(phi, "relativeIncrementTolerance", 1e-12), 'CONI:signals:PolynomialOperator' );
end
function testTranspose(testCase)
	At = testCase.TestData.A.';
	
	testCase.verifyTrue(At(1).coefficient == testCase.TestData.A(1).coefficient');	
	testCase.verifyTrue(At(2).coefficient == testCase.TestData.A(2).coefficient');	
	testCase.verifyTrue(At(3).coefficient == testCase.TestData.A(3).coefficient');	
end
function testCTranspose(testCase)
	At = testCase.TestData.A';
	testCase.verifyTrue(At(1).coefficient == testCase.TestData.A(1).coefficient');	
	testCase.verifyTrue(At(2).coefficient == - testCase.TestData.A(2).coefficient');	
	testCase.verifyTrue(At(3).coefficient == testCase.TestData.A(3).coefficient');	
end
% 
function testFundamentalMatrixSpaceDependent(testCase)
%
	a = 20;
	z = sym('z', 'real');
	Z = quantity.Domain("z", linspace(0, 1, 5));
	A = signals.PolynomialOperator(...
		{quantity.Symbolic([1, z; 0, a], Z)});
	
	F = A.stateTransitionMatrix();
		
	% compute the exact solution from the peano-baker series paper.
	if numeric.near(a, 1)
		f = 0.5 * z^2 * exp(z);
	else
		f = (exp(z) - exp(a * z) - (1 - a) * z * exp( a * z ) ) / (1 - a)^2; 
	end   

	PhiExact = quantity.Symbolic([exp(z), f; 0, exp(a*z)], Z,  'name', 'Phi_A');
		
	testCase.verifyEqual(double(F), PhiExact.on(), 'RelTol', 5e-5);
	
end
% 
function testStateTransitionMatrix(testCase)
N = 2;
z = quantity.Domain("z", linspace(0,1,11));
A0 = quantity.Symbolic([0 1; 1 0], z);
A1 = quantity.Symbolic([0 1; 0 0], z);
A = signals.PolynomialOperator({A0, A1});
B = signals.PolynomialOperator(quantity.Symbolic([-1 -1; 0 0], z));

[Phi1, F0] = A.stateTransitionMatrix("N", N);
Psi1 = A.inputTransitionMatrix(B, F0, "N", N);
[Phi2, Psi2] = A.stateTransitionMatrixByOdeSystem("N", N, "B", B);

testCase.verifyEqual(double(Phi1), double(Phi2), 'AbsTol', 1e-2);
testCase.verifyEqual(double(Psi1), double(Psi2), 'AbsTol', 1e-2);
end

function testChangeDomain(testCase)
	A = testCase.TestData.A;
	a = testCase.TestData.a;
	A0 = A.subs("z", 0);
	
	for k = 1:3
		testCase.verifyEqual( ...
			a{k}.at(0), squeeze(A0(k).coefficient.on()), 'AbsTol', 10*eps);
	end
end % testChangeDomain

function testMTimes(testCase)

	zGrid = linspace(0, pi, 3)';
	z = quantity.Domain("z", zGrid);
	A0 = quantity.Discrete(reshape((repmat([1 2; 3 4], length(zGrid), 1)), length(zGrid), 2, 2),...
		z, 'name', 'A');
	A1 = quantity.Discrete(reshape((repmat([5 6; 7 8], length(zGrid), 1)), length(zGrid), 2, 2), ...
		z, 'name', 'A');
	A2 = quantity.Discrete(reshape((repmat([9 10; 11 12], length(zGrid), 1)), length(zGrid), 2, 2), ...
		z, 'name', 'A');
	
	A = signals.PolynomialOperator({A0, A1, A2});
	B = signals.PolynomialOperator({A0});
	
	C = A*B;
	
	s = sym('s');
	
	As = A0.at(0) + A1.at(0)*s + A2.at(0)*s^2;
	Bs = A0.at(0);
	
	Cs = As*Bs;
	c1 = coeffs(Cs(1));
	c2 = coeffs(Cs(2));
	c3 = coeffs(Cs(3));
	c4 = coeffs(Cs(4));
	
	for k = 1:length(C)
		Ck = C(k).coefficient.at(0);
		testCase.verifyEqual( Ck, double( [c1(k) c3(k); c2(k), c4(k)] ))
	end
	
	K = [0 1; 1 0];
	
	C =	K*A;
	
	for k = 1:3 
		testCase.verifyEqual(double(C(k).coefficient), double(K * A(k).coefficient));
	end
	%% test the multiplication with truncation
	trun.A = signals.PolynomialOperator({A0, A1, A2}, "truncate", true);
	trun.AA = trun.A * trun.A;
	AA = A*A;
	for k = 1:3
		testCase.verifyEqual(double( AA(k).coefficient ), double( trun.AA(k).coefficient ) )
	end
		
	%% 
	A = magic(3);
	syms s 
	op = signals.PolynomialOperator( {-A, eye(3)} );
	det2 = det(op) * det(op);
	
	testCase.verifyEqual( flip( squeeze( double(det2) )' ), conv( charpoly( A ), charpoly( A ) ) );
	
	B = double( adj(op)	);
	C = zeros(3,3);
	for i = 1:3
		C = C + squeeze(B(:,:,i)) * s^(i-1);
	end
	
	testCase.verifyEqual(double( simplify( sym( adj(op) * adj(op) ) - C*C ) ), zeros(3));
	

end

function testSum(testCase)
	z = quantity.Domain("z", linspace(0, pi, 5));
	A0 = quantity.Discrete(reshape((repmat([1 2; 3 4], z.n, 1)), z.n, 2, 2),...
		z, 'name', 'A');
	A1 = quantity.Discrete(reshape((repmat([5 6; 7 8], z.n, 1)), z.n, 2, 2), ...
		z, 'name', 'A');
	A2 = quantity.Discrete(reshape((repmat([9 10; 11 12], z.n, 1)), z.n, 2, 2), ...
	z, 'name', 'A');
	
	A = signals.PolynomialOperator({A0, A1, A2});
	B = signals.PolynomialOperator({A0});
	
	C = A + B;
		
	testCase.verifyTrue( numeric.near(double(C(1).coefficient), double(A0 + A0)) );
	testCase.verifyTrue( numeric.near(double(C(2).coefficient), double(A1)) );
	testCase.verifyTrue( numeric.near(double(C(3).coefficient), double(A2)) );
	
	C = B + A;
	
	testCase.verifyTrue( numeric.near(double(C(1).coefficient), double(A0 + A0)) );
	testCase.verifyTrue( numeric.near(double(C(2).coefficient), double(A1)) );
	testCase.verifyTrue( numeric.near(double(C(3).coefficient), double(A2)) );
	
end
	
function testDet(testCase)
% test the det of OP via the characteristic polynomial of a matrix A
%	det(sI - A) = det(OP) = det( -A + sI )
A = [0 -1; 1 0];
OP = signals.PolynomialOperator( {-A, eye(2)} );
d1 = det(OP);
testCase.verifyEqual( charpoly([0 -1; 1 0]), double( [d1.coefficient] ) );

trun.OP = signals.PolynomialOperator( {-A, eye(2)}, "truncate", true );
d2 = det(trun.OP);

for k = 1:size(trun.OP, 2)
	testCase.verifyEqual( double( d1(k).coefficient ), double( d2(k).coefficient ) );
end

end

function testAdj(testCase)
% adj(sI - A) / det(sI - A) = (sI - A)^-1
A = [0 -1; 1 0];
OP = signals.PolynomialOperator( {-A, eye(2) } );

invA = sym( adj(OP) ) / sym( det(OP) );

invSymA = inv( OP(1).s * eye(2) - A );

testCase.verifyEqual( invA, invSymA )

end

function testInitialization(testCase)

A = signals.PolynomialOperator( {[0 -1; 1 0], [0  1; 0 0]} );
verifyClass(testCase, A, 'signals.PolynomialOperator');

end

function testUminus(testCase)
%%
syms z s
Z = quantity.Domain('z', linspace(0,1,11));
A0 = quantity.Symbolic( [z, z.^2; z.^3, z.^4], Z);
A1 = quantity.Symbolic( [sin(z), cos(z); tan(z), sinh(z)], Z);
o = signals.PolynomialOperator({A0, A1});
mo = -o;

%%
verifyEqual(testCase, o(1).coefficient.on , -mo(1).coefficient.on);

end