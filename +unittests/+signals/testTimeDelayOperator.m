function [tests] = testTimeDelayOperator()
%TESTGEVREY Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function setupOnce(testCase)

z = quantity.Domain('z', linspace(0,1));
zeta = quantity.Domain('zeta', linspace(0,1));
t = quantity.Domain('t', linspace(0, 2, 31));
v = quantity.Discrete( z.grid, z );
c_zeta = quantity.Discrete( zeta.grid * 0, zeta );
c = quantity.Discrete( 1, quantity.Domain.empty);

testCase.TestData.z = z;
testCase.TestData.t = t;
testCase.TestData.zeta = zeta;
testCase.TestData.coefficient.fun_zeta = c_zeta;
testCase.TestData.coefficient.variable = v;
testCase.TestData.coefficient.constant = c;
	
testCase.TestData.delay.variable = signals.TimeDelayOperator(v, t);
testCase.TestData.delay.constant = signals.TimeDelayOperator(c, t);
testCase.TestData.delay.zero = signals.TimeDelayOperator.zero(t);
testCase.TestData.delay.spatialDomain2 = signals.TimeDelayOperator( ...
	v - c_zeta, t);

testCase.TestData.delay.diagonal = diag( ...
	[testCase.TestData.delay.constant, ...
	 testCase.TestData.delay.variable]);
end

function testInit(testCase)

td = testCase.TestData;

testCase.verifyEqual(td.delay.constant.coefficient, ...
	td.coefficient.constant);
testCase.verifyEqual( td.delay.variable.coefficient, td.coefficient.variable);
	
coeffs = [testCase.TestData.coefficient.constant, ...
	 testCase.TestData.coefficient.variable];
 
D = signals.TimeDelayOperator(coeffs);

testCase.verifyEqual([D.coefficient], coeffs )

end


function testDiag(testCase)

	D = testCase.TestData.delay.diagonal;
	v = testCase.TestData.delay.variable;
	c = testCase.TestData.delay.constant;
	o = testCase.TestData.delay.zero;

	testCase.verifyEqual( D(1), c );
	testCase.verifyEqual( D(2), o );
	testCase.verifyEqual( D(3), o );
	testCase.verifyEqual( D(4), v );
	
end

function testApplyTo(testCase)

	z = testCase.TestData.z;
	t = testCase.TestData.t;
	tau = quantity.Domain("tau", linspace(-1, 3, 201));
	v = testCase.TestData.delay.variable;
	
	h = quantity.Discrete( sin(tau.grid * 2 * pi), tau );
	H = quantity.Discrete( sin(z.grid * 2 * pi + t.grid' * 2 * pi), ...
		[z, t]);
	H_ = v.applyTo(h);
	
	%% test a scalar operator
	testCase.verifyEqual(H.on(), H_.on(), 'AbsTol', 5e-3);
	
	d2 = testCase.TestData.delay.spatialDomain2;
	H2 = d2.applyTo(h);
	H2 = subs(H2, "zeta", 0);
	testCase.verifyEqual(H.on(), H2.on(), 'AbsTol', 5e-3);
	
	%% test a diagonal operator
	h = quantity.Discrete( {sin(tau.grid * 2 * pi); ...
		sin(tau.grid * 2 * pi)}, tau );
	H = quantity.Discrete( {sin(z.grid * 0 + 2 * pi + t.grid' * 2 * pi); ...
		sin(z.grid * 2 * pi + t.grid' * 2 * pi)}, [z, t]);
	D = testCase.TestData.delay.diagonal;
	H_ = D.applyTo(h);
	
	testCase.verifyEqual( H.on(), H_.on(), 'AbsTol', 5e-3)
	
	%% test application of the operator to a function h(z,t)
	% h(z,tau) = z + sin( tau * 2 * pi)
	%	D[h] = z + sin( ( t + g(z) ) * 2* pi )
	%		g(z) = z
	%	D[h] = z + sin( ( t + z ) * 2 * pi )
	
	h = quantity.Discrete( z.grid + sin(tau.grid' * 2 * pi), [z, tau]);
	
	Dh = quantity.Discrete(	z.grid + sin( ( z.grid + t.grid') * 2 * pi ), [z, t]);
	
	D = testCase.TestData.delay.variable;
	
	Dh2 = D.applyTo( h, 'domain', tau );

	testCase.verifyEqual( Dh.on, Dh2.on, 'AbsTol', 2e-3)
	
end










