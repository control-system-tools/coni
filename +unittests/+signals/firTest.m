function [tests] = firTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testScalarFilter(testCase)
    t = linspace(-pi,pi,100)';
    rng default  %initialize random number generator
    x = sin(t) + 0.25*rand(size(t));
    
    windowSize = 5; 
    b = (1/windowSize)*ones(windowSize, 1);
    
	% test if the matrix valued filter does the same as the underlying scalar filter:
    y = signals.fir.mFilter(b, x);
    Y = filter(b, 1, x);
    
	testCase.verifyEqual(y, Y);
end


function testVectorFilter(testCase)

    t = linspace(-pi,pi,100)';
    rng default  %initialize random number generator
    x = sin(t) + 0.25*rand(size(t));
    
    windowSize = 5; 
    b1 = (1/windowSize)*ones(windowSize, 1) * 0.1;
    b2 = (1/windowSize)*ones(windowSize, 1) * 0.2;
    
    b = [b1, b2];
    
    y = signals.fir.mFilter(b, x);
    
    Y1 = filter(b1, 1, x);
    Y2 = filter(b2, 1, x);

	testCase.verifyEqual(y, [Y1, Y2]);
    
end


function testMatrixFilter(testCase)

    t = linspace(-pi,pi,100)';
    rng default  %initialize random number generator
    x1 = sin(t) + 0.25*rand(size(t));
    x2 = cos(t) + 0.5 * rand(size(t));
    x = [x1, x2];
    
    windowSize = 5; 
    b1 = ones(windowSize,1) * 1;
    b2 = ones(windowSize,1) * 2;
    b3 = ones(windowSize,1) * 3;
    b4 = ones(windowSize,1) * 4;
    
    % b = [b1 b2; b3 b4];
    b = reshape([b1 b3 b2 b4], windowSize, 2, 2);
    
    y = signals.fir.mFilter(b, x);
    
    Y1 = filter(b1, 1, x1) + filter(b2, 1, x2);
    Y2 = filter(b3, 1, x1) + filter(b4, 1, x2);

    testCase.verifyEqual(y, [Y1, Y2]);

end

function testMFilterOutput(testCase)

dt = 1e-2;
T = quantity.EquidistantDomain("t", 0, 15, "stepSize", dt);
I = quantity.EquidistantDomain("tau", 0, 1, "stepSize", dt);

u = quantity.Discrete( rand(T.n, 2, 1), T);
W = quantity.Discrete.eye(2, I);
w = signals.fir.midpoint(W, I);

y = signals.fir.mFilter( w, u);
u2Num = u.on();

yNum = zeros( T.n - I.n +1, 2, 1);
for i = 1:( T.n - I.n)+1
	yNum(i,:) = sum(u2Num(i+1:(i+I.n-1), :)) ./ (I.n-1);
end

T1 = quantity.EquidistantDomain("t", 1, 15, "stepSize", dt);
y1 = y.on(T1);

testCase.verifyEqual( y1, yNum, "AbsTol", 1e-14)

end

function testFilters(testCase)
dt = 1e-2;
T = quantity.EquidistantDomain("t", 0, 15, "stepSize", dt);
I = quantity.EquidistantDomain("tau", 0, 1, "stepSize", dt);
t = sym("t", "real");
tau = sym("tau", "real");
f = quantity.Symbolic( sin( (t -tau) * pi ), [T, I]);
n = quantity.Symbolic( tau.^2 .* (1-tau).^2, I);

y.sym = int( n * f, "tau", 0, 1);

ff = on(f.subs("tau", 0));

% first-order-hold filter:
fir.foh = signals.fir.firstOrderHold(n, I);
y.foh = quantity.Discrete( signals.fir.mFilter( fir.foh, ff), T);

% zero-order-hold filter:
fir.zoh = signals.fir.zeroOrderHold(n, I);
y.zoh = quantity.Discrete( signals.fir.mFilter( fir.zoh, ff), T);

% trapz filter:
fir.trapz = signals.fir.trapez(n, I);
y.trapz = quantity.Discrete( signals.fir.mFilter( fir.trapz, ff), T);

T1 = quantity.Domain("t", linspace(1, 15));
testCase.verifyEqual( y.sym.on(T1), y.foh.on(T1), "AbsTol", 5e-6)
testCase.verifyEqual( y.sym.on(T1), y.zoh.on(T1), "AbsTol", 5e-4)
testCase.verifyEqual( y.sym.on(T1), y.trapz.on(T1), "AbsTol", 2e-6)

% figure();clf;
% subplot(311);
% plot(T1.grid, y.sym.on(T1)- y.foh.on(T1));
% subplot(312);
% plot(T1.grid, y.sym.on(T1)- y.zoh.on(T1));
% subplot(313);
% plot(T1.grid, y.sym.on(T1)- y.trapz.on(T1));

end

function testRescampling( testCase )

dt1 = 1e-2;
I1 = quantity.EquidistantDomain("tau", 0, 1, "stepSize", dt1);
n1 = quantity.Discrete( I1.grid.^2 .* (1-I1.grid).^2, I1);

dt2 = 1e-1;
I2 = quantity.EquidistantDomain("tau", 0, 1, "stepSize", dt2);
n2 = quantity.Discrete( I2.grid.^2 .* (1-I2.grid).^2, I2);

fir11 = signals.fir.firstOrderHold(n1, I1);
fir21 = signals.fir.firstOrderHold(n2, I1);

fir12 = signals.fir.firstOrderHold(n1, I2);
fir22 = signals.fir.firstOrderHold(n2, I2);

n12 = n1.subs("tau", I2);
fir122 = signals.fir.firstOrderHold(n12, I2);
n21 = n2.subs("tau", I1);
fir211 = signals.fir.firstOrderHold(n21, I1);

% figure(67); clf; hold on;
% plot( fir11 )
% plot( fir21 )
% plot( fir211)
% 
% figure(68); clf; hold on;
% plot( fir12 )
% plot( fir22 )
% plot( fir122 )

testCase.verifyEqual( fir11, fir21, "AbsTol", 1e-4)
testCase.verifyEqual( fir12, fir22, "AbsTol", 1e-4)

end
































