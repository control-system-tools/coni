function [tests] = signalsTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testSmoothStep(testCase)
% computed step by hand:
z = sym('z', 'real');
d = 1;
sig = 3 / d^2 * z^2 - 2 / d^3 * z^3;
Z = quantity.Domain('z', linspace(0, 1, 101));

SI = quantity.Symbolic(sig, Z);

% compute step by function
si = quantity.Symbolic( stateSpace.setPointChange(0, 1, 0, 1, 1, "var", Z.name), Z );

%
testCase.verifyEqual(si.on(), SI.on(), 'AbsTol', 6e-16);
end

function testUniformDistributedSignal(testCase)

t = quantity.Domain("t", linspace(0, 5, 1e3));

% test the init
s1 = signals.UniformDistributedSignal(t, "name", "s", "f0", 2, "fEnd", -2);

testCase.verifyEqual( s1.at(0), 2);
testCase.verifyEqual( s1.atIndex(end), -2 );

s2 = signals.UniformDistributedSignal(t, "name", "s2", "filter", "gevrey", "seedf", 1, "seedt", 1);
s3 = signals.UniformDistributedSignal(t, "name", "s3", "filter", "gevrey", "seedf", 1, "seedt", 1);

testCase.verifyEqual( s2.on(), s3.on() );

T = 0.07;
D = 0.7;
Gpt2 = tf(1, [T^2, 2 * T * D, 1]);

s4 = signals.UniformDistributedSignal(t, 'steps', 7, ...
					'seedt', -1, ...
					'fmin', -100, ...
					'fmax', 299, ...
					'filter', Gpt2, "f0", 3);
testCase.verifyEqual(s4.at(0), 3, "AbsTol", 10*eps);


for k = 1:100
	s5 = signals.UniformDistributedSignal(t, "steps", t.n);
	% verify if the sorted values are close to a linear function from lower to upper value:
	uniformTest = max(  abs((sort(s5.on()) - linspace(-1,1,t.n).').^2) );
% 	fprintf("Uniform test is %g \n", uniformTest)
	testCase.verifyTrue(uniformTest < 1e-1)
end

%% test the normalize option
s5 = signals.UniformDistributedSignal(t, 'steps', 7, ...
					'seedt', -1, ...
					'fmin', -100, ...
					'fmax', 299, ...
					'normalize', true);
s5On = s5.on();
testCase.verifyTrue( all( s5On == s5.fmax | s5On == s5.fmin ) );

end
%%
function testNormalDistribution(testCase)

t = quantity.Domain("t", linspace(0,1,1e4));

for k = 1:3
	s1 = signals.NormalDistributedSignal(t, "steps", t.n);	
	h = vartest(s1.on(), 1, 'alpha', 0.0001);
	testCase.verifyFalse(logical(h))
	if h
		fprintf("Variance is %g \n", var(s1.on()))
	end
end

end