function [tests] = BasicVariableTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testQuantityDiscreteCast(testCase)
d = quantity.Domain("t", linspace(0,pi,3));
q = quantity.Discrete( sin(d.grid), d);
b = BasicVariable(q, "numDiff", 5);

testCase.verifyInstanceOf( b, "signals.BasicVariable");
testCase.verifyEqual( b.highestDerivative, 5);
testCase.verifyEqual( b.fun.on(), q.on());

% test the cast of a symbolic:
s = quantity.Symbolic( sin( sym("t") ) , d);
b = BasicVariable( s, "numDiff", 4);

testCase.verifyEqual( s.on(), b.fun.on() )
for k = 1:b.highestDerivative
	testCase.verifyEqual( s.diff(d, k).on(), b.diff(d, k).on())
end

% test the cast of matrix valued quantities:
c = quantity.Symbolic( cos( sym("t") ), d);
v = [s; c];
b = BasicVariable( v );

testCase.verifySize( b, [2,1]);

r = [s, c];
b = BasicVariable( r );
testCase.verifySize( b, [1,2]);

m = v * r;
b = BasicVariable( m );
testCase.verifySize( b, [2,2]);

testCase.verifyEqual( b(1,1).fun.on(),  on( s*s ) );
testCase.verifyEqual( b(2,1).fun.on(),  on( c*s ) );
testCase.verifyEqual( b(2,2).fun.on(),  on( c*c ) );

end

function taylorPolynomialBasis(testCase)

tau = quantity.Domain("tau", linspace(0,1));
b = signals.Monomial.taylorPolynomialBasis(tau, "order", 4, "numDiff", 4);

ttau = sym("tau");

p0 = quantity.Symbolic( 1, tau);
p1 = quantity.Symbolic( ttau, tau);
p4 = quantity.Symbolic( ttau^4 / factorial(4), tau);

testCase.verifyEqual( b(1).fun.on(), p0.on())
testCase.verifyEqual( b(2).fun.on(), p1.on())
testCase.verifyEqual( b(5).fun.on(), p4.on(), "RelTol", 1e-12)

end

function fourierBasisTest(testCase)

tau = quantity.Domain("tau", linspace(0, ceil( rand(1) * 10 )));
b = signals.Sinusoidal.fourierBasis(tau, "order", 8, "numDiff", 2);

s1 = quantity.Symbolic( sin( pi * sym("tau") / tau.upper), tau);
s4 = quantity.Symbolic( sin( 4 * pi * sym("tau") / tau.upper ), tau);

c0 = quantity.Symbolic( 1, tau);
c2 = quantity.Symbolic( cos( 2 * pi * sym("tau") / tau.upper), tau);
c3 = quantity.Symbolic( cos( 3 * pi * sym("tau") / tau.upper), tau);

testCase.verifyEqual( b(1).fun.on(), c0.on())
testCase.verifyEqual( b(2).fun.on(), s1.on())
testCase.verifyEqual( b(8).fun.on(), s4.on())
testCase.verifyEqual( b(5).fun.on(), c2.on())
testCase.verifyEqual( b(7).fun.on(), c3.on())

%test sinusoidal basis functions on a grid not starting atzero
t = quantity.Domain("t", linspace(0.2, 1.5));
b = signals.Sinusoidal.fourierBasis(t, "order", 2, "numDiff", 2);

s1 = quantity.Symbolic(1, t);
w = pi / (t.upper - t.lower);
s2 = quantity.Symbolic( sin( w * sym("t") - w * t.lower), t);
s3 = quantity.Symbolic( cos( w * sym("t") - w * t.lower), t);

bf = b.f;

testCase.verifyEqual( bf(1).on(), s1.on())
testCase.verifyEqual( bf(2).on(), s2.on())
testCase.verifyEqual( bf(3).on(), s3.on())

end

function sinusoidalTest(testCase)

tau = quantity.Domain("tau", linspace(0, 1));
w = pi;
f = quantity.Symbolic( sin(w * sym("tau") ), tau);
s = signals.Sinusoidal(tau, w, "numDiff", 3);

testCase.verifyEqual( f.on(), s.diff(tau, 0).on())
testCase.verifyEqual( f.diff(tau,1).on(), s.diff(tau, 1).on())
testCase.verifyEqual( f.diff(tau,2).on(), s.diff(tau, 2).on())
testCase.verifyEqual( f.diff(tau,3).on(), s.diff(tau, 3).on())

% verify a different frequency
w = 3 * pi;
h = quantity.Symbolic( cos( w* sym("tau")), tau);
c = signals.Sinusoidal(tau, w, "type", "cos");

testCase.verifyEqual( h.on(), c.diff(tau, 0).on())
testCase.verifyEqual( h.diff(tau,1).on(), c.diff(tau, 1).on())
testCase.verifyEqual( h.diff(tau,2).on(), c.diff(tau, 2).on())

% test the sinusoidal on a grid with t0 \not= 0
t = quantity.Domain("t", linspace( 0.1, 1.5));

f = quantity.Symbolic( sin( (pi*sym("t") - 0.1*pi) / (t.upper - t.lower)), t);
s = signals.Sinusoidal( t, pi/ (t.upper - t.lower), "phi", - t.lower * pi / (t.upper - t.lower));

testCase.verifyEqual( f.on(), s.f.on());
testCase.verifyEqual( f.diff(t,1).on(), s.diff(t, 1).on);
testCase.verifyEqual( f.diff(t,2).on(), s.diff(t, 2).on);
testCase.verifyEqual( f.diff(t,3).on(), s.diff(t, 3).on);

end

function PolyBumpSetGainTest(testCase)

tau = quantity.EquidistantDomain("tau", 0, pi);
p = signals.PolyBump(tau, "norm", true);

maxVal = max(p.fun);
p.setGain( 1/maxVal );

pSym = quantity.Symbolic( ...
	subs(sym(signals.PolyBump.polyBumpFunction( p.a, p.b, p.c, p.T0, p.T1)), "t", "tau"), tau );

testCase.verifyEqual( p.diff(tau, 0).on(), pSym.on(), "AbsTol", 1e-15  );
testCase.verifyEqual( p.diff(tau, 1).on(), on( pSym.diff(tau, 1) ), "AbsTol", 1e-15 );
testCase.verifyEqual( p.diff(tau, 2).on(), on( pSym.diff(tau, 2) ) );
end

function PolyBumpNormTest(testCase)

t = quantity.EquidistantDomain('t', sqrt(2), 2 * sqrt(2), 'stepSize', 0.1);
p = signals.PolyBump(t, 'a', 3, 'b', 7, 'norm', true);
testCase.verifyEqual( int(p.fun, t, p.T0, p.T1), 1, 'AbsTol', 6e-9);

end

function PolyBumpOnTest(testCase)

tau = quantity.EquidistantDomain("tau", 0, pi);
p = signals.PolyBump(tau);

pSym = quantity.Symbolic( ...
	subs(sym(signals.PolyBump.polyBumpFunction( p.a, p.b, p.c, p.T0, p.T1)), "t", "tau"), tau );

testCase.verifyEqual( p.diff(tau, 0).on(), pSym.on() );
testCase.verifyEqual( p.diff(tau, 1).on(), on( pSym.diff(tau, 1) ) );
testCase.verifyEqual( p.diff(tau, 2).on(), on( pSym.diff(tau, 2) ) );

% verify piecewise evaluation
t = quantity.EquidistantDomain('t', -1, 2);
p = signals.PolyBump(t, 'T0', 0, 'T1', 1);
val = p.fun.on;

testCase.verifyTrue( all(val(t.grid < 0) == 0) )
testCase.verifyTrue( all(val(t.grid > 1) == 0) )
testCase.verifyTrue( all(val(t.grid > 0 & t.grid < 1) ~= 0) )


%
t = quantity.Domain("t", linspace(7, 11));
p = signals.PolyBump(t, "a", 2, "b", 2);
testCase.verifyEqual( p.f.domain.grid, t.grid)
testCase.verifyEqual( p.diff(t, 1).at(t.lower), 0)
testCase.verifyEqual( p.diff(t, 1).at(t.upper), 0)

end

function gevreyFunctionGainTest(testCase)

t =  quantity.Domain("t", linspace(0, 1, 11)');
g1 = signals.GevreyFunction('order', 1.8, 'timeDomain', t);
g2 = signals.GevreyFunction('order', 1.8, 'timeDomain', t, "gain", 2);
g1.setGain(2);
testCase.verifyTrue(g1.diffs.near(g2.diffs));

end

function gevreyFunctionInitTest(testCase)

%%
t =  quantity.Domain("t", linspace(0, 1, 11)');
g1 = signals.GevreyFunction('order', 1.8, 'timeDomain', t);
g2 = signals.GevreyFunction('order', 1.5, 'timeDomain', t);

G = [g1; g2];
N_diff = 2;
derivatives = G.diffs(0:N_diff);

GD = zeros(t.n, N_diff+1, 2);
for k = 1:N_diff+1
	GD(:, k, 1) = signals.gevrey.bump.g(t.grid, k-1, t.grid(end), g1.sigma);
	GD(:, k, 2) = signals.gevrey.bump.g(t.grid, k-1, t.grid(end), g2.sigma);
end
	
testCase.verifyEqual(derivatives.on(), GD);
end

function diffTest(tc)
	t = sym('t');
	T = quantity.Domain('t', linspace(0,1));
	fun = t^2 * (1-t)^2;
	
	F = quantity.Symbolic(fun, T);
	d1F = F.diff("t", 1);
	d2F = F.diff("t", 2);
	
	b(1,:) = signals.BasicVariable(F, {d1F, d2F});
	b(2,:) = signals.BasicVariable(F, {d1F, d2F});
		
	tc.verifyEqual(b.diff(T, 0), [F; F]);
	tc.verifyEqual(b.diff(T, 1).on(), on(d1F * ones(2,1)));
	tc.verifyEqual(b.diff(T, 2).on(), on(d2F * ones(2,1)));
	tc.verifyError(@() b.diff(T, 3), 'conI:signals:BasicVariable:derivative')
end

function productRuleTest(testCase)

t = quantity.Domain("t", linspace(0,1,11));
phi = quantity.Symbolic( sin( sym("t") * pi), t);
bfun = quantity.Symbolic( cos(sym("t") * pi), t);
b = signals.BasicVariable( bfun, { bfun.diff(t, 1), bfun.diff(t, 2), bfun.diff(t, 3) });

for k = 0:3
	h1 = b.productRule(phi, k);
	h2 = diff( phi * bfun, t, k);
	testCase.verifyEqual( h1.on, h2.on, 'AbsTol', 1e-13 );
end

%% test product rule for a scalar and a vector:
a = [phi; bfun];
a = signals.BasicVariable( a, { a.diff(t, 1), a.diff(t, 2), a.diff(t, 3) });

for k = 0:3
	c = a.productRule( bfun, k );
	testCase.verifyEqual( c.on(), diff( a.diff(t,0) * bfun, t, k).on(), 'AbsTol', 1e-13);
	
	c = b.productRule( a, k);
	testCase.verifyEqual( c.on(), diff( bfun * a.diff(t,0), t, k).on(), 'AbsTol', 1e-13);
	
end

%% test product rule for two vectors:
for k = 0:3
	c = a.productRule(a',k);
	testCase.verifyEqual( c.on(), diff( a.diff(t,0) * a.diff(t,0).', t, k ).on(), 'AbsTol', 1e-13);
	c = productRule(a', a, k);
	testCase.verifyEqual( c.on(), diff( a.diff(t,0).' * a.diff(t,0), t, k ).on(), 'AbsTol', 1e-13);
end


end

function diffShiftTest(testCase)

I = quantity.Domain("t", linspace(0,1));
m2 = signals.Monomial(I, 2);
m1 = m2.diffShift(1);

testCase.verifyEqual( m2.derivatives{1}.on(), m1.fun.on());
testCase.verifyEqual( m2.derivatives{2}.on(), m1.derivatives{1}.on());

end

function plusTest(testCase)

I = quantity.Domain("t", linspace(0,1));
m2 = signals.Monomial(I, 2);
m3 = signals.Monomial(I, 3, 'numDiff', 7);

m = m2 + m3;

testCase.verifyEqual( m.fun.on(), on(m2.diff(I, 0) + m3.diff(I, 0)));
testCase.verifyEqual( m.derivatives{1}.on(), on(m2.diff(I, 1) + m3.diff(I, 1)));

end

function monomeTest(testCase)

I = quantity.Domain("t", linspace(0,1));
m = signals.Monomial(I, 2);
testCase.verifyEqual( m.derivatives{2}.on(), quantity.Discrete.ones(1, I).on() )
testCase.verifyEqual( m.derivatives{3}.on(), quantity.Discrete.zeros(1, I).on() )

end

function gevreyBumpTest(testCase)
% Test script to check if the class for the bumb function returns the right
% signals.

g = signals.GevreyFunction('diffShift', 1);
b = signals.gevrey.bump.dgdt_1(g.T, g.sigma, g.domain.grid) * 1 / signals.gevrey.bump.dgdt_0(g.T, g.sigma, g.T);

%% Test scaling of Gevrey function
verifyEqual(testCase, b, g.fun.on(), 'AbsTol', 1e-13);

% test the derivatives of the bump
g = signals.GevreyFunction('diffShift', 0);

orders = 1:5;
derivatives = arrayfun(@(i) g.diff(g.domain, i), orders);
verifyEqual(testCase, [derivatives.at(0), derivatives.at(g.T)], zeros( 1, 2*length(orders)), 'AbsTol', 1e-13)

end

function gevreyFunctionBoundaryValuesTest(testCase)
	
N = 3;
tau = quantity.Domain.defaultDomain(42, "tau");
g(1,1) = signals.GevreyFunction('order', 1.5, 'gain', -5, 'offset', 2.5, 'numDiff', N, ...
	'timeDomain', tau, 'diffShift', 0);

g(2,1) = signals.GevreyFunction('order', 1.9, 'gain', 5, 'offset',- 2.5, 'numDiff', N, ...
	'timeDomain', tau, 'diffShift', 0);

testCase.verifyEqual(g.diff(tau, 0).at(0), [2.5; -2.5])
testCase.verifyEqual(g.diff(tau, 0).at(1), [-2.5; 2.5])

% test all the precomputed derivatives plus one higher derivative
for k = 1:N+1
	testCase.verifyEqual(g.diff(tau, k).at(0), zeros(2,1))
	testCase.verifyEqual(g.diff(tau, k).at(1), zeros(2,1))
end

end

function gevreyFunctionMultiplicationTest(testCase)

N = 3;
tau = quantity.Domain.defaultDomain(42, "tau");
g(1,1) = signals.GevreyFunction('order', 1.5, 'gain', -5, 'offset', 2.5, 'numDiff', N, ...
	'timeDomain', tau, 'diffShift', 0);

g(2,1) = signals.GevreyFunction('order', 1.9, 'gain', 5, 'offset',- 2.5, 'numDiff', N, ...
	'timeDomain', tau, 'diffShift', 0);

A = [1 2; 3 4];

b = A*g;
testCase.verifyEqual(b.diff(tau, 0).at(0), A * [2.5; -2.5])
testCase.verifyEqual(b(1).highestDerivative, g(1).highestDerivative)

B = [1; 2];
c = B * g(1);
testCase.verifyEqual( c(1).derivatives{end}.on(), g(1).derivatives{end}.on() )
testCase.verifyEqual( c(2).derivatives{end}.on(), 2*g(1).derivatives{end}.on() )

end

function testBasicVariableFractionalDerivative(testCase)
	
	t = sym('t');
	t0 = 0;
	timeDomain = quantity.Domain('t', linspace(t0, t0+1, 1e2));
	
	%% verify singularity case
	beta = 1.5;
	F = quantity.Symbolic(( t - t0 )^beta, timeDomain);
	b(1,:) = signals.BasicVariable(F, {F.diff("t", 1), F.diff("t", 2), F.diff("t", 3)});
	p = 0.3;
	testCase.verifyError( @() b.diff(timeDomain, p), "conI:BasicVariable:fDiff")
	
	
	%% verify regular case
	for beta = [1, 2, 3]
		F = quantity.Symbolic(( t - t0 )^beta, timeDomain);
		b(1,:) = signals.BasicVariable(F, {F.diff("t", 1), F.diff("t", 2), F.diff("t", 3)});
		
		alpha = 0.3;
		for k = 1:5
			p = k*alpha;
			% compute the fractional derivative with the formula for the polynomial (see Podlubny: 2.3.4)
			f_diff(k,1) = quantity.Symbolic( fractional.monomialFractionalDerivative(t, t0, beta, p), timeDomain);
			
			% compute the fractional derivative numerically
			b_diff(k,1) = b.diff(timeDomain, p);
		end
		% plot(f_diff - b_diff)
		
		testCase.verifyEqual( b_diff.on(), f_diff.on(), 'AbsTol', 2e-2)
	end
	
	%% verify fractional integration
	testCase.verifyEqual( b.diff(timeDomain, 0).on(), F.on() )
	
	for k = 1:5
		alpha = 0.3 * k;
		J = int( (t- sym("tau")).^(alpha -1) * subs(F.sym, "t", "tau"), "tau", t0, t) / gamma(alpha);
		J_num = b.diff( timeDomain, -alpha );
		
		J = quantity.Symbolic(J, timeDomain);
		
		testCase.verifyEqual( J.on() , J_num.on, 'AbsTol', 3e-3 );
	end
	
end

function testBasicVariableFractionalProductRule(testCase)

t = quantity.Domain("t", linspace(0,1));
% choose a polynomial basis
N = 5;


beta = 1;
h(1,1) = quantity.Symbolic(sym("t").^beta, t);
poly(1,1) = quantity.Symbolic( sym("t"), t);
for k = 2:N
	h(k,1) = h(k-1).diff();
	poly(k,1) = quantity.Symbolic( sym("t")^(k-1) / factorial(k-1), t);
end

b = signals.BasicVariable( h(1), num2cell(h(2:end)) );

for k = 1:3
	
	f = sym(b.diff(t, 0)) * sym(poly(k));
	
	alpha = 0.3 * k;
	n = ceil(alpha);
	
	frac_f = quantity.Symbolic(fractional.derivative(f, alpha, t.lower, t.name), t);
	frac_p = b.productRule(poly(k), alpha);
	
	testCase.verifyEqual(frac_f.on(), frac_p.on(), 'AbsTol', 4e-3) 
end

end

function testProductBasicVariables(testCase)

t = quantity.Domain("t", linspace(0,1,11));
a = quantity.Symbolic( [cos(sym("t") * pi); sin(sym("t"))], t);
b = signals.BasicVariable( a, { a.diff(t, 1), a.diff(t, 2), a.diff(t, 3) });

c = a.' * a;
d = b.' * b;
for k = 0:3
	h = productRule(b.', b, k);
	testCase.verifyEqual( h.on(), c.diff(t, k).on(), 'AbsTol', 1e-13 );
	testCase.verifyEqual( d.diff(t, k).on(), c.diff(t, k).on(), "AbsTol", 1e-13);
end

c = a * a.';
d = b * b.';
for k = 0:3
	h = productRule(b, b.', k);
	testCase.verifyEqual( h.on(), c.diff(t, k).on(), 'AbsTol', 1e-13 );
	testCase.verifyEqual( d.diff(t, k).on(), c.diff(t, k).on(), "AbsTol", 1e-13);
end
end

function testElementWiseProductBasicVariables(testCase)

t = quantity.Domain("t", linspace(0,1,11));
a = quantity.Symbolic( [cos(sym("t") * pi); sin(sym("t"))], t);
b = signals.BasicVariable( a, { a.diff(t, 1), a.diff(t, 2), a.diff(t, 3) });

c = a .* a;
d = b .* b;

e = b .* 2;
f = 2 .* b.';
for k = 0:3
	testCase.verifyEqual( d.diff(t, k).on(), c.diff(t, k).on(), "AbsTol", 1e-13);
	testCase.verifyEqual( e.diff(t, k).on(), a.diff(t, k).on() * 2, "AbsTol", 1e-13);
	testCase.verifyEqual( f.diff(t, k).on(), ctranspose(a).diff(t, k).on() * 2, "AbsTol", 1e-13);
end
end

function testKronBasicVariable(testCase)

t = quantity.Domain("t", linspace(0,1,101));
a = quantity.Symbolic( [cos(sym("t") * pi); sin(sym("t"))], t);
b = signals.BasicVariable( a, { a.diff(t, 1), a.diff(t, 2), a.diff(t, 3) });

t2 = quantity.Domain("t", linspace(0.2, 0.8, 101));

c = kron( magic(3), a);
d = kron( magic(3), b);

for k = 0:b.degree
	testCase.verifyEqual( d.diff(t, k).on(t2), c.diff(t, k).on(t2), "AbsTol", 2e-1);
end

end

function basicVariableCast2DiscreteTest(testCase)

t = quantity.Domain("t", linspace(0,1,11));
a = quantity.Symbolic( [cos(sym("t") * pi); sin(sym("t"))], t);
b = signals.BasicVariable( a, { a.diff(t, 1), a.diff(t, 2), a.diff(t, 3) });

c = quantity.Discrete(b);

for k = 0:b.degree
	testCase.verifyEqual( on( c(:,:,k+1) ), a.diff(t, k).on() )
end

A = a * a.';
B = b * b.';
C = quantity.Discrete(B);

for k = 0:B.degree
	testCase.verifyEqual( on( C(:,:,k+1) ), A.diff(t, k).on(), "AbsTol", 1e-14 )
end

end