function resultSs = connect(InputName, OutputName, options, varargin)
% stateSpace.connect  Block-diagram interconnections of dynamic systems based on ss/connect.
% Different from the built-in function, InputName and OutputName are asserted to be
% unique. And it is more robust, since feedthroughs are removed before calling
% connect and are added afterwards again.

% remove empty elements of varargin
% note that empty(ss) does not work, since state space models without inputs are
% considered as empty. Hence, size(v, 1) > 0 is used instead.
if isa(options, "ltioptions.connect")
	inputSs = varargin(cellfun(@(v) size(v, 1) > 0, varargin));
else
	if isa(options, "numlti")
		inputSs = [{options}, varargin(cellfun(@(v) size(v, 1) > 0, varargin))];
	elseif ~isempty(options)
		error("third input must be a connectOptions() or a StateSpaceModel");
	end
	options = connectOptions("Simplify", false);
end

% clean state-spaces
for it = 1 : numel(inputSs)
	% remove feedthroughs
	inputSs{it} = stateSpace.removeInputOfOutput(ss(inputSs{it}));
	inputSs{it}.OutputName = stateSpace.removeSingularEnumeration(inputSs{it}.OutputName);
	inputSs{it}.InputName = stateSpace.removeSingularEnumeration(inputSs{it}.InputName);
end

% get InputNames of result
resultInputNames = unique(stateSpace.removeSingularEnumeration(InputName), "stable");
resultOutputNames = unique(stateSpace.removeSingularEnumeration(OutputName), "stable");
if isempty(resultInputNames)
	% as ss/connect does not support empty cell array for inputNames or outputNames, this dummy
	% object is added for those cases.
	resultInputNames = "dummy.input";
	resultOutputNames = [resultOutputNames; "dummy.output"];
	inputSs = [inputSs, ...
		{ss([], [], [], 0, "InputName", "dummy.input", "OutputName", "dummy.output")}];
	% call built-in function
	resultSs = connect(inputSs{:}, resultInputNames, resultOutputNames, options);
	resultSs = removeDummySignals(resultSs);
else
	% call built-in function
	resultSs = connect(inputSs{:}, resultInputNames, resultOutputNames, options);
end

end % stateSpace.connect()

function newSs = removeDummySignals(mySs)
	dummyInputSelector = strcmp(string(mySs.InputName), "dummy.input");
	dummyOutputSelector = strcmp(string(mySs.OutputName), "dummy.output");
	
	if all(dummyInputSelector)
		newSs = ss(mySs.A,[], mySs.C(~dummyOutputSelector, :), [], mySs.Ts, ...
			"OutputName", mySs.OutputName(~dummyOutputSelector));
		
	elseif all(dummyOutputSelector)
		newSs = ss(mySs.A, mySs.B(:, ~dummyInputSelector), [], [], mySs.Ts, ...
			"InputName", mySs.InputName(~dummyInputSelector));
		
	else
		newSs = ss(mySs.A, mySs.B(:, ~dummyInputSelector), mySs.C(~dummyOutputSelector, :), ...
			mySs.D(~dummyOutputSelector, :), mySs.Ts, ...
			"InputName", mySs.InputName(~dummyInputSelector), ...
			"OutputName", mySs.OutputName(~dummyOutputSelector));
	end

end % removeDummySignals