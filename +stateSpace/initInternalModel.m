function [myOde, spectrumIndividual] = initInternalModel(...
		S, inputVectorMode, numberOfCopies, ...
		stateName, internalModelInputName, outputSignalName, referenceSignalName)
%misc.initInternalModel returns the state-space model myOde, which is an stateSpace.Odes object and
%represents an p-internal model based on the signal model dynamics defined by S. For this, S is
%split into Jordan chains and redundancies are removed, thus only the cyclic part is considered. The
%input vector of mySs is either the 1-vector (inputVectorMode="full") or the input vector with the
%least non-zero entries (inputVectorMode="full" (=default)), i.e. it is zero, but 1 for every last
%row of a Jordan block. outputSignalName, referenceSignalName)
%
%Lastly, for multiple outputs, multiple copies of the internal model are needed, which is specified
%by the last input parameter numberOfCopies.
%
% mySs = misc.initInternalModel(S) generates a state space model which can create all signal forms
%	specified by the dynamic matrix S. The input vector mySs.B contains as many zeros as possible.
%
% mySs = misc.initInternalModel(S, inputVectorMode) specifies if mySs.B is the 1-vector
%	(inputVectorMode="full") or the input vector with the least non-zero entries
%	(inputVectorMode="full" (=default)).
%
% mySs = misc.initInternalModel(S, inputVectorMode, numberOfCopies) creates n=numberOfCopies
%	(default = 1) copies of the internalModel.
%
% mySs = misc.initInternalModel(S, inputVectorMode, numberOfCopies, stateName) the
%	outputName-property of the resulting stateName will be set to stateName
%	(default: 'internalModel')
%
% mySs = misc.initInternalModel(S, inputVectorMode, numberOfCopies, ...
%	stateName, internalModelInputName) the inputName-property of the resulting state space will be
%	set to controlOutputName (default: 'trakcingError').
%
% mySs = misc.initInternalModel(S, inputVectorMode, numberOfCopies, ...
%	stateName, internalModelInputName, outputSignalName, referenceSignalName) 
%	additional output of the internal model state space will be 
%		internalModelInputName = outputSignalName - referenceSignalName.
%	The InputName are outputSignalName and referenceSignalName instead of internalModelInputName.
%
% See also stateSpace.cyclic, stateSpace.outputRegulation.

arguments
	S (:, :) double;
	inputVectorMode (1, 1) string = "minimal";
	numberOfCopies (1, 1) double = 1;
	stateName string = "internalModel";
	internalModelInputName string = "trackingError";
	outputSignalName string = "controlOutput";
	referenceSignalName string = string();
end % arguments

if numberOfCopies > 0
	% Smin: find minimal representation of S
	[sMin, lengthBlocks] = stateSpace.cyclic(S);
	spectrumIndividual = eig(sMin);

	% B: choose B according to inputVector mode
	switch inputVectorMode
		case "full"
			B = ones(size(sMin, 1), 1);
		case "minimal"
			B = zeros(size(sMin, 1), 1);
			B(cumsum(lengthBlocks), 1) = 1;
		otherwise
			error("inputVectorMode must be minimal or full but is " + inputVectorMode);
	end %switch

	% add additional copies
	sMinUnique = sMin;
	Bunique = B;
	for it = 2 : numberOfCopies
		sMin = blkdiag(sMin, sMinUnique);
		B = blkdiag(B, Bunique);
	end

	mySs = ss(sMin, B, eye(size(sMin)), []);
	mySs = stateSpace.setSignalName(mySs, "output", stateName, size(mySs.C, 1));

	if strlength(referenceSignalName) > 0
		% implement tracking error via sum 
		%	internalModelInputName = outputSignalName - referenceSignalName
		mySs = stateSpace.setSignalName(mySs, "input", internalModelInputName, size(mySs.B, 2));
		% get sum to create tracking error signal in simulation, i.e. a sum block:
		trackingSum = sumblk(char(internalModelInputName + ...
			"=" + outputSignalName + "-" + referenceSignalName), ...
			numberOfCopies);
		mySs = stateSpace.connect(trackingSum.InputName, [trackingSum.OutputName; mySs.OutputName], mySs, trackingSum);

	else
		% feed internal model via outputSignalName
		mySs = stateSpace.setSignalName(mySs, "input", internalModelInputName, size(mySs.B, 2));
	end

	myOde = stateSpace.Odes(mySs, stateName);
else
	myOde = stateSpace.Odes(ss(), stateName);
end % if numberOfCopies > 0

end % initInternalModel()