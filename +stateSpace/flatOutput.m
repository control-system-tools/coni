function [C, kappa, de] = flatOutput(A, B)
% FLATOUTPUT computes a flat output for a LTI system given by (A, B)
% [C, r] = flatOutput(A, B) computation of a flat output for the state-space system given by the
% matrix pair (A, B). C is a flat output, kappa is a vector with the controllability indices.

dim.x = size(A,1);
dim.u = size(B,2);

Q = ctrb(A, B);
assert( rank(Q) == dim.x );

% find the controllability indices:
kappa = zeros( dim.u, 1);
for i = dim.x:-1:1
	U = Q(:,1: (dim.u)*i);
	Ui = [U, A^i * B];
	
	for j = dim.u:-1:1
		if rank( Ui(:, 1 : end - (dim.u-j) ) ) == rank( Ui(:, 1 : end - (dim.u-j) -1 ) )
			kappa(j) = i;
		end
	end
end
assert( sum(kappa) == dim.x )

Qs = [];
for i = 1:dim.u
	for j = 1:kappa(i)
		Qs = [Qs, A^(j-1)*B(:,i)];
	end
end

E = zeros( dim.u, dim.x );
for i = 1:dim.u
	E(i,sum(kappa(1:i))) = 1;
end

C = E / Qs;


% compute the differential parametrization:
%T = zeros( sum(r), dim.x);
T = [];
for i = 1:dim.u
	
	Ti = zeros( kappa(i), dim.x);
	for j = 1:kappa(i)
		Ti(j,:) = C(i,:) * A^(j-1);
	end
	T = [T; Ti];
end

Tinv = inv(T);
for i = 1:max(kappa)
	for j = 1:dim.u
		ei{j} = zeros(kappa(j),1);
		if kappa(j) >= i
			ei{j}(i) = 1;
		end
	end
			
	E = blkdiag( ei{:});
	X{i} = Tinv * E;
end
	X{i+1} = zeros(dim.x, dim.u);

de.X = signals.PolynomialOperator( X );

As = zeros(dim.u, dim.x);
Ds = zeros(dim.u);
for i = 1:dim.u
	As(i,:) = C(i,:) * A^(kappa(i));
	Ds(i,:) = C(i,:) * A^(kappa(i)-1) * B;
end

DsInv = inv(Ds);

de.U = - DsInv * As * de.X;

for j = 1:dim.u
	Ej = zeros(dim.u);
	Ej(j,j) = 1;
	de.U(kappa(j)+1).coefficient = de.U(kappa(j)+1).coefficient + DsInv * Ej;
end

