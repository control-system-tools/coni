function [Phi, U, V] = fundamentalMatrices(A, domain, optArgs)
%stateSpace.transitionMatrix.fundamentalMatrices computation of the state transition matrix
%utilizing fundamental matrices
arguments
	A (:,:) quantity.Discrete;
	domain (2,1) quantity.Domain;
	optArgs.options = odeset('RelTol',1e-12,'AbsTol',1e-15);
end

assert(A.nargin == 1);
n = size(A,1);

U = zeros( [domain(1).n, n, n] );
V = zeros( [domain(2).n, n, n] );

fu = @(z, u) A.at(z) * u;
mAT = -A.';
fv = @(z, v) mAT.at(z) * v;


pbar = misc.ProgressBar(...
    'name', 'fundamental matrix_0: ', ...
    'steps', 2*n-1, ...
    'initialStart', true);

q = parallel.pool.DataQueue;
afterEach(q, @pbar.raise);
for i = 1:n
	[~, U(:,:,i)] = ode15s(fu, domain(1).grid, misc.unitVector(n, i), optArgs.options);
	send(q, []);
	[~, V(:,:,i)] = ode15s(fv, domain(2).grid, misc.unitVector(n, i), optArgs.options);
	send(q, []);
end
pbar.stop();

U = quantity.Discrete(U, domain(1));
V = quantity.Discrete(V, domain(2));

Phi = U*V';

end