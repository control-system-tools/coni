function [sMin, lengthBlocks] = cyclic(S)
% cyclic reduces the spectrum of the dynamic matrix S to its cyclic part, i.e., such that still
% all modes can be generated, but - for instance - if there are two integrators in parallel, the
% result will only contain one integrator. Likewise, if there is an integrator and a series of
% integrators, then only the largest series of integrators will remain. This is done by tranforming
% S into a Jordan block matrix, then all the Jordan blocks are compared and those are removed whos
% complete spectrum is part of any other cell element. Lastly, if the Jordan blocks non-zero
% imaginary part, i.e. a sine like diag(-i, i), then this converted into a real matrix (for the sine
% example: [0, 1; -1, 0]). For ease of implementation, this function is restricted for matrices with
% soley imaginary spectrum, i.e. all(real(eig(S))==0). But this restriction might be removed, if the
% conversion from matrices with complex elements into the final matrix with real elements is
% implemented for this case.
%
% See also stateSpace.initInternalModel, stateSpace.outputRegulation
arguments
	S (:, :) double {mustBe.imaginarySpectrum};
end % arguments

% Calculate jordan blocks and split them into cell-elements
[~, jordanMatrix, lengthJordan] = misc.jordanReal(S);
jordanMatrixCell = misc.blkdiagInv(jordanMatrix, lengthJordan);

% calculate spectrum of each jordan block
spectrum = cell(size(jordanMatrixCell));
for it = 1 : numel(jordanMatrixCell)
	spectrum{it} = eig(jordanMatrixCell{it});
end % for it = 1 : numel(S)

% select which jordan block is already contained in other blocks, and set selector =
% false for those, which should be removed later.
selector = true(size(jordanMatrixCell));
for it = 1 : numel(jordanMatrixCell)
	for jt = it + 1 : numel(jordanMatrixCell)
		if numel(spectrum{it}) > numel(spectrum{jt})
			if all(ismember(spectrum{jt}, spectrum{it}), "all")
				selector(jt) = false;
			end
		else
			if all(ismember(spectrum{it}, spectrum{jt}), "all")
				selector(it) = false;
			end
		end		
	end % for jt = it + 1 : numel(jordanMatrixCell)
end % for it = 1 : numel(jordanMatrixCell)

% remove redundant dynamics of the jordanMatrixCell with the selector obtained above
lengthBlocks = lengthJordan(selector);
sMin = blkdiag(jordanMatrixCell{selector});
end % cyclic()