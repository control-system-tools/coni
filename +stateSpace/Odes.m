classdef Odes < handle & matlab.mixin.Copyable
%stateSpace.Odes (ordinary differential equations) is class to implement multiple different
%state-spaces (see doc/ss). Then, the overall state-space resulting from a connection of all
%individual state-spaces is created and available via the .odes-property. Different to the usual
%ss-builtin behavior, stateSpace.Odes also consideres the state-vectors as outputs, which helps for
%devloping state space methods.
	properties
		ss (:, 1) cell;		% cell-array of state spaces
		type (:, 1) string;	% string-array of names of ss and its state vector
		C (:, 1) cell;		% cell-array of output gains / matrices
		D (:, 1) cell;		% cell-array of feedthrough gains / matrices
	end % properties

	properties (Dependent = true)
		% connection of all state spaces in the ss property. The connection is performed by
		% connecting InputName and OutputName, hence a specification of those signal names for all
		% ss inputs-parameters is essential.
		odes (1, 1) ss;
		numTypes (1, 1);	% number of different types
		OutputName (:, 1) cell;	% OutputNames of ss - unique, sorted
		InputName (:, 1) cell;	% InputNames of ss - unique, sorted
		n (1, :) double;	% size(ss(:).A, 1)
	end % properties (Dependent = true)
	
	methods
		function obj = Odes(stateSpaces, type)
			% stateSpace.Odes constructs an object containing ss
			% Example:
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	myOdes = stateSpace.Odes(ss1, "ss1", ss2, "ss2")
			arguments (Repeating)
				stateSpaces ss;
				type string;
			end
			
			if nargin > 0
				obj.type = [type{:}];
				for it = 1 : numel(stateSpaces)
					[obj.ss{it}, obj.C{it}, obj.D{it}] ...
						= obj.separateOutputfromSs(stateSpaces{it}, obj.type(it));
				end % for it = 1 : numel(stateSpaces)
				obj.verifyProperties();
			end % if nargin > 0
		end % stateSpace.Odes() Constructor
		
		function verifyProperties(obj)
			assert(misc.isunique(obj.type), "type of misc.Odes must be unique");
			assert(isequal(size(obj.ss), size(obj.type)), ...
				"number of ss and types are not equal");
			assert(isequal(size(obj.ss), size(obj.C)), ...
				"number of ss and C are not equal");
			assert(isequal(size(obj.ss), size(obj.D)), ...
				"number of ss and D are not equal");
		end % verifyProperties()
		
		function obj = add(obj, varargin)
			% with add() multiple stateSpace.Odes are combined
			% Example: 
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	Ode1 = stateSpace.Odes(ss1, "ss1");
			%	Ode2 = stateSpace.Odes(ss2, "ss2");
			%	Odes12 = Ode1.add(Ode2);
			for myOde = varargin{:}
				assert(isa(myOde, "stateSpace.Odes"), "input must be an stateSpace.Odes-object");
				for it = 1 : myOde.numTypes
					assert(~any(contains(obj.type, myOde.type(it))), ...
						"The ss-type " + myOde.type(it) + " is already defined.");
					obj.ss{end+1} = myOde.ss{it};
					obj.C{end+1} = myOde.C{it};
					obj.D{end+1} = myOde.D{it};
					obj.type(end+1) = myOde.type(it);
				end
			end
			obj.verifyProperties();
		end % add()
		
		function obj = exchange(obj, myType, newSs)
			% Exchange one ss with another one
			% Example:
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	Ode = stateSpace.Odes(ss1, "ss1");
			%	Ode.exchange("ss1", ss2)
			[obj.ss{obj.index(myType)}, obj.C{obj.index(myType)}, obj.D{obj.index(myType)}] ...
				= obj.separateOutputfromSs(newSs, myType);
			obj.verifyProperties();
		end % exchange()
		
		function c = plus(a, b)
			% Example: 
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	Ode1 = stateSpace.Odes(ss1, "ss1");
			%	Ode2 = stateSpace.Odes(ss2, "ss2");
			%	Odes12 = Ode1 + Ode2
			if isempty(a)
				c = b;
			elseif isempty(b)
				c = a;
			elseif isa(a, "stateSpace.Odes") && isa(b, "stateSpace.Odes")
				c = copy(a);
				c = c.add(b);
			else
				error("input data type not recognized");
			end
			c.verifyProperties();
		end %plus()
		
		function obj = remove(obj, myType)
			% remove specified ss and type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	myOdes = stateSpace.Odes(ss1, "ss1", ss2, "ss2");
			%	myOdes.remove("ss1")
			arguments
				obj;
				myType (:, 1) string;
			end
			for it = 1 : numel(myType)
				selectRemaining = (1:1:obj.numTypes) ~= obj.index(myType(it));
				obj.ss = obj.ss(selectRemaining);
				obj.C = obj.C(selectRemaining);
				obj.D = obj.D(selectRemaining);
				obj.type = obj.type(selectRemaining);
			end
			obj.verifyProperties();
		end % remove()
		
		function obj = strrepType(obj, oldText, newText)
			% replace strings in type
			arguments
				obj;
				oldText (1, 1) string;
				newText (1, 1) string;
			end
			
			for it = 1 : numel(obj.type)
				if contains(obj.type(it), oldText)
					obj.type(it) = strrep(obj.type(it), oldText, newText);
					obj.ss{it} = stateSpace.setSignalName(...
							obj.ss{it}, "output", obj.type(it), obj.n(it));
					obj.C{it} = stateSpace.setSignalName(...
							obj.C{it}, "input", obj.type(it), obj.n(it));
				end
			end
		end % strrepType()

		function selectedOde = getOdes(obj, type)
			% getOdes returns among a stateSpace.Odes array only the stateSpace.Odes with the type
			% specified by the string input argument type.
			arguments
				obj stateSpace.Odes;
				type string;
			end
			selector = false(size(obj));
			for it = 1 : numel(type)
				selector = selector | strcmp(obj.getType(), type(it));
			end
			selectedOde = obj(selector);
		end % getOdes
		
		function obj = strrepOutputName(obj, oldText, newText)
			% replace strings in OutputName of obj.C
			arguments
				obj;
				oldText (1, 1) string;
				newText (1, 1) string;
			end
			
			for it = 1 : numel(obj.type)
				outputNameTemp = string(obj.C{it}.OutputName);
				if contains(outputNameTemp, oldText)
					obj.C{it}.OutputName = strrep(outputNameTemp, oldText, newText);
				end
			end
		end % strrepOutputName
		
		function newObj = extractOdes(obj, newType)
			% extractOdes creates a new stateSpace.Odes that only contains the parts of obj, for
			% which obj.type \in newType.
			arguments
				obj;
				newType (:, 1) string;
			end
			
			newArgs = cell([2*numel(newType), 1]);
			for it = 1 : numel(newType)
				idx = obj.index(newType(it));
				
				newArgs{2*it - 1} = series(obj.ss{idx}, obj.C{idx});
				if ~isempty(obj.D{idx})
					newArgs{2*it - 1} = parallel(newArgs{2*it - 1}, obj.D{idx});
				end
				newArgs{2*it} = newType(it);
			end % for it = 1 : numel(newType)
			newObj = stateSpace.Odes(newArgs{:});
		end % extractOde()
		
		function idx = index(obj, myType)
			% find ss by naming its type and return index-value
			% Example:
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	myOdes = stateSpace.Odes(ss1, "ss1", ss2, "ss2");
			%	myOdes.index("ss2")
			arguments
				obj;
				myType (1, 1) string;
			end
			idx = [];
			for it = 1 : obj.numTypes
				if strcmp(obj.type(it), myType)
					idx = it;
					return;
				end
			end
		end % index()
		
		function thisSs = getSs(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	myOdes = stateSpace.Odes(ss1, "ss1", ss2, "ss2");
			%	myOdes.getSs("ss2");
			thisSs = obj.ss{obj.index(myType)};
		end % getSs()
		
		function thisC = getC(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 3, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 4, "InputName", "u2", "OutputName", "y2");
			%	myOdes = stateSpace.Odes(ss1, "ss1", ss2, "ss2");
			%	myOdes.getC("ss2");
			thisC = obj.C{obj.index(myType)}.D; % yes, .D and not .C is correct
		end % getC()
		
		function obj = addB(obj, myType, B, inputName)
			arguments
				obj;
				myType (1, 1) string;
				B (:, :) double;
				inputName (1, 1) string;
			end % arguments
			
			idx = obj.index(myType);
			thisSs = obj.ss{idx};
			newB = [thisSs.B, B];
			newInputName = [thisSs.InputName; inputName + "(" + (1:1:size(B, 2)).' + ")"];
			obj.ss{idx} = ss(thisSs.A, newB, thisSs.C, thisSs.D, ...
				"OutputName", thisSs.OutputName, "InputName", newInputName);
		end % addB()
		
		function thisN = getN(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	myOdes = stateSpace.Odes(ss1, "ss1", ss2, "ss2");
			%	myOdes.getN("ss2");
			thisN = size(obj.ss{obj.index(myType)}.A, 1);
		end % getN()
		
		function thisD = getD(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, "InputName", "u1", "OutputName", "y1");
			%	ss2 = ss(2, 2, 2, 2, "InputName", "u2", "OutputName", "y2");
			%	myOdes = stateSpace.Odes(ss1, "ss1", ss2, "ss2");
			%	myOdes.getD("ss2");
			thisD = obj.D{obj.index(myType)}.D;
		end % getD()
				
		function result = isempty(obj)
			% A stateSpace.Odes is assumed to be empty if numTypes is zero or empty
			% Example:
			%	isempty(stateSpace.Odes())
			result = isempty(obj.numTypes) || (obj.numTypes == 0);
		end %isempty()
				
		function result = isequal(A, B, varargin)
			% isequal compares all parameters of A and B and varargin and returns
			% true if they are equal, and false if not. Two stateSpace.Odes are assumed
			% to be equal, if all ss and type are equal.
			result = (A.numTypes == B.numTypes) && isequal(A.type, B.type);
			if result
				for it = 1 : A.numTypes
					if result
						result = isequal(A.ss(it), B.ss(it));
					end
				end
			end
			if result && (nargin > 2)
				result = result && B.isequal(varargin{:});
			end
		end % isequal()
		
		function B = BofInputName(obj, inputName)
			arguments
				obj stateSpace.Odes;
				inputName (1, 1) string;
			end
			InputNames = stateSpace.removeEnumeration(obj.InputName);
			B = obj.odes.B;
			B = B(:, strcmp(InputNames, inputName));
		end % BofInputName
		
		function [ic, stateName] = getInitialCondition(obj, varargin)
			% getInitialCondition reads varargin if there are initial conditions
			% defined as name-value-pair with the name according to obj.stateName and
			% returns 2 cell arrays: 
			%	1. ic is a cell array of the values of the states initial condition
			%	2. stateName is a cell array of the names of those states.
			
			icStruct = misc.struct(varargin{:});
			
			% Create initial condition for all states specified by obj.type
			% If a value is specified in icStruct, than this value is used.
			% Otherwise, a default value is considered.
			icTemp = cell(obj.numTypes, 1);
			for it = 1 : obj.numTypes
				if misc.isfield(icStruct, obj.type(it))
					icTemp{it} = misc.getfield(icStruct, obj.type(it));
				else
					icTemp{it} = zeros(size(obj.ss{it}.A, 1), 1);
				end
			end
			
			ic = [icTemp(:)];
			stateName = [obj.type(:)];
		end % getInitialCondition()

		function ic = computeEquilibriumInitialCondition(thisOdes, simStateSpace, icVector)
			% computeEquilibriumInitialCondition computes the initial conditions of the
			% stateSpace.Odes thisOdes, which are incorporated in the state space model simModel,
			% such that the overall system simModel is in equilibrium at t=0. This is useful, for
			% instance, in order to derive the intial conditions of the internal model controller
			% dynamics for simulations.
			%
			%	ic = computeEquilibriumInitialCondition(thisOdes, simStateSpace, icVector) returns
			%		the initial conditions for all thisOdes in the cell-array ic as name value
			%		pairs.
			arguments
				thisOdes (:, 1) stateSpace.Odes;
				simStateSpace ss;
				icVector (:, 1) double;
			end

			% get location of internal model states in simModel
			simModelOutputs = stateSpace.removeEnumeration(simStateSpace.OutputName);
			internalModelSelectorOutput = false(size(simModelOutputs));
			for it = 1 : numel(thisOdes)
				internalModelSelectorOutput = internalModelSelectorOutput | ...
					strcmp(simModelOutputs, thisOdes(it).type);
			end
			internalModelSelectorState = any(simStateSpace.C(internalModelSelectorOutput, :) ~= 0).';
			% compute initial excitation based on given initial conditions
			dxdt = simStateSpace.A * icVector;
			% compute initial condition of internal models such that dxdt becomse zero
			%internalModelIcVector = lsqminnorm(A(:, internalModelSelectorState), -dxdt);
			internalModelIcVector = ...
				linsolve(simStateSpace.A(:, internalModelSelectorState), -dxdt) ...
				+ icVector(internalModelSelectorState, 1);
			
			% comute initialCondition of internalModels
			internalModelOrdering = unique(simModelOutputs(internalModelSelectorOutput), "stable");
			ic = cell(1, numel(internalModelOrdering)*2);
			runner = 0;
			for it = 1 : numel(internalModelOrdering)
				tmpOde = thisOdes.getOdes(internalModelOrdering(it));
				ic{2*it-1} = tmpOde.type;
				ic{2*it} = internalModelIcVector(runner + (1:tmpOde.n));
				runner = runner + tmpOde.n;
			end
		end % computeEquilibriumInitialCondition()

		
		function [texString, nameValuePairs] = printParameter(obj)
			myOdes = obj.odes;
			nameValuePairs = {
				"A", myOdes.A, ...
				"B", myOdes.B, ...
				"C", myOdes.C, ...
				"D", myOdes.D, ...
				};
			texString = misc.variables2tex([], nameValuePairs);
		end % printParameter()
		
		function texString = print(obj, NameValue)
			arguments
				obj stateSpace.Odes;
				NameValue.stateName string = "v";
			end
			
			% prepare signal names
			thisOutputName = string(stateSpace.removeEnumeration(obj.OutputName, true));
			thisOutputName = thisOutputName.extractBefore(2) + "_{" ...
				+ thisOutputName.extractAfter(1) + "}(t)";
			thisInputName = string(stateSpace.removeEnumeration(obj.InputName, true));
			thisInputName = thisInputName.extractBefore(2) + "_{" ...
				+ thisInputName.extractAfter(1) + "}(t)";
			
			if strlength(NameValue.stateName) > 1
				myOdeString = "\dot{" + NameValue.stateName.extractBefore(2) + "}" ...
					+ "_{" + NameValue.stateName.extractAfter(1) + "}(t) &= ";
			
				NameValue.stateName = NameValue.stateName.extractBefore(2) ...
				+ "_{" + NameValue.stateName.extractAfter(1) + "}(t)";
			else
				myOdeString = "\dot{" + NameValue.stateName + "}(t) &= ";
				NameValue.stateName = NameValue.stateName + "(t)";
			end
			
			% get ODE-lines
			myOdeString = myOdeString + misc.latexChar(obj.odes.A) + NameValue.stateName;
			if any(obj.odes.B ~= 0)
					myOdeString = myOdeString + " + ";
				if ~misc.iseye(obj.odes.B)
					myOdeString = myOdeString + misc.latexChar(obj.odes.B);
				end
				myOdeString = myOdeString + misc.latexChar(thisInputName);
			end
			
			% get output-lines
			myOutputString = misc.latexChar(thisOutputName) + " &= ";
			plusNeeded = false;
			if any(obj.odes.C ~= 0)
				if misc.iseye(obj.odes.C)
					myOutputString = myOutputString + NameValue.stateName;
				else
					myOutputString = myOutputString + ...
						misc.latexChar(obj.odes.C) + NameValue.stateName;
				end
				plusNeeded = true;
			end
			if any(obj.odes.D ~= 0)
				if plusNeeded
					myOutputString = myOutputString + " + ";
				end
				if ~misc.iseye(obj.odes.D)
					myOutputString = myOutputString + misc.latexChar(obj.odes.D);
				end
				myOutputString = myOutputString + misc.latexChar(thisInputName);
			end
			
			myStringArray = [myOdeString; myOutputString];
			% set output parameter or print
			if nargout > 0
				texString = myStringArray;
			else
				misc.printTex(myStringArray);
			end
		end % print()
		
		%% get methods for Dependent properties
		function myOutputName = get.OutputName(obj)
			myData = [];
			for it = 1 : obj.numTypes
				myData = vertcat(myData, obj.ss{it}.OutputName, ...
					obj.C{it}.OutputName, obj.D{it}.OutputName);
			end
			myOutputName = unique(myData);
		end % get.OutputName();
		
		function myInputName = get.InputName(obj)
			myData = [];
			for it = 1 : obj.numTypes
				myData = vertcat(myData, obj.ss{it}.InputName, obj.D{it}.InputName);
			end
			myInputName = unique(myData);
		end % get.InputName();
		
		function type = getType(obj)
			type = reshape([obj.type], size(obj));
		end % get.type()

		function myOdes = get.odes(obj)
			if isempty(obj)
				myOdes = [];
			else
				myOdes = stateSpace.connect(obj.InputName, obj.OutputName, ...
					obj.ss{:}, stateSpace.parallel(obj.C{:}, obj.D{:}));
			end % if
		end % get.odes();
		
		function numTypes = get.numTypes(obj)
			numTypes = numel(obj.ss);
		end %get.numTypes()
		
		function n = get.n(obj)
			% get system order n
			n = zeros(size(obj.type));
			for it = 1 : numel(n)
				n(it) = size(obj.ss{it}.A, 1);
			end % for it = 1 : numel(n)
		end %get.n()
		
		function outputLength = outputDimension(obj, outputName)
			% outputLength returns the dimension of the output specified by outputName
			nonenumeratedOutputName = stateSpace.removeEnumeration(obj.OutputName, false);
			outputLength = sum(strcmp(nonenumeratedOutputName, outputName));
		end % outputDimension()
		
		function C = Cof(obj, outputName)
			% Cof returns the rows of obj.odes.C that are related to the output signal specified by
			% outputName
			nonenumeratedOutputName = stateSpace.removeEnumeration(obj.OutputName, false);
			outputSelector = strcmp(nonenumeratedOutputName, outputName);
			C = obj.odes.C(outputSelector, :);
		end % Cof()
		
		function E = stateSelectMatrix(obj, stateName)
			% stateSelectMatrix returns a selector matrix E = col(0, I, 0), to obtain a certain 
			% state named by stateName out of the state vector of obj.odes.
			thisIdx = obj.index(stateName);
			E = zeros(sum(obj.n), obj.n(thisIdx));
			E(sum(obj.n(1:(thisIdx-1)))+(1:obj.n(thisIdx)), :) = eye(obj.n(thisIdx));
		end % stateSelectMatrix
		
	end % methods
	
	methods (Static = true)
		
		function [mySs, C, D] = separateOutputfromSs(mySs, type)
			% separateCfromSs() removes the output matrix of a state space model mySs
			% and replaces it with the identity matrix, in order to obtain the states
			% of the ode as output variables. The old output matrix C results in a
			% separate state space
			arguments
				mySs;
				type (:, 1) string;
			end
			
			if all(strcmp(stateSpace.removeEnumeration(mySs.OutputName), type))
				assert(isempty(mySs.A) || ...
					(misc.iseye(mySs.C) && isequal(size(mySs.C), size(mySs.A))), ...
					"output of states space model " + type + " does not include all states");
				C = ss();
				D = ss();
			else
				% get C as state space
				C = ss([], [], [], mySs.C, "OutputName", mySs.OutputName);
				if size(C, 2) > 0
					C = stateSpace.setSignalName(C, "input", type, size(C, 2));
				end
				
				% get D as state space
				if ~isempty(mySs.D) && any(mySs.D(:) ~= 0)
					D = ss([], [], [], mySs.D, ...
						"OutputName", mySs.OutputName, "InputName", mySs.InputName);
				else
					D = ss();
				end
				
				% change C and D in mySs
				if numel(mySs.A) == 1
					mySsOutputName = type;
				else
					mySsOutputName = type + "(" + (1:1:size(mySs.A, 1)).' + ")";
				end
				mySs = ss(mySs.A, mySs.B, eye(size(mySs.A)), zeros(size(mySs.A, 1), size(mySs.B, 2)), ...
					mySs.Ts, "InputName", mySs.InputName, ...
					"OutputName", mySsOutputName);
			end % if
		end % seperateCfromSs()
		
		function signalModel = initSignalModel(name, settings)
			% stateSpace.Odes.initSignalModel creates lumped signal models that can be used, e.g., to
			% model disturbance and reference inputs with shapes of sinusoidals, constants and
			% ramps.
			%
			%	signalModel = initSignalModel(name, settings) creates a signal model for the signal
			%		named name according to settings. settings is a struct, that must contain a
			%		field "shape" that specifies the desired signal shape, e.g., "constant", "ramp",
			%		"sinusoidal". To initialise a vector-signal, specifiy the shape as a vector.
			%		Further fields of settings are used to specify further settings, for instance,
			%			- settings.frequency is a double array of the frequency f of sinusoidals,
			%			  i.e., sin(f*t).
			%			- settings.stateName sets the state name of the resulting stateSpace.Odes
			%			  signalModel
			%		The output signalModel is of the type stateSpace.Odes.
			%
			%	signalModel = initSignalModel(name1, settings1, name2, settings2, ...) by passing
			%		repeated input, multiple signals can be specified and combined,
			%
			%	Example:
			%		To create a signal model that describes a reference signal \in R^2 which
			%		consists of a constant (1st element) and a sin(2 t) (2nd element) and that in
			%		addition contains a disturbance model of a scalar ramp-shaped disturbance, use:
			%
			%		signalModel = stateSpace.Odes.initSignalModel(...
			%			"reference", struct("shape", ["constant"; "sinusoidal"], "frequency", [0; 2]), ...
			%			"disturbance", struct("shape", "ramp"));
			%		
			% See also stateSpace.Odes
			
			arguments (Repeating)
				name (1, 1) string;
				settings;
			end
			
			signalModelCell = cell(size(name));
			for it = 1 : numel(name)
				dynamics = ss();
				settings{it} = stateSpace.Odes.initSignalModelSettingParser(name{it}, settings{it});
				
				for jt = 1 : settings{it}.numberOfSignals
					switch settings{it}.shape(jt)
						case "constant"
							dynamics = append(dynamics, ...
								ss(0, [], 1, [], ...
								"OutputName", name{it} + "("+jt+")"));
						case "ramp"
							dynamics = append(dynamics, ...
								ss([0, 1; 0, 0], [], [1, 0], [], ...
								"OutputName", name{it} + "("+jt+")"));
						case "sinusoidal"
							dynamics = append(dynamics, ...
								ss(settings{it}.frequency(jt) * [0, 1; -1, 0], [], [1, 0], [], ...
								"OutputName", name{it} + "("+jt+")"));
						otherwise
							error("The only signal shapes that are supported are constant, ramp, " ...
								+ "sinusoidal, but not " + settings{it}.shape(jt));
					end % switch shape{it}
				end % for jt = 1 : settings.numberOfSignals
				
				signalModelCell{it} = stateSpace.Odes(ss(dynamics), settings{it}.stateName);
			end
				
			if numel(name) == 1
				signalModel = signalModelCell{1};
			elseif numel(name) > 1
				signalModel = signalModelCell{1}.add(signalModelCell{2:end});
			else
				signalModel = stateSpace.Odes();
			end % if numel(name) > 1
			
		end % initSignalModel()
		
	end % methods (Static = true)
	
	methods (Static = true, Hidden = true)
		
		function settings = initSignalModelSettingParser(name, settings)
			% initSignalModelSettingParser is a helper method for initSignalModel.
			if iscell(settings)
				settings = struct(settings{:});
			end
			assert(isfield(settings, "shape"), "settings must specify a signal shape");
			settings.numberOfSignals = numel(settings.shape);
			
			if ~isfield(settings, "frequency")
				settings.frequency = ones(settings.numberOfSignals, 1);
			end
			
			if ~isfield(settings, "stateName")
				settings.stateName = name + "State";
			end
		end % initSignalModelSettingParser()
		
	end % methods (Static = true, Hidden = true)
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
		end
	end % methods (Access = protected)
	
end

