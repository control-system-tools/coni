classdef FlatSystem
	%stateSpace.FlatSystem is a class that implements the flatness based parametrization of state and
	%input trajectories for MIMO LTI-systems.
	%	For a simple example, see unittests.stateSpace.testFlatSystem
	properties
		A (:, :) double; % system matrix
		B (:, :) double; % input matrix
		C (:, :) double; % output matrix
		n (1, 1) double; % system order
		m (1, 1) double; % number of outputs
		p (1, 1) double; % number of inputs
		degree (:, 1) double; % (m,1)-vector of the relative degree of each output
		Boriginal (:, :) double; % original input matrix, without possibly removed redundancy
		Bminimizer (:, :) double; % Boriginal = B * Bminimizer
	end % properties
	properties (Dependent = true)
		decouplingMatrix (:, :) double; % matrix to decouple input and output transfer behaviour
		transform2controlForm (:, :) double; % transformation matrix into control form
		odes stateSpace.Odes; % object to enease interconnection of the system with other systems
		stateSpace ss; % state space model used for simulations
	end % properties (Dependent = true)
	
	methods
		function obj = FlatSystem(A, B, C)
			% stateSpace.FlatSystem constructs an stateSpace.FlatSystem-object, that implements the 
			% flatness based parametrization of state and input trajectories for LTI-systems.
			%
			%	obj = FlatSystem(A, B, C) returns the FlatSystem-object obj, based on the system
			%		matrix A, the input matrix B and the output matrix C. If C does not define a
			%		flat output, an error is thrown.
			arguments
				A (:, :) double; % system matrix
				B (:, :) double; % input matrix
				C (:, :) double; % output matrix
			end % arguments
			obj.A = A;
			obj.Boriginal = B;
			obj.C = C;
			obj.n = size(obj.A, 1);
			obj.m = size(obj.C, 1);
			obj.p = rank(B);
			obj = Bminimization(obj);
			
			% obj.degree (relative degree, Differenzgrad)
			obj.degree = ones(obj.m, 1);
			for it = 1 : obj.m
				while all(obj.C(it, :) * obj.A^(obj.degree(it)-1) * obj.B == 0, "all")
					obj.degree(it) = obj.degree(it) + 1;
				end
			end % for it = 1 : obj.m
			assert(sum(obj.degree) == obj.n, "C does not define a flat output");
		end % FlatSystem: Constructor
		
		function state = parametrizeState(obj, flatOutput)
			% parametrizeState calculates the trajectory of the state vector for a given
			% flatOutput.
			arguments
				obj (1, 1) stateSpace.FlatSystem;
				flatOutput;
			end % arguments
			state = obj.transform2controlForm \ ...
				obj.diffFlatOutput(flatOutput, obj.degree-1, "mode", "upto");
		end % parametrizeState()
		
		function input = parametrizeInput(obj, flatOutput, redundantInputs, NameValue)
			% parametrizeInput calculates the trajectory of the input vector for a given
			% flatOutput.
			%	input = parametrizeInput(obj, flatOutput) returns the input signal as
			%		quantity.Discrete for the given quantity.Discrete input flatOutput
			%
			%	parametrizeInput(obj, flatOutput, "mode", "minimum") returns the input not for the
			%		original input matrix obj.Boriginal, but for the minimized input matrix obj.B,
			%		see stateSpace.FlatSystem.Bminimization for details on the minimization. If the
			%		name-value pair input "mode" is set "original" (which is also the default), then
			%		the caluclated input suites to the original input matrix obj.Boriginal.
			arguments
				obj (1, 1) stateSpace.FlatSystem;
				flatOutput;
				redundantInputs (1, 1) double = (size(obj.Bminimizer, 2) / size(obj.Bminimizer, 1));
				NameValue.mode (1, 1) string = "original";
				NameValue.distributeInput (:, :) double = repmat(eye(obj.p), [redundantInputs, 1]);
			end % arguments
			flatOutput_dDegree = obj.diffFlatOutput(flatOutput, obj.degree, "mode", "max");
			stateGain = zeros([obj.p, obj.n]);
			for it = 1 : obj.p
				stateGain(it, :) = obj.C(it, :) * obj.A^(obj.degree(it));
			end % for it = 1 : obj.p
			input = -(obj.decouplingMatrix \ stateGain) * obj.parametrizeState(flatOutput) ...
				+ obj.decouplingMatrix \ flatOutput_dDegree;
			if strcmp(NameValue.mode, "original")
				% ensure that
				%	1. inputMinimal = obj.Bminimizer * input
				%	2. input is distributed evenly (if NameValue.distributeInput is default value)
				%		or according to weights in NameValue.distributeInput.
				input = (NameValue.distributeInput/(obj.Bminimizer*NameValue.distributeInput))*input;
			else
				assert(strcmp(NameValue.mode, "minimum"), ...
					"only implemented for mode = minimum or original");
			end
		end % parametrizeStates()
		
		function diffVec = diffFlatOutput(obj, flatOutput, order, NameValue)
			% diffFlatOutput calculates the 1, 2, ..., order(i) of flatOutput(i) for all i and
			% returns the result arranged as
			%	diffVec = [flatOutput(1), flatOutput(1)_dt, ..., flatOutput(1)_(dt)^order(1), ...
			%				flatOutput(2), flatOutput(2)_dt, ..., flatOutput(2)_(dt)^order(2), ...
			%				...
			%				flatOutput(m), flatOutput(m)_dt, ..., flatOutput(m)_(dt)^order(m)]^T.
			% flatOutput must be a (m,1)-size quantity-Discrete defined on the temporal domain t.
			%
			%	diffVec = diffFlatOutput(obj, flatOutput, order) calculates the derivatives upto 
			%		order(i). order must be a (m, 1)-size double array, with non-negative elements.
			%		Default: order = obj.degree.
			%
			%	diffVec = diffFlatOutput(obj, flatOutput, order, "mode", mode) the name-value input
			%		mode changes the behaviour. If it is set to "max", only the highest derivatives,
			%		i.e., order are calculated, but not 0, 1, ... order-1.
			%		Default: mode = "upto".
			arguments
				obj (1, 1) stateSpace.FlatSystem;
				flatOutput;
				order (:, 1) double = obj.degree;
				NameValue.mode (1, 1) string = "upto"
			end % arguments
			assert(numel(flatOutput) == obj.m, "flatOutput must have m elements");
			assert(numel(order) == obj.m, "order must have m elements");
			assert(all(order>=0), "order must have only non-negative elements");
			tDomain = flatOutput(1).domain.find("t");
			
			if strcmp(NameValue.mode, "upto")
				diffVec = quantity.Discrete.ones([sum(order)+obj.m, 1], tDomain);
				diffVecIdx = 1;
				for it = 1 : obj.m
					for degreeTmp = 0 : order(it)
						diffVec(diffVecIdx) = flatOutput(it).diff(tDomain, degreeTmp);
						diffVecIdx = diffVecIdx + 1;
					end % for degreeTmp = 0 : order(it)
				end % for it = 1 : obj.m
			elseif strcmp(NameValue.mode, "max")
				diffVec = quantity.Discrete.ones([obj.m, 1], tDomain);
				for it = 1 : obj.m
					diffVec(it) = flatOutput(it).diff(tDomain, obj.degree(it));
				end % for it = 1 : obj.m
			else
				error("mode = " + NameValue.mode + " is not recognized");
			end % if strcmp(NameValue.mode, "upto")
		end % diffFlatOutput()
		
		function transform2controlForm = get.transform2controlForm(obj)
			% transform2controlForm is the matrix to map the state space 
			%	x_t = A x + B u
			%	y = C x
			% into a new state space with the state-vector...
			%	[y1, y1_t, ..., y1(_t)^degree(1), y2, ..., ym(_t)^degree(m)]^T
			%		 = transform2controlForm x
			transform2controlForm = zeros(obj.n * [1, 1]);
			rowIdx = 1;
			for it = 1 : obj.m
				for jt = 0 : (obj.degree(it)-1)
					transform2controlForm(rowIdx, :) = obj.C(it, :) * obj.A^(jt);
					rowIdx = rowIdx + 1;
				end % for jt = 1 : (obj.degree(it)-1)
			end % for it = 1 : obj.m
		end % get.transform2controlForm
		
		function obj = Bminimization(obj)
			% Bminimization finds a minimum version for the case, that Boriginal does not have full
			% rank. For this it is assumed, that Boriginal can be split into a linear dependend 
			% columns B \in R^{n, p},
			% such that
			%	Boriginal = B * Bminimizer
			% In this method, B and Bminimizer are set.
			obj.B = obj.Boriginal;
			obj.Bminimizer = eye(rank(obj.B)*[1, 1]);
			if rank(obj.B) ~= size(obj.B, 2)
				[~, colIdx] = rref(obj.Boriginal);
				obj.B = obj.B(:, colIdx);
				obj.Bminimizer = lsqminnorm(obj.B, obj.Boriginal);
			end
		end % Bminimization()
		
		function decouplingMatrix = get.decouplingMatrix(obj)
			% decouplingMatrix is the matrix for decoupling the inputs-output behaviour
			decouplingMatrix = zeros([obj.m, obj.p]);
			for it = 1 : obj.m
				decouplingMatrix(it, :) = obj.C(it, :) * obj.A^(obj.degree(it)-1) * obj.B;
			end % for it = 1 : obj.m
		end % get.decouplingMatrix
		
		function odes = get.odes(obj)
			% odes is a stateSpace.Odes object, that can be used for simulations.
			thisStateSpace = ss(obj.A, obj.Boriginal, obj.C, [], ...
				"OutputName", "flatOutput", "InputName", "input");
			odes = stateSpace.Odes(thisStateSpace, "state");
		end % get.odes
		
		function stateSpace = get.stateSpace(obj)
			% stateSpace is a ss (state-space-model) object, that can be used for simulations.
			stateSpace = obj.odes.odes;
		end % get.odes
		
	end % methods
end % classdef FlatSystem