function u = combineInputSignals(myStateSpace, t, varargin)
%stateSpace.combineInputSignals reads the input varargin and searches for name-value-pairs that
%coincide with one of myStateSpace.InputName. If a pair is found, that signal is used
%as elements of u. For inputs that are not found, u-elements are set to zero.
%
% Inputs:
%	myStateSpace	state space for which an input signal is designed
%	t				time for which u shall be defined (double array or quantity.Domain)
%	varargin		name-value-pairs containing input signals with names coinciding
%					with myStateSpace.InputName
% Outputs:
%	u				quantity vector of input signals suiting to myStateSpace for the
%					time t.
%
% Example:
% -------------------------------------------------------------------------
% 	myStateSpace = ss(-1, [1, 2], 1, [], "InputName", {'control', 'disturbance'});
%	t = quantity.Domain("t", linspace(0, 4, 201));
%	disturbanceSignal = quantity.Symbolic(sin(sym("t")), t, "name", "disturbance");
%	u = stateSpace.combineInputSignals(myStateSpace, t.grid, "disturbance", disturbanceSignal);
%	y = quantity.Discrete(lsim(myStateSpace, u.on(), t.grid), t, "name", "y");
%	plot([y; u]);
% -------------------------------------------------------------------------

if isnumeric(t) && isvector(t)
	t = quantity.Domain("t", t);
else
	assert(isa(t, "quantity.Domain"), "time t must be a quantity.Domain object or a double array");
end

% read name of input names from myStateSpace and calculate input lengths
myInputName = stateSpace.removeEnumeration(myStateSpace.InputName);
myInputNameUnique = unique(myInputName, "stable");
myInputLength = zeros(size(myInputNameUnique));
for it = 1 : numel(myInputLength)
	myInputLength(it) = sum(strcmp(myInputNameUnique(it), myInputName));
end

% search for input signals in varargin and set default value = 0
signalStruct = struct();
for it = 1 : numel(myInputNameUnique)
	signalStruct = misc.setfield(signalStruct, myInputNameUnique(it), ...
		quantity.Discrete.zeros([myInputLength(it), 1], t, "name", myInputNameUnique(it)));
end

% iterate through varargin and replace values in signal struct
for it = 1 : 2 : numel(varargin)
	myInputSelector = strcmp(myInputNameUnique, varargin{it});
	if any(strcmp(myInputNameUnique, varargin{it}))
		assert(myInputLength(myInputSelector) == numel(varargin{it+1}), ...
			"The signal " + varargin{it} + " is defined with size " + mat2str(size(varargin{it+1})) ...
			+ ", but the state space is defined for a input signal size of " ...
			+ mat2str([myInputLength(myInputSelector), 1]) + ".");
		if isnumeric(varargin{it+1})
			varargin{it+1} = setName(...
					varargin{it+1} * quantity.Discrete.ones(1, t), ...
				myInputNameUnique(it));
		end
		assert(isa(varargin{it+1}, "quantity.Discrete") && varargin{it+1}(1).domain.index("t") ~= 0, ...
			"The signal " + varargin{it} + " must be defined as a quantity.Discrete-object " ...
			+ "and must have a domain named t.");
		thisT = varargin{it+1}(1).domain.find("t");
		if ~isequal(thisT, t)
			if (thisT.lower > t.lower) || (thisT.upper < t.upper)
				warning("The signal " + varargin{it} + " is not defined for all time t, hence " ...
					+ "it is linearly extrapolated.");
			end
			signalStruct = misc.setfield(signalStruct, varargin{it}, ...
				varargin{it+1}.setName(varargin{it}).changeDomain(t));
		else
			signalStruct = misc.setfield(signalStruct, varargin{it}, ...
				varargin{it+1}.setName(varargin{it}));
		end
	end % if any(myInputNameUnique, strcmp(varargin{it}))
end % for it = 1 : 2 : numel(varargin)

% finally, create resulting input vector
if numel(myInputNameUnique) > 0
	u = misc.getfield(signalStruct, myInputNameUnique(1));
	for it = 2 : numel(myInputNameUnique)
		u = [u; misc.getfield(signalStruct, myInputNameUnique(it))];
	end % for it = 1 : numel(myInputNameUnique)
else
	u = quantity.Discrete([], t);
end
end % combineInputSignals()