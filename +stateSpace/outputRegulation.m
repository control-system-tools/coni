function [Pi, Gamma, K, M] = outputRegulation(A, B, C, G, S, Pd, Pr, closedLoopEigenvalues)
% stateSpace.outputRegulation solves the reference tracking and disturbance rejection problem for
% finite LTI state space models and exogenous signals that are discribed by such state space as
% well.
% The plant is described by
%	x_t = A x + B u + G d
%	  y = C x,
% with the state x, the input u, the output y, while the exogenous signals (reference input r,
% disturbance input d) are described by the signal model
%	v_t = S v
%	  r = Pr v
%	  d = Pd v
% with the state v. Then the controller ensuring output regulation (reference tracking & disturbance
% rejection) is
%	 u = - K x + (K Pi + Gamma) v = - K x + M v
% in which K is a state feedback for shaping the plant dynamics and Pi and Gamma are the solution of
% the regulator equations, given by
%	Pi S - A Pi = G Pd + B Gamma
%		   C Pi = Pr.
% If the input closedLoopEigenvalues specifies valid eigenvalues, a state feedback K is designed to
% ensure this closed loop dynamics.
%
% 	[Pi, Gamma] = stateSpace.outputRegulation(A, B, C, G, S, Pd, Pr) solves the output regulation
%		problem for the plant and the signal model given by
%			A : plant dynamic matrix
%			B : plant input matrix
%			C : plant output matrix
%			G : plant disturbance input matrix
%			S : signal model dynamic matrix
%			Pd: signal model disturbance output matrix
%			Pr: signal model reference output matrix
%
%	[Pi, Gamma, K] = stateSpace.outputRegulation(A, B, C, G, S, Pd, Pr, closedLoopEigenvalues)
%		additionally calculates a state feedback gain K, to ensure the closed loop eigenvalues given
%		by closedLoopEigenvalues.

arguments
	% Plant
	A (:, :) double;							% plant dynamic matrix
	B (:, :) double;							% plant input matrix
	C (:, :) double;							% plant output matrix
	G (:, :) double;							% disturbance input matrix
	% Signal model
	S (:, :) double;							% signal model dynamic matrix
	Pd (:, :) double;							% signal model disturbance output matrix
	Pr (:, :) double;							% signal model reference output matrix
	%
	closedLoopEigenvalues (:, 1) double = [];	% eigenvalues to be assigned
end % arguments

% init some local parameter
n = size(A, 1);		% number of plant states
m = size(C, 1);		% number output signal elements
nv = size(S, 1);	% number of signal model states

% verify sizes of input parameter
assert(size(A, 2) == n, "The system matrix A must be quadratic.")
assert(size(B, 1) == n, "The rows of the input matrix B must fit to the system matrix A.")
assert(size(B, 2) == m, "The size of the input (number of columns of B) must be " ...
					+ "equal to the size of the output (number of rows of C).");
assert(size(G, 1) == n, "The rows of the distrubance input matrix G must fit to the system matrix A.")
assert(size(Pd, 1) == size(G, 2), "The size of the disturbance input (number of columns of G) " ...
					+ "must be equal to the number of rows of Pd.");
assert(size(C, 2) == n, "The columns of the output matrix C must fit to the system matrix A.")
assert(size(Pr, 1) == m, "The size of the reference input (number of rows of Pd) "...
					+ "must be equal to size of the output (number of rows of C).");
assert(size(S, 2) == nv, "The signal models system matrix S must be quadratic.");
assert(size(Pr, 2) == nv, "The columns of the signal models output matrix Pr must fit to the system matrix S.");
assert(size(Pd, 2) == nv, "The columns of the signal models output matrix Pd must fit to the system matrix S.");

% check transmission zeros
mu = eig(S).';
In = eye([n, n]);
O = zeros([m, m]);
for mu_i = mu
	assert(abs(det([A - In*mu_i, B; C, O])) > 1e-6, ...
		"The plant has a transmission zero at the signal models eigenvalue " + string(mu_i)...
		+ ". Hence, output regulation can not be achieved.");
end

% init parameter for solving the regulator equations
I = eye([nv, nv]);	% identity matrix
rhs = G * Pd;%		% right hand side of regulator equations
rhs = [rhs(:); Pr(:)]; % vectorize rhs and add reference input

% compute the solution of regulator equations using vectorization
matrix = [kron(S.', eye([n, n])) + kron(I, -A), kron(I, -B); kron(I, C), zeros(nv*[m, m])];
vectorizedSolution = matrix \ rhs;

% solution of regulator equations
Pi = reshape(vectorizedSolution(1:(n*nv), 1), [n, nv]);
Gamma = reshape(vectorizedSolution((n*nv+1):end, 1), [m, nv]);

% verify solution
residuum1 = Pi * S - A * Pi - G * Pd - B * Gamma;
residuum2 = C * Pi - Pr;
assert(max(abs([residuum1; residuum2]), [], "all") <= 1e-12, ...
	"Solution of regulation is faulty - residuum: " ...
	+ string(max(abs([residuum1; residuum2]), [], "all")));

% eigenvalue assignment
if ~isempty(closedLoopEigenvalues) && nargout > 2
	K = misc.place(A, B, closedLoopEigenvalues);
	M = K * Pi + Gamma;
else
	K = zeros(m, n);
	M = zeros(m, nv);
end
end % outputRegulation