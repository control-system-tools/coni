function [y, t, x] = lsim(sys, u, t, x0, varargin)
% LSIM - simulation of a state space system
% [y, t, x] = lsim(sys, u, t, x0, varargin) simulates the state-space system specified by sys as
% ss object. The time-domain is given by t as quantity.EquidistantDomain object and the input by u
% as quantity.Discrete object.
arguments
	sys ss
	u (:,1) quantity.Discrete
	t (1,1) quantity.EquidistantDomain
	x0 (:,1) double = zeros(size(sys.A,1),1);
end
arguments (Repeating)
	varargin
end
[y, ~, x] = lsim(sys, u.on(t), t.grid, x0, varargin{:} );

y = quantity.Discrete( y, t, "name", "y");
x = quantity.Discrete( x, t, "name", "x");

end