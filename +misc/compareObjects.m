function is = compareObjects(obj1, obj2)
% COMPAREOBJECTS compares properties of two objects
% compareObjects(obj1, obj2) will compare the properties of OBJ1 and
% OBJ2. If a property is different it will be written in the command
% window.
% is = compareObjects(obj1, obj2) will compare the properties of OBJ1 and
% OBJ2. 
%
is = strcmp(class(obj1), class(obj2));
if ~is
	warning('Different type of objects');
end

props = union(properties(obj1), properties(obj2));

for idxObj = 1:numel(obj1)
	for k = 1:numel(props)
		
		is_prop = isequal( obj1(idxObj).(props{k}), obj2(idxObj).(props{k}) );
		
		if nargout == 0 && ~is_prop
			fprintf("Property: %s not equal for obj index %i \n", props{k}, idxObj)
		end
		
		is = is && is_prop;
	end
end
end