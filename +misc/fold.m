function xFolded = fold(x,z0,dim,varargin)
% misc.fold fold discretized vector or array around folding point
%
% xFolded = FOLD(x,z0) folds the vector x which is discretized over linspace(0,1,ndisc) at the
%   point z0. The result is a ndisc x 2 matrix. That means
%   xFolded(zdisc,1) = x(0:z0)
%   xFolded(zdisc,2) = x(z0:1)
%   with a correct resampling. The Folding point itself is assumed to belong to the right
%   side, i.e. xFolded(1,2).
%
% xFolded = FOLD(x,z0,dim) folds the multidimensional array wrt. dimension dim. An
%   additional dimension of length 2 is added at the end.
% 
% xFolded = FOLD(x,z0,dim,nFolded) uses a new discretization linspace(0,1,nFolded)
%
% see also misc.unfold

% created on 07.10.2019 by Simon Kerschbaum


found=0;
passed = [];
arglist = {'preserve','nFolded'};
% preserve: insert the folding point at both left and right part. Important for observer error!
% nFolded resolution of the folded states
if ~isempty(varargin)
	if mod(length(varargin),2) % uneven number
		error('When passing additional arguments, you must pass them pairwise!')
	end
	for index = 1:2:length(varargin) % loop through all passed arguments:
		for arg = 1:length(arglist)
			if strcmp(varargin{index},arglist{arg})
				passed.(arglist{arg}) = varargin{index+1};
				found=1;
				break
			end 
		end % for arg
		% argument wasnt found in for-loop
		if ~found
			error([varargin{index} ' is not a valid property to pass to fold!']);
		end
		found=0; % reset found
	end % for index
end 
if ~isfield(passed,'preserve')
	passed.preserve = 0;
end
if ~isfield(passed,'nFolded')
	passed.nFolded = 0;
end

	if isvector(x)
		x=x(:);
		if dim == 2
			dim=1;
		end
	end
	if nargin < 3
		dim =1;
	end
	if passed.nFolded == 0
		nFolded = size(x,dim);
	else
		nFolded = passed.nFolded;
	end
	zDiscOld = linspace(0,1,size(x,dim));
	

	zLeft = zeros(1,nFolded);
	zRight = zeros(1,nFolded);

	if ~passed.preserve 
		zRight(1,:) = linspace(z0,1,nFolded);
		dZ = diff(zRight(1,:));
		zLeft(1,:) = linspace(0,z0-dZ(1),nFolded);
	else
		zRight(1,:) = linspace(z0,1,nFolded);
		zLeft(1,:) = linspace(0,z0,nFolded);
	end

	sizX = size(x);
	if dim~= 1
		xPerm = permute(x,[dim 1:dim-1 dim+1:length(sizX)]);
% 		sizPermX = permute(sizX,[dim 1:dim-1 dim+1:length(sizX)]);
	else
		xPerm = x;
% 		sizPermX = sizX;
	end
	if ~ismatrix(xPerm)
		xResh = reshape(xPerm,sizX(dim),[]);
		sizXResh = size(xResh);
	else
		xResh = xPerm;
		sizXResh = size(xResh);
	end
	
	xInterp = numeric.interpolant({zDiscOld,1:size(xResh,2)},xResh);
	
	if sizXResh(end) == 1
		xFoldedResh(:,1) = xInterp.evaluate(fliplr(zLeft(1,:)),1:size(xResh,2));
		xFoldedResh(:,2) = xInterp.evaluate(zRight(1,:),1:size(xResh,2));
	else
		xFoldedResh = zeros([nFolded size(xResh,2) 2]);
		xFoldedResh(:,:,1) = xInterp.evaluate(fliplr(zLeft(1,:)),1:size(xResh,2));
		xFoldedResh(:,:,2) = xInterp.evaluate(zRight(1,:),1:size(xResh,2));
	end
	
	
	if ~ismatrix(xPerm)
		sizXPerm  = size(xPerm);
		xFoldedPerm = reshape(xFoldedResh,[nFolded sizXPerm(2:end) 2]);
	else
		xFoldedPerm = xFoldedResh;
	end
	if dim~= 1
		xFolded = permute(xFoldedPerm,[2:dim 1 dim+1:length(sizX)+1]);
	else
		xFolded = xFoldedPerm;
	end
end % function fold
		