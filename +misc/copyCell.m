function cellCopy = copyCell(myCell)
% misc.copyCell returns a copy of the cell, in which to all cell-elements the copy method was
% applied.
%
% Example:
%	myCell = {quantity.Discrete.ones(1, quantity.Domain("z", linspace(0, 1, 5)), "name", "asdf")};
%	myCopy = misc.copyCell(myCell)
arguments
	myCell;
end

if iscell(myCell)
	cellCopy = cell(myCell);
	for it = 1 : numel(myCell)
		cellCopy{it} = misc.copyCell(myCell{it});
	end
	
elseif isa(myCell, "matlab.mixin.Copyable")
	cellCopy = copy(myCell);
	
else
	assert(~isa(myCell, "handle"), "Copy failed, as input is a handle, but no matlab.mixin.Copyable")
	cellCopy = myCell;
	
end

end % misc.copyCell()