function [ i ] = issym( varargin )
% ISSYM checks if arg is a sym
%
% Inputs:
%     varargin    Input variable
% Outputs:
%     i           TRUE or FALSE determining whether varargin is a sym
%
% Example:
% -------------------------------------------------------------------------
% syms u
% issym(u)
% >> ans = 1
% -------------------------------------------------------------------------

i = false;

for arg = [varargin{:}]
  i = isa(arg, 'sym') || i;
end

end

