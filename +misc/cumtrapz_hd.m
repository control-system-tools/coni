function [ F, hd ] = cumtrapz_hd( t, f, tN )
% CUMTRAPZ_HD compute cumulate trapezoidal integration for arbitrary
% functions for arbitrary resolutions.
%
% Description:
% [ F, hd ] = cumtrapz_hd( t, f, tN )
% Evaluates the function f, for the interval t \in (t(1), t(end)) with tN 
% discretization points. The number of discretization points tN is 
% optional. The default value that is used if no number is passed to, is 
% 1e6. The result F is the integration of f, computed with the high reso-
% lution defined by tN, but evaluated at the timesteps defined in t. The 
% high resolution results are returned in the hd structure with the 
% fields hd.t, .f, .F.
%
% Input:
% t       Timesteps on which the antiderivative is evaluated 
% f       Function to be integrated
% tN      Number of discretization points
% Outputs:
% F       Antiderivative of f evaluated on the timesteps t
% hd      Struct which contains .t, .f and .F
%
% Example:
% -------------------------------------------------------------------------
% t = linspace(-pi, pi);
% f = @(t) sin(t) .* sign(t);
% plot(t, cumtrapz(t, f(t)) - misc.cumtrapz_hd(t, f));
% -------------------------------------------------------------------------

if length(t) == 1
    t = [0, t];
    singlevalue = true;
else
    singlevalue = false;
end

if ~exist('deltaT', 'var')
    tN = 1e6;
end

if singlevalue
    F = F(2);
else
    F = zeros(size(t));
    hd.t = linspace(t(1), t(end), tN);

    hd.f = f(hd.t);

    hd.F = cumtrapz(hd.t, hd.f);

    F = interp1(hd.t, hd.F, t);    
end

end

