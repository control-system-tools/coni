classdef Message < handle
	properties
		timeStamp double;
		text string;
		author string;
		data;
	end
	
	methods 
		function obj = Message(text, optArgs)
			arguments
				text;
				optArgs.timeStamp = now();
				optArgs.author = "";
				optArgs.data = [];
			end
			
			obj.timeStamp = optArgs.timeStamp;
			obj.text = text;
			obj.author = optArgs.author;
			obj.data = optArgs.data;
			
		end
		function s = print(obj)
			s = sprintf('%s: %s \n', datestr( obj.timeStamp ), obj.text);
		end
	end
	
end