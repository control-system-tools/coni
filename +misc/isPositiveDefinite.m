function itIs = isPositiveDefinite(x, NameValue)
% isPositiveDefinite checks if x is a symmetric, positive definite matrix. For this it is verified,
% that the eigenvalues of x are greater than NameValue.absTol (default: 10*eps).
%	
%	itIs = isPositiveDefinite(x) returns itIs = true if x is positive definite, and itIs = false
%		otherwise.
%
%	itIs = isPositiveDefinite(x, "AbsTol", tol) uses the tolerance tol, to verify that the
%		eigenvalues of x are greater than tol.
%
% Example:
% -------------------------------------------------------------------------
% misc.isPositiveDefinite(magic(3))
% >> ans = false
% misc.isPositiveDefinite([1, 0, 2])
% >> ans = false
% -------------------------------------------------------------------------
% See also misc.isPositiveSemiDefinite

arguments
	x (:, :) double;
	NameValue.AbsTol (1, 1) {mustBeNonnegative} = 10*eps;
end

eigenValues = eig(x);

itIs = (min(real(eigenValues)) > NameValue.AbsTol);

end % isPositiveDefinite

