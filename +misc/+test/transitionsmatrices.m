
clear Phi P
I = 10;
syms z zeta

A0 = [0 1 + z; 1 0];
A1 = [ 0 1; 0 0];
A = cat(3, A0, A1);

tic
[Phi.W.sym, P.W.sym] = misc.fundamentalmatrix_woittennek(A, 'N', I);
toc

tic
[Phi.P.sym, P.P.sym] = misc.peanobakerseries_poly(A, 'N', I);
toc

Phi.W.fun = matlabFunction(Phi.W.sym(:));
Phi.P.fun = matlabFunction(Phi.P.sym(:));

for j = 1 : I

    z = linspace(0,1);
    tmp = P.W.sym(:,:,j);
    P.W.fun = matlabFunction(tmp(:));
    tmp = P.P.sym(:,:,j);
    P.P.fun = matlabFunction(tmp(:), 'Vars', {'z', 'zeta'});

    P.W.num = misc.eval_pointwise(@(z) P.W.fun(z, 0), z);
    P.P.num = misc.eval_pointwise(@(z) P.P.fun(z, 0), z);
    
    figure(j);
    clf();

    for k = 1:4
       subplot(2,2,k);
       plot(P.W.num(:, k) - P.P.num(:, k));
    end

end
