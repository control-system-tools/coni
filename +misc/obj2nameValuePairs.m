function [namevaluepair] = obj2nameValuePairs(obj)
%OBJ2NAMEVALUEPAIRS Summary of this function goes here
%   Detailed explanation goes here

names = fieldnames(obj);
namevaluepair = cell(length(names), 1);
for k = 1:length(names)
    namevaluepair{k} = names{k};
    namevaluepair{k+1} = obj.(names{k});
end

end

