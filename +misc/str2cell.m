function cellArray = str2cell(stringArray)
%STR2CELL Converts a string-array to a cell-array of that strings.
% This is usefull for sym/subs.
% Example:
%	misc.str2cell(["a", "sd", "f"]);
cellArray = cell(size(stringArray));
for it = 1 : numel(stringArray)
	cellArray{it} = stringArray(it);
end % for it = 1 : numel(stringArray)
end

