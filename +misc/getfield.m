function myValue = getfield(myStruct, myFieldName, NameValue)
% misc.getfield returns data of a struct specified by its field name. In contrast to the matlab
% built-in function getfield(), this method misc.getfield can also read fields of field by
% specifying myFieldName with dots as seperator, for instance myFieldName =
% "topLevel.lowerLevel.data", see example. Additionally, misc.getfield can handle arrays of data,
% i.e. myFieldName string arrays, by concatenating the result.
%
%	myValue = misc.getfield(myStruct, myFieldName) returns the value stored in the field
%		myStruct.(myFieldName), specified by the string myFieldName. If myFieldName is a 
%		string-array, then the values in the related fields are concatenated horizontally.
%
%	myValue = misc.getfield([...], "cell", cell) if cell = true, then the content of
%		the required field is returned as a cell-array. Default: false.
%
%	myValue = misc.getfield([...], "vectorize", vectorize) if vectorize = true, then the data is
%		returned as a column vector myValue = myValue(:). Default: false.
%
% Example 1:
%		myStruct = misc.setfield(struct(), "topLevel.lowerLevel.data", 42);
%		result = misc.getfield(myStruct, "topLevel.lowerLevel.data")
% Example 2:
%		myStruct = struct("a", 1, "b", 2);
%		result = misc.getfield(myStruct, ["a", "b"])
% Example 3:
%		myStruct = struct("a", 1, "b", 2);
%		result = misc.getfield(myStruct, ["a", "b"], "cell", true)
%
% See also misc.setfield, misc.struct, misc.struct2namevaluepair.

arguments
	myStruct struct;
	myFieldName string;
	NameValue.cell (1, 1) logical = false;
	NameValue.vectorize (1, 1) logical = false;
end

if isscalar(myFieldName)
	myFieldNameSplitted = split(myFieldName, '.');
	myValue = myStruct;
	for it = 1 : numel(myFieldNameSplitted)
		if isfield(myValue, myFieldNameSplitted{it}) 
			myValue = myValue.(myFieldNameSplitted{it});
		else
			myValue = [];
			return;
		end
	end
	if NameValue.cell
		myValue = {myValue};
	end
else % array case
	if NameValue.cell
		myValue = cell(size(myFieldName));
		for it = 1 : numel(myFieldName)
			myValue{it} = misc.getfield(myStruct, myFieldName(it));
		end
	else
		myValue = [];
		for it = 1 : numel(myFieldName)
			myValueTmp = misc.getfield(myStruct, myFieldName(it));
			if NameValue.vectorize
				myValue = [myValue(:); myValueTmp(:)];
			else
				myValue = [myValue, myValueTmp];
			end
		end
	end
end % if isscalar(myFieldName)

if NameValue.vectorize
	myValue = myValue(:);
end
end % getfield()