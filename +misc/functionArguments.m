function names = functionArguments(func)
%FUNCTIONARGUMENTS extracts the arguments of a function handle
%	names = misc.functionArguments(func) returns the arguments of the function handle func as the
%	string array names
%% Example
% names = misc.functionArguments(@(z, zeta, t) z*zeta+t)

functionArguments = extractBetween(func2str(func), "@(", ")");
names = split(string(functionArguments{1}), ",");

names = names(strlength(names) > 0);

end