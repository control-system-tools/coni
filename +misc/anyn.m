function [ V ] = anyn( V )
% ANYN recursive calling of function any until the result has only one
% dimension
%
% Description:
% >> V = anyn( V ) 
% Returns a scalar boolean if at least one entry in V is true
%
% Inputs:
% V       n-dimensional array (numeric)
% Outputs:
% V       Scalar
%
% Example:
% -------------------------------------------------------------------------
% M = zeros(2,2,2);
% M(end) = 1;
% misc.anyn(M)
% >> ans = 1
% -------------------------------------------------------------------------
% 
    V = any(V(:));
end

