function newStruct = structMerge(myStructs, NameValue)
% misc.structMerge merges all structs to one struct.
%
%	newStruct = misc.StructMerge(struct1, struct2, ...) merges all fields of all structs struct1,
%		struct2, ... into newStruct. If a field exists in multiple structs, the value of the last
%		struct will be used and a warning will be displayed.
%
%	misc.StructMerge([...], "warnOverwrite", false) deactivates the warning that would be thrown in
%		the case that one field value will be overwritten by another.
%
%	misc.StructMerge([...], "noOverwrite", true) will throw an error instead of a the warning, in
%		order to avoid data being overwritten.
%%
% Example
%		misc.structMerge(struct("asdf", 42), struct("blub", 1337));
arguments (Repeating)
	myStructs struct;
end
arguments
	NameValue.noOverwrite (1, 1) logical = false;
	NameValue.warnOverwrite (1, 1) logical = true;
end
if nargin == 0
	newStruct = struct();
else
	newStruct = myStructs{1};
	for it = 2 : numel(myStructs)
		tmpFieldnames = misc.fieldnames(myStructs{it});
		for tmpFieldname = tmpFieldnames.'
			if NameValue.noOverwrite
				if contains(misc.fieldnames(newStruct), tmpFieldname)
					error("misc:structMerge:overwriteField", ...
						"setting the field" + tmpFieldname + " would overwrite an existing field");
				end
			elseif NameValue.warnOverwrite
				if contains(misc.fieldnames(newStruct), tmpFieldname)
					warning("misc:structMerge:overwriteField:warning", ...
						"setting the field " + tmpFieldname + " overwrites an existing field.");
				end
			end
			newStruct = misc.setfield(...
				newStruct, tmpFieldname, misc.getfield(myStructs{it}, tmpFieldname));
		end % for tmpFieldname = tmpFieldnames.'
	end % for it = 2 : numel(myStructs)
end % if nargin == 0
end % misc.structMerge()