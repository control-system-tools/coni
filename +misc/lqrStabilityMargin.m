function [K, Q, R] = lqrStabilityMargin(A, B, stabilityMargin, Q, R)
%MISC.LQRSTABILITYMARGIN calculates the lqr state feedback K, that ensures a certain stability
% margin for the closed loop system and the control effort is minimized. The LTI-system is given by
%	x_t = A x + B u.
% The stability margin is ensured by solving the lqr problem for a transformed system with
%	w = exp(a t) x, v = exp(a, t) u
% which leads to transformed LTI-system
%	w_t = (A+aI) w + B v.
% For this system the function
%	J = 1/2 int_0^\inf (w^T(t) Q w(t) + v^T(t) R v(t)) dt
% is minimized via the lqr-state feedback 
%	v = - K w.
% If the same state feedback gain ( u = - K x) is applied to the original system, then the
% spectrum(A - B K) has all eigenvalues left of a and the cost functional
%	J = 1/2 int_0^\inf exp(2 a t) * (x^T(t) Q x(t) + u^T(t) R u(t)) dt
% is minimized. For sufficiently small Q and sufficiently large R, the spectrum(A - B K) results
% from mirroring all eigenvalues real(lambda_i) > a  at the vertical line
%	a+ik, k \in R
% and leaving the eigenvalues lambda_i < a at their orignal place.
%
%	K = lqrStabilityMargin(A, B, stabilityMargin) calculates the state feedback gain K that
%		ensures, that all eigenvalues of (A-B K) are left of stabilityMargin.
%
%	[K, Q, R] = lqrStabilityMargin(A, B, stabilityMargin) additionally returns the weighting
%		matrices Q and R used for the design.
%
%	lqrStabilityMargin(A, B, stabilityMargin, Q, R) specifies the weighting matrices Q for the state
%		and R for the input effort. By default 
%			Q = eye(size(A)) * 1e-10 and R = eye(size(B, 2) * [1, 1]).
%
%% Example
%	A = magic(2);
%	B = [0; 1];
%	K = misc.lqrStabilityMargin(A, B, 1)
%	eig(A - B * K)

arguments
	A (:, :) double;
	B (:, :) double;
	stabilityMargin (1, 1) double;
	Q (:, :) double = eye(size(A)) * 1e-10;
	R (:, :) double = eye(size(B, 2) * [1, 1]);
end

% init return parameter
K = zeros(size(B.'));

originalSpectrum = eig(A);
if all(real(originalSpectrum) <= -stabilityMargin)
	% eigenvalues of A already comply with stability margin
	return
end
largestEigenvalueRightOfStabilityMargin = ...
	max(real(originalSpectrum(real(originalSpectrum) > -stabilityMargin)), [], "all");

% a: real-part of parallel line to imaginary axis where the eigenvalues will be mirrored
a = -(largestEigenvalueRightOfStabilityMargin - stabilityMargin) / 2;

% solve lqr problem for transformed system 
K = lqr(A + a * eye(size(A)), B, Q, R);

closedLoopSpectrum = eig(A - B * K);
if all(real(closedLoopSpectrum) <= -stabilityMargin)
	% eigenvalues of A - B * K comply with stability margin
	return
else
	% repeat for the other eigenvalues right of the desired stability margin
	[K_new, Q, R] = misc.lqrStabilityMargin(A - B * K, B, stabilityMargin, Q, R);
	K = K + K_new;
	closedLoopSpectrum = eig(A - B * K);
end

if (max(real(closedLoopSpectrum), [], "all") + stabilityMargin) > 1e-6
	warning("The eigenvalues do not fullfill the stability margin, as the maximum real part " ...
		+ "of the spectrum is: " + string(max(real(eig(A - B * K)), [], "all")));
end
end % lqrStabilityMargin