function [s] = formatMatrix(M)

s = '';

for k = 1:size(M,1)
	s = [s, sprintf(' %.3g', M(k,:)), '\n'];
end

end

