function thisStr = latexComplex(s, option, NameValue)
% latexComplex(s) returns the LaTeX representation of the symbolic expression s, and, different to
% built-in latex(s), complex numbers 2 + j 3 are formatted according to "2+\mathrm{j}3"
%
% Example:
%	misc.latexComplex(sym(2+3i))
%	>> "2+\mathrm{j}3"
%
% See also latex, misc.latexChar, misc.variable2tex
	arguments
		s;
		option = struct("rational", false, "digits", 4);
		NameValue.imaginaryUnit (1, 1) string = "\mathrm{j}";
	end
	if numel(symvar(s)) > 0
		thisStr = latex(s);
		return;
	end
	if ~isscalar(s)
		thisStr = "\begin{bmatrix} ";
		for it = 1 : size(s, 1)
			for jt = 1 : size(s, 2)
				thisStr = thisStr + misc.latexComplex(s(it, jt), option, ...
					"imaginaryUnit", NameValue.imaginaryUnit) + " & ";
			end
			thisStr = char(thisStr);
			thisStr = string(thisStr(1:end-2)) + "\\ ";
		end
		thisStr = char(thisStr);
		thisStr = string(thisStr(1:end-3)) + "\end{bmatrix}";
	else
		thisSymReal = real(s);
		thisSymImag = imag(s);
		if isfield(option, "round")
			thisSymReal = round(thisSymReal, option.round, "significant");
			thisSymImag = round(thisSymImag, option.round, "significant");
		end
		if any(thisSymReal)
			thisStr = latexFormat(thisSymReal, option);
			if any(thisSymImag)
				if (numel(s)>1) || (thisSymImag > 0)
					thisStr = thisStr + "+" + NameValue.imaginaryUnit;
					if thisSymImag ~= 1
						thisStr = thisStr + latexFormat(thisSymImag, option);
					end
				else
					thisStr = thisStr + "-" + NameValue.imaginaryUnit;
					if thisSymImag ~= -1
						thisStr = thisStr + latexFormat(-thisSymImag, option);
					end
				end
			end
		elseif any(thisSymImag)
			if (numel(s)>1) || (thisSymImag > 0)
				thisStr = NameValue.imaginaryUnit;
				if thisSymImag ~= 1
					thisStr = thisStr + latexFormat(thisSymImag, option);
				end
			else
				thisStr = "-" + NameValue.imaginaryUnit;
				if thisSymImag ~= -1
					thisStr = thisStr + latexFormat(-thisSymImag, option);
				end
			end
		else
			thisStr = "0";
		end
	end
end % latexComplex()

function myStr = latexFormat(data, option)
	if option.rational
		myStr = latex(sym(data));
	else
		myStr = latex(vpa(round(data, 15), option.digits));
		myStr = replace(myStr, ".0 ", " ");
		myStr = replace(myStr, ".0e", "e");
		myStr = replace(myStr, ".0\\", " \\");
	end
	if endsWith(myStr, ".0")
		myStr = char(myStr);
		myStr = string(myStr(1:end-2));
	end
	if endsWith(myStr, " ")
		myStr = char(myStr);
		myStr = string(myStr(1:end-1));
	end
end % latexFormat