function Mcell = blkdiagInv(M, lengthBlock)
% misc.blkdiagInv returns the matrixes on the block-diagonal of M. The length of these
% blocks must be specified by lengthBlock.
%	{M1, M2, ... } = misc.blkdiagInv(...
%			blkdiag(M1, M2, ...), [size(M1, 1), size(M2, 1), ...])

Mcell = cell(size(lengthBlock));
startIndex = 0;
for it = 1 : numel(lengthBlock)
	Mcell{it} = M(startIndex + (1:lengthBlock(it)), startIndex + (1:lengthBlock(it)));
	startIndex = startIndex + lengthBlock(it);	
end % for it = 1 : numel(lengthBlock)

assert(all(M == blkdiag(Mcell{:}), 'all'), "result is faulty");
end % blkdiagInv()

