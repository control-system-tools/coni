function arr_unq = unique_accur( array,accuracy )
% UNIQUE_ACCUR get only unique elements in the first dimension of an array, given that their
% derivation is more than accuracy.
% the first and last entry are always taken.
% If array is a matrix, duplicate entries are removed if there is a possibility to remove at
% least one complete row. On contrast to unique, the rows do not need to be identical, by can
% be shifted.
% 
% e.g. in the matrix
% A = [1   1   1   ;
%      2.1 1.1 2   ;
%      3   3   2.1];
% B = unique_accur(A,0.5) will remove the entries 2.1, 1.1 and 2.1 of the three columns
% respectively and give
% B =[1 1 1     ;
%     3 3 2.1  ];.
%
% Created by Simon Kerschbaum
% modified on 04.10.2018 by SK:
%   * added implementation to remove lines which are not rows.
%

if ~isvector(array)
	if ~ismatrix(array)
		error('unique_accur only works for 2D arrays')
	end
	arr_u = unique(array,'rows');
else
	arr_u = unique(array);
	arr_u = arr_u(:);
end
if nargin<2
	accuracy = 1e-12;
end

arr_mid = arr_u;
diffArr = diff(arr_mid);
smallArr = abs(diffArr)<accuracy;
count=0;
while all(any(smallArr)) % in this case, at least one row can be removed!
	idxVec = zeros(1,size(smallArr,2));
	for j=1:size(smallArr,2)
		idxVec(j) = find(smallArr(:,j),1)+1; %get index of duplicate in each row
	end
	oldSiz = size(arr_mid);
	arr_mid(sub2ind(size(arr_mid),idxVec,1:length(idxVec))) = []; % delete correct entries
	arr_mid = reshape(arr_mid,oldSiz(1)-1,oldSiz(2));
	diffArr = diff(arr_mid);
	smallArr = abs(diffArr)<accuracy;
	if count>numel(arr_u)
		warning(['Stop after ' num2str(count) ' tries!'])
	break
	end
end
arr_unq = arr_mid;

if any(arr_unq(end,:)-arr_u(end,:))
	% always take first value!
	% The last value is automatically taken!
	arr_unq(end,:) = arr_u(end,:);
end

end

