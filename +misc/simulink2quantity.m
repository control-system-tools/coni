function quantityStruct = simulink2quantity(simulinkData)
% simulink2quantity converts a Simulink.SimulationOutput object, which results of a simulink
% simulation using sim, and contains data from to-workspace blocks into a structure of
% quantity.Discrete objects.
%
%	quantityStruct = misc.simulink2quantity(simulinkData) the simulation data stored in the 
%		Simulink.SimulationOutput object "simulinkData" is converted into the struct which contains
%		the simulation results in fields of quantity.Discrete-type with the domain "t" specified by
%		simulinkData.tout.
%
%
% See also sim, struct2cell, fieldnames, misc.struct, misc.structMerge, misc.setfield,
% misc.getfield, misc.nameValuePairRemoveDots.

arguments
	simulinkData (1, 1) Simulink.SimulationOutput;
end % arguments

time = quantity.Domain("t", simulinkData.tout);

names = string(simulinkData.who());

quantityStruct = struct();
for name = names.'
	quantityStruct.(name) = quantity.Discrete(simulinkData.(name), time, "name", name);
end
end % misc.simulink2quantity

