function C = multArrayOld(A,B,dimA,dimB,equalDims,~)
% MULTARRAY multiply arrays with arbitrary dimensions
%
% C = multArray(A,B) Multiplies the arrays A*B in that form that the last
% dimension of A must be equal the first dimension in B. That is, the last
% dimension of A is regarded as "rows" and the first dimension of B is
% regarded as "columns.
%
% C = multArray(A,B,dimA,dimB) Specifies which dimensions are used as
% "rows" and "columns" for A,B, respectively. The resulting matrix has the
% dimensions of A without the multiplied dimension, concatenated with the
% dimensions of B without the multiplied dimension.
%
% C = multArray(A, B, dimA, dimB, equalDims) equalDims specifies the
% dimensions of A and B which are considered as "equal". This behaves like
% a pointwise multiplication over the specified dimension, but with matrix
% multiplication on the other dimensions. The result C is ordered like:
% [equalDims,
%  dimensions of A without equal dimensions and multiplied dimension,
%  dimensions of B without equal dimensions and multiplied dimension]
%
% C = multArray(A,B,dimA,dimB,equalDims,'elemWise') Performs an
% element-wise multiplication as in A.*B, but multiplies only over
% dimension dim. That is, the elementwise multiplications of the dimensions
% dim is simply repeated for all other dimensions. The resulting matrix has
% the dimensions of A concatenated with the dimensions of B without the
% multiplied dimension.
%
%
% INPUT PARAMETRS:
%   ARRAY     A          : first element of multiplication
%   ARRAY     B          : second element of multiplication
%   INTEGER   dimA (end) : dimension of A which shall be multiplied
%   INTEGER   dimB (1)   : dimension of B which shall be multiplied
%             ~          : if a 4th argument is given, the multiplication is performed
%                          element-wise over the given dimensions dimA and dimB
%
% OUTPUT PARAMETERS:
%   ARRAY     C          : Result of the multiplication. In the case of the normal
%                          multiplication,
%                          size(C) logically is ["size(A) without dimA" "sizeB without dimB"].
%                          In the case of the element-wise multiplication, the multiplied
%                          dimension does not collapse totally, but is preserved ones. It is kept
%                          at the most logical position, that is in A. This means
%                          size(C) is [size(A) "sizeB without dimB"].
%                          That this is reasonable can be seen at an easy example
%                          a = [1 2];
%                          b = [2;1];
%                          c = multArray(a,b,2,1,'elemWise') = [2 2].
%
% see also doc.multidimensional_operations

% created on 26.09.2018 by Simon Kerschbaum


% Store sizes
sizA = size(A); % here it is more performant to store the size!
sizB = size(B);
ndimsA = length(sizA); % when the size is stored, length and ndims(A) are equally fast
ndimsB = length(sizB);
% import misc.*

% Input check
if nargin<4 % default dimension for B
	dimB = 1;
end
if nargin<3 % default dimension for A
	dimA = ndimsA;
end
if ndimsA>2 || ndimsB>2 % error message is not needed when performing "normal" matrix multiplication.
	% because the normal matrix multiplication error message is enough then.
	% To save time, this check is inserted!
	% sizB(dimB) doesn't work here, because it may be that dim is the last dimension of B,
	% which is not existent in sizB, if its length is 1.
	if size(A,dimA) ~= size(B,dimB)
		error(['Length of dimension ' num2str(dimA) ' of A must equal the length of dimension ' num2str(dimB) ' of B!']);
	end
end

% handle singular dimensions separately. Return zeros of appropriate dimensions.
if size(A, dimA) == 0
	if nargin<5 || isempty(equalDims)
		if nargin>5 % elemwise multiplication
			C = zeros([sizA, sizB([1:dimB-1 dimB+1:ndimsB])]);
		else
			C = zeros([sizA([1:dimA-1 dimA+1:ndimsA]), sizB([1:dimB-1 dimB+1:ndimsB])]);
		end	
		return
	else
		error('misc.multArray not yet implemented for singular dimensions and given equalDims')
	end
end
	

% if an (n, m, ..., 1) should be multipied with others, matlab cuts the last
% dimension. If this is the dimension that should be multiplied, it has to
% be added again.
if ndimsA < dimA
	lostDims = dimA - ndimsA;
	
	sizA = [sizA, ones(1, lostDims)];
	ndimsA = ndimsA + lostDims;
end

if ndimsB < dimB
	lostDims = dimB - ndimsB;
	sizB = [ sizB, ones(1, lostDims)];
	ndimsB = ndimsB + lostDims;
end

if nargin == 5 && ~isempty(equalDims)
	% 	assert(numel(equalDims) == 1, 'Not yet implemented for multiple euqal dims')
	sizeEqual = sizA(equalDims);
	hasEqualDim = true;
else
	equalDims = 0;
	hasEqualDim = false;
	sizeEqual = [];
end

% perprocessing:
[reshA, permSizA, sizeCollapseA, idxA] = preProcessing(A, sizA, dimA, ndimsA, hasEqualDim, equalDims, sizeEqual);
[reshB, permSizB, sizeCollapseB, idxB] = preProcessing(B, sizB, dimB, ndimsB, hasEqualDim, equalDims, sizeEqual);

%Perform multiplication
if nargin<6 % normal matrix multiplication
	if hasEqualDim
		[argA{1:ndims(reshA)}] = deal(':');
		[argB{1:ndims(reshB)}] = deal(':');
		
		numelEqual = prod(sizeEqual);
		
		reshC = zeros([numelEqual, prod(sizeCollapseA), prod(sizeCollapseB)]);
		for k = 1:numelEqual	% iterate over the equal dimension
			argA{1} = k;
			argB{1} = k;			
			reshC(k, :, :) = reshape(reshA(argA{:}), [], sizA(dimA)) * reshape( reshB(argB{:}), [], sizB(dimB)).';
		end
	else
		% why is reshape performed here? reshA and reshB are already reshaped?!?
		reshC = reshape(reshA, [], sizA(dimA)) * reshape( reshB, [], sizB(dimB)).';
	end
	
else % elementwise multiplication
	% make sure that A and B are exactly of same dimension
	newA = repmat(reshA,1,1,size(reshB,2));
	newB = repmat(permute(reshB,[3 1 2]),size(reshA,1),1,1);
	reshC = newA.*newB;
end

% Postprocessing
% Restore the collapsed dimensions and permute the multiplied dimension to the original
% position, if multiplication was element-wise
if nargin<6 % normal multiplication
	% if the dimensions are <3, a normal matrix multiplication is performed.
	if ndimsA>2 || ndimsB>2
		% reshape the result to the original form.
		newSize = [sizeEqual, sizeCollapseA, sizeCollapseB];
		C = reshape(reshC, newSize);
		
		% permute back to original positions (not required, its ok if equal dims are at
		% beginning!
% 		bBackIdx = idxB.back(~idxB.equalVec & ~ idxB.multVec)+numel(idxA.collapse);
% 		C = permute(permC,[idxA.back(~idxA.multVec) bBackIdx]);
	else % No reshape is needed, because A and B were matrices
		C = reshC;
	end
else
	if ndimsA>2 || ndimsB>2 % if dimensions have been collapsed
		newSiz = [permSizA permSizB(2:end)]; % the multiplied dimension is deleted from B
		permC = reshape(reshC,newSiz);
	else
		permC = reshC;
	end
	if dimA~=ndimsA % Shift the multiplied dimension back to the original position in A
		C = permute(permC,[1:dimA-1 ndimsA dimA:ndimsA-1 ndimsA+1:ndimsA+ndimsB-1]);
	else
		C = permC;
	end
end

end


function [M, sizeM, sizeCollapse, idx] = preProcessing(M, sizeM, multDim, ndims, hasEqualDim, equalDims, sizeEqual)
% PREPROCESSING permute desired dimension to correct position and collapse
% all other dimensions
%		*) all equal dimensions (see equalDims) are at the beginning *) the
%		multDim is at the end of M *) all dimensions inbetween will be
%		collapsed
Idx = 1:ndims;
equalIdx = any((Idx == equalDims'), 1);
multIdx = (Idx == multDim);
collapseIdx = ~(equalIdx | multIdx);
sizeCollapse = sizeM(collapseIdx);

idx.collapse = Idx(collapseIdx);
idx.mult = Idx(multIdx);
% permute back to original representation
% idx.back = Idx;
% idx.equalVec = equalIdx;
% idx.multVec = multIdx;

if ndims>2 || multDim~=ndims || hasEqualDim
	% special operation needed (either permute or reshape or both)
	idx.equal = Idx(equalIdx);
	
	if multDim ~= ndims || ~all(equalDims == (1:numel(equalDims))) %all(equalDims ~= (1:numel(equalDims))) %
		%permutation order: [ equalDims, collapseDims, multiplierDim ]
		permIdx = [ idx.equal, idx.collapse, idx.mult ]; % create permutation index
% 		[~,idx.back] = sort(permIdx);
		
		M = permute(M, permIdx);    %permute the matrix
		sizeM = sizeM(permIdx); % permute the size
	end
	
	if hasEqualDim
		pSizeEqual = prod(sizeEqual);
	else
		pSizeEqual = [];
	end
	% all leading dimensions are stored in one
	M = reshape(M, [pSizeEqual, prod(sizeCollapse), sizeM(end)]);
end

end








