function res = eval_pointwise(f_handle, z)
% EVAL_POINTWISE evaluate the function stored in f_handle pointwise, at the
% points of the vector z.

%
% Description:
% res = EVAL_POINTWISE(f_handle,z) 
% Helper function:
% evaluate the function stored in f_handle pointwise, if z is a vector.
% must be used for example to pass
%      l_z = @(z) A*z*b
% as  a distributed input to a function (e.g. approximate_hyperbolic_system)
% l_z(zdisc) will lead to an error, if zdisc is a vector.
% Calling l_z2 = @(z)eval_pointwise(l_z,z) and then using
% l_z2(zdisc) will do the trick.
%
% Inputs:
%     f_handle        Function handle of varibale to be discretized
%     z               Vector containing discretization points
% Outputs:
%     res             Discretized version of f_handle
%
% Example:
% -------------------------------------------------------------------------
% f = @(z) z^2;
% zdisc = 0:1/5:1;
% fdisc = eval_pointwise(f,zdisc)
% >> ans = [0.0
%           0.04
%           0.16
%           0.36
%           0.64
%           1.0]
% -------------------------------------------------------------------------


% history:
% created on 02.02.2016 by Simon Kerschbaum
% modified on 12.09.2016 by SK:
%   - enable handling of functions f_handle of arbitrary dimensions.
%     f_handle can be any nxm matrix, or a multidimensional arrray.

	% preallocate
	dim_f = size(f_handle(z(1)));
	dim_f_cell = mat2cell(dim_f, 1, ones(1, numel(dim_f))); %#ok<*MMTC>
	% preallocate res:
	res = zeros(length(z), dim_f_cell{:});
	number_dims = length(dim_f);
	number_cases = prod(dim_f); %alle Dimensionen multipliziert gibt die Anzahl der verschiedenen Indexkombinationen
	for z_idx=1:length(z) % Auswertung punktweise
		dim_idx = ones(1,number_dims); % Alle Dimensionen haben zun�chst Index 1
		fz = f_handle(z(z_idx));
		dim_idx = mat2cell(dim_idx, 1, ones(1, numel(dim_idx))); % in Cell-Array kopieren, 
		% damit als Index in den Klammern verwendet werden kann. Als
		% Matrix/Vektor nicht m�glich, weil damit der Bereich der Inidzes
		% angegeben wird.
		for case_idx = 1:number_cases
			res(z_idx, dim_idx{:}) = fz(dim_idx{:}); % Auswertung bei dieser Indexkombination
			dim_idx = incdim(dim_f, dim_idx); %n�chster Indexfall
		end
	end
end
  
function inc_dim = incdim(dims, current_dims)
	% iterate through all dimension indices. Works like binary increment, with
	% the difference, that the maximum number of a dimension is set by dims.
	curdim = length(dims);
	inc_dim = current_dims;
	while curdim > 0
		if inc_dim{curdim} < dims(curdim)
			inc_dim{curdim} = inc_dim{curdim}+1;
			break
		else
			inc_dim{curdim} = 1;
			curdim = curdim-1;
		end
	end
end