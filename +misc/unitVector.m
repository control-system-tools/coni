function [e] = unitVector(dim, i)
%UNITVECTOR Computes the unit vector 
%   [e] = unitVector(dim, i) returns the i-th unitvector of size dim.

e = double( (1:dim == i )' );

end

