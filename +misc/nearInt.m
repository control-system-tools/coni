function result = nearInt(number, tolerance)
    if nargin == 1
        result = numeric.near(number, round(number));
    else
        result = numeric.near(number, round(number), tolerance);
    end
end