function used = findUsing(funcname,folder)
%FINDUSING Summary of this function goes here
% INPUT PARAMETERS:
% 	CHAR    	funcname           : name of used function if added to
%                                    workspace, else path of function
%	CHAR		folder             : path of folder where to search;
%                                    default: working directory
%
% OUTPUT PARAMETERS:
% 	Array of strings	used       : Array of paths to functions that use
%                                    funcname
%   example:
%   %% search actual folder
%   >> findUsing('funcname')
%   >> XX�1 string array
%
%    "***\func1.m"
%    "***\func2.m"

% set default value
if ~exist('folder','var')
    folder = pwd;
end

used = [];
% change directory to folder
currFolder = cd(folder);
% list of all functions within folder and subfolders
list =  dir('**/*.m');

% iterate over all functions in list 
for i = 1:numel(list)
    % list all functions that are used
    usedFuncs = cell2mat(matlab.codetools.requiredFilesAndProducts(which(strcat(list(i).folder,'\',list(i).name))));
    % if function contains funcname --> add to used
    if contains(usedFuncs,which(funcname))
        used = [used;string(which(list(i).name))];
    end
end
% change directory to origin
cd(currFolder);
end

