function subanimate(x, NameValue)
% misc.subanimate animates a vector function x(z, t) defined on a spatial domain z and a temporal
% domain t. 
%
%	misc.subanimate(x) animates over time t while showing the 1D spatial domain for every time 
%		instance as one frame.
%
%	misc.subanimate(..., "frames", frames) allows to specify how many frames are display.
%
%	misc.subanimate(..., "timeDomain", t) allows to set the temporal domain displayed
%
%	misc.subanimate(..., "spatialDomain", z) allows to set the spatial domain to be displayed
%
%	misc.subanimate(..., "compareWith", reference) allows to display a second reference signal next
%		 to x for comparison
%
% See also quantity.Discrete.plot

arguments
	x quantity.Discrete;
	NameValue.timeDomain quantity.Domain = x(1).domain.find("t");
	NameValue.spatialDomain quantity.Domain = x(1).domain.find("z");
	NameValue.frames double = timeDomain.n;
	NameValue.compareWith quantity.Discrete = [];
end

t = NameValue.timeDomain;
z = NameValue.spatialDomain;
lr.x = x.on([t, z]);

doCompare = ~isempty(NameValue.compareWith);
if doCompare
	assert(isequal(size(NameValue.compareWith), size(x)) ...
		&& isa(NameValue.compareWith, 'quantity.Discrete'));
	lr.xReference = NameValue.compareWith.on([t, z]);
end

subplotCell = cell(size(x));
for it = 1 : numel(x)
	subplotCell{it}.s = subplot(numel(x),1,it);
	subplotCell{it}.x = animatedline('Color','[0.00000,0.44700,0.74100]','LineWidth',2);
	if doCompare
		subplotCell{it}.xReference = animatedline('LineStyle', '--', ...
			'Color','[0.8500 0.3250 0.0980]','LineWidth', 1);
	end
	grid on;
	axis([0,1,min(-1, min(lr.x(:))), max(1, max(lr.x(:)))]);
	xlabel("$$" + z.name + "$$", 'Interpreter','latex');
	ylabel("$$" + x(1).name + "_" + num2str(it) + "(" + z.name ...
		+ ", " + t.name + ")$$", 'Interpreter','latex');
	aTemp = gca();
	aTemp.TickLabelInterpreter = 'latex';
end

for k = 1 : ceil(t.n/NameValue.frames) : t.n
	for it = 1 : numel(x)
		subplotCell{it}.s;
		clearpoints(subplotCell{it}.x)
		addpoints(subplotCell{it}.x, z.grid, squeeze(lr.x(k, :, it)));
		if doCompare
			clearpoints(subplotCell{it}.xReference)
			addpoints(subplotCell{it}.xReference, z.grid, squeeze(lr.xReference(k, :, it)));
		end
		drawnow
		title(subplotCell{1}.s, ...
			"$$t = " + num2str(t.grid(k), "%10.2f\n") + "$$ of $$" + num2str(t.upper, "%10.2f\n") ...
			+ "$$", 'Interpreter','latex');
	end
end

end % misc.subanimate()