function out = sd( in,n )
% SD Short shiftdim
% behaves like shiftdim, but uses n=1 if not passed.
%
% Descriptiuon:
% out = sd(in,n) 
% Shifts the dimensions in in n times to the left
%
% Inputs:
%     in      Input array
%     n       Amount of shifts t the left (dafult n = 1)
% Outputs:
%     out     Shifted array
%
% Example:
% -------------------------------------------------------------------------
% A = rand(2,3,4);
% size(misc.sd(A))
% >> ans = [3 4 2]
% -------------------------------------------------------------------------

if nargin==1
	n=1;
end
out=shiftdim(in,n);


end

