function [d, sizes] = getDimensions(args, names, dim)
% GETDIMENSION computes the maximal dimension of matrices or operators
%	[d, sizes] = getDimensions(args, names, dim) computes the the maximal order of dimension
%	specified by 'dim'. The matrices or operators are assumed to fields of the struct 'args' and the
%	fields that should be evaluated can be specified by the string-array "names".
arguments
	args;
	names string;
	dim = 1;
end
sizes = zeros(numel(names), 1);
for k = 1:length(names)
	if isfield(args, names(k))
		
		% add plus 1 to dimension if the object is a polynomialOperator:
		tmpDim = dim + isa( args.(names(k)), 'signals.PolynomialOperator');
		
		sizes(k) = size(args.(names(k)), tmpDim);
	end
end

d = max(sizes);

end