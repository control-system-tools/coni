function myStr = latexChar(data, option, NameValue)
% misc.latexChar converts input-arrays into latex compatible char-arrays. data may be
% numeric, symbolic, quantity.Symbolic, quantity.Discrete or function_handle.
% Example:
% -------------------------------------------------------------------------
% option.rational = false;
% option.digits = 4;
% tex = misc.latexChar(ones(2), option)
% -------------------------------------------------------------------------
%	See also misc.variables2tex

arguments
	data;
	option = struct("rational", false, "digits", 4);
	NameValue.set (1, 1) logical = false;
	NameValue.imaginaryUnit (1, 1) string = "\mathrm{j}";
end

if ischar(data) || (iscell(data) && ~isempty(data{1}) && ischar(data{1}))
	data = string(data);
end

if NameValue.set
	myStr = "\{ ";
	for it = 1 : numel(data)
		myStr = myStr + misc.latexChar(data(it), option, ...
			"set", false, "imaginaryUnit", NameValue.imaginaryUnit);
		if it < numel(data)
			myStr = myStr + " , ";
		end
	end
	myStr = myStr + " \}";
	return;
end % if NameValue.set

if isstring(data)
	if isscalar(data)
		myStr = data;
	else
		myStr = "\begin{bmatrix} ";
		for it = 1 : size(data, 1)
			for jt = 1 : (size(data, 2)-1)
				myStr = myStr + data(it,jt) + " & ";
			end
			myStr = myStr + data(it,end) + " \\ ";
		end
		myStr = myStr + "\end{bmatrix}";
	end
		

elseif isnumeric(data)
	myStr = misc.latexComplex(sym(data), option);

elseif isa(data, "sym")
	if isempty(symvar(data))
		myStr = misc.latexComplex(data, option);
	else
		myStr = latex(data);
	end

elseif isa(data, "quantity.Symbolic")
	myStr = latex(data.sym());

elseif isa(data, "quantity.Discrete")
	if ~isempty(data(1).name)
		myStr = data(1).name;
	else
		myStr = "numeric";
	end

elseif isa(data, "function_handle")
	try
		myStr = latex(sym(data));
	catch
		try
			myStr = func2str(data);
		catch
			myStr = "Unknown";
		end
	end

elseif isa(data, "logical")
	myStr = latex(sym(data));
	
else
	error("The data type " + class(data) + " is not considered in this function");
end

% some formating:
while any(strfind(myStr, "\left(\begin{array}{cc"))
	myStr = strrep(myStr, "\left(\begin{array}{cc", "\left(\begin{array}{c");
end
myStr = strrep(myStr, "\left(\begin{array}{c}", "\begin{bmatrix}");
myStr = strrep(myStr, "\end{array}\right)", "\end{bmatrix}");

myStr = strrep(myStr, "\left(", "(");
myStr = strrep(myStr, "\right)", ")");
myStr = strrep(myStr, "\,", "");
myStr = strrep(myStr, "\mathrm{heaviside}", "\sigma");

myStr = strrep(myStr, ".0 ", " ");
myStr = strrep(myStr, ".0\\", " \\");
myStr = strrep(myStr, ".0&", " &");
end % latexChar()