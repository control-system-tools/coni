function [N, iterations] = solveVolterraIntegroDifferentialIVP(z, ic, n, m, NameValue)
% MISC.solveVolterraIntegroDifferentialIVP calculates the solution N(z) \in R^{n x m} a volterra
% integro differential initial value problem.
%	N = solveVolterraIntegroDifferentialIVP(z, ic, 'M', M, 'A', A, 'B', B, 'C', C, 'D', D)
%		calculates the desired solution N. With M, A, B, C, D being optional
%		parameter that have to be quantity.Discrete objects and M being non-singular.
%
% The Volterra integrodifferential equation to be solved is defined as:
%	M(z) N'(z) + A(z) N(z) + N(z) B(z) + C(z) + int_0^z D(z, zeta) N(zeta) dzeta = 0
%	N(0) = N0 \in R^{nxm}.
% This equation is solved by vectorization and successive approximations. By dividing with M follows
%	N'(z) = -M(z) \ (A(z) N(z) + N(z) B(z) + C(z) + int_0^z D(z, zeta) N(zeta) dzeta)
%	N(0) = N0.
% Vectorization n = vec(N) = N(:) yields
%	n'(z) = T1(z) n(z) + int_0^z T2(z, zeta) n(zeta) dzeta + t3
%	n(0) = n0
% with
%	T1 = -kron(B.', inv(M)) - kron(Im, M \ A)
%	T2 = -kron(Im, M \ D)
%	t3 = -vec(M \ C)
%	n0 = vec(N0).
% Then, formal integration and changing the order of integrals yields
%	n(z) = t4(z) + int_0^z T5(z, zeta) n(zeta) dzeta
% which is a Volterra integral equation of the second kind, which can be solved by successive
% approximations. The parameters are
%	t4 = n0 + int_0^z t3(zeta) dzeta
%	T5 = T1(zeta) + int_zeta^z T2(eta, zeta) deta.
%
% Afterwards, N(z) = reshape(n(z), [n, m]) yields the result.

arguments
	z (1, 1) quantity.Domain;
	ic (:, :) double;
	n (1, 1) double = size(ic, 1);
	m (1, 1) double = size(ic, 2);
	NameValue.M quantity.Discrete = quantity.Discrete.eye([n, n], z);
	NameValue.A (:, :) = zeros([n, n]);
	NameValue.B (:, :) = zeros([m, m]);
	NameValue.C (:, :) = zeros([n, m]);
	NameValue.D quantity.Discrete = ...
		quantity.Discrete.zeros([n, n], z.rename("zeta"));
	NameValue.iterationMargin (1, :) {mustBePositive, mustBe.ascending} = [2, 100];
	NameValue.silent (1, 1) logical = false;
	NameValue.tolerance (1, 1) double = 1e-9;
end

% Calculate parameters
Minv = quantity.Discrete(inv(NameValue.M));
T1 = - kron(NameValue.B.', Minv) - kron(eye(m), Minv * NameValue.A);
T2 = - kron(eye(m), Minv * NameValue.D);
t3 = - reshape(Minv * NameValue.C, [n*m, 1]);
n0 = ic(:);
t4 = n0 + int(t3, "z", 0, "z");
T5 = T1.subs("z", "zeta") + int(T2.subs(["z", "zeta"], ["eta", "zeta"]), "eta", "zeta", "z");

% cast to quantity.Discrete for better computational performance
T5 = T5.changeDomain([z; z.rename("zeta")]);

% Successive approximations
iterations = misc.ProgressBar("name", ...
	"### Successive approximations of Volterra integro differential equation: ", ...
	"steps", NameValue.iterationMargin(end), ...
	"terminalValue", NameValue.iterationMargin(end), ...
	"progress", 1, "silent", NameValue.silent, ...
	"printAbsolutProgress", true, ...
	"info", struct("tolerance", NameValue.tolerance, "iterationMargin", NameValue.iterationMargin));

nOld = t4.changeDomain(z);
iterations.start(...
	"value", struct("solution", nOld, "maxAbsChange", inf), ...
	"addMessage", "steps");
differenceOldNew = 0;
while iterations.progress < NameValue.iterationMargin(1) ...
		|| (~(iterations.progress > NameValue.iterationMargin(2)) ...
		&& (differenceOldNew > NameValue.tolerance))
% 	iterate until limit of iterations is reached or until the the difference between the latest
% 	result and the result of the iteration before is smaller than the.

	% calculate Volterra integral equation
	nNew = t4 + int(T5 * nOld.subs("z", "zeta"), "zeta", 0, "z");

	%% Prepare next iteration
	differenceOldNew = MAX(abs(nNew - nOld));
	iterations.raise([], ...
		"value", struct("solution", nNew, "maxAbsChange", differenceOldNew), ...
		"addMessage", "steps | changed: " + string(differenceOldNew) ...
			+ " (tolerance=" + string(NameValue.tolerance) + ")");
	nOld = nNew;
end %while
N = reshape(nNew, [n, m]).setName("N");
end % solveVolterraIntegroDifferentialIVP()