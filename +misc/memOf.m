function size = memOf(var)
name = who;
r = whos(name{1});
if nargout == 0
	fprintf("Size: %.2g MB \n", r.bytes/1e6);
else
	size = r.bytes;
end
end

