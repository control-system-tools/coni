function [y_res,t_res] = resample_time(y,t,num,upscale)
% RESAMPLE_TIME: resample a time signal to num elements
%
% [y_res,t_res] = RESAMPLE_TIME(y,t,num)
% resamples the signal y over the time t width num data points.
% The algorithm doesnt use interpolation but only selection of existing
% data points.
%
% INPUT PARAMETRS:
%   MATRIX          y       : Time signals: can contain multiple signals,
%                             as row vectors. 
%   VECTOR          t       : time belonging to y
%   SCALAR          num     : number of desired signal points
%   LOGICAL         upscale : allow upsampling
%
% Outputs:
%     y_res         New y sasmplepoints
%     t_res         New t sample points
%
% Example:
% -------------------------------------------------------------------------
% syms x real
% t = 0:0.1:10;
% y = double(subs(sin(x),'x',t));
% [y_res,t_res] = misc.resample_time(y,t,25);
% plot(t,y,t_res,y_res,'o');
% -------------------------------------------------------------------------
%

% (c) 2014 Simon Kerschbaum

% history:
% modified on 10.01.2018 by SK:
% add parameter upscale to allow upsampling!

if size(y,2)==1 %falls Spaltenvektor übergeben wird!
	y=y.'; 
end
	
if length(t)~= size(y,2)
	error('Signals y and time t must be of same size!')
end
if num<2
	error('num must be at least 2!')
end

if ~exist('upscale','var')
	upscale=0;
end

if num>length(t)
	if upscale ~= 0
		num_t = num;
		t_fine=t;
		while length(t_fine) < num
			t_temp = t_fine(1:end-1)+1/2*diff(t_fine); %jeweils einen Punkt dazwischen legen
% 			t_temp = t_temp(1:end-1); % letzter Wert sinnlos.
			t_fine = sort([t_fine t_temp]);
		end
		y = interp1(t,y,t_fine);
	else
		num_t = length(t);
	end
else
	num_t = num;%Zeitauflösung zum Plotten
end

		t_res = t(1);
		y_res(:,1) = y(:,1);
		b = length(t)-2;
		delta_sum=0;
		for i=2:num_t-1
			delta = b/(num_t-1);
			delta_r = round(delta);
			t_res = [t_res,t(1+delta_sum+delta_r)];
			y_res = [y_res,y(:,1+delta_sum+delta_r)];
			delta_sum=delta_sum+delta_r;
			b = b - delta_r;
			num_t = num_t-1;
		end
		t_res = [t_res,t(end)];
		y_res = [y_res,y(:,end)];

		
