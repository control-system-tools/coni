function itIs = isPositiveSemiDefinite(x, NameValue)
% isPositiveSemiDefinite checks if x is a symmetric, positive semi-definite matrix. For this, it is
% verified, that the eigenvalues of x are greater than -NameValue.absTol < 0 (default: 10*eps).
%	
%	itIs = isPositiveSemiDefinite(x) returns itIs = true if x is positive semi-definite, and itIs =
%		false otherwise.
%
%	itIs = isPositiveSemiDefinite(x, "AbsTol", tol) uses the tolerance tol, to verify that the
%		eigenvalues of x are greater than -tol.
%
% Example:
% -------------------------------------------------------------------------
% misc.isPositiveSemiDefinite(magic(3))
% >> ans = false
% misc.isPositiveSemiDefinite(diag([1, 0, 1]))
% >> ans = true
% -------------------------------------------------------------------------
% See also misc.isPositiveSemiDefinite

arguments
	x (:, :) double;
	NameValue.AbsTol (1, 1) {mustBeNonnegative} = 10*eps;
end

eigenValues = eig(x);

itIs = (min(real(eigenValues)) > -NameValue.AbsTol);

end % isPositiveSemiDefinite