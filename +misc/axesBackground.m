function A = axesBackground(optArgs)
% AXESBACKGROUND colorization of the background of an axes
% 	A = axesBackground(ax, optArgs) uses the area function to create a colored background for the
% 	specified axes. The axes can be specified by the name-value pair argument "ax".  If no axes is
% 	specified, the current axes is selected. Other arguments are "xRange" and "yRange", which are
% 	vectors with two entries indicating the boundaries of the rectangle to be colored. The
% 	default values are the corners of the background rectangle of the axes. To be specific, the area
% 	Omega = { xRange(1) <= x <= xRange(2), yRange(1) <= y <= yRange(2)} will be colored.
%	The colored rectangle will be drawn per default at the background of the axes. With "color", the
%	color can be specified and with "LineStyle" the border line of the rectangle (default is
%	"none"). 
arguments
	optArgs.ax = gca,
	optArgs.xRange (2,1) double;
	optArgs.yRange (2,1) double;
	optArgs.color = '#A3C9F5';
	optArgs.LineStyle = "none";
end

if ~isfield(optArgs, "xRange")
	optArgs.xRange = optArgs.ax.XLim;
end

if ~isfield(optArgs, "yRange")
	optArgs.yRange = optArgs.ax.YLim;
end

% remember the hold setting of the axes:
axHold = ishold(optArgs.ax);

% set the hold steting to true, so that the original plot will be preserved
hold(optArgs.ax, "on");

A = rectangle("Position", [optArgs.xRange(1), optArgs.yRange(1), ...
	optArgs.xRange(2)-optArgs.xRange(1), optArgs.yRange(2) - optArgs.yRange(1)], ...
	"FaceColor", optArgs.color, ...
 	"LineStyle", optArgs.LineStyle);
	
uistack(A, 'bottom');

% restore the original hold setting
if ~axHold
	hold(optArgs.ax, "off");
end

end