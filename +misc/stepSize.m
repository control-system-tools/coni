function [delta] = stepSize( grid, optArgs )
%STEPSIZE compute the step size of a vector
% [delta] = stepSize( grid ) computes the stepsize of a grid vector grid. If the grid is not
% equidistant spaced, an error is thrown.
arguments
	grid
	optArgs.AbsTol (1,1) double = 5e-13;
end

if isa(grid, "quantity.Domain")
	grid = grid.grid;
end

	delta_t = diff( grid );
	assert( all( abs( diff( delta_t ) ) < optArgs.AbsTol ), "CONI:misc", "Grid is not equidistant spaced" );
	delta = delta_t(1);

end

