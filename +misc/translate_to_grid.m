function [valueNewGrid, valueInterpolant] = translate_to_grid(value, gridNew, varargin)
% TRANSLATE_TO_GRID mighty interpolation
% it is a wide spread problem, that a parameter is given
% in some resolution or as a function_handle, but is needed in a specific
% grid. This function translates the value val into valueNewGrid which fits to
% the grid called grid. 
% The optional input varargin is considered as the old grid of val, but is
% only needed if the grid of value is non homogenious. If no varargin is
% defined, the old grid of value is assumed to be linspace(0,1,size(val,1)).
% If the value to be interpolated is empty, then an empty array of the size
% gridNew x 0 is returned.
%
% INPUT PARAMETERS:
%   DOUBLE-ARRAY or FUNCTION_HANDLE  
%							   value : variable to be transformed
%   VECTOR or CELL-ARRAY		grid : grid to which value is translated
% OPTIONAL INPUT PARAMTERS as name-value-pairs
%   DOUBLE ARRAY			 gridOld : old grid in which value is defined
%										default:linspace(0,1,size(value,idx)
%   INTEGER					    zdim : number of spatial coordinates to be
%										interpolated
%   STRING                   method  : interpolation method which is used. See
%                                      griddedInterpolant for all options. Default is 'linear'.
%   
% OUTPUT PARAMETERS:
%	MATRIX					valueNewGrid : resulting matrix of value fitting to
%										the grid called grid
%	NUMERIC.INTERPOLANT  valueInterpolant: corresponding interpolant
%
% Example:
% see unittest.misc.testTranslate_to_grid.m or:
% -------------------------------------------------------------------------
% v = [0 1 2 3 4 5 6 7 8 9].';
% NewGrid = linspace(0,1,20);
% misc.translate_to_grid(v,NewGrid)
% ans = [0
%        0.4737
%        0.9474
%        1.4211
%        1.8947
%        2.3684
%        2.8421
%        3.3158
%        3.7895
%        4.2632
%        4.7368
%        5.2105
%        5.6842
%        6.1579
%        6.6316
%        7.1053
%        7.5789
%        8.0526
%        8.5263
%        9.0000]
% -------------------------------------------------------------------------
%
% required subprograms:
%   - misc.eval_pointwise
if isa(value, 'function_handle')
%% value is a function_handle -> use eval_pointwise
	if iscell(gridNew) && (numel(gridNew) == 1)
		valueNewGrid = misc.eval_pointwise(value, gridNew{1});
	elseif isvector(gridNew)
		valueNewGrid = misc.eval_pointwise(value, gridNew);
	else
		error(['If value is a function_handle, then gridNew must be vector', ...
			' or a cell-array with one element. Multiple distributed ', ...
			'arguments for function_handle values are not supported yet.']);
	end
else
	%% value is an array -> use interpolant
	%% read input
	valueSize = size(value);
	valueNdims = ndims(value);
	parser = misc.Parser();
	addParameter(parser, 'gridOld', []);
	addParameter(parser, 'zdim', []);
	addParameter(parser, 'method','linear');
	parser.parse(varargin{:});
	if ~isempty(fieldnames(parser.Unmatched))
		warning('unmatched parameter in input parser');
	end
	gridOld = parser.Results.gridOld;
	zdim = parser.Results.zdim;
	method = parser.Results.method;
	
	%% zdim and gridNew:
	% This function supports nD-interpolation of the zdim-first dimensions
	% of value. The user is free to specify zdim explicitly or implicitly
	% by choosing gridNew a cell-array with the number of a cells that
	% represents the number of leading dimensions of values to be
	% interpolated. Hence, if zdim is not specified explicitly, zdim is set
	% to numel(gridNew) (or to zdim=1 if gridNew is a vector). This is done
	% in the following lines:
	gridNewCell = cell(valueNdims, 1);
	if any(contains(parser.UsingDefaults, 'zdim'))
		if isvector(gridNew) && ~iscell(gridNew)
			zdim = 1;
			gridNewCell{1:zdim} = gridNew;
		elseif iscell(gridNew)
			zdim = numel(gridNew);
			for it = 1:zdim
				gridNewCell{it} = gridNew{it};
			end
		else
			error('what is gridNew?')
		end
	else
		if isvector(gridNew) && ~iscell(gridNew)
			for it = 1:zdim
				gridNewCell{it} = gridNew;
			end
		elseif iscell(gridNew)
			if zdim == numel(gridNew)
				for it = 1:zdim
					gridNewCell{it} = gridNew{it};
				end
			elseif numel(gridNew) == 1
				for it = 1:zdim
					gridNewCell{it} = gridNew{1};
				end
			else
				error('zdim and gridNew are inconsistent');
			end
		else
			error('what is gridNew?')
		end
	end
	for it = (zdim+1) : valueNdims % index values
		gridNewCell{it} = 1:1:valueSize(it);
	end
	
	%% gridOriginal and gridOld
	% gridOriginal is used to create the interpolant. The first
	% zdim-dimensions result from gridOld or the default linspace(0, 1, ...).
	% The other elements are assumed to be indices, and therefore are 
	% 1:size(value,it).
	gridOriginal = cell(valueNdims,1);
	for it = 1 : zdim % gridded values
		if isvector(gridOld) && ~iscell(gridOld)
			gridOriginal{it} = gridOld;
		elseif iscell(gridOld) && (numel(gridOld) == zdim)
			gridOriginal{it} = gridOld{it};
		elseif iscell(gridOld) && (numel(gridOld) == 1)
			gridOriginal{it} = gridOld{1};
		else % default
			gridOriginal{it} = linspace(0, 1, valueSize(it));
		end
	end
	for it = (zdim+1) : valueNdims % index values
		gridOriginal{it} = 1:1:valueSize(it);
	end
	
	
	if isempty(value)
		%% value is empty -> return empty value of appropriate size
		newSizeOfOuputValue = valueSize;
		for it=1:zdim
			% the first entrys of valueNewGrid (and newSizeOfOutputValue)
			% represent the spatial variables which have the size of the grid
			% vectors in gridNewCell.
			newSizeOfOuputValue(it) = numel(gridNewCell{it});
		end
		if newSizeOfOuputValue(end) ~= 0
			newSizeOfOuputValue = [newSizeOfOuputValue, 0];
		end
		valueNewGrid = zeros(newSizeOfOuputValue);
		valueInterpolant = []; % empty instead of interpolant
		
	else
		%% Create interpolant object
		valueInterpolant = numeric.interpolant(gridOriginal, value, method);

		%% Get interpolated values
		valueNewGrid = valueInterpolant.evaluate(gridNewCell{:});
	end
		
end
