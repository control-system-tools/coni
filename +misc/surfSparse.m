function varargout = surfSparse(xdisc,ydisc,val,varargin)
% surfSparse surf of a 2D area which contains NaN and connects the borders as straight as possible.
%
% surfSparse(xdisc,ydisc,val) 
%   plots the function val defined over (xdisc,ydisc) 
%
% h = surfSparse(...) returns the corresponding plot handle.
%
% h = surfSparse(...,varargin) passes varargin to the used patch function.

% for debugging
% xdisc=1:size(val,1);
% ydisc=1:size(val,2);

F = double.empty(0,4);
ndiscX=length(xdisc);
ndiscY = length(ydisc);
ydisc = ydisc(:).'; % row vector

% all vertices (points)
kGes = val(:).';
xGes = repmat(xdisc(:),ndiscY,1).';
yGes = repmat(ydisc,ndiscX,1);
yGes = yGes(:).';


for xIdx=1:ndiscX-1
	for yIdx = 1:ndiscY-1
		% create rectangles to connect
		if ~isnan(val(xIdx,yIdx))
			if ~isnan(val(xIdx+1,yIdx)) && ~isnan(val(xIdx+1,yIdx+1)) && ~isnan(val(xIdx,yIdx+1))
				idxLow = [sub2ind([ndiscX ndiscY],xIdx,yIdx), sub2ind([ndiscX ndiscY],xIdx+1,yIdx)];
				idxHigh = [sub2ind([ndiscX ndiscY],xIdx+1,yIdx+1), sub2ind([ndiscX ndiscY],xIdx,yIdx+1)];
				F = [F;idxLow idxHigh];
% 				F = [idxLow idxHigh];
% 				h = patch('Faces',F,'Vertices',[xGes;yGes;kGes].',...
% 				'FaceVertexCData',kGes.','FaceColor','interp',varargin{:});
			elseif ~isnan(val(xIdx+1,yIdx)) && ~isnan(val(xIdx,yIdx+1))
				idxLow = [sub2ind([ndiscX ndiscY],xIdx,yIdx), sub2ind([ndiscX ndiscY],xIdx+1,yIdx)];
				idxHigh = [sub2ind([ndiscX ndiscY],xIdx,yIdx+1), sub2ind([ndiscX ndiscY],xIdx,yIdx+1)];
				F = [F;idxLow idxHigh];
% 				F = [idxLow idxHigh];
% 				h = patch('Faces',F,'Vertices',[xGes;yGes;kGes].',...
% 				'FaceVertexCData',kGes.','FaceColor','interp',varargin{:});
			elseif ~isnan(val(xIdx+1,yIdx)) && ~isnan(val(xIdx+1,yIdx+1))
				idxLow = [sub2ind([ndiscX ndiscY],xIdx,yIdx), sub2ind([ndiscX ndiscY],xIdx+1,yIdx)];
				idxHigh = [sub2ind([ndiscX ndiscY],xIdx+1,yIdx+1), sub2ind([ndiscX ndiscY],xIdx+1,yIdx+1)];
				F = [F;idxLow idxHigh];
% 				F = [idxLow idxHigh];
% 				h = patch('Faces',F,'Vertices',[xGes;yGes;kGes].',...
% 				'FaceVertexCData',kGes.','FaceColor','interp',varargin{:});
			end
		elseif ~isnan(val(xIdx+1,yIdx)) && ~isnan(val(xIdx,yIdx+1)) && ~isnan(val(xIdx+1,yIdx+1))
			idxLow = repmat(sub2ind([ndiscX ndiscY],xIdx+1,yIdx),1,2);
			idxHigh(1) = sub2ind([ndiscX ndiscY],xIdx+1,yIdx+1);
			idxHigh(2) = sub2ind([ndiscX ndiscY],xIdx,yIdx+1);
			F = [F;idxLow idxHigh];
% 			F = [idxLow idxHigh];
% 			h = patch('Faces',F,'Vertices',[xGes;yGes;kGes].',...
% 			'FaceVertexCData',kGes.','FaceColor','interp',varargin{:});
		end
	end
end
% h = patch('Faces',F,'Vertices',[xGes;yGes;kGes].',...
% 		'FaceVertexCData',kGes.','FaceColor','interp',varargin{:});
% hold on
% Fill triangles at lower border and maybe upper right:
% plot patches to fill triangles:
% FNew = double.empty(0,4);
for xIdx = 1:ndiscX
	for yIdx = 1:ndiscY
		if yIdx <= ndiscY-1
			if isnan(val(xIdx,yIdx,1)) && ~isnan(val(xIdx,yIdx+1,1))
				for xSIdx = xIdx+1:ndiscX
					if ~isnan(val(xSIdx,yIdx))
						idxLow(1) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx);
						idxLow(2) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx);
% 						idxHigh(1) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx+1);
% 						idxHigh(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx+1);
						if ~isnan(val(xSIdx,yIdx+1))
							idxHigh(1) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx+1);
							idxHigh(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx+1);
						else
							for xSSIdx = xSIdx:-1:xIdx+1 %suche nach Links
								if ~isnan(val(xSSIdx,yIdx+1))
									idxHigh(1) = sub2ind([ndiscX,ndiscY],xSSIdx,yIdx+1);
									idxHigh(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx+1);
									break
								end
							end
						end
						break
					end
				end
				F = [F; idxLow idxHigh];
% 				FNew = [idxLow idxHigh];
				% links unten
% 				h = patch('Faces',FNew,'Vertices',[xGes;yGes;kGes].',...
% 					'FaceVertexCData',kGes.','FaceColor','red',varargin{:});
			end
		end
		if yIdx>1
			if isnan(val(xIdx,yIdx,1)) && ~isnan(val(xIdx,yIdx-1,1))
				for xSIdx = xIdx+1:ndiscX
					if ~isnan(val(xSIdx,yIdx))
						idxLow(1) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxLow(2) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx-1);
						idxHigh(1) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx);
						idxHigh(2) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx);
						break
					else %create no verice. Its just a point
						idxLow(1) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxLow(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxHigh(1) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxHigh(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
					end
				end
				F = [F; idxLow idxHigh];
% 				FNew = [idxLow idxHigh];
% 				% links oben
% 				h = patch('Faces',FNew,'Vertices',[xGes;yGes;kGes].',...
% 					'FaceVertexCData',kGes.','FaceColor','red',varargin{:});				
			end
		end
		if xIdx>1 && yIdx>1
			if isnan(val(xIdx,yIdx,1)) && ~isnan(val(xIdx,yIdx-1,1))
				for xSIdx = xIdx-1:-1:1
					if ~isnan(val(xSIdx,yIdx))&& ~isnan(val(xSIdx,yIdx-1))
						idxLow(1) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx-1);
						idxLow(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxHigh(1) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx);
						idxHigh(2) = sub2ind([ndiscX,ndiscY],xSIdx,yIdx);
						break
					else % create not vertice, its just a point
						idxLow(1) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxLow(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxHigh(1) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
						idxHigh(2) = sub2ind([ndiscX,ndiscY],xIdx,yIdx-1);
					end
				end
				F = [F; idxLow idxHigh];
% 				FNew = [idxLow idxHigh];
% 				rechts oben
% 				h = patch('Faces',FNew,'Vertices',[xGes;yGes;kGes].',...
% 					'FaceVertexCData',kGes.','FaceColor','red',varargin{:});				
			end
		end
	end
end

% Attention: when CData is given as column vector, is assumes RGB values which need to be
% inside 0,1, leading to an error.
% matlab patch works fine with data F, xGes, yGes, kGes, but when exporting with matlab2tikz,
% kGes must not contain NaN, otherwise creates whitespace. No problem to replace NaNs because F
% contains no vertices with value NaN. BUT: remember that it effects the colormap data!
kGes(isnan(kGes)) = min(kGes(:));
	h = patch('Faces',F,'Vertices',[xGes;yGes;kGes].',...
		'FaceVertexCData',kGes.','FaceColor','interp',varargin{:});
if nargout>0
	varargout{1} = h;
end



end % function