function slideSurf(data, varargin)
%ANIMATE_KERNEL plot different steps of fixpoint iteration
%   animate_kernel(data) plots the different iteration steps
%   of the ppide_kernel created by 
%   [~,data] = fixpoint_iteration(...)

% Created on 12.09.2017 by Simon Kerschbaum

myFigure = figure('Name','slide data');%,'Position',[10 50 600 800]);
myPanel = uipanel('title','step 1', 'position',[0 .05 1 .95],...
	              'titleposition','centertop',...
				  'parent',myFigure);
callback = @(src,evt) scrollCallback(src,evt,data, varargin{:});
myScrollBar = uicontrol('style','slider','units','normalized',...
	                 'position',[0 0 1 .05],'min',1,'max',length(data),...
					 'value',1,...
					 'SliderStep',[1/length(data) 1/length(data)],...
					 'callback',callback);
% ax1 = axes('parent',hpanel,'outerposition',[0 0 0.5 1]);
if nargin > 1
	misc.subsurf(data{1}.value, 'myFigure', myPanel, varargin{:});
else
	misc.subsurf(data{1}.value, 'myFigure', myPanel);
end
drawnow()
%data{1}.plot('G',0,myPanel)

% end

function scrollCallback(src, evt, data, varargin)
	id = round(get(src,'value'));
	set(myScrollBar,'value',id);
	if nargin > 3
		misc.subsurf(data{id}.value, 'myFigure', myPanel, varargin{:});
	else
		misc.subsurf(data{id}.value, 'myFigure', myPanel);
	end
	myPanel.Title = ['step ', num2str(id)];
%	data{id}.plot('G',0,myPanel)
	drawnow()
end

end

