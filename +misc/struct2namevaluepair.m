function [nameValuePair] = struct2namevaluepair(s, optArgs)
% STRUCT2NAMEVALUEPAIR converts struct to name value pairs
%
% Inputs:
%		s								Struct to be converted into name-value pair.
%		optArgs.removeEmptyEntries		If true, empty values are removed. Default: false.
%		optArgs.convertNestedStructs	If true, also fields that are struct are converted into 
%										name-value pairs. Default: false.
%		optArgs.fieldNames              A list of field names that shoud be taken into the
%										nameValuePair result. If set, only the fieldnames in
%										fieldNames are taken into account.
%		optArgs.exclude					A list of field names that should not be taken into account.
% Outputs:
%		nameValuePair			Name value pairs in cell array
%
% Example:
% -------------------------------------------------------------------------
% s.a = 5;
% s.hallo = 1;
% misc.struct2namevaluepair(s)
% >> ans = {'a'}  {[5]}  {'hallo'}  {[1]}
% -------------------------------------------------------------------------
%
% See also struct2cell, fieldnames, misc.struct, misc.structMerge, misc.setfield, misc.getfield,
% misc.nameValuePairRemoveDots.

arguments
	s;
	optArgs.removeEmptyEntries (1,1) logical = false;
	optArgs.convertNestedStructs (1,1) logical = false;
	optArgs.fieldNames (:,1) string;
	optArgs.exclude (:,1) string;
end % arguments

names = fieldnames(s);
values = struct2cell(s);

if isfield(optArgs, "fieldNames")
	idx = string(names).matches(optArgs.fieldNames);
	names = names(idx);
	values = values(idx);
end

if isfield(optArgs, "exclude")
	idx = string(names).matches(optArgs.exclude);
	names = names(~idx);
	values = values(~idx);
end

if optArgs.convertNestedStructs
	outerLoop = 1;
	while outerLoop <= numel(values)
		if isstruct(values{outerLoop})
			nestedNameValue = ...
				misc.struct2namevaluepair(values{outerLoop}, ...
					"removeEmptyEntries", optArgs.removeEmptyEntries, ...
					"convertNestedStructs", optArgs.convertNestedStructs);
			for innerLoop = 1 : 2 : numel(nestedNameValue)
				nestedNameValue{innerLoop} = [names{outerLoop}, '.', nestedNameValue{innerLoop}];
			end % for innerLoop = 1 : 2 : numel(nestedNameValue)
			names = [names(1:outerLoop-1); nestedNameValue(1:2:end).'; names(outerLoop+1 : end)];
			values = [values(1:outerLoop-1); nestedNameValue(1+(1:2:end-1)).'; ...
				values(outerLoop+1 : end)];
			outerLoop = outerLoop + numel(nestedNameValue)/2;
		else
			outerLoop = outerLoop + 1;
		end % if isstruct(values(it))
	end
end % if optArgs.convertNestedStructs

if optArgs.removeEmptyEntries
	isEmpty = cellfun(@isempty, values);
	names = names(~isEmpty);
	values = values(~isEmpty);
end % if optArgs.removeEmptyEntries

nameValuePair = reshape({names{:}; values{:}}, 1, 2 * length(names));

end % misc.struct2namevaluepair

