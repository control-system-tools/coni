function Z = nullTol(A, tol)
%NULLTOL   Compute the null space of matrix A by considering the tolerance TOL
%   Z = nullTol(A, tol) computes an orthonormal basis for the null space of A utilizing the singular
%   value decomposition, so that A*Z becomes very small and Z'*Z = I. With TOL, a tolerance for the
%   singular values can be specified that should be taken into account for the computation of the
%   null space, i.e., all singular values below TOL will be regarded to be zero.
%
%   See also NULL, SVD, ORTH, RANK, RREF.

[m,n] = size(A);

% Orthonormal basis 
[~,S,V] = svd(A,0);
if isempty(A)
	Z = V;
else
	if m == 1
		s = S(1);
	else
		s = diag(S);  
	end
	r = sum(s > tol);
	Z = V(:,r+1:n);
end

