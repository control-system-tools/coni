function C = intersect(A, B, N)
% intersect Set intersection. In addition to built-in intersect function a tolerance can be
% specified.
%
%     C = intersect(A,B) for vectors A and B, returns the values common to
%     the two vectors with no repetitions. C will be sorted.
%
%     C = intersect(A, B, N) calls intersect(round(A, N), round(B, N)) instead
%
% See also intersect

arguments
	A;
	B;
	N = [];
end

if isempty(N)
	C = intersect(A, B);
else
	C = intersect(round(A, N), round(B, N));
end

end % intersect