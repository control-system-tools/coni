function value = ensureIsCell(value, optArg)
%ENSUREISCELL ensures that the value is a cell.
% c = ensureIsCell(value) checks if value is a cell. If it is not a cell,
% it is converted as a cell.
arguments
	value
	optArg.stringArray2cell (1,1) logical = false;
end

if ~iscell(value)
	if optArg.stringArray2cell
		if isstring(value)
			value = num2cell(value);
		else
			value = misc.ensureIsCell(value);
		end
	else
		value = {value};
	end
end
end

