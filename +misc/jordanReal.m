function [V, J, l] = jordanReal(A)
% jordanReal Real Jordan form of the matrix A
% [V, J] = jordanReal(A) computes the transformation matrix V and the jordan
% block matrix J, so that A*V = V*J. The columns of V are the generalised 
% eigenvectors.
%
% [V, J, l] = jordanReal(A) also returns the lengths of the jordan
% blocks. l is a vector containing the lengths. So length(l) is the
% number of jordan blocks.
%
% Example:
% -------------------------------------------------------------------------
% A = [ 6  -2   6   1   1;...
%       1  -1   2   1  -2;...
%      -2   0  -1   0  -1;...
%      -1   0  -2   2  -1;...
%      -4   4  -6  -2   3]
% [~, J, l] = misc.jordanReal(A)
% % yields:
% % J =
% %     1.0000         0         0         0         0
% %          0    2.0000    1.0000         0         0
% %          0   -1.0000    2.0000         0         0
% %          0         0         0    2.0000    1.0000
% %          0         0         0   -1.0000    2.0000
% % l =
% %      1
% %      2
% %      2
% -------------------------------------------------------------------------
%   See also jordan and misc.jordan_complete

% init result
V = zeros(size(A)); % transformation matrix
J = zeros(size(A)); % system matrix in block-diagonal jordan form
l = [];				% length of jordan blocks
% get eigen vectors and eigen values
[eigVectors, eigValues] = eig(A, 'vector');

% First, the jordan blocks of real eigen values are considered:
% Their eigen vectors will be the first columns of V and their jordan blocks will be
% the first in J.
selectReal = (eigValues == real(eigValues));
nReal = sum(selectReal);
eigValuesReal = eigValues(selectReal);

% add eigenvectors of real eigenvalues first
V(:, 1:nReal) = eigVectors(:, selectReal);
% create jordan blocks in J and l
k = 1; % length of current Jordan block l = [k1, k2, ... ]
I = eye(size(A)); %
for it = 1 : nReal
	if (it+1 <= nReal) && (rank([(A - eigValuesReal(it) * I) * V(:, it+1), V(:, it)]) == 1) ...
			&& ~all((A - eigValuesReal(it) * I) * V(:, it+1) == 0, 'all')
		k = k + 1;
		V(:, it) = (A - eigValuesReal(it) * I)* V(:, it+1);
	else
		l = [l; k];
		J(it+(-(k-1):0), it+(-(k-1):0)) = diag(eigValuesReal(it+(-(k-1):0))) + diag(ones(k-1, 1), 1);
		k = 1;
	end
end

% complex eigen values
eigVectorsComplex = eigVectors(:, ~selectReal);
eigValuesComplex = eigValues(~selectReal);
for it = 1 : 2 : size(eigVectorsComplex, 2)
	V(:, nReal + it) = real(eigVectorsComplex(:, it));
	V(:, nReal + it + 1) = imag(eigVectorsComplex(:, it));
	l = [l; 2];
	J(nReal + it + (0:1), nReal + it + (0:1)) = ...
		[real(eigValuesComplex(it)), imag(eigValuesComplex(it)); ...
		-imag(eigValuesComplex(it)), real(eigValuesComplex(it))];
end
end