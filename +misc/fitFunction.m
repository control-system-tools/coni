function [fitFun,gof] = fitFunction(f,rmseMax,nDisc,inverse)
% fitFunction symbolically fit function using discretisation and curve fitting
% Compute a symbolic fit to a function by discretizing and using curve fitting.
%
% Description:
% fitFun = fitFunction(f,rmseMax) discretizes the function f. The order 
% of the polynomial approximation is increased as long as the root mean 
% square error is less then rmseMax
%
% fitFun = fitFunction(f,rmseMax,nDisc) uses nDisc grid points for the 
% discretization. The argument of f is always normed to the range (0,1).
%
% fitFun = fitFunction(f,rmseMax,nDisc,inverse) computes the inverse 
% function, if inverse=1.
%
% [invFun, gof] = polyInv(...) also returs the goodness of fit
%
% Inputs:
%     f           Symbolic function
%     rmseMax     Maximal allowed root mean square error
%     nDisc       Amount of points used for discretizing f
%     inverse     Flag to determine whether inversefunction should be
%                 computed
%
% Outputs:
%     fitFun      Symbolic fit function
%     gof         Goodness of fit struct (see Matalb referance)
%
% Example:
% -------------------------------------------------------------------------
% syms z real
% fun = exp(-2*z)+log(1+z);
% f = misc.fitFunction(fun,0.0005,51)
% zdisc = 0:0.01:1;
% y = subs(f,'x',zdisc);
% plot(zdisc,y);
% -------------------------------------------------------------------------

% created on 26.09.2018 by Simon Kerschbaum

syms x y
if nargin<3
	nDisc = 51;
end
if nargin<2
	rmseMax = 1e-3;
end
if nargin<4
	inverse=0;
end

xdisc = linspace(0,1,nDisc);
vars = symvar(f);
if length(vars)==2
	[X,Y] = meshgrid(xdisc,xdisc);
	fDisc = double(subs(f,{vars(1),vars(2)},{X,Y}));
elseif length(symvar(f))==1
	fDisc = double(subs(f,xdisc));
else
	error('fitFunction only defined for functions with 1 or 2 arguments!')
end


polyOrder = 0;
rmse=inf;

str = 'poly';
attempt = 1;
while rmse>rmseMax
	polyOrder = polyOrder+1;
	if polyOrder >9 && attempt==1
		warning('Switching to fourier approximation!')
		polyOrder = 1;
		attempt=2;
		str = 'fourier';
	end
	if polyOrder > 9 && attempt==2
		warning('Cannot achieve tolerances with polynomial or fourier approximation of order 9!')
		break
	end
		
	if inverse
		if length(vars)==1
			[fO, gof] = fit(fDisc(:),xdisc(:),[str num2str(polyOrder)]);
		elseif length(vars)==2
			error('cannot invert 2D function!')
		end
	else
		if length(vars)==1
			[fO, gof] = fit(xdisc(:),fDisc(:),[str num2str(polyOrder)]);
		elseif length(vars)==2
			[XOut, YOut, ZOut] = prepareSurfaceData(X, Y, fDisc);
			[fO, gof] = fit([XOut,YOut],ZOut,[str num2str(polyOrder) num2str(polyOrder)]);
		end
	end
	rmse = gof.rmse;
end

if attempt==1 % polynomial approximation
	if length(vars)==1
		maxp = ['p' num2str(polyOrder+1)];
		fitFun = fO.(maxp);
		for j=1:polyOrder
			pnum = ['p' num2str(j)];
			fitFun = fitFun + fO.(pnum) *x^(polyOrder-j+1);
		end
	else
		fitFun = 0;
		for j=0:polyOrder
			for k=0:polyOrder
				pnum = ['p' num2str(j) num2str(k)];
				if k+j<=2 % highest for both not available 
					fitFun = fitFun + fO.(pnum)*x^j*y^k;
				end
			end
		end
	end
else % fourier approximation
	if length(vars)==1
		fitFun = fO.a0;
		for j=1:polyOrder
			anum = ['a' num2str(j)];
			bnum = ['b' num2str(j)];
			fitFun = fitFun + fO.(anum)*cos(j*x*fO.w) + fO.(bnum)*sin(j*x*fO.w)	;
		end
	else
		error('This case is not yet implemented!')
	end
end

end

