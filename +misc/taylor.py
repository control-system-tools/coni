#!/usr/bin/python
import sympy
from sympy import *
import sys

s = sympy.symbols(sys.argv[3])

N = int(sys.argv[1])
w = eval(sys.argv[2])
taylorTermsIterator = w.series(s, x0=0, n=None)
taylorTerms = []

for i in range(N):
    taylorTerms.append( next(taylorTermsIterator).subs(s, 1) )
    
sys.stdout.write(sympy.octave_code(taylorTerms))
sys.stdout.flush()