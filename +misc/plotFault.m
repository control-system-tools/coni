function [parameters, plt] = plotFault(k, time, faultModel, f_hat, threshold, optArg)
arguments
	k double;
	time quantity.Domain;
	faultModel signals.SignalModel;
	f_hat double;
	threshold double;
	optArg.T double = 0;
	optArg.f_hat_i;
	optArg.uncertainFault;
end

if any(abs(f_hat(time.grid < faultModel(k).occurrence, k)) >= threshold(k))
	warning('threshold exceeded without fault');
end

tDindx = find( time.grid > faultModel(k).occurrence & abs( f_hat(:,k) ) > threshold(k), 1);
tDTindx = find( time.grid > faultModel(k).occurrence + optArg.T & abs( f_hat(:,k) ) > threshold(k), 1);


parameters.t_Detection = time.grid(tDindx);
parameters.t_DetectionT = time.grid(tDTindx);
parameters.threshold = threshold(k);
parameters.occurrence = faultModel(k).occurrence;

plt.t = time.grid;
plt.bound_lower = zeros(time.n, 1) - threshold(k);
plt.bound_upper = zeros(time.n, 1) + threshold(k);
plt.f_hat = f_hat(:,k);
plt.f_hat_bound_upper = f_hat(:,k) + threshold(k);
plt.f_hat_bound_lower = f_hat(:,k) - threshold(k);

hold on;
h.fhat = plot(time.grid, f_hat(:,k), 'r', 'LineWidth', 2);
if isfield(optArg, "uncertainFault")
	h.f = plot(time.grid, optArg.uncertainFault(k).on(time), 'b--', 'LineWidth', 2);
else
	h.f = plot(time.grid, faultModel(k).on(time), 'b--', 'LineWidth', 2);
end
h.bounds = plot(time.grid, plt.f_hat_bound_upper, 'g:', 'LineWidth', 2);
plot(time.grid, plt.f_hat_bound_lower, 'g:', 'LineWidth', 2);
h.threshold = plot(time.grid, plt.bound_lower, 'k-.', 'LineWidth', 1);
plot(time.grid, plt.bound_upper, 'k-.', 'LineWidth', 1);

plot(time.grid(tDTindx), f_hat(tDTindx, k), 'xb', 'LineWidth', 3, 'MarkerSize', 15)
plot(time.grid(tDindx), f_hat(tDindx, k), 'xr', 'LineWidth', 3, 'MarkerSize', 15)

if isfield(optArg, 'T')
	T_indx = find( time.grid > optArg.T + faultModel(k).occurrence, 1 );
	plot( time.grid(T_indx), f_hat(T_indx, k), 'or', 'LineWidth', 3, 'MarkerSize', 15)
end

legend([h.fhat, h.f, h.threshold, h.bounds], "$\hat{f}$", "$f$", "$f_B$", "$f_{B}^{\pm}$",...
	"Orientation", "horizontal", "interpreter", "latex")

end