function J = unitSelector(dim, i)
%UNITSELECTOR - A row/column selection matrix
%	J = unitSelector(dim, i) returns the i-th unit selection matrix of size DIM, i.e., 
% J(i,i) = 1, otherwise = 0
arguments
	dim (1,1) double {mustBeInteger},
	i (1,:) double, 
end

J = zeros(dim);

for k = i
	J(k,k) = 1;
end

end

