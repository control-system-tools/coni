function [p] = defaultParser()
% DEFAULTPARSER Initializes an inputParser Object with the default
% parameters
%   
% Description:
% p = defaultParser() 
% Initializes an inputParser object with the properties KeepUnmatched = true,
% CaseSensitive = true, PartialMatching = false.
%
% Outputs:
%     p       Input parser object
%
% Example:
% -------------------------------------------------------------------------
% p = defaultParser();
% -------------------------------------------------------------------------
	warning('DEPRECATED WILL BE REMOVED! Use misc.Parser instead.')
	p = inputParser();
	p.KeepUnmatched = true;
	p.CaseSensitive = true;
	p.PartialMatching = false;

end

