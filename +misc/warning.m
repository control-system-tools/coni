function [varargout] = warning( condition, varargin)

if ~condition
	varargout{:} = warning(varargin{:});
end

end

