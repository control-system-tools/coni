function [newvarargin, value] = removeArg(fnames, varargin)
% REMOVEARG removes all name-value-pairs that are named in fnames from the
% name-value-pair-list in varargin. The output newvarargin contains the
% input varargin without the removed name-value-pairs. The output value
% contains the values of the name-value-pairs in fnames.
%
% Inputs:
%     fnames          Names to be removed
%     varargin        Name value list to be corrupted
% Outputs:
%     newvarargin     Courrped name-value pairs
%     value           Values of names in fnames
%
% Example:
% -------------------------------------------------------------------------
% fnames = {'bli', 'bla', 'not_included'};
% nameValuePairs = {'bli', 1, 'bla', 2, 'blub', 3};
% [remaining_nameValuePairs, value] = misc.removeArg(fnames, nameValuePairs{:})
% >> remaining_nameValuePairs = {'blub'}  {[3]}
% >> value = {[1]}  {[2]}  {0�0 double}
% -------------------------------------------------------------------------
s = struct(varargin{:});

if ~iscell(fnames)
    cellinput = false;
    fnames = {fnames};
else
    cellinput = true;
end

for k = 1:length(fnames)
    if isfield(s, fnames{k})
       value{k} = s.(fnames{k});
       s = rmfield(s, fnames{k});
    else
        value{k} = [];
    end
end

if isempty(varargin) 
    newvarargin = {};
else
    newvarargin = misc.struct2namevaluepair(s);
end

if ~cellinput
    value = value{1};
end
end