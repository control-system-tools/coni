function h = hash(data)
%HASH compute the hash for some data
%	h = hash(DATA) computes the SHA-1 hash for DATA

	B = getByteStreamFromArray(data);
	Engine = java.security.MessageDigest.getInstance('SHA-1');
	Engine.update(B);

	h = reshape( dec2hex(typecast(Engine.digest(), 'uint8')), 1, []);
end

