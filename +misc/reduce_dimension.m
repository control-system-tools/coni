function red = reduce_dimension(in)
% REDUCE_DIMENSION reduce the dimensions of in to neglect leading singleton
% dimensions.
%
% Needed for example to do a matrix multiplication.
% In contrast to squeeze, the dimension order is logically preserved.
%
% Inputs:
%     in      Input array
% Outputs:
%     red     Reduced array
%
% Example:
% -------------------------------------------------------------------------
% A = ones(2,2,2);
% A_squeeze = squeeze(A(1,:,1))
% >> ans =  [1 1]
% A_red = reduce_dimension(A(1,:,1))
% >> ans = [1
%           1]
% -------------------------------------------------------------------------

% The problem with squeeze is that if there is only a single dimension,
% then this is equivalent to a row vector, which is an 1xn matrix. That
% means that the second dimension is of length n and not the first.

% Created by Simon Kerschbaum on 12.09.2016

red =in;
dim = size(red);
numsing = 0;
if any(dim~=1)
	for i  = 1:length(dim)
		if dim(i) == 1
			numsing = numsing+1;
		else
			break;
		end
	end
	if numel(dim(numsing+1:end))<2
		red = reshape(red,[dim(numsing+1:end) 1]);
	else
		red = reshape(red,dim(numsing+1:end));
	end
end

end

