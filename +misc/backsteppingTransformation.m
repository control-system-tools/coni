function xOut = backsteppingTransformation(xIn, backsteppingKernel, signOfIntegralTerm)
%BACKSTEPPINGTRANSFORMATION calculates the backstepping-transform of x_in
%	x_out(z) = x_in(z) + signOfIntegralTerm ...
%						* int_0^z backsteppingKernel(z, zeta) x_in(zeta) dzeta
% backsteppingKernel is sampled to spatial resolution of xIn.
%
% INPUT PARAMETERS:
% 	DOUBLE-ARRAY			  x_in : value to be transformed of format 
%									(spatialGrid, index, optional index)
% 	DOUBLE-ARRAY	backsteppingKernel : kernel of transformation of format
%									 (z-grid, zeta-grid, index, index)
%	INTEGER		signOfIntegralTerm : sign before integral, either +1 or -1
%
% OUTPUT PARAMETERS:
% 	DOUBLE-ARRAY			 x_out : transformed value of format 
%									(spatialGrid, index, optional index)

	%% Init and input check
	[z.n, n, columns] = size(xIn, [1, 2, 3]);
	z.grid = linspace(0, 1, z.n);
	z.gridKernel = linspace(0, 1, size(backsteppingKernel, 1));
	if any(isnan(backsteppingKernel))
		error('nan in kernel')
	end
	
	if isequal(size(backsteppingKernel, [1, 2]), [z.n, z.n])
		kernelSampled = backsteppingKernel;
	else
		kernelInterpolant = numeric.interpolant(...
			{z.gridKernel, z.gridKernel, 1:n, 1:n}, ...
			backsteppingKernel, 'spline');
		kernelSampled = kernelInterpolant.evaluate(z.grid, z.grid, 1:n, 1:n);
	end

	integral = zeros(size(xIn));
	for zIdx = 2:z.n
		integrand = zeros(z.n, n, columns);
		for cIdx = 1:columns
			for it = 1:n 
				integrand(:, it, cIdx) = ...
					sum(reshape(kernelSampled(zIdx, :, it, :), [z.n, n]) ...
													.* xIn(:, :, cIdx), 2);
			end
		end
		integral(zIdx, :, :) = reshape(numeric.trapz_fast_nDim(...
			z.grid(1:zIdx), integrand(1:zIdx, :, :), 1), [n, cIdx]);
	end
	
	xOut = xIn + signOfIntegralTerm * integral;
	
end % backsteppingTransformation