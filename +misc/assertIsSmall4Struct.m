function assertIsSmall4Struct(myStruct, varargin)
% misc.assertIsSmall4Struct applies the lessThen-Tolerance-assertion ("<") 
% to every field of a struct. If the field is numeric, then the absolute-max is
% evaluated. This method also supports quantity.Discrete-objects
%% Example:
%	myStruct.a = 0;
%	myStruct.b = [0, 0, 0; 0, 0, 1e-6];
%	myStruct.c = [0, 0, 0; 0, 0, -1];
%	misc.assertIsSmall4Struct(myStruct, 'AbsTol', 1e-5, 'structName', 'Dieter')

% read input
myParser = misc.Parser();
myParser.addRequired('myStruct', @(v) isstruct(v));
myParser.addParameter('AbsTol', 10*eps, @(v) isnumeric(v) & (numel(v) == 1));
myParser.addParameter('structName', "", @(v) ischar(v));
myParser.parse(myStruct, varargin{:});
myFieldNames = fieldnames(myStruct);

for it = 1 : numel(myFieldNames)
	% assert lessThan for every field of myStruct
	myField = myStruct.(myFieldNames{it});
	if isstruct(myField)
		misc.assertIsSmall4Struct(myField, 'AbsTol', myParser.Results.AbsTol, ...
			'structName', [myParser.Results.structName, '.', myFieldNames{it}]);
	else
		if isa(myField, 'quantity.Discrete')
			myField = MAX(abs(myStruct.(myFieldNames{it})));
		end % get numeric value

		myMax = max(abs(myField(:)));
		assert(myMax < myParser.Results.AbsTol, ...
			"max |" + myParser.Results.structName + "." + myFieldNames{it} + "| = " + ...
			num2str(myMax) + " >= " + num2str(myParser.Results.AbsTol));
	end
end % for it = 1 : numel(myFieldNames)

end % assertLessThan4Struct()