function rgb = color(idx, NameValue)
% misc.color returns a M x 3 color array which simplifies the specification of colors in plots. 
%
%	rgb = misc.color(idx) with a non-negative scalar integer idx returns the 1 x 3 vector rgb, which
%		specifies the idx-th color of the colormap lines, which is the standard matlab colormap for
%		the plot function.
%
%	rgb = misc.color(idx) with a integer vector idx returns the numel(idx) x 3 array rgb, in which
%		each row specifies the related idx-th color of the colormap lines.
%
%	rgb = misc.color([...], "colormap", colormap) uses the colormap specified by the string-input
%		colormap instead of lines. See doc colormap for suitable strings.
%		Default value: "lines".
%
%	rgb = misc.color([...], "span", span) the scalar integer input argument span sets the maximum
%		value of idx to the value span. Moreover, the modulus is applied to idx, i.e., 
%			idx = mod(idx-1, span)+1,
%		so that if idx values larger than span are used, a cyclic color scheme is used.
%		Default value: 768.
%
% Example:
%	figure(); 
%	for idx = 1 : 12
%		subplot(2, 1, 1); plot([0, 1], idx*[1, 1], ...
%			"Color", misc.color(idx, "span", 5)); hold on;
%		subplot(2, 1, 2); plot([0, 1], idx*[1, 1], ...
%			"Color", misc.color(idx, "colormap", "parula", "span", 5)); hold on;
%	end
%
%	For using the colormap "viridis", the external file "viridis.m" from MatPlotLib is required, see:
%	Stephen (2022). MatPlotLib Perceptually Uniform Colormaps
%	("https://www.mathworks.com/matlabcentral/fileexchange/62729-matplotlib-perceptually-uniform-colormaps"),
%	MATLAB Central File Exchange. Retrieved March 18, 2022.
%
% See also colormap, lines, parula.

arguments
	idx (:, 1) double;
	NameValue.colormap (1, 1) string = "lines"
	NameValue.span (1, 1) double = 768;
	NameValue.warn (1, 1) logical = false;
end

if strcmp(NameValue.colormap, "viridis") && ~exist("viridis.m", "file")
	NameValue.colormap = "viridis";
	if NameValue.warn
		warning("For using the colormap 'viridis', the external file 'viridis.m' from MatPlotLib " ...
			+ "is required, see: Stephen (2022). MatPlotLib Perceptually Uniform Colormaps" ...
			+ "(https://www.mathworks.com/matlabcentral/fileexchange/62729-matplotlib-perceptually-uniform-colormaps)," ...
			+ " MATLAB Central File Exchange. Retrieved March 18, 2022." ...
			+ "Since viridis.m is not found, the colormap 'winter' is used.");
	end
	NameValue.colormap = "winter";
else
	validColormaps = ["viridis", "parula", "jet", "hsv", "hot", "cool", "spring", "summer", ...
			"autumn", "winter", "gray", "bone", "copper", "pink", ...
			"lines", "colorcube", "prism", "flag", "white"];
	assert(any(strcmp(NameValue.colormap, validColormaps)), ...
			"The colormap '" + NameValue.colormap + "' is not supported. Instead, use one of: " ...
			+ strjoin(validColormaps, ", ") + ".")
end

idx = mod(idx-1, NameValue.span)+1;
allColors = eval(NameValue.colormap + "(" + string(NameValue.span) + ")");
rgb = allColors(idx, :);

end % color()