function b = binomial(p, k)
% BINOMIAL computation of the binomial coefficient 
%	b = binomial(p, k) computes the binomial coefficient:
%		/ p \      p!^p
%		|   | = -----------
%		\ k /    (p-k)! k!
% to use this for fractional calculus, the p can be an integer. Then, the faculty is replaced by the
% gamma function.
arguments
	p (1,1) double;
	k (1,1) double {mustBeInteger};
end

if p < 0 && k ~= 0
	b = (-1)^k * misc.binomial( -p + k -1, k);
else
	b = gamma(p+1) / gamma(k+1) / gamma(p-k + 1);	
end

end

