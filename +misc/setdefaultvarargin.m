function [varargins] = setdefaultvarargin(varargins, name, value)
% SETDEFAULTVARARGIN Adds a name value pair to varargins
%
% [varargins] = setdefaultvarargin(...)
% Adds a new name value pair to varargins, doesn�t update values on
% existing name value pairs in varargins
%
% Inputs:
%     varargins       Cell array containing name value pairs
%     name            Name of new name value pair
%     value           Value of new name value pair
% Outputs:
%     varargins       New list of name value pairs
%   
% Example:
% -------------------------------------------------------------------------
% arg = {'hallo',4,'bla',3,'blub',10};
% misc.setdefaultvarargin(arg,'drei',10)
% >> ans = {'hallo'} {[4]} {'bla'} {[3]} {'blub'} {[10]} {'drei'} {[10]}
% -------------------------------------------------------------------------

inputs = struct(varargins{:});
if ~isfield(inputs, name)
   varargins = {varargins{:}, name, value};
end

end

