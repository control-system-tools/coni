function newObj = constructorOf(oldObj, varargin)
% misc.constructorOf runs the constructor specified by its name with the string input oldObj or by
% the class of oldObj. Moreover, the inputs for the constructor are specified by varargin.
%
% Example:
%		oldObj = quantity.Discrete(linspace(0, 1, 11).', ...
%				quantity.Domain("z", linspace(0, 1, 11)), "name", "Berthold");
%		newObj = misc.constructorOf(oldObj, ...
%				2*linspace(0, 1, 11).', oldObj.domain, "name", "Lars");
%		newObjAlternative = misc.constructorOf("quantity.Discrete", ...
%				2*linspace(0, 1, 11).', oldObj.domain, "name", "Marga");
%
% See also eval

if isa(oldObj, "string")
	className = oldObj;
else
	className = class(oldObj);
end
newObj = eval(className + "(varargin{:})");
end % misc.constructorOf()