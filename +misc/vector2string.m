function s = vector2string(v, optArg)
%vector2string
% s = vector2string(v, optArg)
%
% see also mat2str
arguments
	v double;
	optArg.separator string = " ";
	optArg.formatSpec = "%g",
	optArg.format,
end
s = "[";
for k = 1:length(v)-1
	s = s + sprintf(optArg.formatSpec, v(k) ) + optArg.separator;
end
s = s + sprintf(optArg.formatSpec, v(k+1) ) + "]";

if iscolumn( v )
	s = s + "^T";
elseif isrow(v)
	% do nothing, this is required to avoid that row vectors are considered as matrices...
elseif ismatrix( v )
	error("Not yet implemented")
end

if isfield( optArg, "format") && optArg.format == "latex"
	s = regexprep(s, "e([+-]\d+)", "\\times 10^{$1}");
elseif isfield( optArg, "format")
	error("Unknown required format: " + optArg.format);
end

end

