function [ x_i ] = inv_fun(y,x,y_i,return_index)
% INV_FUN compute the inverse function to a function y(x), which is:
% find the values x_i belonging to a function value vector y_i, where the
% function is y(x).
%
% Description:
% INV_FUN(y,x,y_i) gives the values x_i belonging to the function values
% y_i, for the function y(x)
%
% INV_FUN(y,x,y_i,true) returns the indices belonging to x_i in x
% instead of the function values
%
% Inputs:
%     y               Discretized function y(x)
%     x               Values where y is evaluated
%     y_i             Values of y(x)
%     return_index    Enable index returning
% Outputs:
%     x_i             Corresponding x values to y_i
%
% Example:
% -------------------------------------------------------------------------
% y = [0 1 4 9 16 25 36 49];
% x = [0 1 2 3 4 5 6 7];
% y_i = [9 36];
% inv_fun(y,x,y_i)
% >> ans = [3 6]
% -------------------------------------------------------------------------
%


%  created by Simon Kerschbaum on 03.04.2017
%  modified on 12.04.2017 by SK:
%    * enable index returning

import misc.*

if length(y)~= length(x)
	error('length(y) must equal length(x)!')
end
if nargin<4
	return_index=false;
end

[~, I] = sort(x);
y_srt = y(I);
y_srt = y_srt(:); % Spaltenvektor erzwingen

if ~mono_increase(y_srt) && ~mono_increase(flipud(y_srt))
	warning(['the given function y(x) is not monotonically increasing or decreasing! ',...
		     'The result will be the first argument value x_i which fulfills y(x_i) = y_i']);
end

x_i(1,length(y_i)) = 0; %preallocate
for y_i_idx = 1:length(y_i)
	if return_index
		x_i(1,y_i_idx) = get_idx(y_i(y_i_idx),y);
	else
		x_i(1,y_i_idx) = x(get_idx(y_i(y_i_idx),y));
	end
end

end

