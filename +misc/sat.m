function y_sat = sat( y,y_min,y_max )
% SAT saturize input function
%
% Description:
% y_sat = SAT(y) saturizes the input y between the values 0 and 1. This
% is y_sat = max(0,min(y,1)).
%
% y_sat = SAT(y,y_min,y_max) saturized an input y to the lower and upper
% bounds y_min resp. y_max.
%
% y can be scalar, vector, matrix or an array of arbitrary dimension and 
% is saturized element-wise.
%
% y_min and y_max must be scalars or arrays of same dimension as y.
%
% Inputs:
%     y       "Signal" of any kind
%     y_min   Lower saturation bound
%     y_max   Upper saturation bound
% Outputs:
%     y_sat   Saturized "signal"
%
% Example:
% -------------------------------------------------------------------------
% x = -3:0.1:3;
% y = normpdf(x,0,1);
% y_sat = misc.sat(y,0.025,0.35);
% plot(x,y,'-.',x,y_sat);
% -------------------------------------------------------------------------


% created on 22.01.2018 by Simon Kerschbaum

if nargin == 1
	y_min = 0;
	y_max = 1;
elseif nargin == 2
	y_max = 1;
end

if numel(y_min)>1
	if ~isequal(size(y_min),size(y)) 
		error('y_min must be a scalar value or of same dimension as y!')
	end
end
if  numel(y_max)>1
	if ~isequal(size(y_max),size(y))
		error('y_max must be a scalar value or of same dimension as y!')
	end
end

% get array dimensions
if numel(y_min)==1
	y_min = y_min*ones(size(y));
end
if numel(y_max)==1
	y_max = y_max*ones(size(y));
end
y_sat = 0*y; % preallocate, get same dimension
for lin_idx = 1:length(y(:)) % use linear indexing -> no need to care about dimensions
		y_sat(lin_idx) = max(min(y(lin_idx),y_max(lin_idx)),y_min(lin_idx));
end

end

