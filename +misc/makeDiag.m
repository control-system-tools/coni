function outArray = makeDiag(inputArray,dim)
%makeDiag Make a dimension of an array to a diagonal
%   outArray = makeDiag(inputArray,dimension) takes the elements in the dimension and creates a
%   "diagonal matrix" of it. That is, another dimension with the same length is inserted after
%   it. Regarding those two dimensions, the result is diagonal.
%   Specifically: outArray(...,i,i,...) = inputArray(...,i,...)
%   ATTENTION: If used on a row vector, the result will be a 1xnxn array! It has the expected
%   behaviour for a column vector.
%

% created on 04.06.2019 by Simon Kerschbaum
if nargin<2
	dim=1;
end
sizIn = size(inputArray);
% outArraySize = [sizIn(1:dim) sizIn(dim) sizIn(dim+1:end)];
% outArraySizePerm = [sizIn(dim) sizIn(dim)]
inputPerm = permute(inputArray,[dim 1:dim-1 dim+1:length(sizIn)]); % permute desired to first
inputResh = reshape(inputPerm,sizIn(dim),[]); % collapse further dimensions
outArrayResh = zeros(sizIn(dim),sizIn(dim),size(inputResh,2)); % preallocate
for i=1:sizIn(dim)
	outArrayResh(i,i,:) = inputResh(i,:);
end
outArrayPerm = reshape(outArrayResh,[sizIn(dim),sizIn(dim),sizIn(1:dim-1) sizIn(dim+1:end)]);
outArray = permute(outArrayPerm,[3:dim+1,1,2,dim+2:dim+2+length(sizIn)-dim]);

