function [ z_plot, t_plot, w_plot ] = isurf( z, t, w, varargin )
% ISURF 3-D colored surface with fixed mesh size and default axis
% description
%
% Description:
% [ z_plot, t_plot, w_plot ] = ISURF( z, t, w, N )
% plots the data W over the axis z and t. z and t have to be vectors. 
% w has to be a matrix. 
%
% Inputs:
%     z       Axis in z direction
%     t       Axis in t direction
%     w       Function over (z,t)
%     N       Number of grid points in one direction
% Outputs:
%     *_plot  Variables containing the actually ploted values
%
%
% Example:
% -------------------------------------------------------------------------
% x = 0:0.05:10;
% y = 0:0.05:10;
% [X,Y] = meshgrid(x,y);
% w = sin(X) + cos(Y);
% misc.isurf(w);
%
% % OR
% syms z t w
% w = sin(10*z) + cos(10*t);
% misc.isurf(z,t,w,30);
% -------------------------------------------------------------------------

ip = misc.Parser();
ip.addParameter('xlabel', 'z');
ip.addParameter('ylabel', 'y');
ip.addParameter('zlabel', '');
ip.addParameter('Nz', 60);
ip.addParameter('Nt', 60);
ip.parse(varargin{:});

Nz = ip.Results.Nz;
Nt = ip.Results.Nt;

if nargin == 1
    w = squeeze(z);
    z = linspace(0, 1, size(w,1));
    t = linspace(0, 1, size(w,2));
end

w=squeeze(w);


if nargin==1
   t=linspace(1,size(w,2),Nz);
   z=linspace(1,size(w,1),Nz);
   [t_grid, z_grid]=meshgrid(t, z);
    w_plot=interp2(w, t_grid, z_grid);
   
elseif isa(w,'sym') && isa(z,'sym') && isa(t, 'sym')
    %syms tmp
    t_plot=linspace(0, 1, Nt);
    z_plot=linspace(0, 1, Nz);
    [t_grid, z_grid]=meshgrid(t_plot, z_plot);
    w_plot=double(subs(w, {z,t}, {z_grid, t_grid}));
else
% 	
	idx_t = ceil(linspace(1, length(t), min(Nt, length(t))));
	idx_z = ceil(linspace(1, length(z), min(Nz, length(z))));
	
    t_plot = t(idx_t);
    z_plot = z(idx_z);
    [t_grid, z_grid]=meshgrid(t_plot, z_plot);

	w_plot = w(idx_z, idx_t);
end

if isvector(w_plot)
	
	if numel(t) >= numel(z)
		myGrid = t_grid;
	else
		myGrid = z_grid;
	end
	
	plot(myGrid, w_plot); grid on; box on;
	
else
	surf(z_grid, t_grid, real(w_plot), 'Linestyle', 'none'); grid on; box on;
		xlabel(ip.Results.xlabel);
		ylabel(ip.Results.ylabel);
		zlabel(ip.Results.zlabel);
		view(56,28);
end