classdef ddd < export.Data
	% DDD export 3D data
	% obj = ddd(varargin) initialization of an object for the export of 3d data to plot them
	% with latex/pgfplots
	% The data and properties for the export are specififed by the name-value-pairs:
	%	"x": the data for the x-axis
	%	"y": the data for the y-axis
	%	"z": the data for the z-axis
	%	"xRange": the range of the x-coordinate that should be used for the export
	%	"yRange": the range of the y-coordinate that should be used for the export
	%	"N": a struct with the fields "x" and "y". These specify the number of points that
	%	should be considered in the export.
	%	for the remaining plot options see
	% see also export.Data, export.Data.Data, export.dd
	properties
		x;
		y;
		z;
		xRange;
		yRange;
	end
	
	properties(Dependent)
		out;
	end
	
	methods
		
		function obj = ddd(x, y, z, optArgs)
			arguments
				x (:,1) double;
				y (:,1) double;
				z (:,:) double;
				optArgs.xRange (1,2) double = [x(1), x(end)];
				optArgs.yRange (1,2) double = [y(1), y(end)];
				optArgs.N;
				optArgs.basepath;
				optArgs.foldername;
				optArgs.filename;
			end
			varArgIn = namedargs2cell(optArgs);
			
			obj@export.Data(varArgIn{:});
			
			obj.x = x;
			obj.y = y;
			obj.z = z;
			
			obj.xRange = optArgs.xRange;
			obj.yRange = optArgs.yRange;
			
			if isnumeric( obj.N ) || isequal(obj.N, struct())
				obj.N =  struct('x', obj.N, 'y', obj.N);
			end
		end
		
		
		function set.z(obj, z)
			obj.z = squeeze(z);
		end
		
		function out = get.out(obj)
			o = obj.interpolateData();
			out = [o.x(:), o.y(:), o.z(:)];
		end
		
		function o = interpolateData(obj)
			assert(isfield(obj.N, "x"), "N must be a structure with the field x")
			assert(isfield(obj.N, "y"), "N must be a structure with the field y")
			o.x = linspace(obj.xRange(1), obj.xRange(2), obj.N.x);
			o.y = linspace(obj.yRange(1), obj.yRange(2), obj.N.y);
			[o.x, o.y] = meshgrid(o.x, o.y);
			[grid.x, grid.y] = meshgrid(obj.x, obj.y);
			o.z = interp2(grid.x, grid.y, obj.z', o.x, o.y);
		end
		
		function i = isempty(obj)
			i = isempty(obj.x) || isempty(obj.y) || isempty(obj.z);
		end
		
		
		function plot(obj)
			o = obj.interpolateData();
			surf(o.x, o.y, o.z)
		end
		
	end
	
	methods( Access = protected)
		function innerexport(obj)
			o = obj.out;
			save(obj.path, 'o',  '-ascii', '-tabs')
		end
	end
	
	
end

