
%% PLOT
t = (0:0.2:10)';            % Darstellung als Spaltenvektor ist wichtig!
y1 = sin(t);
y2 = cos(t);

basepath = [pwd filesep '+export'];

%% export data values

data.nameValues = export.namevalues(["a", "b"], {1 2}, "filename", "parameters", "basepath", basepath );

%% export line-plots
data.plot = export.dd(...
	'M', [t, y1, y2], ...
	'header', {'t', 'y1', 'y2'}, ...
	'filename', 'plot', ...
	'basepath', basepath ...
	);

% to export data.plot use: data.plot.export()

%% export surf-plots
z = (0:20)';

[X, Y] = meshgrid(t , z);
w = sin(X) + cos(Y);
surf(X,Y,w)

data.surf = export.ddd(...
	z, t, w, ...
	'filename', 'surf', ...
	'basepath', basepath, ...
	'N', 60);

export.Data.exportAll(data);

disp(['call "pdflatex !example.tex" in the path "' basepath '" to build the pdf file']);