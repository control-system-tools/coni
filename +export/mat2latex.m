function s = mat2latex(v, path, optArgs)
% MAT2LATEX export a matrix into a tex file
% s = mat2latex(v, path, optArgs) converts the matrix v into tex format and export this text into
% the file specified by path. With the optional arguments the output can be modified:
%	separator: define the spearator of the columns in the matrix. Default is &
%	linebreak: define the linebreak for the rows. Default is \\
%	formatSpec: define the format in the fprintf specification. Default is %.3g
%	matrixEnvironment: define the environment that should be used. Default is bmatrix. If it is set
%	to "", then no tex-environment is used.
%	verbose: if true, then the export path is printed.
arguments
	v double;
	path string;
	optArgs.separator string = "&";
	optArgs.linebreak string = "\\";
	optArgs.formatSpec = "%.3g";
	optArgs.matrixEnvironment = "bmatrix"
	optArgs.verbose (1,1) logical = true;
end

if optArgs.matrixEnvironment == ""
	s = "";
else
	s = "\begin{" + optArgs.matrixEnvironment + "}";
end

for k = 1:size(v,1)
	for l = 1:size(v,2)
		s = s + sprintf(optArgs.formatSpec, v(k,l) );
		if l < size(v,2)
			s = s + optArgs.separator;
		end
	end
	if k < size(v,1)
		s = s + optArgs.linebreak;
	end
end
if optArgs.matrixEnvironment ~= ""
	s = s + "\end{" + optArgs.matrixEnvironment + "}";
end

s = regexprep(s, "e([-]\d+)", "\\times 10^{$1}");
s = regexprep(s, "e[+](\d+)", "\\times 10^{$1}");
s = regexprep(s, "{-0(\d+)}", "{-$1}");

fid = fopen(path,'w');
fprintf(fid,'%s',s);
fclose(fid);

if optArgs.verbose
	fprintf('export: %s \n', path);
end

end

