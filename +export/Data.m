classdef (Abstract) Data < handle & matlab.mixin.Heterogeneous
    %EXPORTDATA super class for export of numeric data to csv files
	% see also export.Data.Data
    
    properties
        basepath char = '.';
        foldername char = '.';
        filename char = 'data';
        N = 201;
    end
        
    methods
        
        function obj = Data(varargin)
			% DATA constructor for the export.Data object
			% obj = Data(varargin) initializes an object to export some data to create
			% latex/pgfplots figures
			% The path for the exported *.dat file can be specified by
			%	"basepath"
			%	"foldername"
			%	"filename"
			% the bath is build to <basepath>/<foldername>/<filename>.dat
			% With the name-value-pair "N" the number of points considered for the export can be
			% specified. 
			
           for arg=1:2:length(varargin)
               obj.(varargin{arg}) = varargin{arg + 1}; 
           end
        end
        
        function p = path(obj, k)
            if ~exist('k', 'var')
                k = '';
            end
            
           p = [obj.absolutepath filesep obj.addExtension(k)]; 
        end
                
        function p = absolutepath(obj)
            p = [obj.basepath filesep obj.foldername];
        end
        
        function f = addExtension(obj, k)
            
            if ~exist('k', 'var')
                name = obj.filename;
            elseif ischar(k)
                name = [obj.filename k];
            elseif isnumeric(k)
                name = [obj.filename num2str(k)];
            end
            
            if isempty(regexpi(obj.filename, '\w*\.(dat)$'))
                f = [name '.dat'];
            else
                error("filename must not have a data type ending");
            end                        
        end
        
    end
    
	methods(Sealed)
		function export(obj, NameValue)
			arguments
				obj;
				NameValue.silent (1, 1) logical = false;
			end
			for k = 1:length(obj)
				if ~obj(k).isempty
					
					if ~isfolder(obj(k).absolutepath)
						mkdir(obj(k).absolutepath);
					end
					
					obj(k).innerexport();
					
					if ~NameValue.silent
						fprintf(['export: %s \n'], obj(k).path);
					end
					
				else
					warning(['Object ' num2str(k) ' is empty']);
				end
			end % k = 1:length(obj)
		end % function export(obj, NameValue)
	end % methods(Sealed)
   
    methods (Abstract, Access = protected)
       innerexport(obj); 
    end
    
    methods (Static)
		function exportAll(exp_struct, varargin)
           structfun(@(x)export(x, varargin{:}), exp_struct);
		end
		function s = numericStruct(parametersStruct, optArgs )
			arguments 
				parametersStruct struct;
				optArgs.parameters = struct();
			end
			s = optArgs.parameters;
			
			for name = fieldnames(parametersStruct)'
				if isnumeric(parametersStruct.(name{:}))
					
					if isfield(s, name{:})
						warning("The value in the field " + name{:} + " is overwritten");
					end
					
					s.(name{:}) = parametersStruct.(name{:});
				end
			end
			
		end
    end
end

