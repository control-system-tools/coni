function [export] = catQuantities(quantities, optArgs)
% CATQUANTITIES concatenation of quantities for the export
% [export] = catQuantities(quantities, optArgs) generates a export.dd object for the export of
% quantities. At this, the header names are generated from the quantities indexed names. 
% This special function is required, since the getIndexedName is used to generate the headers for
% the export, but these numbers would be ongoing. To avoid this, the quantities are specified in a
% cell, so that the groups are maintained.
arguments
	quantities cell;
	optArgs.filename (1,1) string = "export";
	optArgs.N (1,1) {mustBeInteger} = int32(201);
	optArgs.basepath (1,1) string = "";
	optArgs.foldername (1,1) string = ".";
	optArgs.jumps;
end

headerCell = cellfun(@(q) q.getIndexedName, quantities, 'UniformOutput', false);

header(1) = quantities{1}(1).domain.name;
data = [];
for k = 1:length( headerCell)
	tmpHeader = headerCell{k};
	header = [header; tmpHeader(:)];
	
	tmpData = quantities{k};
	data = [data; tmpData(:)];	
end

oArgs = namedargs2cell(optArgs);
export = exportData( data(:), "header", header, oArgs{:});

end

