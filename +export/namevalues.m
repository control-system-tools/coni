classdef namevalues < export.Data
	% NAMEVALUES export of data values
	%	This class provides a handler to export data values to a name-value-pair list in a file.
	%	This file can be used, e.g., to import the data values in a tex-file. The export data is
	%	specified via name-value-pairs: "vars" and "values". The name-value-pair separator can be
	%	specified by the optional name-value-pair argument "separator".
	%
	%   example:
	% --------------------------------------
	%   data = export.namevalues('vars', {'key1', 'key2', 'key3'}, 'values',
	%           [1, 2, 3]);
	%   keys.export();
	%
	%	see also export.namevalues.struct2namevalues, export.Data
	
	properties
		vars;
		values;
		separator = '=';
	end
	
	properties (Dependent)
		Tab;
	end
	
	methods
		
		function obj = namevalues(vars, values, optArgs)
			arguments
				vars (:,1) string;
				values (:,1) cell;
				optArgs.separator string = "=";
				optArgs.basepath char = '.';
				optArgs.foldername char = '.';
				optArgs.filename char = 'data';
			end
			
			obj@export.Data("basepath", optArgs.basepath, "foldername", optArgs.foldername, ...
				"filename", optArgs.filename);
			
			obj.vars = vars;
			obj.values = values;
			obj.separator = optArgs.separator; 
			
		end
		
		function T = get.Tab(obj)
			% make "" around each name:
			for k = 1:length(obj.vars)
				exportVars{k} = '"' + obj.vars(k) + '"';
			end
			
			seps = repmat({obj.separator}, size(obj.vars));
			
			T = table(exportVars(:), seps(:), obj.values(:));
		end
		
		function is = isempty(obj)
			is = isempty(obj.vars) || isempty(obj.values);
		end
		
		function addnamevalue(obj, name, value)
			obj.vars(end+1) = name;
			obj.values(end+1) = value;
		end
		
	end
	
	methods ( Access = protected)
		function innerexport(obj)
			if ~exist( "isMATLABReleaseOlderThan", 'file' ) || isMATLABReleaseOlderThan("R2022a")
				writetable(obj.Tab, obj.path, 'Delimiter', 'space', 'WriteVariableNames', false);
			else
				writetable(obj.Tab, obj.path, 'Delimiter', 'space', 'WriteVariableNames', false, "QuoteStrings", "none");
			end
		end
	end
	
	methods (Static)
		function nv = struct2namevalues(s, varargin)
			% expand the structure to a name-value pair list
			% If the structure contains arrays, the arrays will be expanded to indexed name-value
			% pairs
			
			vars = string();
			values = cell(0);
			
			for field = fieldnames(s)'
				if isscalar( s.(field{1}) )
					vars(end+1) = field{1};
					values{end+1} = s.(field{1});
				elseif isvector( s.(field{1}) )
					
					for i = 1:length( s.(field{1}) )
						
						vars(end+1) = field{1} + "_" + i;
						values{end+1} = s.(field{1})(i);
						
					end
					
				elseif ismatrix( s.(field{1}) )
					
					for i = 1:size( s.(field{1}), 1 )
						for j = 1:size( s.(field{1}), 2 )
							vars(end+1) = field{1} + "_" + i + j;
							values{end+1} = s.(field{1})(i,j);
						end
					end
					
					
				else
					error("Not yet implemented")
				end
			end %for
			
			% remove the first field from the field names:
			vars(1) = [];
			
			nv = export.namevalues( vars, values, varargin{:} );
			
		end
	end
	
end

