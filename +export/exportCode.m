function [ files ] = exportCode( function_name, exportPath, excludePath )
%EXPORTFUNCTION !experimental!
%   [ files ] = exportCode( function_name, exportPath, excludePath )
%   Uses the matlab.codetools.requiredFilesAndProducts to find the
%   functions, the matlab function "function_name" needs to run and copies
%   all of these functions to the path "exportPath". If special paths
%   should not be considered, they can be given in "excludePath". As result
%   "files" a list with all the copied files, i.e., matlab functions, is
%   returned.
%
%   #TODO example

rmpath(excludePath)

files = matlab.codetools.requiredFilesAndProducts(function_name);

if ~exist('exportPath', 'var')
    exportPath = function_name;
end

if ~isdir(exportPath)
    mkdir(exportPath)
else
    userinp = questdlg(['Der Pfad ' exportPath ' ist bereits vorhanden. Sollen die Dateien ?berschrieben werden?']);
    if ~strcmp(userinp, 'Yes')
        return
    end
    
    %TODO: Falls im Ordner noch dateien eliegen, dann müssen diese gelöscht
    %werden!
end

for k = 1:length(files)
        
    fid=fopen(files{k});
    tline = fgetl(fid);
    while ischar(tline)
       if ~isempty(strfind(tline, 'addpath'))
           warning(['Die Datei ' files{k} ' enthält einen addpath Befehl!'])
       end
       tline = fgetl(fid);
    end
    fclose(fid);
    copyfile(files{k}, exportPath, 'f');
end

addpath(excludePath)
end
