classdef Monomial < signals.BasicVariable
	%MONOMIAL representation for a monomial of the form
	%	t^i / factorial(i)
	% and its derivatives.
	
	properties
		timeDomain (1,1) quantity.Domain;
		order (1,1) double;
		variable (1,1) sym = sym("t");
		numDiff (1,1) double;
	end		
	methods
		function obj = Monomial(timeDomain, order, optArgs)
			% MONOMIAL representation of a monomial
			% obj = Monomial(timeDomain, order, optArgs) creates a monomial of ORDER on the
			% TIMEDOMAIN. The monomial has the form
			%	f(t) = t^i / i!
			% The number of computed derivatives can be specified by the name-value-pair "numDiff".
			% The name of the symbolic variable can be defined by "variable".
			
			arguments
				timeDomain (1,1) quantity.Domain
				order (1,1) double,
				optArgs.variable sym = sym(timeDomain.name);
				optArgs.numDiff (1,1) double = 3;
			end
						
			for k = 0:optArgs.numDiff
				derivatives{k+1} = signals.Monomial.privateEvaluateFunction( timeDomain, ...
					optArgs.variable, order, k);
			end
			
			obj@signals.BasicVariable(derivatives{1}, derivatives(2:end));
			
			obj.timeDomain = timeDomain;
			obj.order = order;
			obj.variable = optArgs.variable;	
			obj.numDiff = optArgs.numDiff;
			
		end
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, domain, k)
			v = obj.privateEvaluateFunction( domain, obj.variable, obj.order, k);
		end
	end
	methods (Static, Access = private)
		function f = privateEvaluateFunction(domain, t, i, k)
% 			
% 			if i - k >= 0
% 				f = quantity.Discrete( 1 / factorial(i-k) * (domain.grid - domain.lower).^(i-k), ...
% 					domain );
% 			else
% 				f = quantity.Discrete.zeros(1, domain);
% 			end
			

% 			
			if i - k >= 0
				f = diff( (t - domain.lower).^i / factorial(i), k);
				f = quantity.Symbolic( f, domain );
			else
				f = quantity.Symbolic.zeros(1, domain);
			end

% 			
			
		end


	end
	methods ( Static )
		
		function b = taylorPolynomialBasis( timeDomain, optArgs)
			arguments
				timeDomain (1,1) quantity.Domain;
				optArgs.order (1,1) double = 3;
				optArgs.numDiff (1,1) double = 3;
			end
			
			for i = 0:optArgs.order
				b(i+1, 1) = signals.Monomial(timeDomain, i, "numDiff", optArgs.numDiff);
			end
			
		end
		
	end
end

