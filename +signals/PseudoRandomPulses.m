classdef PseudoRandomPulses < quantity.Discrete
	
	properties
		steps double = [];
		deltaMin (1,1) double;
		deltaMax (1,1) double;
		seedt double;
		seedf double;
		seedPulse double;
		fmin (1,1) double;
		fmax (1,1) double;
		filter 
		t0 (1,1) double;
		tEnd (1,1) double;
	end
	
	methods
	
		function obj = PseudoRandomPulses(t, optArgs)
			arguments
				t (1,1) quantity.EquidistantDomain;
				optArgs.steps (1,1) double = ceil( t.stepSize * t.n );
				optArgs.seedt double = [];
				optArgs.seedf double = [];
				optArgs.seedPulse double = [];
				optArgs.fmin (1,1) double = -1;
				optArgs.fmax (1,1) double = 1;
				optArgs.deltaMin (1,1) double = 3 * t.stepSize;
				optArgs.deltaMax (1,1) double = 20 * t.stepSize;
				optArgs.filter logical = true;
				optArgs.name string = "";
				optArgs.t0 (1,1) double = t.lower;
				optArgs.tEnd (1,1) double = t.upper;
			end
			
			obj@quantity.Discrete(zeros(t.n,1), t, "name", optArgs.name);
			
			% set all specified properties
			for k = fieldnames(optArgs)'
				obj.(k{1}) = optArgs.(k{1});
			end			
			
			obj.generate(t);
			
		end
		
		function generate(obj, t)
			arguments
				obj
				t (1,1) quantity.Domain;
			end
			
			dt = t.stepSize;
		
			if ~isempty(obj.seedf)
				rng(obj.seedf);
			else
				rng('shuffle')
			end
			impulses.u = obj.fmin + rand(obj.steps, 1) * (obj.fmax - obj.fmin);
			
			inputSmoother.T = 4 * obj.deltaMin; % ceil( 2 / t.stepSize ) * t.stepSize;
			
			idx = t.grid >= obj.t0 & t.grid <= (obj.tEnd - obj.deltaMax - inputSmoother.T);
			tGrid = t.grid( idx );
			
			impulses.t = obj.jumpingPoints( tGrid );
			
			if ~isempty(obj.seedf)
				rng(obj.seedf);
			else
				rng('shuffle')
			end
			impulses.Delta = obj.deltaMin + rand(obj.steps, 1) * (obj.deltaMax - obj.deltaMin);
			
			uImpulseRaw = zeros(size(t.grid));
			for k = 1:obj.steps
				idxP = t.grid >= impulses.t(k) & t.grid <= impulses.t(k) + impulses.Delta(k);
				idxM = t.grid >= impulses.t(k) + impulses.Delta(k) ...
					& t.grid <= impulses.t(k) + 2 * impulses.Delta(k);

				uImpulseRaw(idxP) = (-1)^k * impulses.u(k);
				uImpulseRaw(idxM) = (-1)^(k+1) * impulses.u(k);
			end
			
			if ~isempty(obj.filter)
				% define a gevrey-fir filter to smooth the input jumps
				
				inputSmoother.t = 0:dt:inputSmoother.T;
				inputSmoother.fir = signals.gevrey.bump.g(inputSmoother.t, 1, inputSmoother.T, 1.1);
				uImpulse = filter(t.stepSize * inputSmoother.fir, 1, uImpulseRaw);

				obj.valueDiscrete = quantity.Discrete( uImpulse, t);
			else
				obj.valueDiscrete = quantity.Discrete( uImpulseRaw, t );
			end
		end
		
		function tk = jumpingPoints(obj, t)
           %JUMPINGPOINTS computes the points at which a jump occurs,
           %depent if they should be equally or randomly distributed.
		   
		   mySteps = obj.steps-1;
		   
           if ~isempty(obj.seedt) && obj.seedt < 0       
               % create equally distributed intervals
               tk = linspace(t(1), t(end), mySteps + 1).';
           else
               
               if isempty(obj.seedt)
                  rng('shuffle');
               else
                  % create intervals of random length
                  rng(obj.seedt);
               end
           
              tk = rand(mySteps,1);
              tk = [0; cumsum(tk)];
              tk = tk * (t(end) - t(1)) / tk(end) + t(1);
           end
           
        end
		
		function s = quantity.Discrete(obj)
			for k = 1:numel(obj)
				s(k) = quantity.Discrete( obj(k).valueDiscrete, obj(k).domain, "name", obj(k).name);
			end
			s = reshape(s, size(obj));
		end
		
	end
	
end
	