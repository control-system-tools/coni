function y = mFilter(F, u, optArgs)
% MFILTER MIMO FIR filter
%	Y = mFilter(F, U) filters the data in vector U with the filter
%	described by F to create the filtered data Y. The filter has the form
%		y(n) = F(1,:,:) * u(n) + F(2,:,:) * u(n-1) + ... 
% The filter F is a multidimensional array of size
%   size(fil) = (Ntau, n, m); 
%       Ntau: number of filter coefficients
%       n: number of rows
%       m: number of columns
%
%   size(u) = (Nt, m, p)
%       Nt: number of time steps
%       m: number of rows
%       p: number of columns
arguments
	F, 
	u,
	optArgs.t (1,1) quantity.Domain;
end
	if isa(u, "quantity.Discrete")
		flag.quantity = true;
		domain = u(1).domain;
		u = u.on();
	else
		flag.quantity = false;		
	end

   assert(size(F, 3) == size(u, 2), ...
	   sprintf("Incompatible filter size. The filter must satisfy size(F,3)==size(u,2), but it is of size(F,3)=%g and size(u,2)=%g.", size(F,3), size(u,2)));
   
   % dimensions and initialization
   q = size(u, 1);
   n = size(F, 2);
   o = size(u, 2);
   m = size(u, 3);

   y = zeros(q, n, m);

    for k = 1 : n % rows of P
        for l = 1 : m % columns of P
            for p = 1 : o % rows of B or columns of A
                y(:, k, l) = y( :, k, l ) + filter(F(:, k, p), 1, u(:, p, l));
            end
        end
	end

	if isfield( optArgs, "t")
		flag.quantity = true;
		domain = optArgs.t;
	end
	
	if flag.quantity
		y = quantity.Discrete(y, domain);
	end
	
end