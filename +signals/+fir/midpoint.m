function n_mid = midpoint(n, tau, optArg)
%MIDPOINT compute the fir filter with the kernel n
% 
%	n_foh = midpoint(n, tau ) computes the fir filter based on the
%	kernel n on the grid defined by tau. The filter coefficients are
%	computed on the basis of a midpoint integral approximation. 

%   INPUT PARAMETERS:   n: The quasi continuous kernel n discretized with
%                          the sample period dtau on the interval [0,T]
%                     tau: auxiliary time grid discretized with the
%                          simulation sample period dt on the interval [0,T]                
%                          
%                          dtau >> dt should hold.

%   For a more detalied explanation see the documentation [Chapter: Implementation]

arguments
	n quantity.Discrete;
	tau quantity.Domain;
	optArg.flip = false;
	optArg.gridName = tau.name;
end

dt = tau.stepSize;
T  = tau.upper;
T0 = tau.lower;
TAU_mid = (T0 + dt/2):dt:(T - dt/2);

if optArg.flip
	n_mid = dt * flip(n.on(TAU_mid), 1);
else
	n_mid = dt * n.on(TAU_mid);
end

end

