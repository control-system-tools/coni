classdef UniformDistributedSignal < signals.PseudoRandomSignal
	% UniformDistributedSignal class to generate pseudo random samples with a uniform distribution
	%	Simplification to generate signals that take values pseudo randomly from a uniform
	%	distribution.
	%	
	%	see also signals.PseudoRandomSignal, signalsNormalDistribuedSignal

	properties (SetAccess = protected)
		fmin double;        % Lower bound for the hight of the jumps
        fmax double;        % Upper bound for the hight of the jumps
		normalize (1,1) logical = false;
	end
	
	methods
		function obj = UniformDistributedSignal(t, optArgs)
			%PseudoRandomSignal Class to generate pseudo random signals
			%	signals.PseudoRandomSignal(t, optArgs) create a pseudo-random signal on the domain 
			%	specified by t, which takes values pseudo-randomly from a uniform distribution. 
			%	For the optional arguments "steps", "stepsRatio", "seedt", "seedf", "filter", "f0",
			%	"fEnd", "t0", "tEnd" see signals.PseudoRandomSignal
			%
			%	With the optArgs the following options can be specified:
			%
			%		fmin/fmax: maximal and minimal value between which the pseudo random values are
			%		taken from a uniform distribution.
			%
			%		normalize: If true, then the the function values are not chosen from a uniform
			%		distribution, only the points are. The function values are chosen
			%		pseudo-randomly from upper and lower bound with a uniform distribution.

			arguments
				t (1,1) quantity.Domain;
				optArgs.steps (1,1) double;
				optArgs.stepsRatio (1,1) double;
				optArgs.seedt (1,1) double;
				optArgs.seedf (1,1) double;
				optArgs.fmin (1,1) double = -1;
				optArgs.fmax (1,1) double = 1;
				optArgs.filter;
				optArgs.f0 (1,1) double = NaN;
				optArgs.fEnd (1,1) double = NaN;
				optArgs.name string = "";
				optArgs.normalize (1,1) logical = false;
				optArgs.transitionFactor (1,1) double = 1;
				optArgs.t0 (1,1) double = t.lower;
				optArgs.tEnd (1,1) double = t.upper;
			end
			
			argsIn = misc.struct2namevaluepair(optArgs, "exclude", ["fmin", "fmax", "normalize"]);
			obj@signals.PseudoRandomSignal(t, argsIn{:});
			
			% set all non-parent arguments to the objects properties
			obj.fmin = optArgs.fmin;
			obj.fmax = optArgs.fmax;
			obj.normalize = optArgs.normalize;
			
			% compute the required pseudo random sequence:
			obj.generate(t);
			
		end
	end
	
	methods (Access = public)
		function [f, tk, fk] = pseudoRandomSequence(obj, t)
            %pseudoRandomSequence compute a piecewise constant squence
			% [f, tk, fk] = pseudoRandomSequence(obj, t) will compute a pseudo-random sequence as
			% piecewise constant signal where the function values are either computed with the
			% random number generator specified by 'seedf' or normalized to upper and lower bounds.
			% The jumping points of the piecewise function are obtained from obj.jumpingPoints.
			%	see also signals.PseudoRandomSignal.jumpingPoints
			arguments
				obj
				t (:,1) double;
			end
            tk = obj.jumpingPoints(t(:));
            
			if ~isempty(obj.seedf)
				rng(obj.seedf);
			else
				rng('shuffle')
			end
			
			mySteps = obj.getMySteps(t);

			fk = rand(mySteps, 1) * diag(obj.fmax - obj.fmin) ...
					+ repmat(obj.fmin', mySteps, 1);
    
            fk = [fk; fk(end)];
			
			if ~isnan( obj.f0 )
				fk(1) = obj.f0;
			end
			
			% if required normalize fk to min max values:
			if obj.normalize
				
				% get all non zero entries
				i = ( fk ~= 0 );
				
				fk(i) = fk(i) ./ fk(i) .* sign(fk(i));
				
				fk( fk > 0 ) = fk( fk > 0 ) * obj.fmax;
				fk( fk < 0 ) = fk( fk < 0 ) * abs(obj.fmin);
			end
			
			if ~isnan( obj.fEnd )
				fk(end-1) = obj.fEnd;
				fk(end) = obj.fEnd;
			end

            f = interp1(tk, fk, t(:), 'previous', 'extrap');
		end
	end
	
	
end

