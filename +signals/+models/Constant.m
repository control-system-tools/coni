classdef Constant < signals.SignalModel
	%CONSTANT signal model to describe a constant
	% obj = Constant(offset, optArgs) creates the signal model for a constant with the value
	% 'offset'. The optional arguments are 'occurrence', 'timeDomain', 'signalName', 'modelName'.
	
	properties (Dependent)
		offset (1,1) double;
	end
	
	methods
		function obj = Constant(offset, optArgs)
			arguments
				offset (1,1) double;	
				optArgs.occurrence (1,1) double = 0;
				optArgs.offTime (1,1) double = inf;
				optArgs.timeDomain (1,1) quantity.Domain = quantity.Domain.empty;
				optArgs.signalName string = "r";
				optArgs.modelName string = "v";
			end

			optArgs.initialCondition = offset;
			varArgs = namedargs2cell(optArgs);
			
			S = 0;
			p = 1;
			obj@signals.SignalModel(S, p, varArgs{:});
		end		
		function c = get.offset(obj)
			c = obj.initialCondition(1);
		end
	end
end

