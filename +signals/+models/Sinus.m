classdef Sinus < signals.SignalModel
% SINUSOIDAL creat a sinusoidal signal model
% sm = sinusoidal(omega, optArgs) will create a signal model with
% sinusoidal form. With 'omega' the frequency is specified. The 'occurrence', 'timeDomain',
% 'initialCondition', 'offTime', 'signalName' and 'modelName' can be set by name-value-pairs.
% --- EXAMPLE ---
% Create a sinusoidal signal with omega = 1 and amplitude = 1
% f = signals.models.Sinus(1, 'time', timeDomain, 'initialCondition', [0; 1]);
%
	properties (Dependent)
		omega
	end
	
	methods
		function obj = Sinus(omega, optArgs)
			arguments
				omega (1,1) double;
				optArgs.occurrence (1,1) double = 0;
				optArgs.timeDomain (1,1) quantity.Domain = quantity.Domain.empty;
				optArgs.initialCondition (:,1) double;
				optArgs.signalName string = "r";
				optArgs.modelName string = "v";
				optArgs.offTime (1,1) double = inf;
			end
			
			varArgs = namedargs2cell(optArgs);
			
			S = [0, -omega; omega 0];
			p = [0, 1];
			obj@signals.SignalModel(S, p, varArgs{:});
		end		
		
		function o = get.omega(obj)
			o = obj.S(2,1);
		end
	end
end

