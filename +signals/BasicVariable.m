classdef BasicVariable < handle
	%BasicVariable Class to handle quantities with a priori known derivatives
	% This class is intended to facilitate operations for the derivatives of quantity.Discrete
	% objects. For this purpose, the derivatives are calcluated in advance and kept ready together
	% with the underlying function. Hence, application of signals.PolynomialOpterator or
	% multiplication of such functions can be performed without recomputing the derivatives. 
	%
	%	see also signals.BasicVariable.BasicVariable (constructor), signals.GevreyFunction,
	%	signals.Monomial, signals.PolyBump, signals.Sinusoidal, signals.PolynomialOperator
	
	properties ( SetAccess = protected )
		derivatives cell;
		fun (1,1) quantity.Discrete;
	end
	
	properties ( Dependent )
		highestDerivative;  % order of the highest computed derivative
		T;					% the transition time T
		dt;					% step size
		name
	end
	
	methods
		function obj = BasicVariable(fun, derivatives)
			% BasicVariable Class to handle quantities and their derivatives obj =
			% BasicVariable(fun, derivatives) creates a new object based on the function 'fun' as
			% quantity.Discrete with the derivatives specified in 'derivatices', which must be a
			% cell, where the first entry is the first derivative etc. 
			
			% If a vector/matrix valued 'fun' is passed, the BasicVariable object is created for
			% each element and then attached to the required matrix
			if numel(fun) > 1
				
				for i = 1:numel(fun)
					for j = 1:numel(derivatives)
						d{j} = derivatives{j}(i);
					end
					obj(i) = signals.BasicVariable( fun(i), d ); 
				end
				obj = reshape(obj, size(fun));
				
			else
			% For scalar 'fun':
				obj.fun = fun;
				obj.derivatives = derivatives;

				% do the pre-initialization, because if it is done implicit, some strange bug causes the
				% functions to be evaluated more often.
				obj.fun.on();
				for i = 1:numel(derivatives)
					obj.derivatives{i}.on();
				end
			end
		end
		
		function setName(obj, name)
			% setName set the name of the basic variable if it is a matrix
			for i = 1:numel(obj)
				obj(i).fun.setName( name );
			end
		end
		function set.name(obj, name)
			% set.name set the name of scalar basic variables
			obj.fun.setName(name);
		end
		function n = get.name(obj)
			% get.name return the name of scalar basic variables
			n = obj.fun.name;
		end
		
		function h = get.highestDerivative(obj)
			% get.highestDerivative the number of precomputed derivatives 
			h = numel(obj(1).derivatives);
		end
		
		function T = get.T(obj)
			% get.T the upper bound of the underlying time-domain
			T = obj.domain.upper;
		end
		function dt = get.dt(obj)
			% get.dt the step size of the underlying time-domain
			if isa(obj.domain, "quantity.EquidistantDomain")
				dt = obj.domain.stepSize;
			else
				dt = misc.stepSize(obj.domain.grid);
			end
		end
		
		function F = quantity.Discrete(obj)
			% QUANTITY.DISCRETE cast of a basic variable to quantity.Discrete object
			%	F = quantity.Discrete(obj) will return the underlying quantity objects from this
			%	basic variable. They are returned in the form of three-dimensional array:
			%		F(:,:,1) 0-th derivative
			%		F(:,:,2) 1-th derivative ...
			F = quantity.Discrete.empty();	
			numberOfDims = ndims(obj) + 1;
			for i = 0 : obj.degree()
				F = cat(numberOfDims, F, obj.diff( obj(1).fun(1).domain, i));
			end
		end
		
		function ff = f(obj)
			ff = obj.diff( obj(1).domain, 0);
		end
	end
	
	methods ( Access = public)
						
		function D = diff(obj, domain, n)
			% DIFF get the k-th derivative 
			%	D = diff(obj, domain, n) tries to find the n-th derivative of obj.fun in the saved
			%	derivatives. If it does not exist, it tries to compute it with the evaluateFunction.
			%	If n is a non integer number, the Riemann-Liouville fractional derivative is
			%	computed. For this, a formula is used which requires the next integer + 1 derivative
			%	of the function to avoid the singularity under the integral. Actually, the domain is
			%	clear, but it is used hear to be consistent with the quantity.Discrete/diff.
			arguments
				obj
				domain (1,1) quantity.Domain = obj(1).domain;
				n (1,1) double = 1;
			end
						
			for l = 1:numel(obj)
				
				assert( domain.isequal( obj(l).domain  ),...
					"The derivative wrt. this domain is not possible.")
				
				if n == 0
					D(l) = obj(l).fun;
					
				elseif ~numeric.near(round(n), n)
					% fractional derivative:
					D(l) = obj(l).fDiff(n);
					
				elseif n > length(obj(l).derivatives)
					% For some functions their maybe a possibility to compute new derivatives on the
					% fly. For this, the evaluateFunction(k) can be used, where k is the order of
					% the derivative.
					D(l) = obj(l).evaluateFunction(obj.domain, n);
					obj(l).derivatives{end+1} = D(l);
				else
					% use the pre-computed derivatives
					D(l) = obj(l).derivatives{n};
				end
			end
			D = reshape(D, size(obj));
		end
		
		function D = diffs(obj, k)
			% DIFFS compute multiple derivatives
			%	D = diffs(obj, k) computes the k-th derivatives of OBJ. At this, k can be a vector
			%	of derivative orders.
			arguments
				obj,
				k (:,1) double {mustBeInteger, mustBeNonnegative} = 0:numel(obj(1).derivatives);
			end
			D_ = arrayfun( @(i) obj.diff(obj.domain, i), k, 'UniformOutput', false);
			
			for i = 1:numel(k)
				D(i,:) = D_{i};
			end
		end

		function h = productRule(a, b, n)
			% PRODUCTRULE compute the k-derivative of the product of the basic variable a and b
			%	h = productRule(a, b, n) computes the k-th derivative 
			%		h = d_t^n ( a * b ) 
			%	using the product rule. This is usefull if the derivatives of the basic variable a
			%	are known exactly and the derivatives of b, but not the multiplication a*b
			t = b.domain;
			assert( size(a,2) == size(b,1) || all( size(b) == 1) || all( size(a) == 1) )
			
			if size(a,2) == size(b,1)
				h = quantity.Discrete.zeros( [size(a, 1), size(b,2)], t );	
			elseif all( size(a) == 1)
				h = quantity.Discrete.zeros( size(b), t );
			elseif all( size(b) == 1 )
				h = quantity.Discrete.zeros( size(a), t );
			else
				error("The product rule for quantities of the size " + size(a) + " and " + size(b) + " is not possible.")
			end
			
			if numeric.near(round(n), n)
				% for integer order derivatives
				for k = 0:n
					h = h + misc.binomial(n, k) * a.diff(t, n-k) * b.diff(t, k);
				end
			else
				% for fractional order derivatives: [see Podlubny]
				%         p                     \~~ oo  / p \     (k)       p-k
				%        D  (phi(t) f(t)) =  >      |   |    | phi   (t)   D   f(t)
				%      a  t                     /__ k=0 \ k /               a  t				
				h = misc.binomial( n, 0 ) * b * a.fDiff(n);
				h0 = h;
				k = 1;
				% do the iteration on the loop until the difference is very small.
				while max( abs( h0 ) ) / max( abs( h ) ) > 1e-9 
					h0 = misc.binomial(n,k) * b.diff(t, k) * a.fDiff( n - k);
					h = h + h0;
					k = k+1;
				end					
			end
		end
		
		function d = domain(obj)
			% domain returns the domain of the function represented by this BasicVariable
			d = obj(1).fun(1).domain;
		end
		
		function plot(obj, varargin)
			% plot display the function and its derivatives of this BasicVariable
			F = quantity.Discrete(obj);			
			plot(squeeze(F), varargin{:});
		end
		
		function c = diffShift(obj, k)
			% diffShift raise the order of derivatives 
			% c = diffShift(obj, k) will raise the order of derivatives, i.e. k=1, will return
			%	c = d/dt obj, (assuming obj is a function wrt t)
			% the paramter k is optional, the default value is 1.
			arguments
				obj
				k (1,1) double = 1;
			end
			assert( k+1 < obj.highestDerivative );
			
			c = signals.BasicVariable(obj.derivatives{k}, obj.derivatives((k+1):end));
		end
		
		function d = degree(obj, optArgs)
			% DEGREE returns the degree of this basic variable
			% d = degree(obj, optArgs) returns the degree of the highest derivative that is
			% pre-computed for this basic variable. If the basic variable is array-valued, the
			% minimal degree of all elements is chosen. With the optional name-value-pair <"max",
			% true>, the maximal number of available derivatives is returned.
			arguments
				obj
				optArgs.max = false
			end
			
			if optArgs.max
				d = max( [obj.highestDerivative] );
			else
				d = min( [obj.highestDerivative] );
			end
			
		end
		
		function c = plus( a, b )
			% plus summation of two signals.BasicVariable objects.
			%	c = plus( a, b ) returns c = a + b, where also the sum of each derivative in a and b
			%	is computed, i.e., c is also a signals.BasicVariable with the corresponding
			%	derivatives.
			arguments
				a signals.BasicVariable
				b signals.BasicVariable
			end
			
			assert( all(size(a) == size(b), 'all') );
			%todo write unittest for addition of matrix valued BasicVariables
			
			degree = min( a.degree, b.degree );
			
			% do the addition for each derivative:
			for i = 1:numel(a)
				A = [a(i).diffs(0:degree)];
				B = [b(i).diffs(0:degree)];

				C = A + B;
				c(i) = signals.BasicVariable(C(1,:), num2cell( C(2:end, :) ));
			end
			
			c = reshape(c, size(a) );
		end
		
		function c = kron(a, b)
			% kron   Kronecker tensor product.
			%     kron(X,Y) is the Kronecker tensor product of X and Y.
			%     The result is a large matrix formed by taking all possible
			%     products between the elements of X and those of Y. For
			%     example, if X is 2 by 3, then kron(X,Y) is
			%  
			%        [ X(1,1)*Y  X(1,2)*Y  X(1,3)*Y
			%          X(2,1)*Y  X(2,2)*Y  X(2,3)*Y ]
			c = [];
			for i = 1:size(a,1)
				cRow = [];
				for j = 1:size(a,2)
					cRow = [cRow, a(i,j) * b];
				end				
				c = [c; cRow];
			end
		end
		
		function c = times(a, b)
			% times elementwise multiplication of signal.BasicVariable.
			% c = times(a, b) computes the elementwise multiplication of a and b
			if all( size(a) == size(b) )
				for i = 1:numel(a)
					c(i) = a(i) * b(i);
				end
				c = reshape(c, size(a));
			elseif all( size(a) == 1) || all( size(b) == 1)
				c = a * b;
			else
				error("not yet implemented")
			end
		end
		
		function c = mtimes(a, b)
			% mtimes matrix multiplication
			% c = mtimes(a, b) returns the matrix multiplication of signal.BasicVariable. If two
			% signal.BasicVariable matrices are multiplied, the derivatives of the result are
			% computed using the product rule.
			%	see also signals.BasicVariable/productRule
			arguments
				a 
				b 
			end
			
			if isa( a, "signals.BasicVariable")
				I = a.domain;
			elseif isa( b, "signals.BasicVariable")
				I = b.domain;
			end
			if size(a,2) == size(b,1)
				O = quantity.Discrete.zeros( [size(a, 1), size(b,2)], I );
			elseif all( size(a) == 1)
				O = quantity.Discrete.zeros( size(b), I );
			elseif all( size(b) == 1 )
				O = quantity.Discrete.zeros( size(a), I );
			else
				error("The product for quantities of the sizes " + mat2str(size(a)) + " and " ...
					+ mat2str(size(b)) + " is not possible.")
			end			
			
			if isa( a, "double") && isa( b, "signals.BasicVariable" )
%				assert( size(a,2) == size(b,1), "dimensions of the terms for multiplication do not match")
				assert( numel(size(a)) <= 2 )
				assert( numel(size(b)) <= 2 )

				degree = max([b.highestDerivative]);

				% do the multiplication for each derivative:
				for i = 1:degree+1
					diffs{i} = a * b.diff(I, i-1);
				end

			elseif isa( a, "signals.BasicVariable") && isa( b, "double" )
				c = ( b.' * a.' ).';
				return;
				
			elseif isa( a, "signals.BasicVariable" ) && isa( b, "signals.BasicVariable" )
				
				assert( a(1).domain.isequal(b(1).domain) , "Both BasicVariables must have the same domain");

				order = min( a.degree, b.degree );
				
				diffs = cell(order+1,1);
				parfor n = 0 : order
					diffs{n+1} = a.productRule(b, n);
				end
			else
				error("The multiplication of " + class(a) + " and " + class(b) + " is not yet implemented");
			end
			
			c = signals.BasicVariable(diffs{1}, diffs(2:end));
			c = reshape(c, size(O) );
		end

	end % methods (Access = public)
	
	methods (Access = protected)
		function v = evaluateFunction(obj, domain, k)
			% EVALUATEFUNCTION evalutes the k-th derivative of the basic variable on the domain.
			% v = evaluateFunction(obj, domain, k) computes the k-th derivative of the basic
			% variable obj on the domain and returns a quantity.Discrete object as v.
			error('conI:signals:BasicVariable:derivative', ['Requested derivative ' num2str(k) ' is not available']);
		end
		
		function D = fDiff(obj, p)
			% FDIFF compute the p-fractional derivative
			%	D = fDiff(obj, p) computes the fractional derivative of order p. To be
			%	concrete the Riemann-Liouville fractional derivative with initial point t0 = lower
			%	end of the considered time domain is computed.
			%	Because the basisc variable must be C^infinity function, we use the form
			%	of the fractional derivative described in eq. (2.80) in the book [Podlubny: Fractional
			%	differential equations]. Then, integration by parts is applied to eliminate the
			%	singularity:
			%	
			
			n = ceil(p);
			tDomain = obj(1).domain;
			t0 = tDomain.lower;
			t = Discrete( tDomain );
			tauDomain = tDomain.rename( tDomain.name + "_Hat" );
			tau = Discrete( tauDomain );
			if p > 0
				
				% TODO Try to implement the polynomial part as function or as symbolic
				D = quantity.Discrete.zeros(size(obj), tDomain);
				for k = 0:n
					f_dk = obj.diff(tDomain, k);
					
					if f_dk.at(t0) ~= 0
						D = D + f_dk.at(t0) * (t - t0).^(k-p) / gamma( k-p + 1);
					end
					
				end
				assert( ~ any( isinf( obj.diff(tDomain, n+1).on() ) ), "conI:BasicVariable:fDiff", ...
					"The function has a singularity! Hence, the fractional derivative can not be computed")
				D = D + int( ( t - tau ).^(n-p) * subs(obj.diff(tDomain, n+1), tDomain.name, tauDomain.name), ...
				tauDomain, tauDomain.lower, tDomain.name) / gamma( n+1-p );
				
			elseif p < 0 && p > -1
				alpha = -p;
				D = (t - t0).^(alpha) / gamma( alpha + 1) * obj.diff(tDomain, 0).at(t0) + ...
					int( (t-tau).^alpha * subs(obj.diff(tDomain, 1), tDomain.name, tauDomain.name), ...
					tauDomain, tauDomain.lower, tDomain.name) / gamma( alpha + 1 );

			elseif p <= -1
				alpha = -p;
				D = int( (t-tau).^(alpha-1) * subs( obj.diff(tDomain, 0), tDomain.name, tauDomain.name), ...
					tauDomain, tauDomain.lower, tDomain.name) / gamma( alpha );
				
			elseif p == 0
				D = [obj.fun()];	
			else
				error("This should not happen")
			end
		end		
		
	end	
	
end
