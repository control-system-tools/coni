function out = dgdt_8(T,si,t)
t1 = 1 ./ T;
t3 = -t .* t1 + 1;
t6 = (t3 .* t .* t1) .^ si;
t7 = t6 .^ 2;
t8 = t7 .^ 2;
t10 = 1 ./ t8 ./ t6;
t11 = (si .^ 2);
t12 = (t11 .^ 2);
t13 = (t12 .* t11);
t14 = t10 .* t13;
t15 = T .^ 2;
t16 = 1 ./ t15;
t19 = -t16 .* t + t3 .* t1;
t20 = t19 .^ 2;
t21 = t20 .^ 2;
t22 = t21 .* t20;
t23 = t14 .* t22;
t24 = t3 .^ 2;
t25 = t24 .^ 2;
t27 = 1 ./ t25 ./ t24;
t28 = t .^ 2;
t29 = t28 .* t;
t30 = t28 .^ 2;
t32 = 1 ./ t30 ./ t29;
t34 = t15 .^ 2;
t35 = t34 .* t15;
t36 = 1 ./ t6;
t37 = exp(-t36);
t39 = t27 .* t32 .* t35 .* t37;
t43 = 1 ./ t8 ./ t7;
t44 = (t11 .* si);
t45 = t12 .* t44;
t47 = t20 .* t19;
t48 = t21 .* t47;
t50 = t24 .* t3;
t52 = 1 ./ t25 ./ t50;
t54 = t15 .* T;
t57 = t52 .* t32 .* t34 .* t54 .* t37;
t60 = 1 ./ t8;
t61 = t60 .* t12;
t62 = t61 .* t21;
t63 = 1 ./ t25;
t65 = t34 .* t37;
t66 = t63 .* t32 .* t65;
t69 = t43 .* t13;
t70 = t69 .* t22;
t72 = 1 ./ t30 ./ t28;
t74 = t34 .* T;
t75 = t74 .* t37;
t76 = t52 .* t72 .* t75;
t79 = t12 .* si;
t80 = t10 .* t79;
t81 = t21 .* t19;
t82 = t80 .* t81;
t84 = 1 ./ t25 ./ t3;
t86 = t84 .* t32 .* t75;
t91 = t7 .* t6;
t97 = 1 ./ t91;
t98 = t97 .* t44;
t99 = 1 ./ t50;
t100 = t98 .* t99;
t102 = 1 ./ t30 ./ t;
t104 = t19 .* t37;
t109 = T .* t37;
t113 = t36 .* t12;
t115 = 1 ./ t30;
t116 = t63 .* t115;
t117 = t116 .* t104;
t120 = 1 ./ t7;
t121 = t120 .* t12;
t125 = 0.315e3 .* t23 .* t39 - 0.21e2 .* t43 .* t45 .* t48 .* t57 - 0.735e3 .* t62 .* t66 + 0.21e2 .* t70 .* t76 + 0.175e3 .* t82 .* t86 - 0.21e2 .* t70 .* t39 + 0.1e1 ./ t8 ./ t91 .* t45 .* t48 .* t57 + 0.6160e4 .* t100 .* t102 .* t1 .* t104 + 0.5908e4 .* t100 .* t72 .* t20 .* t109 + 0.840e3 .* t113 .* t16 .* t117 - 0.5880e4 .* t121 .* t16 .* t117;
t126 = t97 .* t12;
t130 = t113 .* t21;
t132 = t52 .* t115 .* t109;
t135 = t121 .* t21;
t138 = t113 .* t47;
t140 = t84 .* t102 .* t109;
t143 = t121 .* t47;
t146 = t126 .* t47;
t152 = t120 .* t44;
t153 = t152 .* t47;
t156 = t98 .* t47;
t159 = t60 .* t79;
t162 = t102 .* t47 .* t109;
t165 = t80 .* t84;
t168 = 0.5040e4 .* t126 .* t16 .* t117 - 0.8960e4 .* t98 .* t16 .* t117 - 0.4200e4 .* t159 .* t84 .* t162 - 0.735e3 .* t130 .* t132 + 0.5145e4 .* t135 .* t132 - 0.4200e4 .* t138 .* t140 + 0.29400e5 .* t143 .* t140 - 0.25200e5 .* t146 .* t140 - 0.10332e5 .* t153 .* t140 + 0.3444e4 .* t156 .* t140 + 0.420e3 .* t165 .* t162;
t170 = t36 .* t79;
t174 = t120 .* t79;
t178 = t97 .* t79;
t182 = t61 .* t84;
t185 = t36 .* t44;
t187 = t99 .* t102;
t188 = t187 .* t104;
t191 = t152 .* t1;
t194 = t185 .* t47;
t197 = t185 .* t20;
t199 = t99 .* t72 .* t109;
t203 = t15 .* t37;
t204 = t63 .* t72 .* t203;
t207 = t152 .* t20;
t210 = t120 .* t11;
t214 = 0.6160e4 .* t185 .* t1 .* t188 + 0.8064e4 .* t210 .* t1 .* t188 + 0.420e3 .* t170 .* t84 .* t162 - 0.6300e4 .* t174 .* t84 .* t162 + 0.10500e5 .* t178 .* t84 .* t162 + 0.3444e4 .* t194 .* t140 + 0.4200e4 .* t182 .* t162 - 0.18480e5 .* t191 .* t188 - 0.2954e4 .* t194 .* t204 + 0.5908e4 .* t197 .* t199 - 0.17724e5 .* t207 .* t199;
t215 = t210 .* t20;
t223 = t54 .* t37;
t224 = t99 .* t32 .* t223;
t229 = t36 .* t11;
t230 = t229 .* t20;
t233 = 1 ./ t24;
t235 = t233 .* t32 .* t203;
t238 = t36 .* si;
t239 = t238 .* t19;
t240 = t37 .* t1;
t241 = t187 .* t240;
t244 = 1 ./ t3;
t249 = t84 .* t115;
t250 = t249 .* t240;
t253 = t126 .* t21;
t255 = t27 .* t102 .* t203;
t260 = 0.720e3 .* t239 .* t244 .* t32 .* t109 + 0.735e3 .* t130 .* t66 - 0.5145e4 .* t135 .* t66 - 0.4872e4 .* t153 .* t224 + 0.1624e4 .* t194 .* t224 + 0.2688e4 .* t215 .* t199 - 0.2688e4 .* t230 .* t199 + 0.3108e4 .* t215 .* t250 + 0.1764e4 .* t230 .* t235 + 0.720e3 .* t239 .* t241 + 0.8190e4 .* t253 .* t255 - 0.1365e4 .* t62 .* t255;
t263 = t229 .* t19;
t264 = t16 .* t37;
t265 = t116 .* t264;
t268 = 1 ./ t29;
t269 = t84 .* t268;
t270 = 0.1e1 ./ t54;
t271 = t37 .* t270;
t272 = t269 .* t271;
t278 = t27 .* t268 .* t264;
t281 = 1 ./ t28;
t283 = 0.1e1 ./ t34;
t285 = t27 .* t281 .* t37 .* t283;
t289 = t52 .* t281 .* t271;
t292 = t210 .* t19;
t299 = 1 ./ t;
t301 = 0.1e1 ./ t74;
t311 = 0.6160e4 .* t98 .* t84 .* t268 .* t19 .* t271 + 0.720e3 .* t239 .* t52 .* t299 .* t37 .* t301 + 0.1764e4 .* t215 .* t289 + 0.2688e4 .* t230 .* t278 - 0.1764e4 .* t230 .* t289 + 0.720e3 .* t239 .* t272 - 0.720e3 .* t239 .* t285 + 0.8904e4 .* t263 .* t265 - 0.8064e4 .* t263 .* t272 + 0.5376e4 .* t263 .* t285 - 0.5376e4 .* t292 .* t285;
t320 = t61 .* t63;
t325 = t170 .* t21;
t328 = t174 .* t21;
t331 = t178 .* t21;
t334 = t159 .* t21;
t349 = -0.560e3 .* t80 .* t27 .* t102 .* t21 .* t203 + 0.2940e4 .* t182 .* t115 .* t20 .* t240 - 0.840e3 .* t320 .* t115 .* t19 .* t264 + 0.560e3 .* t165 .* t72 .* t21 .* t223 - 0.2730e4 .* t61 .* t47 .* t204 - 0.4410e4 .* t253 .* t132 + 0.735e3 .* t62 .* t132 - 0.560e3 .* t325 .* t255 + 0.8400e4 .* t328 .* t255 - 0.14000e5 .* t331 .* t255 + 0.5600e4 .* t334 .* t255;
t351 = t185 .* t270;
t353 = t99 .* t115 .* t37;
t356 = t152 .* t270;
t360 = t27 .* t115 .* t37;
t364 = t63 .* t102 .* t37;
t377 = t233 .* t72 .* t37;
t388 = 0.1440e4 .* t238 .* t1 .* t244 .* t72 .* t37 + 0.5376e4 .* t210 .* t270 .* t353 + 0.1440e4 .* t238 .* t270 .* t353 - 0.2954e4 .* t194 .* t360 - 0.9968e4 .* t197 .* t364 + 0.29904e5 .* t207 .* t364 - 0.3108e4 .* t215 .* t364 + 0.3108e4 .* t230 .* t364 + 0.5376e4 .* t263 .* t377 + 0.1680e4 .* t351 .* t353 - 0.5040e4 .* t356 .* t353;
t393 = t63 .* t268 .* t37;
t413 = t84 .* t281 .* t37;
t428 = t98 .* t63;
t433 = -0.720e3 .* t239 .* t377 - 0.1680e4 .* t185 .* t283 .* t393 + 0.5040e4 .* t152 .* t283 .* t393 - 0.5376e4 .* t210 .* t283 .* t393 - 0.5376e4 .* t229 .* t270 .* t353 - 0.1440e4 .* t238 .* t283 .* t393 + 0.5376e4 .* t229 .* t283 .* t393 - 0.3696e4 .* t229 .* t301 .* t413 + 0.3696e4 .* t210 .* t301 .* t413 + 0.1440e4 .* t238 .* t301 .* t413 - 0.1440e4 .* t238 ./ t35 .* t27 .* t299 .* t37 - 0.1680e4 .* t428 .* t268 .* t283 .* t37;
t439 = t102 .* t37 .* t20;
t460 = t115 .* t47 .* t37;
t468 = t233 .* t102 .* t37;
t471 = 0.2940e4 .* t113 .* t63 .* t439 - 0.20580e5 .* t121 .* t63 .* t439 + 0.16380e5 .* t126 .* t27 .* t460 + 0.17640e5 .* t126 .* t63 .* t439 + 0.3696e4 .* t229 .* t16 .* t468 - 0.2730e4 .* t61 .* t27 .* t460 + 0.2730e4 .* t138 .* t360 - 0.19110e5 .* t143 .* t360 + 0.8862e4 .* t153 .* t360 - 0.2954e4 .* t156 .* t360 - 0.9968e4 .* t428 .* t439;
t472 = t210 .* t233;
t505 = 0.1680e4 .* t100 .* t115 .* t270 .* t37 - 0.3696e4 .* t472 .* t102 .* t16 .* t37 - 0.5376e4 .* t472 .* t72 .* t19 .* t37 + 0.26880e5 .* t152 .* t16 .* t117 - 0.8960e4 .* t185 .* t16 .* t117 - 0.8904e4 .* t210 .* t16 .* t117 - 0.1440e4 .* t238 .* t16 .* t468 + 0.9968e4 .* t197 .* t250 - 0.720e3 .* t239 .* t265 - 0.8064e4 .* t263 .* t241 - 0.2940e4 .* t320 .* t439;
t511 = t269 .* t104;
t527 = t84 .* t72 .* t223;
t532 = 0.1365e4 .* t130 .* t255 - 0.1365e4 .* t130 .* t527 - 0.9555e4 .* t135 .* t255 + 0.9555e4 .* t135 .* t527 + 0.2730e4 .* t138 .* t204 - 0.19110e5 .* t143 .* t204 - 0.5908e4 .* t197 .* t278 - 0.2688e4 .* t215 .* t278 + 0.8064e4 .* t292 .* t272 + 0.6160e4 .* t351 .* t511 - 0.18480e5 .* t356 .* t511;
t539 = t170 .* t81;
t542 = t174 .* t81;
t545 = t178 .* t81;
t554 = t249 .* t37 .* t20;
t565 = -0.2940e4 .* t113 .* t1 .* t554 + 0.20580e5 .* t121 .* t1 .* t554 - 0.17640e5 .* t126 .* t1 .* t554 + 0.16380e5 .* t146 .* t204 + 0.8862e4 .* t153 .* t204 - 0.2954e4 .* t156 .* t204 - 0.29904e5 .* t191 .* t554 - 0.1764e4 .* t215 .* t235 + 0.4410e4 .* t253 .* t66 + 0.175e3 .* t539 .* t86 - 0.2625e4 .* t542 .* t86 + 0.4375e4 .* t545 .* t86;
t576 = t52 .* t268 .* t240;
t587 = t52 .* t102 .* t223;
t590 = t159 .* t81;
t597 = 0.9968e4 .* t98 .* t1 .* t554 - 0.5908e4 .* t98 .* t20 .* t278 - 0.4872e4 .* t153 .* t576 + 0.1624e4 .* t156 .* t576 + 0.1624e4 .* t194 .* t576 + 0.17724e5 .* t207 .* t278 - 0.3108e4 .* t230 .* t250 + 0.175e3 .* t539 .* t587 + 0.4375e4 .* t545 .* t587 - 0.1750e4 .* t590 .* t587 + 0.175e3 .* t82 .* t587;
t607 = t27 .* t72 .* t65;
t620 = t36 .* t13;
t621 = t620 .* t22;
t624 = -0.8190e4 .* t253 .* t527 + 0.560e3 .* t325 .* t527 - 0.8400e4 .* t328 .* t527 + 0.14000e5 .* t331 .* t527 - 0.5600e4 .* t334 .* t527 + 0.21e2 .* t621 .* t39 + 0.1365e4 .* t62 .* t527 - 0.280e3 .* t539 .* t607 - 0.2625e4 .* t542 .* t587 + 0.4200e4 .* t542 .* t607 - 0.7000e4 .* t545 .* t607;
t626 = t120 .* t13;
t627 = t626 .* t22;
t630 = t97 .* t13;
t631 = t630 .* t22;
t634 = t60 .* t13;
t635 = t634 .* t22;
t658 = 0.42e2 .* t620 .* t81 .* t607 - 0.1302e4 .* t626 .* t81 .* t607 + 0.3780e4 .* t630 .* t81 .* t607 - 0.2730e4 .* t634 .* t81 .* t607 + 0.1624e4 .* t156 .* t224 - 0.651e3 .* t627 .* t39 + 0.1890e4 .* t631 .* t39 - 0.1365e4 .* t635 .* t39 - 0.1750e4 .* t590 .* t86 - 0.21e2 .* t621 .* t76 + 0.651e3 .* t627 .* t76;
t696 = -0.42e2 .* t69 .* t27 .* t72 .* t81 .* t65 + 0.140e3 .* t10 .* t45 .* t48 .* t57 - 0.63e2 .* t120 .* t45 .* t48 .* t57 + t36 .* t45 .* t48 .* t57 - 0.350e3 .* t60 .* t45 .* t48 .* t57 + 0.301e3 .* t97 .* t45 .* t48 .* t57 + 0.630e3 .* t14 .* t81 .* t607 - 0.315e3 .* t23 .* t76 + 0.2800e4 .* t590 .* t607 - 0.280e3 .* t82 .* t607 - 0.1890e4 .* t631 .* t76 + 0.1365e4 .* t635 .* t76;
out = t125 + t168 + t214 + t260 + t311 + t349 + t388 + t433 + t471 + t505 + t532 + t565 + t597 + t624 + t658 + t696;

