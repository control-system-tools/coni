function e = dgdt_0(T,si,t)
%dgdt_1
%    E = dgdt_1(T,SI,T)
e = zeros(size(t));
for k=1:length(t)
    e(k) = integral(@(tau) signals.gevrey.bump.dgdt_1(T, si, tau), 0, t(k));
end