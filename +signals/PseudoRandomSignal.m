classdef(Abstract) PseudoRandomSignal < quantity.Discrete
    %PseudoRandomSignal Class to generate pseudo random signals
    %   With this class some pseudo-random signals can be generated. The signal can be either
    %   piecewise constant or continuous up to a certain differentiability. For the latter, the
    %   piecewise constant signal is filtered with an IIR or FIR filter, so that a smooth trajectory
    %   is obtained.
	%		
    %   see also signals.PseudoRandomSignal.example()
    
    properties
        % Number of the steps
		steps = [];         
		% Or ratio between the length of the interval and the number of steps
		stepsRatio = [];
        
        % seed for the random number generator for the timestamps
		% If empty, i.e., [], a random seed will be chosen. This is the
		% default.
		% If -1, the jumping points will be equally distributed over the
		% temporal domain.
        seedt double;         
        
		% seed for the random number generator for the hight of the jumps
		% If empty, i.e., [], a random seed will be chosen. This is the
		% default.
        seedf double;         
		

        filter;             % filter for smoothing the picewise constant pseudo-random-squence
		
		% start and end time for the signal to be not equal to zero
		t0 (1,1) double = 0;
		tEnd (1,1) double = 0;
		
		f0 (1,1) double = NaN;% Initial-values for the filtered values.
		fEnd (1,1) double = NaN;
		
		transitionFactor (1,1) double = 1;
	end
	
	properties (SetAccess = protected )
		tk;
		fk;
		sequence;
	end
        
    methods
        
        function obj = PseudoRandomSignal(t, optArgs)
			%PseudoRandomSignal Class to generate pseudo random signals
			% Optional arguments:
			%		steps:	the number of pseudo-random values that should be spread on the domain.
			%
			%		stepsRatio: an alternative to "steps": the number of pseudo-random values that
			%			are spread on the domain is computed by t(end) - t(1)) * obj.stepsRatio
			%
			%		seedt: the seed for the random number generator for the generation of the points
			%		on the domain at which the pseudo-random value changes its value. If a seed is
			%		set, then the result can be reproduced, i.e., a pseudo-random squence is used.
			%		If no seed is set, a random variable is chosen. If seedt is set to a negative
			%		number, than the points are equidistant.
			%
			%		seedf: the seed for the random number generator for the function values. If a 
			%		seed is set, then the result can be reproduced, i.e., a pseudo-random squence is 
			%		used. If no seed is set, a random variable is chosen.
			%
			%		filter: If no filter is set, a piecewise constant function with pseudo-random
			%		values is generated (default). If the a filter is set, the piecewise constant
			%		funcion is smoothed by this filter. The filter can be a state-space object, a
			%		transfer function, or the string "gevrey", which uses a fir filter where the
			%		coefficients are generated from a gevrey function so that the result is C^\infty
			%
			%		f0: specifies the initial value; default is zero
			%		fEnd: specifies the last value; default is defined by the last pseudo-random
			%		value
			%
			%		name: give the function a name
			%
			%		transition factor: factor to change the length of the time window considered in
			%		the filtering with a gevrey function.
			%		
			%		t0 and tEnd: specify the initial and end point on which the pseudo-random values
			%		should be spread. Default values are first and last point in t.
			%
			
			arguments
				t (1,1) quantity.Domain;
				optArgs.steps (1,1) double;
				optArgs.stepsRatio (1,1) double;
				optArgs.seedt (1,1) double;
				optArgs.seedf (1,1) double;
				optArgs.filter;
				optArgs.f0 (1,1) double = NaN;
				optArgs.fEnd (1,1) double = NaN;
				optArgs.name string = "";
				optArgs.transitionFactor (1,1) double = 1;
				optArgs.t0 (1,1) double = t.lower;
				optArgs.tEnd (1,1) double = t.upper;
			end
			
			if isfield(optArgs, "stepsRatio")
				optArgs.stepsRatio = optArgs.stepsRatio;
				optArgs.steps = [];
			elseif isfield(optArgs, "steps")
				optArgs.steps = optArgs.steps;
				optArgs.stepsRatio = [];
			else
				% default value if none is set.
				optArgs.stepsRatio = 2;
				optArgs.steps = [];
			end
						
			if isfield(optArgs, "filter")
				% verify data type of filter.
				assert( isa(optArgs.filter, "ss") || isa(optArgs.filter, "tf" ) || ...
					isa(optArgs.filter, "double") || ...
					(isa(optArgs.filter, "string") && strcmp( optArgs.filter, "gevrey")), ...
					"The filter for data smoothing must be either an IIR filter represented by a tf or ss object, or an FIR filter represented by a column-vector or the string gevrey");
			end
			
			obj@quantity.Discrete(zeros(t.n,1), t, "name", optArgs.name);
			
			% set all specified properties
			for k = fieldnames(optArgs)'
				obj.(k{1}) = optArgs.(k{1});
			end
		end
		
		function s = quantity.Discrete(obj)
			for k = 1:numel(obj)
				s(k) = quantity.Discrete( obj(k).valueDiscrete, obj(k).domain, "name", obj(k).name);
			end
			s = reshape(s, size(obj));
		end
      
		function f = generate(obj, t)
			arguments
				obj
				t (1,1) quantity.Domain;
			end
			idx = t.grid >= obj.t0 & t.grid <= obj.tEnd;
			tGrid = t.grid( idx );
			obj.sequence = zeros( t.n, 1);
			
			[obj.sequence(idx), obj.tk, obj.fk] = obj.pseudoRandomSequence( tGrid );
			
			if ~isempty( obj.filter )
				f = obj.smoothSignal(t.grid, obj.sequence);
			else
				f = obj.sequence;
			end
			
			obj.valueDiscrete = f;
		end
		
        function tk = jumpingPoints(obj, t)
           %JUMPINGPOINTS computes the points at which a jump occurs,
           %depent if they should be equally or randomly distributed.
		   
		   mySteps = obj.getMySteps(t);
		   
           if ~isempty(obj.seedt) && obj.seedt < 0       
               % create equally distributed intervals
               tk = linspace(t(1), t(end), mySteps + 1).';
           else
               
               if isempty(obj.seedt)
                  rng('shuffle');
               else
                  % create intervals of random length
                  rng(obj.seedt);
               end
           
              tk = rand(mySteps,1);
              tk = [0; cumsum(tk)];
              tk = tk * (t(end) - t(1)) / tk(end) + t(1);
           end
           
        end
        
        function f = smoothSignal(obj, t, f)
            %  SMOOTHJUMPS filters the signal with the filter *G*
            if nargin == 2
                f = obj.signal(t(:));
            end            
            
            if isa( obj.filter, "ss") || isa( obj.filter, "tf")
                if isnan(obj.f0)
                    fo = f(1,:);
                else
                    fo = obj.f0;
                end

                stsp = ss(obj.filter);
                u0 = fo.';
                y0 = stsp.C * inv(-stsp.A) * stsp.B * u0;

                x0 = [stsp.A; stsp.C] \ [-stsp.B * u0; y0];

                f = lsim(stsp, f, t - t(1), x0);
				
			elseif isa( obj.filter, "double")
				f = filter( obj.filter , 1, f);
				
			elseif isa( obj.filter, "string") && strcmp( obj.filter, "gevrey")
                %% gevrey-function filter
                T = mean(diff(obj.tk) * obj.transitionFactor);
                dt = misc.stepSize(t);
                tau = dt/2:dt:(T-dt/2);
                b = signals.gevrey.bump.g(tau, 1, T, 1.1);
                f = filter(dt * b, 1, f);
			else
				error("Not yet implemented")
			end
        end
        
        function f = plot(obj, optArgs)
            %PLOT show the created random signal for time t.
			% with the optional argument "plotSteps", the steps of a smoothed pseudo random signal
			% can be displayed. 
			% For further arguments 
			%
			% See also quantity.Discrete/plot
			
			arguments
				obj
				optArgs.name2title (1,1) logical = quantity.Settings.instance().plot.name2title;
				optArgs.dock (1,1) logical = quantity.Settings.instance().plot.dock;
				optArgs.name2axis (1,1) logical = quantity.Settings.instance().plot.name2axis;
				optArgs.nameWithIndex (1,1) logical = quantity.Settings.instance().plot.nameWithIndex;
				optArgs.grabFocus (1,1) logical = quantity.Settings.instance().plot.grabFocus;
				optArgs.hold (1,1) logical = false;
				optArgs.export (1,1) logical = false;
				optArgs.figureId (1,1) double {mustBeInteger};
				optArgs.smash (1,1) logical = false;
				optArgs.exportOptions (1, 1) cell;
				optArgs.style (1,1) string;
				optArgs.LineWidth;
				optArgs.Color;
				optArgs.LineStyle;
				optArgs.Marker;
				optArgs.MarkerIndices;
				optArgs.MarkerEdgeColor;
				optArgs.MarkerFaceColor;
				optArgs.MarkerSize;
				optArgs.DatetimeTickFormat;
				optArgs.DurationTickFormat
				optArgs.plotSteps (1,1) logical = false;
			end
			
			fieldNamesToDiscrete = fieldnames(optArgs);
			fieldNamesToDiscrete = fieldNamesToDiscrete( ~contains( fieldNamesToDiscrete, "plotSteps") );
			args = misc.struct2namevaluepair(optArgs, "fieldNames", fieldNamesToDiscrete);
			
			h = plot@quantity.Discrete(obj, args{:});
			
			if optArgs.plotSteps
				for k = 1:numel(obj)
					
					hold( h.Children(k), 'on' );
					objK = h.Children(k).UserData;
					
					plot(objK.tk, objK.fk, 'x', "Parent", h.Children(k));
					
					if ~isempty( objK.filter )
						plot(objK.domain.grid, objK.sequence, "Parent", h.Children(k));
					end
				end
			end
        end
        
	end
	
	methods ( Access = protected )
		function mySteps = getMySteps(obj, t)
			if isempty( obj.steps )
				mySteps = ceil( (t(end) - t(1)) * obj.stepsRatio );
			else
				mySteps = obj.steps;
			end
		end
	end
	
    methods (Static)
        function example()
            %EXAMPLE for the usage of RandomStairs
            %   
            
            % create a filter to smooth the signal
            T = 0.07;
            D = 0.7;
            Gpt2 = tf(1, [T^2, 2 * T * D, 1]);
           
			t = quantity.Domain("t", linspace(0, 5, 1e3));
			
            % create the object for the generation of random signal. 
            signal1 = signals.PseudoRandomSignal(t, 'steps', 7, ...
                                'seedt', -1, ...
                                'fmin', -100, ...
                                'fmax', 299, ...
                                'filter', Gpt2);
            
			% show the created signal
            signal1.plot();
			title("Piecewise constant sequence filtered with a PT2-transfer function.")
			
			% create a pseudo random continuous signal based on a FIR filter:
			signal2 = signals.PseudoRandomSignal(t, 'steps', 7, ...
					'seedt', -1, ...
					'fmin', -100, ...
					'fmax', 299, ...
					'filter', "gevrey");
			
			signal2.plot();
			title("Piecewise constant sequence filtered with a FIR filter based on a Gevrey function.")
        end
        
        function s = sample(t, bounds)
            if nargin < 2
               bounds = [-100, 299]; 
            end
                
            pulse = signals.RandomStairs('steps', 7, ...
                                'seedt', -1, ...
                                'seedf', 1, ...
                                'fmin', bounds(1), ...
                                'fmax', bounds(2));
                            
            s = pulse.signal(t);
        end
        
    end
    
end

