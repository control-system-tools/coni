classdef Sinusoidal < signals.BasicVariable
	%SINUSOIDAL representation for a fourier series basis function
	%
	% See also signals.Sinusoidal.Sinusoidal (Constructor), signals.BasicVariable,
	% signals.Sinusoidal.fourierBasis
	
	properties
		timeDomain (1,1) quantity.Domain;
		numDiff (1,1) double;
		omega (1,1) double;
		type (1,1) string;
		phi (1,1) double;
	end		
	methods
		function obj = Sinusoidal(timeDomain, omega, optArgs)
			% SINUSOIDAL 
			% obj = Sinusoidal(timeDomain, omega, optArgs) creates a sinusoidal with frequency omega on the
			% timeDomain. It has the form
			%	f(t) = sin( w * t )
			% The number of computed derivatives can be specified by the name-value-pair "numDiff".
			% The name of the symbolic variable can be defined by "variable".
			
			arguments
				timeDomain (1,1) quantity.Domain
				omega (1,1) double,
				optArgs.numDiff (1,1) double = 3;
				optArgs.type (1,1) string = "sin";
				optArgs.phi (1,1) double = 0;
			end
						
			for k = 0:optArgs.numDiff			
				derivatives{k+1} = signals.Sinusoidal.privateEvaluateFunction( timeDomain, ...
					omega, optArgs.phi, k, optArgs.type);
			end
			
			obj@signals.BasicVariable(derivatives{1}, derivatives(2:end));
			
			obj.timeDomain = timeDomain;
			obj.omega = omega;
			obj.type = optArgs.type;
			obj.numDiff = optArgs.numDiff;
			obj.phi = optArgs.phi;
			
		end
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, domain, k)
			v = obj.privateEvaluateFunction( domain, obj.omega, obj.phi, k, obj.type);
		end
	end
	methods (Static, Access = private)
		function f = privateEvaluateFunction(domain, omega, phi, k, type)
			% This functions computes the k-th derivative of a fourier basis function:
			%	f(t) = d^k/dt^k ( sin( t*omega + phi ) )
			% input arguments:
			%	domain: 
			%	omega: frequency of the sinus or consinus
			%	phi: phase shift
			%	k: derivative
			%	type: if it is a sin or cos
			t = sym(domain.name);
			
			if omega == 0
				f0 = sym(1); % for k = 0: 0 and for k > 0 : 1
			elseif strcmp( type, "sin")
				f0 = sin(t * omega + phi);
			elseif strcmp( type, "cos")
				f0 = cos(t * omega + phi);
			else
				error("Not yet implemented")
			end
			
			if k == 0
				f = quantity.Symbolic( f0, domain);
			else	
				f = quantity.Symbolic( diff(f0, k), domain );
			end
		end
	end
	methods ( Static )
		function b = fourierBasis(tau, optArgs)
			% FOURIERBASIS computes the terms for a fourier basis
			% b = fourierBasis(tau, optArgs) computes the terms
			%  b(1) = 1
			%	k >= 1:
			%  b(2*k) = sin( k * pi / T * t)
			%  b(2*k+1) = cos( k * pi / T * t)
			
			arguments
				tau (1,1) quantity.Domain
				optArgs.order (1,1) double = 4;
				optArgs.numDiff (1,1) double = 3;
			end
			
			b(1,1) = signals.Sinusoidal(tau, 0, "type", "cos", "numDiff", optArgs.numDiff);
			T = tau.upper - tau.lower;
			
			for k = 1:ceil( optArgs.order / 2 )
				omega = k * pi / T;
				b(end+1,1) = signals.Sinusoidal( tau, omega, "type", "sin", ...
					"numDiff", optArgs.numDiff, "phi", - tau.lower * omega);
				b(end+1,1) = signals.Sinusoidal( tau, omega, "type", "cos", ...
					"numDiff", optArgs.numDiff, "phi", - tau.lower * omega);
			end
			
			if ceil( optArgs.order / 2 ) ~= optArgs.order / 2
				b = b(1:end-1);
			end
			
		end
	end
	
end

