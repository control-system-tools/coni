classdef GevreyFunction < signals.BasicVariable
	%GEVREYFUNCTION - representation of a Gevrey function
	% Class to generate a gevrey function and its derivative. The general function has the form
	%
	%          / 0             : t <= 0
	%          |
	%   f(t) = | f0 + K * g(t) : 0 < t < T
	%          | 
	%          \ 0             : t >= T
	%
	% with the additional property int_0^T h(t) = 1. Here h(t) is the exponential based Gevrey
	% function
	%	g(t) = exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-sigma))
	%
	%
	% See also signals.GevreyFunction.GevreyFunction signals.gevrey.bump, signals.BasicVariable
	
	properties (SetAccess = protected)
		% Gevrey-order
		order (1,1) double = 1.99;
		% the constant gain
		gain double = 1;
		% the offset:
		offset (1,1) double = 0;
		% the number of derivatives to be considered
		numDiff (1,1) double = 5;
		% underlying gevery function
		g;
		% derivative shift
		diffShift (1,1) double = 0;
	end
	
	properties (Dependent)
		% the exponent of the exponential function
		sigma;
	end
	
	methods
		function obj = GevreyFunction(optArgs)
			% GEVREYFUNCTION initialization of a gevrey function as signals.BasicVariable
			%	obj = GevreyFunction(optArgs) creates a gevrey function object. The general function
			%	has the form
			%
			%          / 0             : t <= 0
			%          |
			%   f(t) = | f0 + K * g(t) : 0 < t < T
			%          | 
			%          \ 0             : t >= T
			%
			% The properties can be set by name-value-pairs and "gain" (as a multiplicative factor
			% K), "offset" (as the additional factor f0), "numDiff" (the number of required
			% derivatives), "timeDomain" (the domain on which the Gevrey function should be
			% defined), "g" (the underlying Gevrey function), "diffShift" (an offset to the
			% derivatives). The default value for "g" is
			%
			%              / 0											: t <= 0
			%		g(t) = | exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-sigma))	: 0 < t < T
			%              \ 0											: t >= 0
			%
			% At this, "order" is the Gevrey-order given by 1 + 1/sigma and T is the length of the
			% domain specified with "timeDomain".
			%
			arguments
				optArgs.order (1,1) double = 1.99;
				optArgs.gain double = 1;
				optArgs.offset (1,1) double = 0;
				optArgs.numDiff (1,1) double = 5;
				optArgs.timeDomain (1,1) quantity.Domain  = quantity.Domain.defaultDomain(100, "t"); 
				optArgs.g = @signals.gevrey.bump.g;
				optArgs.diffShift (1,1) double = 0;
				optArgs.name (1,1) string = "g";
			end
			
			% generate the underlying gevrey functions:
			f_derivatives = cell(optArgs.numDiff + 1, 1);
			for k = 0:optArgs.numDiff			
				f_derivatives{k+1} = ...
					signals.GevreyFunction.evaluateFunction_p(optArgs, optArgs.timeDomain, k);
			end
			obj@signals.BasicVariable(f_derivatives{1}, f_derivatives(2:end));	

			% set the parameters to the object properties:
			for k = fieldnames(rmfield(optArgs, "timeDomain"))'
				obj.(k{:}) = optArgs.(k{:});
			end
			
		end
		function s = get.sigma(obj)
			% get.sigma the parameter of the Gevrey function related to its order
			s = 1 ./ ( obj.order - 1);
		end
		function set.sigma(obj, s)
			% set.sigma set the sigma parameter
			obj.order = 1 + 1/s;
		end
		function obj = setGain(obj, g)
			% setGain set the gain property of the Gevrey function
			% This requires to rescale also its derivatives.
			obj.gain = g;
			obj.offset = obj.offset * g;
			degree = max([obj.highestDerivative]);
			obj.fun = g * obj.fun;
			for i = 1:degree
				obj.derivatives{i} = g * obj.derivatives{i};
			end
		end
	end
	
	methods (Static, Access = private)
		function v = evaluateFunction_p(optArgs, domain, k)
			% evaluateFunction_p private function to compute the derivative of the Gevrey function
			% based on the precomputed derivatives.
			
			derivativeOrder = k + optArgs.diffShift;
			% The offset is only added to the zero-order derivative. 
			offset_k = optArgs.offset * 0^derivativeOrder;
			
			functionHandle = eval("@(" + domain.name + ...
				") offset_k + optArgs.gain * optArgs.g(" + domain.name + ...
				", derivativeOrder, domain.upper, 1 ./ ( optArgs.order - 1))");
			
			v = quantity.Function(functionHandle, domain, "name", optArgs.name);
		end
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, domain, k)
			% evaluateFunction compute the k-th derivative of the Gevrey function
			v = signals.GevreyFunction.evaluateFunction_p(obj, domain, k);
		end
	end
end

