classdef PolyBump < signals.BasicVariable
	% POLYBUMP wrapper for a polynomial bump
	%          / 0;								t <= T0
	%		   |
	%	f(t) = | c * (t - T0)^a * (T1 - t)^b,	T0 < t < T1
	%          |
	%          \ 0;								t >= T1
	
	properties (SetAccess = protected)
		a (1,1) double {mustBeInteger};
		b (1,1) double {mustBeInteger};
		c (1,1) double;
		T0 (1,1) double;
		T1 (1,1) double;
		norm logical;
		coeffs;
	end
		
	methods
		function obj = PolyBump(timeDomain, optArgs)
			arguments
				timeDomain (1,1) quantity.Domain;
				optArgs.a (1,1) double {mustBeInteger} = 1;
				optArgs.b (1,1) double {mustBeInteger} = 1;
				optArgs.T0 (1,1) double = timeDomain.lower;
				optArgs.T1 (1,1) double = timeDomain.upper;
				optArgs.c (1,1) double = 1;
				optArgs.norm (1,1) logical = false;
				optArgs.numDiff (1,1) double = 5;
				optArgs.name (1,1) string = "";
			end
						
			if optArgs.norm
				if optArgs.c ~= 1
					warning('The gain factor c will be overwritten so set the norm')
				end
				f = signals.PolyBump.polyBumpFunction(optArgs.a, optArgs.b, optArgs.c, ...
					optArgs.T0, optArgs.T1);
				optArgs.c = 1 / integral(f, optArgs.T0, optArgs.T1);
			end
			polyBumpFun = signals.PolyBump.polyBumpFunction(optArgs.a, optArgs.b, optArgs.c, ...
					optArgs.T0, optArgs.T1);
			coeffs = sym2poly( sym( polyBumpFun ) );
			w = signals.PolyBump.windowFunction( optArgs.T0, optArgs.T1);
			
 			derivatives = cell(optArgs.numDiff+1,1);
			for k = 0:optArgs.numDiff
				derivatives{k+1} = signals.PolyBump.diffInner(timeDomain, coeffs, k, w, optArgs.name);
			end
			
			obj@signals.BasicVariable(derivatives{1}, derivatives(2:end));
			
			obj.a = optArgs.a;
			obj.b = optArgs.b;
			obj.c = optArgs.c;
			obj.T0 = optArgs.T0;
			obj.T1 = optArgs.T1;
			obj.norm = optArgs.norm;
			obj.coeffs = coeffs;
		end
		
		function obj = setGain(obj, g)
			obj.c = obj.c * g;
			degree = max([obj.highestDerivative]);
			obj.fun = g * obj.fun;
			for i = 1:degree
				obj.derivatives{i} = g * obj.derivatives{i};
			end
		end
	end
		
	methods (Static)
		
		function w = windowFunction( T0, T1)
			w = @(t)  (T0 <= t & t <= T1);
		end
		
		function f = polyBumpFunction(a, b, c, T0, T1)
			f = @(t) c * ( t - T0 ).^a .* ( T1 - t ).^b;
		end
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, domain, k)
			v = signals.PolyBump.diffInner(obj, domain, k);
		end
	end	
	
	methods (Static, Access = private)
		
		function result = diffInner(domain, coeffs, k, windowFun, name)
			
			dk_coeffs = coeffs(:);
			for i = 1:k
				dk_coeffs = polyder( dk_coeffs );
			end

			functionHandleString = "@(" + domain.name + ") polyval(dk_coeffs, " + ...
				domain.name + ") .* windowFun(" + domain.name + ")";

			result = quantity.Function( eval( functionHandleString ), ...
				domain, "name", "d_{" + domain.name + "} " + name + ")");
		end
	end
end
