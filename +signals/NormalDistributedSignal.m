classdef NormalDistributedSignal < signals.PseudoRandomSignal
	% NormalDistributedSignal class to generate pseudo random samples with a normal distribution
	%	Simplification to generate signals that take values pseudo randomly from a normal
	%	distribution.
	%	
	%	see also signals.PseudoRandomSignal, signals.UniformDistribution
		
	properties
		
	end
	
	methods
		function obj = NormalDistributedSignal(t, optArgs)
			%NormalRandomSignal Class to generate pseudo random signals
			%	signals.NormalRandomSignal(t, optArgs) create a pseudo-random signal on the domain
			%	specified by t, which takes values pseudo-randomly from a normal distribution.
			%	For the optional arguments "steps", "stepsRatio", "seedt", "seedf", "filter", "f0",
			%	"fEnd", "t0", "tEnd" see signals.PseudoRandomSignal
			
			arguments
				t (1,1) quantity.Domain;
				optArgs.steps (1,1) double;
				optArgs.stepsRatio (1,1) double;
				optArgs.seedt (1,1) double;
				optArgs.seedf (1,1) double;
				optArgs.filter;
				optArgs.f0 (1,1) double = NaN;
				optArgs.fEnd (1,1) double = NaN;
				optArgs.name string = "";
				optArgs.transitionFactor (1,1) double = 1;
				optArgs.t0 (1,1) double = t.lower;
				optArgs.tEnd (1,1) double = t.upper;
			end
			
			argsIn = misc.struct2namevaluepair(optArgs);
			obj@signals.PseudoRandomSignal(t, argsIn{:});
						
			% compute the required pseudo random sequence:
			obj.generate(t);
		end
		
		function [f, tk, fk] = pseudoRandomSequence(obj, t)
			%pseudoRandomSequence compute a piecewise constant squence
			% [f, tk, fk] = pseudoRandomSequence(obj, t) will compute a pseudo-random sequence as
			% piecewise constant signal where the function values are either computed with the
			% random number generator specified by 'seedf' or normalized to upper and lower bounds.
			% The jumping points of the piecewise function are obtained from obj.jumpingPoints.
			%	see also signals.PseudoRandomSignal.jumpingPoints
			arguments
				obj
				t (:,1) double;
			end
			tk = obj.jumpingPoints(t(:));
			
			if ~isempty(obj.seedf)
				rng(obj.seedf);
			else
				rng('shuffle')
			end
			
			mySteps = obj.getMySteps(t);
			

			fk = randn(mySteps, 1);

			
			fk = [fk; fk(end)];
			
			if ~isnan( obj.f0 )
				fk(1) = obj.f0;
			end
						
			if ~isnan( obj.fEnd )
				fk(end-1) = obj.fEnd;
				fk(end) = obj.fEnd;
			end
			
			f = interp1(tk, fk, t(:), 'previous', 'extrap');
		end
	end
end

