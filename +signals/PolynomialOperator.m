classdef (InferiorClasses = {?quantity.Discrete, ?quantity.Function, ?quantity.Symbolic}) ...
		PolynomialOperator < handle & matlab.mixin.Copyable
	%OPERATOR define operator matrices
	% This class describes operators of the form
	%		A[x(t)](z) = A0(z) x(z,t) + A1(z) dz x(z,t) + A2(z) dz^2 x(z,t)
	%			... + Ak dz^k x(z,t)
	
	properties (SetAccess = protected)
		% The variable that is used for the algebraic representation of the
		% operator
		s;
		% The grid, on which the operator is defined
		domain quantity.Domain {mustBe.scalarOrEmpty};
	end
	
	properties 
		% The values of the operator matrices in form of a
		% quantity.Discrete object
		coefficient quantity.Discrete;
		
		truncate (1,1) logical = false;
	end
	
	methods
		function obj = PolynomialOperator(A, optArgs)
			% OPERATOR initialization of an operator object
			%	 obj = Operator(A, varargin) initializes an operator of the
			%	 form 
			%		A[x(t)](z) = A0(z) x(z,t) + A1(z) dz x(z,t) + ...
			%			A2(z) dz^2 x(z,t) + Ak dz^k x(z,t)
			%	The input parameter A can be:
			%		cell-array of doubles 
			%			this is good to initialize operators with constant 
			%			coefficients
			%			A[x](t) = a_0 x(t) + a_1 dt x(t) + ...
			%
			%		cell-array of quantities
			%			this helps to initialize operators with variable
			%			coefficients:
			%		A[x(t)](z) = A0(z) x(z,t) + A1(z) dz x(z,t) + ...
			%			A2(z) dz^2 x(z,t) + Ak dz^k x(z,t)
			%
			%
			%	
			arguments
				A = [];
				optArgs.s sym = sym("s");
				optArgs.name string = "";
				optArgs.domain = quantity.Domain.empty();
				optArgs.truncate (1,1) logical = false;
			end
			
			if nargin > 0
								
				%TODO assert that A has entries with the same size
				
				if isa(A, 'sym') && symvar(A) == optArgs.s
					% if the matrix A is a symbolic expression which
					% contains the algebraic representation of the
					% operator, then the object can be generated if the
					% symbolic expression is converted into the right form
					assert(length(symvar(A)) == 1, 'Not yet implemented')
					
					A = num2cell( double( ...
						signals.PolynomialOperator.polynomial2coefficients(A, optArgs.s) ), ...
						[1, 2]);
					
				end
				
				if iscell(A)
					for k = 1 : numel(A)
						if isnumeric(A{k})
							obj(k).coefficient = ...
								quantity.Discrete(A{k}, optArgs.domain);
						else
							obj(k).coefficient = A{k}; %#ok<AGROW>
						end
						
						if k > 1
							assert( all( size( obj(k).coefficient ) == size( obj(k-1).coefficient ) ), ...
								"The size of the coefficient matrices changes, but a polynomial operator requires same size for each coefficient matrix!");
						end
						
					end
				elseif isnumeric(A)
					obj(1).coefficient = quantity.Discrete(A, optArgs.domain);
					A = {A};
				else
					error("Not yet implemented for objects of type " + class(A) + ".")
				end
				
				if isempty( obj(1).coefficient )
					myDomain = quantity.Domain.empty();
				else
					myDomain = obj(1).coefficient(1).domain;
				end
				
				[obj.domain] = deal( myDomain );
				[obj.s] = deal(optArgs.s);
				[obj.truncate] = deal( optArgs.truncate );
				for k = 1:numel(A)
					[obj(k).coefficient.name] = deal(optArgs.name);
				end
			end
		end
		function c = horzcat(a, varargin)
			c = cat(2, a, varargin{:});
		end
		function c = vertcat(a, varargin)
			c = cat(1, a, varargin{:});
		end
		function c = cat(dim, a, varargin)
			% todo write unittest
			if nargin == 1
				objCell = {a};
			else
				objCell = [{a}, varargin(:)'];
			end
			
			trunc = false;
			
			for i = 1:numel( objCell )
				if ~isa( objCell{i}, "signals.PolynomialOperator" )
					objCell{i} = signals.PolynomialOperator( objCell{i} );
				end
				trunc = trunc | any( [objCell{i}.truncate] );
			end
			
			nElements = max( cellfun(@(c) size(c, 1), objCell) );
			for k = 1:nElements
				
				kArgIn = cell(numel(objCell), 1);
				
				for i = 1:numel(objCell)
					
					if size( objCell{i}, 1 ) >= k
						kArgIn{i} = objCell{i}(k).coefficient;
					else
						kArgIn{i} = quantity.Discrete.zeros( size(objCell{i}, [2, 3]), ...
							quantity.Domain.empty() );
					end
					
				end
				
				K{k} = cat( dim, kArgIn{:});
			end
			
			c = signals.PolynomialOperator(K, "truncate", trunc);
			
		end
		function I = int(obj, varargin)
			II = cell(size(obj,1), 1);
			for k = 1:size(obj, 1)
				if isempty( obj(k).coefficient )
					II{k} = obj(k).coefficient.copy;
				else					
					II{k} = int(obj(k).coefficient, varargin{:});
				end
			end
			I = signals.PolynomialOperator(II, "truncate", obj(1).truncate);
		end
		
		function n = name(obj, name)
			arguments
				obj
				name (1,1) string = ""; 
			end
			
			if nargin == 1
				n = obj(1).coefficient.name;
			else
				for i = 1:numel(obj)
					obj(i).coefficient.setName(name);
				end
			end
			
		end
		
		function n = norm(obj, optArgs)
			arguments
				obj
				optArgs.type = "infty"
			end
			
			if optArgs.type == "infty"
			
				for i = 1:obj.length
					n(i) = max( max( abs( obj(i).coefficient) ), [], 'all');
				end
			
			else
				error("The norm of type " + optArgs.type + "is not yet implemented");
			end
			
		end
		
		function [value, relativeIncrement, stopIteration] = applyTo(obj, y, optArgs)
			% APPLYON evaluation of the differential parameterization
			%	[value, relativeIncrement, stopIteration] = applyOn(OBJ, Y, varargin) applies the
			%	operator OBJ on the function Y, i.e.,
			%		value = A0(t) y(t) + A1(t) dt y(t) + A2(t) dt^2 y(t) ... + A{n-1}(t) dt^{n-1} y(t)
			%	is evaluated. The object y must have implemented the properties "domain" and "name"
			%	and a method "diff( domain, order)". With the optional name-value-pairs, the
			%	"domain" for differentiation can be set explicitly. With "n", the number of terms to
			%	be taken into account is specified, i.e., n-1 is the highest derivative. The
			%	parameter "resultName" allows to specifiy the name of the resuting quantity.Discrete
			%	object. With "relativeIncrementTolerance", the summation is stopped if the relative
			%	increment of the last term is below the given threshold. The relative increment is
			%	computed by || Ai dt^i y(t) ||_infty / || sum_{i=0...n} Ai dt^i y(t) ||_infty.
			%	The output parameters are the evaluated operator, i.e., the computed sum as "value".
			%	The relative increment as "relativeIncrement" and the number of considered
			%	iterations.
			arguments
				obj
				y
				optArgs.domain (1,1) quantity.Domain = y(1).domain;
				optArgs.n (1,1) double = size(obj, 1);
				optArgs.resultName (1,1) string = obj.name + "[" + y(1).name + "]";
				optArgs.relativeIncrementTolerance (1,1) double;
			end
	
			if isfield(optArgs, "relativeIncrementTolerance")
				warning("The option relativeIncrementTolerance must be used with care. Ensure that non of the coefficients is zero!")
			end
			
			value = obj(1).coefficient * y.diff(optArgs.domain, 0);
			stopIteration = optArgs.n;
			
			for k = 2:optArgs.n
				increment = obj(k).coefficient * y.diff(optArgs.domain, k - 1);
				value = value + increment;
				
				if isfield(optArgs, "relativeIncrementTolerance")
					relativeIncrement = max( abs( increment ) ) ./ max( abs( value ) );
					%disp(" Relative increment is " + relativeIncrement)
					if max( relativeIncrement(:) ) < optArgs.relativeIncrementTolerance
						stopIteration = k;
						break;
					end					
				end				
			end
			
			if isfield(optArgs, "relativeIncrementTolerance") && max( relativeIncrement(:) ) >= optArgs.relativeIncrementTolerance
				warning("CONI:signals:PolynomialOperator", "Maximal number of terms reached, but relativeIncrementTolerance is not satisfied");
			end
			
			if nargout >= 2
				relativeIncrement = max( abs( increment ) ) ./ max( abs( value ) );
			end
						
			value.setName( optArgs.resultName );		
		end
		
		function C = mtimes(A, B)
			
			if isnumeric(A) && numel(A) == 1
				A = signals.PolynomialOperator(eye(size(B(1).coefficient, 1)) * A);
			end
% 			
			if ~isa(A, 'signals.PolynomialOperator')
				A = signals.PolynomialOperator(A);
			end
			if ~isa(B, 'signals.PolynomialOperator')
				B = signals.PolynomialOperator(B);
			end
			
			trunc = A(1).truncate || B(1).truncate;
			
			[n, m] = size(A(1).coefficient * B(1).coefficient);
			myDomain = join( A(1).domain, B(1).domain );
			
% 			assert(size(A(1).coefficient, 2) == size(B(1).coefficient, 1))

			if trunc
				degreeC = max(length(A), length(B));
			else
				degreeC = ( length(A) + length(B) - 1);%
			end
			
			[C{1:degreeC}] = deal( quantity.Discrete.zeros([n, m], myDomain) );
			for k = 1 : degreeC
				for l = 1:k
					if l <= length(A) && k-l+1 <= length(B)
						% do the compuation only if the coefficients do
						% exist
						C{k} = C{k} + A(l).coefficient * B(k-l+1).coefficient;
					end
				end	
			end
			
			C = signals.PolynomialOperator(C, "truncate", trunc);
		end
		
		function C = plus(A, B)
			
			if ~isa(A, 'signals.PolynomialOperator')
				A = signals.PolynomialOperator(A);
			end
			if ~isa(B, 'signals.PolynomialOperator')
				B = signals.PolynomialOperator(B);
			end			
			
			trunc = A(1).truncate || B(1).truncate;
			
			for k = 1 : max(length(A), length(B))
							
				if k <= length(A)
					C{k} =  A(k).coefficient;
				end
				
				if k <= length(B)
					if length(C) == k
						C{k} = C{k} + B(k).coefficient;
					else
						C{k} = B(k).coefficient;
					end
				end
			end
			C = signals.PolynomialOperator(C, "truncate", trunc);			
		end
		
		function C = minus(A, B)
			C = A + (-1)*B;
		end
		
		function C = adj(obj)
			assert(obj(1).coefficient.isNumber())
			
			C = signals.PolynomialOperator.polynomial2coefficients(misc.adj(sym( obj )), obj(1).s);
			
			if obj(1).truncate
				degree = size(obj,1);
			else
				degree = size(C,3);
			end
			
			c = cell(1, degree);
			for k = 1:degree
				c{k} = quantity.Discrete(double(C(:,:,k)), ...
					quantity.Domain.empty(), 'name', "adj(" + obj(1).coefficient(1).name + ")");
			end
			
			C = signals.PolynomialOperator(c, "truncate", obj(1).truncate);
		end
		
		function C = det(obj)
			
			if isempty(obj)
				C = 1;
				return 
			end
			assert( diff( size(obj, [2, 3] ) ) == 0, "The determinante can only be computed for quadratic operators" )
			assert(obj(1).coefficient.isNumber())
			
			C = det( sym(obj) );
			C = signals.PolynomialOperator.polynomial2coefficients(C, obj(1).s);
			
			if obj(1).truncate
				degree = size(obj,1);
			else
				degree = size(C,3);
			end
			
			c = cell(1, degree);
			for k = 1:degree
				c{k} = quantity.Discrete(double(C(:,:,k)), ...
					quantity.Domain.empty(), ...
					"name", "det(" + obj(1).coefficient(1).name + ")");
			end
			
			C = signals.PolynomialOperator(c, "truncate", obj(1).truncate);
		end
		
		function C = uminus(A)
			C = (-1)*A;
		end
		
		function s = size(obj, varargin)
			s = [length(obj), size(obj(1).coefficient)];
			
			if nargin > 1
				s = s(varargin{:});
			end
			
		end
		
		function l = length(obj)
			l = numel(obj);
		end
		
		function d = degree(obj)
			d = numel(obj) - 1;
		end
	end
	
	methods (Access = public)
		function d = double(obj)
			d = double(obj.M);
		end
		
		function [i, m, s] = near(obj, B, varargin)
			
			if isa(B, "signals.PolynomialOperator")
				b = B.M;
			else
				b = B;
			end
			
			[i, m, s] = obj.M.near(b, varargin{:});
			if nargout == 0
				i = s;
			end
		end
		
		function i = isempty(obj)
			i = numel(obj)==0 || isempty(obj(1).coefficient);
		end
		
		function h = plot(obj, varargin)
			h = obj.M.plot(varargin{:});
		end
		
		function S = sym(obj)
			S = signals.PolynomialOperator.powerSeries2Sum( obj.M.on() , obj(1).s);
		end
		
		function A = transpose(obj)
			for k = length(obj):-1:1
				M{k} = obj(k).coefficient.';
			end
			A = signals.PolynomialOperator(M);
		end
		
		function A = ctranspose(obj)
			for k = length(obj):-1:1
				M{k} = (-1)^(k-1) * obj(k).coefficient'; %#ok<AGROW>
			end
			A = signals.PolynomialOperator(M);
		end
		
		function [Phi, Psi] = stateTransitionMatrixByOdeSystem(obj, optArgs)
			% STATETRANSTITIONBYODE
			% [Phi, Psi] = stateTransitionMatrixByOdeSystem(A, varargin) computes the
			% state-transition matrix of the boundary-value-problem
			%	dz x(z,s) = A(z,s) x(xi,s) + B(z,s) * u(s)
			% so that Phi is the solution of
			%	dz Phi(z,xi,s) = A(z,s) Phi(z,xi,s)
			% and Psi is given by
			%	Psi(z,xi,s) = int_xi_z Phi(z,zeta,s) B(zeta,s) d zeta
			% if B(z,s) is defined as signals.PolynomialOperator object by
			% name-value-pair.
			% To solve this problem the recursion formula from [1]
			%	dz w_k(z) = sum_j=0 ^d A_j(z) * w_{k-j}(z) + B_k nu
			% is used. This recursion of odes can be reformulated to a
			% large ode:
			%
			%	dz w_0 = A_0 w_0 + B_0 nu
			%	dz w_1 = A_1 w_0 + A_0 w_0 + B_1 nu
			%	dz w_2 = A_2 w_0 + A_1 w_1 + A_0 w_2 + B_2 nu
			%	...
			%
			%	[dz w_0]   [ A_0  0   0   0  ... ][w_0]   [B_0]
			%	[dz w_1]   [ A_1 A_0  0   0  ... ][w_1]   [B_1]
			%	[dz w_2] = [ A_2 A_1 A_0  0  ... ][w_2] + [B_2] nu
			%	[ ...  ]   [  ...                [[...]   [...]
			%
			% By some sorting of the terms, as shown in [2], the solution
			% of the recursion can be described by
			%	w_k(z) = Phi_k(z, xi) gamma + Psi_k(z,xi) nu
			% with w_0(xi) = gamma and w_k(xi) = 0, k > 0. From the
			% solution for the ode of the recursion:
			%	W(z) = PHI(z, xi) W(0) + PSI(z,xi) nu
			% and W(z) = [w_0'(z), w_1'(z), w_2'(z), ... ]' it is obvious,
			% that the required Phi_k(z,xi) and Psi_k(z,xi) can be
			% determined by appropriate partitioning of PHI(z,xi),
			% PSI(z,xi):
			%
			%	[w_0]   [ Phi_0  *   *  ... ][gamma]   [Psi_0]
			%	[w_1]   [ Phi_1  *   *  ... ][  0  ]   [Psi_1]
			%	[w_2] = [ Phi_2  *   *  ... ][  0  ] + [Psi_2] nu
			%	[...]   [  ...              ][ ... ]   [ ... ]
			%
			
			arguments
				obj signals.PolynomialOperator;
				optArgs.N  double = 1;
				optArgs.B signals.PolynomialOperator = signals.PolynomialOperator.empty(1, 0);
				optArgs.F0;
			end
			
			n = size(obj(1).coefficient, 2);
			B = optArgs.B;
			N = optArgs.N;
			
			if isempty(B)
				m = 0;
			else
				m = size(B(1).coefficient, 2);
			end
			
			n = size(obj(1).coefficient, 1);
			d = length(obj);
			spatialDomain = obj(1).coefficient(1).domain;
			
			% build the large system of odes from the recursion
			A_full = zeros(spatialDomain.n, N*n, N*n);
			B_full = zeros(spatialDomain.n, N*n, m);
			
			idx = 1:n;
			for k = 1:N
				idxk = idx + (k-1)*n;
				if length(B) >= k
					B_full(:, idxk, :) = B(k).coefficient.on();
				end
				for j = 1:N
					idxj = idx + (j-1)*n;
					if k - (j-1) > 0 && k - (j-1) <= length(obj)
						A_full(:, idxk, idxj) = obj(k - (j-1)).coefficient.on();
					end
				end
			end
			f = quantity.Discrete(...
				stateSpace.transitionMatrix.odeSolver_par( A_full, spatialDomain.grid ), ...
				[spatialDomain, spatialDomain.rename("zeta")]);
			
			b = quantity.Discrete(B_full, spatialDomain.rename("zeta"));
			p = int(f * b, "zeta", spatialDomain.lower, "z");
						
			z0 = obj(1).domain.lower;
			for k = 1:N
				idxk = idx + (k-1)*n;
				Phi(:,:,k) = f(idxk, idx).subs("zeta", z0);
				Psi(:,:,k) = p(idxk,:);
			end
			
			Phi = signals.PolynomialOperator(Phi);
			Psi = signals.PolynomialOperator(Psi);
		end
		
		function [Phi, F0] = stateTransitionMatrix(obj, optArgs)
			% STATETRANSITIONMATRIX computation of the state transition matrix
			%	[Phi, Psi] = stateTransitioMatrix(obj, varargin) computes
			% the state-transition matrix of the boundary value problem
			%	dz x(z,s) = A(z,s) x(xi,s) + B(z,s) * u(s)
			% so that Phi is the solution of
			%	dz Phi(z,xi,s) = A(z,s) Phi(z,xi,s)
			% and Psi is given by
			%	Psi(z,xi,s) = int_xi_z Phi(z,zeta,s) B(zeta,s) d zeta
			% if B(z,s) is defined as signals.PolynomialOperator object by
			% name-value-pair.
			arguments
				obj signals.PolynomialOperator;
				optArgs.N  double = 1;
				optArgs.F0;
			end
			
			n = size(obj(1).coefficient, 2);
			N = optArgs.N;
			
			nA = length(obj); % As order of a polynom is zero based
						
			% initialization of the ProgressBar
			counter = N - 1;
			pbar = misc.ProgressBar(...
				'name', 'Computation of the state transition matrix', ...
				'terminalValue', counter, ...
				'steps', counter, ...
				'initialStart', true);
			
			%% computation of transition matrix as power series
			% The fundamental matrix is considered in the laplace space as the state
			% transition matrix of the ODE with complex variable s:
			%   dz w(z,s) = A(z, s) w(z,s) * B(z) v(s),
			% Hence, fundamental matrix is a solution of the ODE:
			%   dz Phi(z,zeta) = A(Z) Phi(z,zeta)
			%   Phi(zeta,zeta) = I
			%
			%   Psi(z,xi,s) = int_zeta^z Phi(z, xi, s) B(xi) d_xi
			%
			% At this, A(z, s) can be described by
			%   A(z,s) = A_0(z) + A_1(z) s + A_2(z) s^2 + ... + A_d(z) s^d
			%
			% With this, the recursion formula
			%   Phi_k(z, zeta) = int_zeta^z Phi_0(z, xi) * sum_i^d A_i(xi) *
			%   Phi_{k-i}(z, xi) d_xi
			%   Psi_k(z, zeta) = int_zeta^z Phi_0(z, xi) * sum_i^d A_i(xi) *
			%   Psi_{k-i}(z, xi) + B_k(xi) d_xi
			
			if isfield(optArgs, "F0")
				F0 = ip.Results.F0;
			else
% 				F1 = quantity.Discrete(...
% 					obj(1).coefficient.stateTransitionMatrix(...
% 					'gridName1', 'z', 'gridName2', 'zeta'));
				spatialDomain(1) = obj(1).coefficient(1).domain;
				spatialDomain(2) = obj(1).coefficient(1).domain.rename("zeta");
				F0 = stateSpace.transitionMatrix.fundamentalMatrices(obj(1).coefficient, spatialDomain, ...
					"options", odeset('RelTol',1e-6,'AbsTol',1e-9));
			end
			z = F0(1).domain(1);
			Phi(:,:,1) = F0.subs("zeta", 0);
						
			On = quantity.Discrete.zeros([n n], z);
			for k = 2 : N
				pbar.raise();

				% compute the temporal matrices:
				sumPhi = On;
				for l = 2 : min(nA, k)
					sumPhi = sumPhi + obj(l).coefficient * Phi(:, :, k-l+1);
				end
				
				% compute the integration of the fundamental matrix with the temporal
				% values:
				%   Phi_k(z) = int_0_z Phi_0(z, zeta) * M(zeta) d_zeta
				Phi(:, :, k) = int(F0 * sumPhi.subs("z", "zeta"), "zeta", z(1).lower, "z");
			end
			%
			pbar.stop();
			
			% Note: from a certain order it can happen that the matrices contain only numerical
			% noise. Maybe this values can be set explicitly to zero?
			Phi = signals.PolynomialOperator(Phi, "truncate", obj(1).truncate);
		end
				
		function [Psi] = inputTransitionMatrix(obj, B, Phi0, optArgs)
			% STATETRANSITIONMATRIX computation of the state-transition matrix
			%	[Phi, Psi] = stateTransitioMatrix(obj, varargin) computes
			% the state-transition matrix of the boundary value problem
			%	dz x(z,s) = A(z,s) x(xi,s) + B(z,s) * u(s)
			% so that Phi is the solution of
			%	dz Phi(z,xi,s) = A(z,s) Phi(z,xi,s)
			% and Psi is given by
			%	Psi(z,xi,s) = int_xi_z Phi(z,zeta,s) B(zeta,s) d zeta
			% if B(z,s) is defined as signals.PolynomialOperator object by
			% name-value-pair.
			arguments
				obj signals.PolynomialOperator;
				B signals.PolynomialOperator 
				Phi0 quantity.Discrete;
				optArgs.N  double = 1;
				optArgs.F0;
			end
			
			n = size(obj(1).coefficient, 2);
			N = optArgs.N;
			
			if isempty(B)
				Psi = B;
				return;
			else
				m = size(B(1).coefficient, 2);
			end
			nA = length(obj); % As order of a polynom is zero based
						
			% initialization of the ProgressBar
			counter = N - 1;
			pbar = misc.ProgressBar(...
				'name', 'Computation of the state transition input matrix', ...
				'terminalValue', counter, ...
				'steps', counter, ...
				'initialStart', true);
			
			%% computation of transition matrix as power series
			% The fundamental matrix is considered in the laplace space as the state
			% transition matrix of the ODE with complex variable s:
			%   dz w(z,s) = A(z, s) w(z,s) * B(z) v(s),
			% Hence, fundamental matrix is a solution of the ODE:
			%   dz Phi(z,zeta) = A(Z) Phi(z,zeta)
			%   Phi(zeta,zeta) = I
			%
			%   Psi(z,xi,s) = int_zeta^z Phi(z, xi, s) B(xi) d_xi
			%
			% At this, A(z, s) can be described by
			%   A(z,s) = A_0(z) + A_1(z) s + A_2(z) s^2 + ... + A_d(z) s^d
			%
			% With this, the recursion formula
			%   Psi_k(z, zeta) = int_zeta^z Phi_0(z, xi) * sum_i^d A_i(xi) *
			%   Psi_{k-i}(z, xi) + B_k(xi) d_xi
			
			z = Phi0(1).domain(1);
			Psi(:,:,1) = int( Phi0 * B(1).coefficient.subs("z", "zeta"), "zeta", z.lower, "z");
			
			Om = quantity.Discrete.zeros([n m], z);
			for k = 2 : N
				pbar.raise();

				% compute the temporal matrices:
				sumPsi = Om;
				for l = 2 : min(nA, k)
					sumPsi = sumPsi + obj(l).coefficient * Psi(:, :, k-l+1);
				end
				if size(B, 1) >= k && ~isempty(B)
					sumPsi = sumPsi + B(k).coefficient(:, :);
				end
				
				% compute the integration of the fundamental matrix with the temporal
				% values:
				Psi(:, :, k) = int(Phi0 * sumPsi.subs("z", "zeta"), "zeta", z.lower, "z");
			end
			%
			pbar.stop();
			
			% Note: from a certain order it can happen that the matrices contain only numerical
			% noise. Maybe this values can be set explicitly to zero?
			Psi = signals.PolynomialOperator(Psi, "truncate", obj(1).truncate);
		end
		
		function newOperator = subs(obj, varargin)
			newOperator = obj.M.subs(varargin{:});
			
			if isnumeric(newOperator)
				newOperator = squeeze( num2cell(newOperator, [1 2]) );
				newOperator = signals.PolynomialOperator(newOperator, "truncate", obj(1).truncate);
			end
			
		end
		
		function [X, R] = zerosForm(obj)
			
			% assert that the object is only defined as numbers
			assert( isempty( obj(1).domain ) );
			
			coeffs = obj.double;
			assert( ndims( coeffs ) <= 3 );

			dim = size(obj, [2 3]);
			
			for i = 1:dim(1)
				for j = 1:dim(2)
					r = roots(permute(flip(coeffs(i,j,:)), [3, 1, 2])); % permute ensures vector
					% %													% input for roots
					%TODO remove very small numbers! For example in the hyperbolic example system
					%very small numbers occur in the real part that should be actually zero!				
					if isempty(r)
						poly = 0;
					else
						poly = coeffs(i,j,length(r)+1);
						for k = 1:numel(r)
							poly = poly * (obj(1).s - r(k));
						end
					end

					if isnumeric( poly )
						poly = sym( poly );
					end
					R{i,j} = r;
					X(i,j) = poly;
				end
			end
		end
		
		function r = findCommonZeros(obj)
			
			[~, R] = obj.zerosForm();
			
			% at frist remove all zero entries
			RR = {};
			for k = 1:numel(R)
				if ~isempty( R{k} )
					RR{end+1} = R{k};
				end
			end
			
			if isempty( RR )
				r = [];
			else
				r = RR{1};
				for i = 2:numel(RR)
					r = numeric.intersect( r, RR{i});
				end
			end
		end
		function q = quantity.Discrete(obj)
			% QUANTITY.DISCRETE convert a polynomial operator object to a quantity.Discrete object.
			q = [obj.coefficient];
		end
	end
	
	methods (Access = protected)
		function m = M(obj)
			m = builtin('cat', 3, obj.coefficient);
		end
	end
	
	methods (Static)
		function [ S ] = powerSeries2Sum( M, s_var )
			% POWERSERIES2SUM Sum of power series in s_var
			%
			% Description:
			% [ S ] = powerSeries2Sum( M, s_var )
			% Summation of all entries in *M* as power series with the symbolic
			% varivable *s_var*. *M* can be a cell or a three dimensional array. For
			% the 3-D array the dimension 1 and 2 are for the matrices that sould be
			% summed up and dimension 3 defines the exponent of the series. Its index
			% k corresponds to the coefficient for s_var^(k-1).
			%
			% Inputs:
			%     M           Matrix of interest
			%     s_var       Symbolic variable in which S should be developed
			% Outputs:
			%     S           Summation of power Series
			%
			% Example:
			% -------------------------------------------------------------------------
			% import misc.*
			% syms s
			% M = cat(3, [0 1], [2 0], [0 3]);
			% powerSeries2Sum(M, s)
			% >> ans = [2*s, 3*s^2 + 1]
			% -------------------------------------------------------------------------
			
			if iscell(M)
				
				S=M{1};
				for k=2:size(M,1)
					S=S+M{k}*s_var^(k-1);
				end
				
			else
				
				S = M(:,:,1);
				for k=2:size(M,3)
					S = S + M(:,:,k)*s_var^(k-1);
				end
			end
		end
		function e = empty(varargin)
			o = quantity.Discrete.empty(varargin{:});
			e = signals.PolynomialOperator(o);
		end
		function [ APoly ] = polynomial2coefficients(A, s_var, N)
			% OPERATOR2POLYNOMIAL Returns the coefficient matrices of an operator
			%
			% Inputs:
			%     A       Operator matrix with polynomials A \in C[s]^(n x n)
			%     s_var   symbolic variable
			%     N       maximal degree to be regarded for the coefficients. If the
			%             polynomial contains higher degree entries than *N*,
			%             the values are cut off.
			% Outputs:
			%     APoly   Coefficients
			%
			% Example:
			% -------------------------------------------------------------------------
			% syms s;
			% A = [10, s; 2*s^2, 3*s^3]
			% signals.PolynomialOpterator.polynomial2coefficients(A, s)
			%
			% >> ans(:,:,1) = [10, 0]
			%                 [ 0, 0]
			% >> ans(:,:,2) = [ 0, 1]
			%                 [ 0, 0]
			% >> ans(:,:,3) = [ 0, 0]
			%                 [ 2, 0]
			% >> ans(:,:,4) = [ 0, 0]
			%                 [ 0, 3]
			% -------------------------------------------------------------------------
			
			if ~exist('N', 'var')
				% determine maximal degree of spatial variable s
				sDegreeMax = -1;
				for i=1:numel(A)
					sDegreeMax = max([sDegreeMax, length(coeffs(A(i), s_var, 'All'))]);
				end
				N=sDegreeMax;
			end
			
			% convert system-operator into polynomal expression
			APoly = zeros([size(A), N]);
			for i=1:size(A,1)
				for j=1:size(A,2)
					tmp = flip( coeffs(A(i,j), s_var, 'All') );
					APoly(i, j, 1:numel(tmp)) = tmp;
				end
			end
		end
		function E = matlabTFdataForm2polynomialOperatorCellForm( n )
			
			% get the maximal dimension:
			deg = max( cellfun( @(c) length( c ), n), [], 'all' );
			
			E = cell(deg, 1);
			for k = 1:deg
				Ek = zeros( size( n ) );
				for i = 1:numel(n)
					ei = [flip( n{i} ), zeros( 1, deg - length( n{i} )) ];
					Ek(i) = ei(k);
				end
				E{k} = Ek;
			end		
		end		
		
	end
	
	methods (Static, Access = protected)
		function var = getVariable(symbolicFunction, obj)
			var = getVariable@quantity.Symbolic(symbolicFunction);
			idxS = arrayfun(@(s) isequal(s, obj.operatorVar), var);
			var = var(~idxS);
		end
	end
	
end

