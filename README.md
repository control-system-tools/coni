# coni - a Matlab toolbox facilitating the solution of control problems
The "coni" library is a collection of Matlab scripts, functions and classes to simplify the implementation of research results, in particular for the control and fault diagnosis of distributed-parameter systems (DPS). An object-oriented framework is provided to facilitate the usage of time and space dependent functions and operators. This significantly simplifies calculations with matrix-valued functions.

The library contains Matlab code created for the research of the Infinite-dimensional Systems group at the [Institute of Measurement, Control and Microtechnology](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/) of Ulm University, Germany.

We work on the backstepping control, output regulation, fault diagnosis and simulation for DPS, hence most parts of this library are useful for working with DPS, which is the origin for the name "coni" (CONtrol of Infinite-dimensional systems). However, the toolbox has grown to facilitate the implementation of solutions for other system classes as well.

## Prerequisites
Some parts of our code rely on Matlab toolboxes, for instance the Symbolic Math Toolbox, however, there are useful functions and methods that can be used with plain Matlab installations. Matlab 2020a or more recent version is required. Testing and maintenance is done with Matlab 2020a.

## How to use
Probably the most outstanding contribution is the "quantity" framework. This enhances working with distributed parameters (for instance, matrices with time and/or space dependent elements), since it combines the speed of numerical calculations with the simple interface of symbolic calculations. The implementation of equations will almost look like in your paper, and does not require cascades of for-loops anymore. An illustrative example is given in `+quantity\+example.m`

Besides that, this is a collection of many Matlab functions resulting from years of work in our research group. There is a reasonable chance, that you will find something useful.

### Package structure
The folder structure uses the Matlab packages, thus each folder starts with a plus "+" sign. 
* `+export` some classes and functions to simplify the export of data to *.dat (respectively *.csv) files. These are useful to generate plots with the pgfplot package in latex.
* `+fractional` some functions simplify fractional calculus.
* `+misc` is used for miscellaneous functions.
* `+mustBe` contains verification functions for properties of classes and input arguments of function, see `doc Validate Property Values` in the Matlab documentation. 
* `+numeric` contains functions for FEM, numerical integration, etc. 
* `+quantity` is a framework to facilitate calculations with matrix-valued functions that are based on numerical (`Discrete`), symbolic (`Symbolic`), function_handle (`Function`) data. 
* `+signals` contains some functions to simplfy the handling of signals, e.g., realization of FIR filters in `+fir`, generation of pseudo-random input data, but also a simple framework based on `signals.BasicVariable` and `signals.PolynomialOperator` to implement flatness-based trajectory planning as well as ODE signal models `signal.models`.
* `+stateSpace` contains methods related to state-space models of Matlabs `Control Systems Toolbox`. For instance, methods for simulation, controller design or trajectory planning of LTI systems. The subpackage `+stateTransitionMatrix` proposes some implementations for the numerical computation of the state transition matrix for linear time-variant systems.
* `+unittests` there is the same structure of folders, but it contains the unittests.

### Coding Guidelines
For general hints, see [the related wiki-page.](https://gitlab.com/control-system-tools/coni/-/wikis/Coding-guidelines)

### Contributing
We are looking forward to your feedback, criticism and contributions. Feel free contact us and don't hesitate to commit suggestions. We will review all commits to ensure quality.

## Questions & Contact
Feel free to contact [Jakob](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/gabriel-jakob-m-sc/) or [Ferdinand](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/fischer-ferdinand-m-sc/) in case of suggestions or questions.

## License
GNU Lesser General Public License v3.0 (LGPL), see [Licence-file](https://gitlab.com/control-system-tools/coni/-/blob/master/COPYING.LESSER).
